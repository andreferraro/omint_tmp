﻿app.factory('navCarousel', function (NavigatorTransitionAnimator) {
    var navCarousel = NavigatorTransitionAnimator.extend({
        backgroundMask: angular.element(
          '<div style="z-index: 2; position: absolute; width: 100%;height: 100%; background-color: black; opacity: 0;"></div>'
        ),
        timing: 'cubic-bezier(.1, .7, .4, 1)',
        duration: 0.3,
        blackMaskOpacity: 0.4,
        init: function (options) {
            options = options || {};
            this.timing = options.timing || this.timing;
            this.duration = options.duration !== undefined ? options.duration : this.duration;
        },
        push: function (enterPage, leavePage, done) {
            var mask = this.backgroundMask.remove();
            enterPage.element[0].parentNode.insertBefore(mask[0], enterPage.element[0].nextSibling);
            animit.runAll(
              animit(mask[0])
                .queue({
                    opacity: this.blackMaskOpacity,
                    transform: 'translate3d(0, 0, 0)'
                })
                .queue({
                    opacity: 0
                }, {
                    duration: this.duration,
                    timing: this.timing
                })
                .resetStyle()
                .queue(function (done) {
                    mask.remove();
                    done();
                }),
              animit(enterPage.element[0])
                .queue({
                    css: {
                        transform: 'translate3D(100%, 0px, 0px)',
                        opacity: 0.9
                    },
                    duration: 0
                })
                .queue({
                    css: {
                        transform: 'translate3D(0px, 0px, 0px)',
                        opacity: 1.0
                    },
                    duration: this.duration,
                    timing: this.timing
                })
                .resetStyle(),
              animit(leavePage.element[0])
                .queue({
                    css: {
                        transform: 'translate3D(0px, 0px, 0px)'
                    },
                    duration: 0
                })
                .queue({
                    css: {
                        transform: 'translate3D(-100%, 0px, 0px)'
                    },
                    duration: this.duration,
                    timing: this.timing
                })
                .wait(0.2)
                .queue(function (finish) {
                    done();
                    finish();
                })
            );
        },
        pop: function (enterPage, leavePage, done) {
            var mask = this.backgroundMask.remove();
            enterPage.element[0].parentNode.insertBefore(mask[0], enterPage.element[0].nextSibling);
            animit.runAll(
              animit(mask[0])
                .queue({
                    opacity: this.blackMaskOpacity,
                    transform: 'translate3d(0, 0, 0)'
                })
                .queue({
                    opacity: 0
                }, {
                    duration: this.duration,
                    timing: this.timing
                })
                .resetStyle()
                .queue(function (done) {
                    mask.remove();
                    done();
                }),
              animit(enterPage.element[0])
                .queue({
                    css: {
                        transform: 'translate3D(-100%, 0px, 0px)',
                        opacity: 0.9
                    },
                    duration: 0
                })
                .queue({
                    css: {
                        transform: 'translate3D(0px, 0px, 0px)',
                        opacity: 1.0
                    },
                    duration: this.duration,
                    timing: this.timing
                })
                .resetStyle(),
              animit(leavePage.element[0])
                .queue({
                    css: {
                        transform: 'translate3D(0px, 0px, 0px)'
                    },
                    duration: 0
                })
                .queue({
                    css: {
                        transform: 'translate3D(100%, 0px, 0px)'
                    },
                    duration: this.duration,
                    timing: this.timing
                })
                .wait(0.2)
                .queue(function (finish) {
                    done();
                    finish();
                })
            );
        }
    });
    return navCarousel;
});