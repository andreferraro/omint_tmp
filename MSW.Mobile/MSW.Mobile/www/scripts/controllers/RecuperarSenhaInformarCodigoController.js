﻿app.controller("RecuperarSenhaInformarCodigoController", function($scope, $http) {

    $scope.submit = function()
    {
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "RecuperarSenha/InformarCodigo",
            params: {
                codigo: $scope.codigo,
                email: app.recuperarSenha.email,
                senhaEncrypted: app.rsaEncrypt($scope.senha)
            },
            success: function (data) {
                ons.notification.alert({
                    title: "Informação",
                    message: "Senha alterada com sucesso. Por favor realize o login."
                });
                app.navigate("views/login.html", true);
            }
        });
    }
});