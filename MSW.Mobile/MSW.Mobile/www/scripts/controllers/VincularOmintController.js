﻿app.controller("VincularOmintController", function($scope, $http, $rootScope) {
    $scope.emails = [];
    $scope.emailSelecionado = "";

    app.ajax({
        scope: $scope,
        http: $http,
        url: app.apiUrl + "MinhaConta/ListarEmailsDisponiveis",
        success: function(data)
        {
            $scope.emails = data.Data;
        },
        failure: function(data)
        {
            app.navigate("views/restrito/home.html", true);
        }
    });

    $scope.submit = function()
    {
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "MinhaConta/InserirAutenticacaoOmint",
            params: {
                login: $scope.emailSelecionado,
                senhaEncrypted: app.rsaEncrypt($scope.senha)
            },
            success: function(data)
            {
                $rootScope.possuiAutenticadorOmint = true;
                ons.notification.alert({
                    title: "Informação",
                    message: "Sua conta de acesso foi registrada com sucesso."
                });
                app.navigate("views/restrito/home.html", true);
            }
        });
    };
});