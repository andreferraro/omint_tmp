﻿app.controller("AtendimentoFiltroController", function($scope, $http, AtendimentoService) {

    app.ajax({
        scope: $scope,
        http: $http,
        url: app.apiUrl + "Atendimento/Filtro",
        success: function (data) {
            var assuntos = _(_(data.Data.Demandas).filter(function (v) {
                return v.AutoEncerra !== "S";
            })).groupBy(
                function (v) {
                    return v.IdTipoDemanda;
                });

            var demandas = _(assuntos).map(function (v) {
                return {
                    Id: v[0].IdTipoDemanda,
                    Desc: v[0].DescricaoTipoDemanda
                };
            });

            AtendimentoService.setDemandas({
                assuntos: assuntos,
                demandas: demandas
            });

            $scope.filtro = AtendimentoService.getFiltro();

            $scope.assuntos = AtendimentoService.getDemandas().assuntos;

            $scope.demandas = AtendimentoService.getDemandas().demandas;
        }
    });

    $scope.submit = function()
    {
        AtendimentoService.setFiltro($scope.filtro);
        app.navigate("views/restrito/atendimento-lista.html", true);
    };
});