﻿app.controller("LoginController", function($scope, $http, $rootScope) {

    $scope.online = navigator.connection && navigator.connection.type != "none";
    $scope.loading = false;
    $scope.passwordType = 'password';
    $scope.eyeType = 'fa-eye';

    $scope.openMenu = function ()
    {
        menu.toggleMenu();
    };

    if(window.localStorage.loginData && $scope.online)
    {
        setTimeout(function() {
            $rootScope.$broadcast("refreshMenu");
        }, 1000);
    }

    $scope.viewPassword = function ($event) {
        if ($scope.passwordType == 'password') {
            $scope.passwordType = 'text';
            $scope.eyeType = 'fa-eye-slash';
        }
        else
        {
            $scope.passwordType = 'password';
            $scope.eyeType = 'fa-eye';
        }
    };

    $scope.submit = function()
    {
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "Login",
            data: "UserName=" +
                $scope.email +
                "&password=" +
                encodeURIComponent(app.rsaEncrypt($scope.password)) +
                "&grant_type=password",
            headers: { 'Content-Type': "application/x-www-form-urlencoded" },
            success: function(data)
            {
                // Save email and password for token renewal.
                window.localStorage.credentials = JSON.stringify({ email: $scope.email, password: app.rsaEncrypt($scope.password) });
                window.localStorage.loginData = JSON.stringify(data);
                window.localStorage.lastLogin = new Date().toString();
                $rootScope.$broadcast("refreshMenu");
            }
        });
    };

    $scope.submitFacebook = function()
    {
        facebookConnectPlugin.logout(function(response) {
                FacebookLogin();
            },
            function(response) {
                FacebookLogin();
            });
    };

    var FacebookLogin = function()
    {
        facebookConnectPlugin.login([],
            function(response) {
                app.ajax({
                    http: $http,
                    scope: $scope,
                    url: app.apiUrl + "Login",
                    data: "UserName=Face&grant_type=password&password=" +
                        encodeURIComponent(app.rsaEncrypt(response.authResponse.userID)),
                    headers: { 'Content-Type': "application/x-www-form-urlencoded" },
                    success: function(data)
                    {
                        // Save facebook user id for token renewal.
                        window.localStorage.credentials = JSON.stringify({ facebookUserId: app.rsaEncrypt(response.authResponse.userID) });
                        window.localStorage.loginData = JSON.stringify(data);
                        window.localStorage.lastLogin = new Date().toString();                        
                        $rootScope.$broadcast("refreshMenu");
                    }
                });
            },
            function(response) {
                console.log("Facebook error", response);
            });
    };
});