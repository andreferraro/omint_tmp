﻿app.controller("RedeCredenciadaBuscaNomeController", function($scope, $http, $rootScope, RedeCredenciadaService) {

    $scope.buscaNome = RedeCredenciadaService.getBuscaNome();
    $scope.buscarOutrasRedes = RedeCredenciadaService.getPlano() !== undefined;
    $scope.plano = RedeCredenciadaService.getPlano() !== undefined
        ? RedeCredenciadaService.getPlano().Plano
        : "",

    $scope.latitude = app.geolocationPosition.latitude;
    $scope.longitude = app.geolocationPosition.longitude;

    $scope.credencial = {};

    $scope.buscaNomeSubmit = function()
    {
        app.ajax({
            scope: $scope,
            http: $http,
            method: "Get",
            url: app.apiUrl + "RedeCredenciada/ObterPorNome",
            params: {
                codigoRede: $rootScope.numeroVinculo,
                vinculo: $rootScope.numeroVinculo,
                nome: $scope.buscaNome,
                inativos: RedeCredenciadaService.getBuscarCredenciadosInativos(),
                tipoPrecisao: $scope.latitude && $scope.longitude !== undefined ? $scope.latitude + '-' + $scope.longitude
                   : null
            },
            success: function(data)
            {
                $scope.buscaNome = "";
                RedeCredenciadaService.setBuscaNome("");
                $scope.buscaNomeResultados = data.Data;

                app.ajax({
                    scope: $scope,
                    http: $http,
                    url: app.apiUrl + "Associado/ObterCredencialEletronica",
                    params: {
                        codigoTitulo: $rootScope.numeroVinculo,
                    },
                    success: function (data) {
                        $scope.credencial = data.Data;
                        $scope.carregaFiltro();
                    }
                }); 
            }
        });
    };

    $scope.prestadorClick = function(prestador)
    {
        RedeCredenciadaService.setUnidade(prestador);
        RedeCredenciadaService.setUnidadeDetalhes();
        app.navigate("views/restrito/redecredenciada-detalhamento.html");
    };

    $scope.filtroClick = function (tipo, filtro) {
        if (tipo == 'servico') {
            var codsRede = filtro.split(",");
            var filtroPrestadores = $scope.buscaNomeResultados.filter(function (item) {
                for (var i = 0; i < codsRede.length; i++)
                    if (item.CodigoRede.trim().indexOf(codsRede[i]) != -1)
                        return true;
                return false;
            });
        }

        if (tipo == 'uf') {
            var filtroPrestadores = $scope.buscaNomeResultados.filter(function (item) {
                return item.CodigoEstado.trim() == filtro.trim();
            });
        }

        if (tipo == 'cidade') {
            var filtroPrestadores = $scope.buscaNomeResultados.filter(function (item) {
                return item.EnderecoCidade.trim() == filtro.trim();
            });
        }

        if (tipo == 'esp') {
            var filtroPrestadores = $scope.buscaNomeResultados.filter(function (item){
	                                    return item.Especialidades.indexOf(filtro.trim()) > -1
                                    });
        }

        // if (tipo == 'score') {
        //     var filtroPrestadores = $scope.buscaNomeResultados.filter(function (item) {
        //         return item.ScoreTotal.trim() == filtro.trim();
        //     });
        // }

        $scope.buscaNomeResultados = filtroPrestadores;
        $scope.carregaFiltro();
        document.getElementById('slider').classList.toggle('closed');
    };

    $scope.carregaFiltro = function ()
    {
        // Serviços
        var servicoCount = {};
        var servicos = _.chain($scope.buscaNomeResultados).pluck('CodigoRede').value();
        servicos.forEach(function (i) {
            servicoCount[i] = (servicoCount[i] || 0) + 1;
        });
        $scope.servicoQtdePrestadoresLista = [];
        Object.getOwnPropertyNames(servicoCount).forEach(function (val, idx, array) {
            var novoServico = {};
            novoServico.nome = val;
            novoServico.qtde = servicoCount[val];
            $scope.servicoQtdePrestadoresLista.push(novoServico);
        });

       $scope.NewservicoCount = { qtdeMedico:0, qtdeOdonto:0 };
       $scope.servicoQtdePrestadoresLista.forEach(function (i) {
           if (i.nome.trim() == '0002' || i.nome.trim() == '0005' || i.nome.trim() == '0006' )
               $scope.NewservicoCount.qtdeOdonto = $scope.NewservicoCount.qtdeOdonto + i.qtde;
           else
               $scope.NewservicoCount.qtdeMedico = $scope.NewservicoCount.qtdeMedico + i.qtde;
       });

        // Estados
        var estadoCount = {};
        var estados = _.chain($scope.buscaNomeResultados).pluck('CodigoEstado').value();
        estados.forEach(function (i) {
            estadoCount[i] = (estadoCount[i] || 0) + 1;
        });
        $scope.estadoQtdePrestadoresLista = [];
        Object.getOwnPropertyNames(estadoCount).forEach(function (val, idx, array) {
            var novoEstado = {};
            novoEstado.nome = val;
            novoEstado.qtde = estadoCount[val];
            $scope.estadoQtdePrestadoresLista.push(novoEstado);
        });


        // Cidades
        var cidadeCount = {};
        var cidades = _.chain($scope.buscaNomeResultados).pluck('EnderecoCidade').value();
        cidades.forEach(function (i) {
            cidadeCount[i] = (cidadeCount[i] || 0) + 1;
        });
        $scope.CidadeQtdePrestadoresLista = [];
        Object.getOwnPropertyNames(cidadeCount).forEach(function (val, idx, array) {
            var novaCidade = {};
            novaCidade.nome = val;
            novaCidade.qtde = cidadeCount[val];
            $scope.CidadeQtdePrestadoresLista.push(novaCidade);
        });


        // Especialidades
        var espCount = {};
        $scope.buscaNomeResultados.forEach(function(p) {
            for (var i = 0; i < p.Especialidades.length; i++){
                espCount[p.Especialidades[i]] = (espCount[p.Especialidades[i]] || 0) + 1;  
            }
        });

        $scope.EspecialidadeQtdePrestadoresLista = [];
        Object.getOwnPropertyNames(espCount).forEach(function (val, idx, array) {
            var novaEsp = {};
            novaEsp.nome = val;
            novaEsp.qtde = espCount[val];
            $scope.EspecialidadeQtdePrestadoresLista.push(novaEsp);
        });

        // Score
        // var scoreCount = {};
        // var score = _.chain($scope.buscaNomeResultados).pluck('ScoreTotal').value();
        // score.forEach(function (i) {
        //     scoreCount[i] = (scoreCount[i] || 0) + 1;
        // });
        // $scope.ScoreQtdePrestadoresLista = [];
        // Object.getOwnPropertyNames(scoreCount).forEach(function (val, idx, array) {
        //     var novaScore = {};
        //     novaScore.nome = val;
        //     novaScore.qtde = scoreCount[val];
        //     $scope.ScoreQtdePrestadoresLista.push(novaScore);
        // });
    }

    $scope.buscaAvancada = function () {
        app.navigate("views/restrito/redecredenciada-home.html");
    };

    if($scope.buscaNome.length >= 4)
    {
        $scope.buscaNomeSubmit();
    }
});