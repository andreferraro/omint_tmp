﻿app.controller("RedeCredenciadaBuscaEspecialidadeController", function ($scope, $rootScope, $http, RedeCredenciadaService) {

    $scope.buscaTipoAtendimento = RedeCredenciadaService.getBuscaTipoAtendimento();
    $scope.buscaNomeEspecialidade = "";
    $scope.buscaEspecialidadeResultados = [];

    $scope.buscaNomeEspecialidadeSubmit = function () {
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "RedeCredenciada/BuscaEspecialidade",
            params: {
                vinculo: $rootScope.numeroVinculo,
                rede: $scope.buscaTipoAtendimento.CodigoRede,
                tipoAtendimento: $scope.buscaTipoAtendimento.TipoView,
                nomeEspecialidade: $scope.buscaNomeEspecialidade,
                inativos: RedeCredenciadaService.getBuscarCredenciadosInativos()
            },
            success: function (data) {
                $scope.buscaNomeEspecialidade = "";
                $scope.buscaEspecialidadeResultados = data.Data;
            }
        });
    };

    $scope.exibirTodasEspecialidadesClick = function()
    {
        $scope.buscaNomeEspecialidade = "";
        $scope.buscaNomeEspecialidadeSubmit();
    };

    $scope.especialidadeClick = function (especialidade) {
        RedeCredenciadaService.setBuscaEspecialidade(especialidade);
        app.navigate("views/restrito/redecredenciada-busca-localizacao.html");
    };
});