app.controller("EscolherTituloController", function ($scope, $rootScope, $http, RedeCredenciadaService) {
    $scope.listaTitulo = [];
    $scope.mostraHeader = false;
    $scope.tituloPagina = "";
    $scope.usuario = {tituloEscolhido: $rootScope.numeroVinculo } ;

    function GerarChamadoAtendimento() {
        app.ajax({
        scope: $scope,
        http: $http,
        url: app.apiUrl + "chamados",
        showProgress: false,
        handleErrors: false,
        data: {Vinculo: $rootScope.numeroVinculo},
        headers: { 'Content-Type': "application/json;charset=utf-8" },
        success: function (result) 
        {
            console.log(result);
            //$rootScope.numeroAtendimento = result.Data.NumeroAtendimento;
        },
        error: function (data) {
            console.log(data);
        }
    });
    }
    app.ajax({
        method: "GET",
        scope: $scope,
        http: $http,
        url: app.apiUrl + "vinculos",
        showProgress: false,
        modalLoading: true,
        success: function (result) 
        {
            $scope.listaTitulo = result.Data.Vinculos;
            if($rootScope.numeroVinculo == null && $scope.listaTitulo.length <= 1)
            {
                $rootScope.numeroVinculo = $scope.listaTitulo[0].NumeroIdentificacaoVinculo;
                GerarChamadoAtendimento();
                $rootScope.$broadcast("refreshMenu");
            }
            $scope.tituloPagina = $scope.listaTitulo.length > 1? "Selecionar seus Titulos" : "Titulos com a Omint";
            $scope.mostraHeader = true;
        }
    });

    $scope.gravarTitulo = function()
    {
        if(!$scope.usuario.tituloEscolhido) 
            return ons.notification.alert({
                title: "Erro", 
                message: "Escolha um contrato"
            });
        $rootScope.numeroVinculo = $scope.usuario.tituloEscolhido;
        GerarChamadoAtendimento()
        $rootScope.$broadcast("refreshMenu");
        ons.notification.alert({
            title: "Sucesso",
            message: "Contrato alterado com sucesso. No item “Configurações” do Menu você poderá alternar entre os seus contratos com a Omint."
        });
    }

});