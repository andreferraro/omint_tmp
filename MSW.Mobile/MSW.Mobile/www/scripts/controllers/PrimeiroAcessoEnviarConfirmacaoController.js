﻿app.controller("PrimeiroAcessoEnviarConfirmacaoController", function($scope, $http, $rootScope) {

    $scope.info = app.primeiroAcesso.info;

    $scope.info.Emails = _($scope.info.Emails).map(function(v) {
        return {
            Email: v,
            Mask: v.replace(/^(.*?)(\@.*?)$/, function(a, b, c) {
                var halfSize = Math.floor(b.length / 100 * 50);
                return b.substr(0, halfSize) + Array(halfSize + 1).join("*") + c;
            })
        };
    });

    $scope.submit = function()
    {
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "PrimeiroAcesso/EnviarConfirmacao",
            params: {
                hash: app.primeiroAcesso.hash,
                email: $scope.emailSelecionado
            },
            success: function (data) {
                app.navigate("views/primeiroacesso-informar-confirmacao.html");
            }
        });
    }

    $scope.CadastrarEmail = function()
    {   
        $rootScope.mensagemEmail = "Cadastre mais um e-mail com a Omint"
        app.navigate("views/primeiroacesso-cadastrar-email.html");
    }
});