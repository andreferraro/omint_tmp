﻿app.controller("PrimeiroAcessoVinculoController", function($scope, $http, $rootScope) {

    $scope.vinculo = "";
    var rota = "";

    $scope.tipos = [
        {
            Id: 1,
            Tipo: "Saúde - Associado",
            Instrucao: "Informe seu CPF ou os 13 dígitos de sua credencial médica ou odontológica",
            Label: "CPF ou Nº Credencial"
        }
    ];
    
    $scope.submit = function()
    {   
        var vinculoTeste = app.rsaEncrypt($scope.vinculo.toString());
        var vinculo = $scope.vinculo.toString().length > 11 ? "PrimeiroAcesso/ValidarVinculo" : "PrimeiroAcesso/ValidarVinculoPorCPF";
        $rootScope.vinculo = $scope.vinculo;
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + vinculo,
            params: {
                hash: app.primeiroAcesso.hash,
                vinculoEncrypted: vinculoTeste
            },
            success: function (data) {
                app.primeiroAcesso.info = data.Data;
                if (app.primeiroAcesso.info != 0) {
                    app.primeiroAcesso.info.Emails.length > 0 ? rota = "views/primeiroacesso-enviar-confirmacao.html" : rota = "views/primeiroacesso-cadastrar-email.html";
                    $rootScope.mensagemEmail = "Você não possui e-mail cadastrado com a Omint. Informe abaixo seu melhor e-mail para poder continuar com o cadastro no Minha Omint."
                    app.navigate(rota);
                }
            }
        });
     }
});