﻿app.controller("PrimeiroAcessoInformarConfirmacaoController", function($scope, $http) {

    $scope.info = app.primeiroAcesso.info;

    $scope.submit = function () {
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "PrimeiroAcesso/InformarConfirmacao",
            params: {
                hash: app.primeiroAcesso.hash,
                codigo: $scope.codigo
            },
            success: function (data) {
                app.navigate("views/primeiroacesso-cadastro.html");
            }
        });
    }
});