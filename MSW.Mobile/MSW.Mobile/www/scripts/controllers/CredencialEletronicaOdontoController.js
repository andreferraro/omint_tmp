app.controller("CredencialEletronicaOdontoController", function ($scope, $http,$timeout, 
    CredencialEletronicaService, CredencialImagemService, CredencialOfflineService) 
    {

    $scope.credencial = CredencialEletronicaService.getCredencial();
    // $timeout(function(){
    //     if (window.cordova && window.device) {
    //         screen.lockOrientation('landscape');
    //     }
    // },3000);
    $(".barcode").barcode(
        $scope.credencial.NumeroCodigoBarra, // Value barcode (dependent on the type of barcode)
        "codabar" // type (string)
    );

    ons.createPopover('views/restrito/popover-terceira.html').then(function (popover) {
        $scope.popoverTerceira = popover;
    });

    $scope.showPopOverTerceira = function (e) {
        $scope.popoverTerceira.show(e);
    };

    $scope.compartilharCredencial = function()
    {
        $scope.credencial.tipo = "odontologico";
        CredencialImagemService.get($("#credencial-eletronica"), 
            $scope.credencial.CaminhoImagemCredenciaOdonto, function(imagem)
        {
            window.plugins.socialsharing.share(null, 'credencial', imagem, null);
        }, $scope.credencial)
    }
});
