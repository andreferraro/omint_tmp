app.controller("MeusArquivosController", function ($scope, $rootScope, MenuArquivosService, $http) 
{
    function getPath(file)
    {
        if(!file) file = "/default.pdf";
        return (cordova.file.externalCacheDirectory || cordova.file.cacheDirectory) + file;
    }

    MenuArquivosService.obterDocumentos($rootScope.numeroVinculo).then(
        function(documentos){
            $scope.listaDocumentoAgrupado = documentos
        },
        function(error){
            alert(JSON.stringify(error));
        }
    );

    app.ajax({
        scope: $scope,
        http: $http,
        url: app.apiUrl + "Menu/AlterarNotificacaoStatus",
        params: {
            tipoServico: "D",
            idServico: "",
            novoStatus: "E"
        },
        success: function (data) {
            $rootScope.notificacoes["Meus Documentos"] = [];
            $rootScope.$broadcast("refreshNotifications");
        }
    });



    $scope.download = function(item)
    {
        if(!item.Url && !item.IdDocumento) return;
        $rootScope.ajaxLoadingModal = true;

        new FileTransfer().download(item.FullUrl, getPath(item.Url), function (entry) 
        {
            $rootScope.ajaxLoadingModal = false;
            $rootScope.$apply(function () {
                entry.file(function(fileResult) 
                {
                    cordova.plugins.fileOpener2.open(entry.toURL(), fileResult.type, 
                    {
                        error: function (e) {
                            ons.notification.alert({
                                title: "Erro",
                                message: "Ocorreu um erro ao abrir o arquivo. Verifique se existem aplicativos que suportem o mesmo."
                            });
                            console.log("Error status: " + e.status + " - Error message: " + e.message);
                        },
                        success: function () {}
                    });
                })
                
            });


        }, function () {
            $rootScope.ajaxLoadingModal = false;
            ons.notification.alert({
                title: "Erro",
                message: "Falha ao obter arquivo"
            });
        });
    }

});



// var localPath = (cordova.file.externalCacheDirectory || cordova.file.cacheDirectory) +
//     "Autorizacao_" +
//     autorizacao.IdAutorizacao +
//     ".pdf";

// $rootScope.ajaxLoadingModal = true;

// $rootScope.$apply(function () {
//     new FileTransfer().download(url, localPath, function (entry) {

//         console.log("download complete: " + entry.toURL());

//         $rootScope.ajaxLoadingModal = false;
//         $rootScope.$apply(function () {

//             cordova.plugins.fileOpener2.open(entry.toURL(), "application/pdf", {
//                 error: function (e) {
//                     ons.notification.alert({
//                         title: "Erro",
//                         message:
//                             "Ocorreu um erro ao abrir o arquivo. Por favor verifique seu leitor de PDF e tente novamente."
//                     });
//                     console.log("Error status: " + e.status + " - Error message: " + e.message);
//                 },
//                 success: function () {
//                 }
//             }
//             );
//         });


//     }, function (error) {

// });