﻿app.controller("RedeCredenciadaPlanosAtendidosController", function($scope, $http, $rootScope, RedeCredenciadaService) {
    $scope.unidade = RedeCredenciadaService.getUnidade();
    $scope.unidadeDetalhes = RedeCredenciadaService.getUnidadeDetalhes();

    $scope.planosAtendidos = [];

    app.ajax({
        scope: $scope,
        http: $http,
        url: app.apiUrl + "RedeCredenciada/PlanosAtendidos",
        params: {
            vinculo: $rootScope.numeroVinculo,
            rede: $scope.unidade.CodigoRede,
            codigoPrestador: $scope.unidade.CodigoPrestador

        },
        success: function(data)
        {
            $scope.planosAtendidos = data.Data;
        }
    });


});