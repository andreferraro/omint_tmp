﻿app.controller("AtendimentoListaController", function($scope, $http, AtendimentoService, $rootScope) {

    $scope.atendimentoLista = [];

    $scope.filtro = AtendimentoService.getFiltro();

    app.ajax({
        scope: $scope,
        http: $http,
        url: app.apiUrl + "Atendimento/Lista",
        params: {
            vinculo: $rootScope.numeroVinculo,
            situacao: $scope.filtro.situacao || "",
            idChamado: $scope.filtro.idChamado || -1,
            idClassifica: $scope.filtro.idClassifica || -1,
            idTipoDemanda: $scope.filtro.idTipoDemanda || -1,
            idTipoCliente: 1
        },
        success: function(data)
        {
            $scope.atendimentoLista = data.Data.Atendimentos;

            var assuntos = _(_(data.Data.Demandas).filter(function(v) {
                return v.AutoEncerra !== "S";
            })).groupBy(
                function(v) {
                    return v.IdTipoDemanda;
                });

            var demandas = _(assuntos).map(function(v) {
                return {
                    Id: v[0].IdTipoDemanda,
                    Desc: v[0].DescricaoTipoDemanda
                };
            });

            AtendimentoService.setDemandas({
                assuntos: assuntos,
                demandas: demandas
            });
        }
    });

    $scope.detalhe = function(atendimentoDetalhe)
    {
        AtendimentoService.setAtendimentoDetalhe(atendimentoDetalhe);
        app.navigate("views/restrito/atendimento-detalhe.html");
    };
});