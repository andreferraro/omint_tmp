﻿app.controller("AssociadoPlanoListaController", function ($scope, $http, $rootScope, CredencialEletronicaService) {

    $scope.data = [];
    $scope.credencial = {};

    app.ajax({
        scope: $scope,
        http: $http,
        url: app.apiUrl + "Associado/ListaDadosPlano",
        params: {
            vinculo: $rootScope.numeroVinculo
        },
        success: function (data) {
            $scope.data = data.Data;
        }
    });

    $scope.credencialDetalhe = function (titulo, tipoPlano) {

        var vinculo = app.nrVinculo;

        app.ajax({
            scope: $scope,
            http: $http,
            showProgress: false,
            modalLoading: true,
            url: app.apiUrl + "Associado/ObterCredencialEletronica",
            params: {
                codigoTitulo: titulo,
            },
            handleErrors: false,
            success: function (data) {
                $scope.credencial = data.Data;

                CredencialEletronicaService.setCredencial($scope.credencial);

                if (tipoPlano == "Odontologia") {
                    app.navigate("views/restrito/credencial-eletronica-odonto.html");
                }
                else {
                    app.navigate("views/restrito/credencial-eletronica-medicina.html");
                }
            }
        });
        
    }



});