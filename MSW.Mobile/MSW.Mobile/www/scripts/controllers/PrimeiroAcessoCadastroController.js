﻿app.controller("PrimeiroAcessoCadastroController", function($scope, $http) {

    $scope.info = app.primeiroAcesso.info;

    $scope.submit = function()
    {
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "PrimeiroAcesso/CadastrarUsuarioOmint",
            params: {
                hash: app.primeiroAcesso.hash,
                senhaEncrypted: app.rsaEncrypt($scope.senha)
            },
            success: function(data)
            {
                ons.notification.alert({
                    title: "Informação",
                    message: "Cadastro realizado com sucesso. Por favor realize o login."
                });
                app.navigate("views/login.html", true);
            }
        });
    };

    $scope.submitFacebook = function()
    {
        facebookConnectPlugin.logout(function(response) {
                FacebookLogin();
            },
            function(response) {
                FacebookLogin();
            });
    };

    var FacebookLogin = function()
    {
        facebookConnectPlugin.login([], function(response) {
                app.ajax({
                    scope: $scope,
                    http: $http,
                    url: app.apiUrl + "PrimeiroAcesso/CadastrarUsuarioFacebook",
                    params: {
                        hash: app.primeiroAcesso.hash,
                        facebookEncrypted: app.rsaEncrypt(response.authResponse.userID)
                    },
                    success: function(data)
                    {
                        ons.notification.alert({
                            title: "Informação",
                            message: "Cadastro realizado com sucesso. Por favor realize o login com Facebook."
                        });
                        app.navigate("views/login.html", true);
                    }
                });
            },
            function(response) {
                console.log("Facebook error", response);
            });
    };
});