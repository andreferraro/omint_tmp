﻿app.controller("AutorizacaoListaController", function($scope, $http, AutorizacaoService, $rootScope) {

    $scope.autorizacaoLista = [];

    $scope.filtro = AutorizacaoService.getFiltro();

    $scope.filtro.vinculo = $rootScope.numeroVinculo;

    app.ajax({
        scope: $scope,
        http: $http,
        url: app.apiUrl + "Autorizacao/Lista",
        params: $scope.filtro,
        success: function(data)
        {
            $scope.autorizacaoLista = data.Data;
        }
    });

    $scope.detalhe = function(autorizacaoDetalhe)
    {
        AutorizacaoService.setAutorizacaoDetalhe(autorizacaoDetalhe);
        app.navigate("views/restrito/autorizacao-detalhe.html");
    };
});