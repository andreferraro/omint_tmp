﻿app.controller("RedeCredenciadaSubstitutosController", function($scope, $http, $rootScope, RedeCredenciadaService) {
    $scope.unidade = RedeCredenciadaService.getUnidade();
    $scope.unidadeDetalhes = RedeCredenciadaService.getUnidadeDetalhes();

    $scope.resultados = [];

    app.ajax({
        scope: $scope,
        http: $http,
        url: app.apiUrl + "RedeCredenciada/Substitutos",
        params: {
            vinculo: $rootScope.numeroVinculo,
            rede: $scope.unidade.CodigoRede,
            codigoPrestador: $scope.unidade.CodigoPrestador,
            srEndereco: $scope.unidade.SrEndereco
        },
        success: function(data)
        {
            $scope.resultados = data.Data;
        }
    });

    $scope.prestadorClick = function (prestador) {
        RedeCredenciadaService.setUnidade(prestador);
        RedeCredenciadaService.setUnidadeDetalhes({});
        app.navigate("views/restrito/redecredenciada-detalhamento.html");
    };


});