﻿app.controller("AutorizacaoFiltroController", function($scope, $http, AutorizacaoService) {

    var monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
      "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
    ];

    $scope.meses = _(0).range(12).map(function (mes) {
        return { nome: monthNames[mes], value: mes + 1 };
    });

    $scope.anos = _(2000).range(new Date().getUTCFullYear() + 1).reverse();
    
    $scope.filtro = AutorizacaoService.getFiltro();

    $scope.submit = function()
    {
        AutorizacaoService.setFiltro($scope.filtro);
        app.navigate("views/restrito/autorizacao-lista.html", true);
    };
});