﻿app.controller("RedeCredenciadaBuscaEmergenciaController", function($scope, $http, $rootScope, RedeCredenciadaService) {

    $scope.buscaNome = RedeCredenciadaService.getBuscaNome();

    $scope.buscaEmergenciaResultados = [];
    $scope.buscaEmergenciaResultadosPrestadoresLength = 0;

    $scope.buscarOutrasRedes = RedeCredenciadaService.getPlano() !== undefined;

    $scope.buscaNomeSubmit = function()
    {
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "RedeCredenciada/BuscaNome",
            params: {
                codigoRede: RedeCredenciadaService.getPlano() !== undefined
                                ? RedeCredenciadaService.getPlano().Plano.CodigoRede
                                : "",
                vinculo: $rootScope.numeroVinculo,
                nome: 'Ortopedista',
                inativos: RedeCredenciadaService.getBuscarCredenciadosInativos()
            },
            success: function(data)
            {
                $scope.buscaNome = "";
                RedeCredenciadaService.setBuscaNome("");

                $scope.buscaNomeResultados = data.Data;
                $scope.buscaNomeResultadosPrestadoresLength =
                    _($scope.buscaNomeResultados).chain().pluck("Prestadores").flatten().size().value();
            }
        });
    };

    $scope.prestadorClick = function(prestador)
    {
        RedeCredenciadaService.setUnidade(prestador);
        RedeCredenciadaService.setUnidadeDetalhes({});
        app.navigate("views/restrito/redecredenciada-detalhamento.html");
    };
    if($scope.buscaNome.length >= 4)
    {
        $scope.buscaNomeSubmit();
    }
});