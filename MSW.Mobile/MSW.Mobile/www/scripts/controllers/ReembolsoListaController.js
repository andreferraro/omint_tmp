﻿app.controller("ReembolsoListaController", function ($scope, $http, ReembolsoService, $rootScope) {

    $scope.reembolsoLista = [];

    $scope.filtro = ReembolsoService.getFiltro();

    $scope.filtro.vinculo = $rootScope.numeroVinculo;

    app.ajax({
        scope: $scope,
        http: $http,
        url: app.apiUrl + "Reembolso/ListaSimples",
        params: $scope.filtro,
        success: function (data) {
            $scope.reembolsoLista = data.Data;
        }
    });

    $scope.detalhe = function (reembolsoDetalhe) {
        ReembolsoService.setReembolsoDetalhe(reembolsoDetalhe);
        app.navigate("views/restrito/reembolso-detalhe.html");
    };

    $scope.detalhePorId = function (reembolso) {
        var navigateToDetalhe = function (detalhe) {
            ReembolsoService.setReembolsoDetalhe(detalhe);
            app.navigate("views/restrito/reembolso-detalhe.html");
        };

        var detalhe = ReembolsoService.getReembolsoDetalheFromCache(reembolso.IdReembolso);
        if (detalhe) {
            navigateToDetalhe(detalhe);
        } else {
            app.ajax({
                scope: $scope,
                http: $http,
                url: app.apiUrl + "Reembolso/Detalhe/?idReembolso=" + reembolso.IdReembolso,
                success: function (data) {
                    var detalhe = {
                        Detalhes: data.Data.Detalhes,
                        Pagamento: data.Data.Pagamento,
                        Reembolso: reembolso
                    };
                    ReembolsoService.setReembolsoDetalheToCache(detalhe);
                    navigateToDetalhe(detalhe);
                }
            });
        }
    };
});