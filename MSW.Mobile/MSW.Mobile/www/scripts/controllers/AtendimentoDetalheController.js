﻿app.controller("AtendimentoDetalheController", function ($scope, $http, AtendimentoService, $rootScope, focus, $q, fileService) {
    var vm = this;


    vm.atendimentoDetalhe = AtendimentoService.getAtendimentoDetalhe();
    vm.novaObservacao;
    vm.insercaoVisivel = false;
    vm.inserindoObservacao = false;
    vm.filtro = AtendimentoService.getFiltro();
    vm.imageList = [];

    app.ajax({
        scope: $scope,
        http: $http,
        url: app.apiUrl + "Atendimento/Detalhe",
        params: {
            vinculo: $rootScope.numeroVinculo,
            situacao: vm.filtro.situacao || "",
            idChamado: vm.atendimentoDetalhe.IdChamado || $rootScope.atendimentoDetalhe.IdChamado,
            idClassifica: vm.filtro.idClassifica || -1,
            idTipoDemanda: vm.filtro.idTipoDemanda || -1,
            idTipoCliente: 1
        },
        success: function (data) {
            vm.atendimentoDetalhe = data.Data.Atendimentos[0];
        }
    });

    if ($rootScope.notificacoes["Atendimento"].indexOf(vm.atendimentoDetalhe.IdChamado) >= 0 
        && vm.atendimentoDetalhe.IdTipoDemanda != 8) {//8 - tipo de demanda = Convocação
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "Menu/AlterarNotificacaoStatus",
            params: {
                tipoServico: "A",
                idServico: vm.atendimentoDetalhe.IdChamado,
                novoStatus: vm.atendimentoDetalhe.CodigoStatus
            },
            success: function (data) {
                $rootScope.notificacoes["Atendimento"] = _($rootScope.notificacoes["Atendimento"]).without(vm.atendimentoDetalhe.IdChamado);
            }
        });
    }

    vm.inserirObservacao = function () {        
        var liPromise = [];
        if(vm.novaObservacao){
            vm.isSending = true;
            var deferObs = $q.defer();
            app.ajax({
                scope: $scope,
                http: $http,
                url: app.apiUrl + "Atendimento/InserirObservacaoChamado",
                params: {
                    numeroChamado: vm.atendimentoDetalhe.IdChamado,
                    observacaoChamado: vm.novaObservacao
                },
                success: function (data) {
                    var idObservacao = data.Data;
                    var observacao = { DataInclusao: new Date().toLocaleDateString("pt-BR"), DescricaoObservacao: vm.novaObservacao };
                    vm.atendimentoDetalhe.Observacoes.splice(0, 0, observacao);
                    deferObs.resolve();
                },
                onError: function(error){
                    deferObs.reject();
                }
            });
            liPromise.push(deferObs.promise);
        }

        if (vm.imageList && vm.imageList.length){
            vm.isSending = true;
            liPromise = liPromise.concat(AtendimentoService.adicionarArquivosAoChamado(vm.imageList, vm.atendimentoDetalhe.IdChamadoCC, $scope));
        }
        $q.all(liPromise).then(
            function () {
                vm.imageList.forEach(function(item){
                    vm.atendimentoDetalhe.Imagens.Table.push({id_documento: item.id_documento , tp_arquivo: item.tp_arquivo });
                });
                mostraMensagem("Informação", "Informacões adicionados com sucesso.");                
            },
            function () {
                mostraMensagem("Informação", "Algumas informações não foram enviadas.");                            
            }
        );
    };

    vm.verDocumento = function(documento){
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "Atendimento/ObterDocumentoChamado",
            params: {
                idDocumento: documento.id_documento
            },
            success: function (data) {
                var objDocumento = data.Data.Table[0];
                var file = {};
                file.base64 = objDocumento.im_DocumentoImagem;
                file.extension = documento.tp_arquivo
                file.contentType = fileService.getMimeType(file);
                file.folderpath = ons.platform.isIOS() ? cordova.file.tempDirectory : (cordova.file.externalCacheDirectory || cordova.file.cacheDirectory);
                file.filename = 'Doc_' + documento.id_documento + '.' + documento.tp_arquivo;
                fileService.saveBase64AsFile(file.folderpath, file.filename, file.base64, file.contentType)
                    .then(function(data){
                        fileService.openFile(data.path, data.contentType);
                    },
                    function(error){
                        console.log(error);
                    });
            }
        });
    }

    vm.getIconByFileExtension = function(doc){
        switch(doc.tp_arquivo){
            case 'pdf':
                return 'fa fa-file-pdf-o';
                break;
            default:
                return 'fa fa-file-image-o';
                break;
        }
    }
    function mostraMensagem(titulo, mensagem, observacao) {
        ons.notification.alert({
            title: titulo,
            message: mensagem
        });
        vm.novaObservacao = '';
        vm.isSending = false;
        vm.imageList = [];
        dialogContatarAtendimento.hide();
    }
});
