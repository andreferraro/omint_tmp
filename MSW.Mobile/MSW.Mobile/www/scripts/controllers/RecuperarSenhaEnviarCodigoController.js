﻿app.controller("RecuperarSenhaEnviarCodigoController", function($scope, $http) {

    app.recuperarSenha.email = "";
    $scope.submit = function()
    {
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "RecuperarSenha/EnviarCodigo",
            params: {
                email: $scope.email
            },
            success: function (data) {
                app.recuperarSenha.email = $scope.email;
                app.navigate("views/recuperar-senha-informar-codigo.html");
            }
        });
    }
});