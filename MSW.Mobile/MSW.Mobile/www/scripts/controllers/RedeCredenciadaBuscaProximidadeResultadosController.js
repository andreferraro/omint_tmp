﻿app.controller("RedeCredenciadaBuscaProximidadeResultadosController",
    function($scope, $rootScope, $http, RedeCredenciadaService) {
        $scope.buscaTipoAtendimento = RedeCredenciadaService.getBuscaTipoAtendimento();
        $scope.buscaEspecialidade = RedeCredenciadaService.getBuscaEspecialidade();

        $scope.buscaProximidadeTipo = RedeCredenciadaService.getBuscaProximidadeTipo();

        $scope.resultados = [];

        var url;

        var params = {
            vinculo: $rootScope.numeroVinculo,
            rede: $scope.buscaTipoAtendimento.CodigoRede,
            tipoAtendimento: $scope.buscaTipoAtendimento.TipoView,
            codigoEspecialidade: $scope.buscaEspecialidade.CodigoEspecialidade,
            inativos: RedeCredenciadaService.getBuscarCredenciadosInativos()
        };

        if($scope.buscaProximidadeTipo.TipoProximidade === "L")
        {
            url = app.apiUrl + "RedeCredenciada/BuscaProximidadeAtual";
            params.latitude = app.geolocationPosition.latitude;
            params.longitude = app.geolocationPosition.longitude;
        }
        else
        {
            url = app.apiUrl + "RedeCredenciada/BuscaProximidade";
            params.buscaProximidadeTipo = $scope.buscaProximidadeTipo.TipoProximidade;
        }

        app.ajax({
            scope: $scope,
            http: $http,
            url: url,
            params: params,
            success: function(data)
            {
                $scope.resultados = data.Data;
            }
        });

        $scope.prestadorClick = function(prestador)
        {
            RedeCredenciadaService.setUnidade(prestador);
            RedeCredenciadaService.setUnidadeDetalhes({});
            app.navigate("views/restrito/redecredenciada-detalhamento.html");
        };
    });