﻿app.controller("NoticiaListaController", function ($scope, $http, NoticiaService) {
    $scope.noticiaLista = [];

    app.ajax({
        scope: $scope,
        http: $http,
        url: app.apiUrl + "Noticia/Listar",
        params: {
            quantidade: 30,
        },
        showProgress: true,
        handleErrors: false,
        success: function (data) {
            $scope.noticiaLista = data.Data;
        }
    });

    $scope.detalhe = function (noticiaDetalhe) {
        NoticiaService.setNoticiaDetalhe(noticiaDetalhe);
        app.navigate("views/restrito/noticia-detalhe.html");
    };
});