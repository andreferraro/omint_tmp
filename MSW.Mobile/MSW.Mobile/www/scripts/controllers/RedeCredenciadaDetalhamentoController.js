﻿app.controller("RedeCredenciadaDetalhamentoController", function($scope, $http, $rootScope, RedeCredenciadaService) {
    $scope.unidade = RedeCredenciadaService.getUnidade();
    $scope.unidadeDetalhes = RedeCredenciadaService.getUnidadeDetalhes();

        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "RedeCredenciada/Detalhamento",
            params: {
                vinculo: $rootScope.numeroVinculo,
                rede: $scope.unidade.CodigoRede,
                codigoPrestador: $scope.unidade.CodigoPrestador,
                srEndereco: $scope.unidade.SrEndereco,
                inativos: typeof $scope.unidade.DataDescredenciamento === "string"
            },
            success: function(data)
            {
                $scope.unidadeDetalhes = data.Data;

                $scope.unidade.Telefones = $scope.unidadeDetalhes.Contatos.map(function(contato) { return {
                    telefone : contato.EnderecoTelefoneDdd
                     + ' ' + contato.EnderecoTelefonePrefixo
                     + ' ' + contato.EnderecoTelefone}.telefone});
            }
        });
    //}

    $scope.nomePlano = _($rootScope.redeCredenciada.planos.TodosPlanos).find(
        function (v) {
            return v.CodigoRede.trim() === $scope.unidade.CodigoRede.trim();
            }).DescricaoRede.capitalize();

    var redeUltimosAcessos = JSON.parse(window.localStorage.redeUltimosAcessos || "{}");

    if(redeUltimosAcessos.versao !== 3)
    {
        redeUltimosAcessos = {
            versao: 3,
            itens: []
        };
    }

    //redeUltimosAcessos.itens = _(redeUltimosAcessos.itens).reject(function(v) {
    //    return v.Prestador.SrEndereco.trim() === $scope.unidade.SrEndereco.trim();
    //});

    redeUltimosAcessos.itens.unshift({
        Prestador: $scope.unidade,
        Detalhes: $scope.unidadeDetalhes
    });
    redeUltimosAcessos.itens = _(redeUltimosAcessos.itens).first(5);

    window.localStorage.redeUltimosAcessos = JSON.stringify(redeUltimosAcessos);
    $rootScope.$broadcast("redeUltimosAcessosOnChange");

    $scope.getDataDescredenciamento = function()
    {
        var date = new Date($scope.unidade.DataDescredenciamento);
        return date.getUTCDate() + "/" + (date.getUTCMonth() + 1) + "/" + date.getUTCFullYear();
    };
    $scope.planosAtendidosClick = function()
    {
        app.navigate("views/restrito/redecredenciada-planos-atendidos.html");
    };
    $scope.credenciadosSubstitutosClick = function()
    {
        app.navigate("views/restrito/redecredenciada-substitutos.html");
    };

    $scope.formatarTelefone = function (telefone) {
        if (!telefone || telefone === '')
         return '';
        
        var splitedPhone = telefone.split(' ');

        if (splitedPhone.length > 2)
            return '' + splitedPhone[0] + ' ' + splitedPhone[1] + ' ' + splitedPhone[2];
        else
            return splitedPhone[0] + ' ' + splitedPhone[1];
    }

    $scope.formatarTelefoneParaClick = function (telefone) {
        if (!telefone || telefone === '')
         return '';
        
        var splitedPhone = telefone.split(' ');

        if (splitedPhone.length > 2)
            return '' + splitedPhone[0] + '-' + splitedPhone[1] + '-' + splitedPhone[2];
        else
            return splitedPhone[0] + '-' + splitedPhone[1];
    }
});