app.controller("AssociadoAtualizarCadastroController", function ($scope, $http, $rootScope, BeneficiarioService, $timeout, focus) {
    $scope.beneficiarios = [];
    ListaBeneficiarios();
    
    $timeout(function() {
        focus('focus');
    });

    function ListaBeneficiarios() {
        $scope.beneficiarios = BeneficiarioService.getBeneficiarios();
        $timeout(function(){
            $(".cpf").inputmask({mask: ['999.999.999-99'], keepStatic: true});
        },0,false);
    };

    $scope.cancelarAtualizacao = function () {
        app.navigate("views/restrito/home.html");
    }

    $scope.atualizarCadastro = function() {
        var beneficiarios = document.getElementsByClassName("beneficiario");
        
        for (var index = 0; index < beneficiarios.length; index++) {
            
            var element = beneficiarios[index];
            var id_pessoa_view = document.getElementsByClassName("id_pessoa")[index].textContent
            var cpf_view = document.getElementsByClassName("cpf")[index].value
        
            if (ValidarCPF(cpf_view))
            {
                app.ajax({
                scope: $scope,
                http: $http,
                url: app.apiUrl + "Associado/AtualizarCpfBeneficiario",
                params: {
                    codigoTitulo: $rootScope.numeroVinculo,
                    id_pessoa: id_pessoa_view,
                        cpf: cpf_view.replace(/-/g, "").replace(/\./g, "")
                },
                showProgress: true,
                handleErrors: false,
                success: function (data) {
                    ons.notification.alert({
                        title: "Informação", 
                        message: "Cadastro atualizado com sucesso."
                    });

                    app.navigate("views/restrito/home.html");
                },
                failure: function(message, status)
                {
                    if(status == 400)   
                        return ons.notification.alert({
                            title: "Erro ao atualizar cadastro", 
                            message: message
                        });
                    ons.notification.alert({
                        title: "Erro", 
                        message: "Houve um erro ao alterar o cadastro para o usuário "+$scope.beneficiarios[index].DescricaoBeneficiario
                    });       
                }
                });
            }
            else {
                ons.notification.alert({
                    title: "Informação",
                    message: "O CPF digitado para o usuário " + $scope.beneficiarios[index].DescricaoBeneficiario + "é inválido, tente novamente."
                });
                break;
            }
        }
    }

    function ValidarCPF(cpf) {
        cpf = cpf.replace(/[^\d]+/g,'');    
        if(cpf == '') return false; 
        // Elimina CPFs invalidos conhecidos    
        if (cpf.length != 11 || 
            cpf == "00000000000" || 
            cpf == "11111111111" || 
            cpf == "22222222222" || 
            cpf == "33333333333" || 
            cpf == "44444444444" || 
            cpf == "55555555555" || 
            cpf == "66666666666" || 
            cpf == "77777777777" || 
            cpf == "88888888888" || 
            cpf == "99999999999")
                return false;       
        // Valida 1o digito 
        add = 0;    
        for (i=0; i < 9; i ++)       
            add += parseInt(cpf.charAt(i)) * (10 - i);  
            rev = 11 - (add % 11);  
            if (rev == 10 || rev == 11)     
                rev = 0;    
            if (rev != parseInt(cpf.charAt(9)))     
                return false;       
        // Valida 2o digito 
        add = 0;    
        for (i = 0; i < 10; i ++)        
            add += parseInt(cpf.charAt(i)) * (11 - i);  
        rev = 11 - (add % 11);  
        if (rev == 10 || rev == 11) 
            rev = 0;    
        if (rev != parseInt(cpf.charAt(10)))
            return false;       
        return true;   
    }
});