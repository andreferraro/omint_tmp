﻿app.controller("RedeCredenciadaBuscaRegiaoResultadosController",
    function($scope, $rootScope, $http, RedeCredenciadaService) {
        $scope.buscaTipoAtendimento = RedeCredenciadaService.getBuscaTipoAtendimento();
        $scope.buscaEspecialidade = RedeCredenciadaService.getBuscaEspecialidade();

        $scope.buscaRegiao = RedeCredenciadaService.getBuscaRegiao();

        $scope.resultados = [];

        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "RedeCredenciada/BuscaRegiao",
            params: {
                vinculo: $rootScope.numeroVinculo,
                rede: $scope.buscaTipoAtendimento.CodigoRede,
                tipoAtendimento: $scope.buscaTipoAtendimento.TipoView,
                codigoEspecialidade: $scope.buscaEspecialidade.CodigoEspecialidade,
                codigoEstado: $scope.buscaRegiao.estado || "",
                cidade: $scope.buscaRegiao.cidade || "",
                bairro: $scope.buscaRegiao.bairro || "",
                inativos: RedeCredenciadaService.getBuscarCredenciadosInativos()
            },
            success: function(data)
            {
                $scope.resultados = data.Data;
            }
        });

        $scope.prestadorClick = function(prestador)
        {
            RedeCredenciadaService.setUnidade(prestador);
            RedeCredenciadaService.setUnidadeDetalhes({});
            app.navigate("views/restrito/redecredenciada-detalhamento.html");
        };
    });