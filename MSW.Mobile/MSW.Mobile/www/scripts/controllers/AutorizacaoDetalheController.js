﻿app.controller("AutorizacaoDetalheController", function ($scope, $http, $rootScope, AutorizacaoService) {

    $scope.autorizacaoDetalhe = AutorizacaoService.getAutorizacaoDetalhe();

    $scope.Beneficiarios = _.uniq($scope.autorizacaoDetalhe.Detalhes, false, function (item, key, a) {
        return item.NomeBeneficiario;
    });

    $scope.downloadAutorizacao = function (autorizacao) {
        ons.notification.confirm({
            title: "Confirmação",
            message: "Deseja fazer o download da autorização?",
            callback: function (answer) {
                if (answer) {
                    var url = app.apiUrl + "Autorizacao/ObterAutorizacao?idAutorizacao=" + autorizacao.IdAutorizacao;

                    var localPath = (cordova.file.externalCacheDirectory || cordova.file.cacheDirectory) +
                        "Autorizacao_" +
                        autorizacao.IdAutorizacao +
                        ".pdf";

                    $rootScope.ajaxLoadingModal = true;

                    $rootScope.$apply(function () {
                        new FileTransfer().download(url, localPath, function (entry) {

                            console.log("download complete: " + entry.toURL());

                            $rootScope.ajaxLoadingModal = false;
                            $rootScope.$apply(function () {

                                cordova.plugins.fileOpener2.open(entry.toURL(), "application/pdf", {
                                    error: function (e) {
                                        ons.notification.alert({
                                            title: "Erro",
                                            message:
                                                "Ocorreu um erro ao abrir o arquivo. Por favor verifique seu leitor de PDF e tente novamente."
                                        });
                                        console.log("Error status: " + e.status + " - Error message: " + e.message);
                                    },
                                    success: function () {
                                    }
                                }
                                );
                            });


                        }, function (error) {
                            console.log("FileTransfer.download error", error);

                            var errorMessage = "Não há imagem disponível para consulta deste documento.";

                            // if (error.code === 1) {
                            //     errorMessage = "Não há imagem disponível para consulta deste documento.";
                            // }

                            $rootScope.ajaxLoadingModal = false;
                            $rootScope.$apply(function () {
                                ons.notification.alert({
                                    title: "Erro",
                                    message: errorMessage
                                });
                            });
                        });
                    });
                }
            }
        });
    };
});