﻿app.controller("RedeCredenciadaHomeController", function($scope, $http, $rootScope, $timeout, RedeCredenciadaService) {

    $scope.buscaNome = "";

    $scope.buscarCredenciadosInativos = RedeCredenciadaService.getBuscarCredenciadosInativos();
    $scope.buscarOutrasRedes = RedeCredenciadaService.getPlano() !== undefined;

    $scope.tiposAtendimento = RedeCredenciadaService.getPlano() !== undefined
                                  ? RedeCredenciadaService.getPlano().TiposAtendimento
                                  : $rootScope.redeCredenciada.planos.TiposAtendimentoPadrao;

    var carregarRedeUltimosAcessos = function()
    {
        $scope.redeUltimosAcessos = JSON.parse(window.localStorage.redeUltimosAcessos || "{}");
        if(typeof $scope.redeUltimosAcessos !== "object" || $scope.redeUltimosAcessos.versao !== 3)
        {
            $scope.redeUltimosAcessos = { versao: 3, itens: [] };
        }
    };
    carregarRedeUltimosAcessos();

    $scope.$on("redeUltimosAcessosOnChange", function() {
        carregarRedeUltimosAcessos();
    });
     
    $scope.buscarCredenciadosInativosChange = function()
    {
        $timeout(function() {
            RedeCredenciadaService.setBuscarCredenciadosInativos($scope.buscarCredenciadosInativos);
        });
    };

    $scope.buscarOutrasRedesChange = function()
    {
        $timeout(function() {
            if($scope.buscarOutrasRedes)
            {
                $scope.buscarOutrasRedes = false;
                app.navigate("views/restrito/redecredenciada-selecao-plano.html");
            }
            else
            {
                $scope.tiposAtendimento = $rootScope.redeCredenciada.planos.TiposAtendimentoPadrao;
                RedeCredenciadaService.setPlano(undefined);
            }
        });
    };

    $scope.buscaNomeSubmit = function()
    {
        RedeCredenciadaService.setBuscaNome($scope.buscaNome);
        app.navigate("views/restrito/redecredenciada-busca-nome.html");
    };
    $scope.buscaEmergencia = function(){
        app.navigate("views/restrito/redecredenciada-busca-emergencia.html");
    };
    $scope.tipoAtendimentoClick = function(tipoAtendimento)
    {
        RedeCredenciadaService.setBuscaTipoAtendimento(tipoAtendimento);
        app.navigate("views/restrito/redecredenciada-busca-especialidade.html");
    };
    $scope.unidadeClick = function(item)
    {
        RedeCredenciadaService.setUnidadeDetalhes(item.Detalhes);
        RedeCredenciadaService.setUnidade(item.Prestador);
        app.navigate("views/restrito/redecredenciada-detalhamento.html");
    };
});