app.controller("TokenRenewalController", function($scope, $http, $rootScope) {
    if (window.localStorage.credentials) {
        var storedCredentials = JSON.parse(window.localStorage.credentials);

        if (storedCredentials) {
            if (storedCredentials.facebookUserId) {
                renewAccess("Face", storedCredentials.facebookUserId);
            } else {
                renewAccess(storedCredentials.email, storedCredentials.password);
            }
        } else {
            app.redirectToLogin();
        }
    } else {
        app.redirectToLogin();
    }

    function renewAccess(username, senha){
        app.ajax({
            http: $http,
            scope: $scope,
            url: app.apiUrl + "Login",
            modalLoading: false,            
            data: "UserName=" + username + "&grant_type=password&password=" + encodeURIComponent(senha),
            headers: { 'Content-Type': "application/x-www-form-urlencoded" },
            success: function(data)
            {
                window.localStorage.loginData = JSON.stringify(data);
                window.localStorage.lastLogin = new Date().toString();
                $rootScope.$broadcast("refreshMenu");
            }
        });
    }
});