﻿app.controller("RedeCredenciadaBuscaLocalizacaoController",
    function($scope, $rootScope, $http, RedeCredenciadaService) {
        $scope.buscaTipoAtendimento = RedeCredenciadaService.getBuscaTipoAtendimento();
        $scope.buscaEspecialidade = RedeCredenciadaService.getBuscaEspecialidade();

        $scope.regioes = {};

        $scope.getObjectSize = app.getObjectSize;

        $scope.buscaRegiao = {};

        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "RedeCredenciada/Regioes",
            params: {
                vinculo: $rootScope.numeroVinculo,
                rede: $scope.buscaTipoAtendimento.CodigoRede,
                tipoAtendimento: $scope.buscaTipoAtendimento.TipoView,
                codigoEspecialidade: $scope.buscaEspecialidade.CodigoEspecialidade,
                inativos: RedeCredenciadaService.getBuscarCredenciadosInativos()
            },
            success: function(data)
            {
                $scope.regioes = data.Data.Regioes;

                $scope.buscaRegiao.estado = data.Data.EnderecoResidencial.Estado;
                $scope.buscaRegiao.cidade = data.Data.EnderecoResidencial.Cidade;
            }
        });

        $scope.buscaRegiaoSubmit = function()
        {
            RedeCredenciadaService.setBuscaRegiao($scope.buscaRegiao);
            app.navigate("views/restrito/redecredenciada-busca-regiao-resultados.html");
        };

        $scope.buscaProximidade = function(tipo)
        {
            if(tipo.TipoProximidade === "L" &&
            ((app.geolocationPosition.latitude || 0) === 0 || (app.geolocationPosition.longitude || 0) === 0))
            {
                ons.notification.alert({
                    title: "Erro",
                    message: "Não foi possível obter sua localização atual. Ative o recurso de GPS e tente novamente."
                });
                return;
            }

            RedeCredenciadaService.setBuscaProximidadeTipo(tipo);

            app.navigate("views/restrito/redecredenciada-busca-proximidade-resultados.html");
        };
    });