﻿app.controller("MenuController", function ($scope, $http, $rootScope, ReembolsoService) {
    $scope.menu = [];

    $scope.$on("refreshNotifications", function () {
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "Menu/ObterNotificacoes",
            showProgress: false,
            handleErrors: false,
            success: function (data) {
                $rootScope.notificacoes = data.Data;
                $rootScope.$broadcast("saveCurrentPosition");
            }
        });
    });

    $scope.openKitPage = function () {
        app.navigate('views/restrito/meus-arquivos.html', true)
    }

    $scope.$on("saveCurrentPosition", function () {
        if (window.cordova && window.device && window.device.isVirtual !== undefined) {
            var posicaoAtual = {
                latitude: app.geolocationPosition.latitude,
                longitude: app.geolocationPosition.longitude
            };

            app.ajax({
                scope: $scope,
                http: $http,
                url: app.apiUrl + "Associado/GuardarPosicaoAtualUsuario",
                showProgress: false,
                handleErrors: false,
                params: posicaoAtual,
                success: function (data) {
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    });

    $scope.$on("refreshMenu", function () {

        if (!$rootScope.numeroVinculo) {
            if (ons.platform.isIOS() || ons.platform.isAndroid()) {
                document.addEventListener("deviceready", function () {
                    // Validate fingerprint only if user has configured this option on the configuration menu.
                    if (app.fingerprintAuth.isEnabled() && !app.fingerPrintAuthenticated) {
                        // If fingerprint authentication is configured on device, then validate fingerprint.
                        app.fingerprintAuth.isAvailable(function () {
                            // If fingerprint auth is passed then redirect to token renewal controller, if not redirect to login.
                            app.fingerprintAuth.validateIdentity(app.redirectToTokenRenewal, navigateToLogin);
                        }, app.redirectToLogin);
                    } else {
                        app.navigate("views/restrito/escolher-titulo.html", false);
                    }
                });
            }else{
                app.navigate("views/restrito/escolher-titulo.html", false);
            }
            return;
        }

        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "Menu/ObterServicosDisponiveis",
            modalLoading: true,
            showProgress: false,
            data: {
                vinculo: $rootScope.numeroVinculo
            },
            headers: { 'Content-Type': "application/json;charset=utf-8" },
            success: function (data) {
                $rootScope.redeCredenciada = {
                    planos: data.Data.RedePlanos,
                    tiposBuscaProximidade: data.Data.RedeTiposBuscaProximidade
                };

                $rootScope.enderecoResidencial = data.Data.EnderecoResidencial;
                $rootScope.numeroVinculo = data.Data.Menu[1].Itens[0].Vinculo.NumeroIdentificacaoVinculo;
                $scope.DescricaoIdentificacaoVinculo = data.Data.Menu[1].Itens[0].Vinculo.DescricaoIdentificacaoVinculo.split(' ')[0];
                $scope.menu = data.Data.Menu;
                app.menu = data.Data.Menu[1].Itens;
                //console.log(app.menu);

                $rootScope.possuiAutenticadorOmint = data.Data.PossuiAutenticadorOmint;
                $rootScope.possuiAutenticadorFacebook = data.Data.PossuiAutenticadorFacebook;

                $rootScope.$broadcast("refreshNotifications");

                var nrVinculo = data.Data.Menu[1].Itens[0].Vinculo.NumeroIdentificacaoVinculo;
                $rootScope.CodigoTitulo = nrVinculo;
                app.registerDevice(nrVinculo, $scope, $http, $rootScope, ReembolsoService);

                app.navigate("views/restrito/home.html", true);
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    function navigateToLogin(){
        app.redirectToLogin(true);
    }

    setInterval(function () {
        if (window.localStorage.loginData) {
            $rootScope.$broadcast("refreshNotifications");
        }
    }, 1000 * 60);

    $scope.navigateByViewTitle = function (title) {
        for (var index = 0; index < app.menu.length; index++) {
            var element = array[index];
            if (element.Title === title)
                $scope.navigate(element);
        }
    };

    $scope.navigate = function (menuItem) {
        console.log("navegação: ", menuItem);
        $rootScope.currentMenuItem = menuItem;
        app.navigate(menuItem.View, false, true);
        app.unlockScreenOrientation();
    };

    $scope.logout = function () {
        ons.notification.confirm({
            title: "Confirmação",
            message: "Tem certeza que deseja sair?",
            callback: function (answer) {
                if (answer) {
                    window.localStorage.credentials = "";
                    window.localStorage.loginData = "";
                    app.fingerPrintAuthenticated = false;
                    $scope.menu = [];
                    app.navigate("views/login.html", true);
                }
            }
        });
    };
});