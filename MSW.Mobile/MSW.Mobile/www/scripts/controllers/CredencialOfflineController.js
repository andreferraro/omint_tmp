app.controller("CredencialOfflineController", function ($scope, CredencialOfflineService, CredencialEletronicaService) 
{
    $scope.credenciais = [];
    var ultimoLogin = !localStorage.lastLogin? null: new Date(localStorage.lastLogin);
    var dozeHoras = 60 * 60 * 1000 * 1440;
    var dataAtual = new Date();
    if(!ultimoLogin || (dataAtual - ultimoLogin) > dozeHoras)
    {

        return ons.notification.alert({
            title: "Alerta", 
            message: "O último login foi realizado há mais de 60 dias. Por favor, faça o login para ter acesso as credênciais."
        });
    }


    CredencialOfflineService.listar().then(function(credenciais)
    {
        $scope.credenciais = credenciais;
    })
    $scope.credencialMedicina = function (credencial) {
        
        if(credencial.NumeroCns.indexOf("CARTÃO NACIONAL DE SAÚDE (CNS):") === -1){
            credencial.NumeroCns = "CARTÃO NACIONAL DE SAÚDE (CNS): " + credencial.NumeroCns;
        }

        CredencialEletronicaService.setCredencial(credencial);
        app.navigate("views/restrito/credencial-eletronica-medicina.html");
    }

    $scope.credencialOdonto = function (credencial) {
        
        if(credencial.NumeroCns.indexOf("CARTÃO NACIONAL DE SAÚDE (CNS):") === -1){
            credencial.NumeroCns = "CARTÃO NACIONAL DE SAÚDE (CNS): " + credencial.NumeroCns;
        }

        CredencialEletronicaService.setCredencial(credencial);
        app.navigate("views/restrito/credencial-eletronica-odonto.html");
    }

});