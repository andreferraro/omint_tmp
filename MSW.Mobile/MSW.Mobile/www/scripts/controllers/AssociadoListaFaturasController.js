﻿app.controller("AssociadoListaFaturasController", function($scope, $http, $rootScope) {

    $scope.faturas = [];

    $scope.faturaDocumento = function(fatura)
    {
        ons.notification.confirm({
            title: "Confirmação",
            message: "Deseja fazer o download da fatura?",
            callback: function(answer)
            {
                if(answer)
                {
                    var url = app.apiUrl + "Associado/ObterFatura?nrTitulo=" + fatura.NrTitulo;

                    var localPath = (cordova.file.externalCacheDirectory || cordova.file.cacheDirectory) +
                        "Fatura_" +
                        fatura.NrTitulo +
                        ".pdf";

                    $rootScope.ajaxLoadingModal = true;

                    $rootScope.$apply(function() {
                        new FileTransfer().download(url, localPath, function(entry) {

                            console.log("download complete: " + entry.toURL());

                            $rootScope.ajaxLoadingModal = false;
                            $rootScope.$apply(function() {

                                cordova.plugins.fileOpener2.open(entry.toURL(), "application/pdf", {
                                        error: function(e)
                                        {
                                            ons.notification.alert({
                                                title: "Erro",
                                                message:
                                                    "Ocorreu um erro ao abrir o arquivo. Por favor verifique seu leitor de PDF e tente novamente."
                                            });
                                            console.log("Error status: " + e.status + " - Error message: " + e.message);
                                        },
                                        success: function()
                                        {
                                        }
                                    }
                                );
                            });


                        }, function(error) {
                            console.log("FileTransfer.download error", error);

                            var errorMessage = "Ocorreu um erro ao realizar o download do arquivo.";

                            if(error.code === 1)
                            {
                                errorMessage = "Não há imagem disponível para consulta deste documento.";
                            }

                            $rootScope.ajaxLoadingModal = false;
                            $rootScope.$apply(function() {
                                ons.notification.alert({
                                    title: "Erro",
                                    message: errorMessage
                                });
                            });
                        });
                    });
                }
            }
        });
    };
    app.ajax({
        scope: $scope,
        http: $http,
        url: app.apiUrl + "Associado/ListaFaturas",
        params: {
            vinculo: $rootScope.numeroVinculo
        },
        success: function(data)
        {
            $scope.faturas = data.Data;
        }
    });
});