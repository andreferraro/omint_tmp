app.controller('AtendimentoNovaObservacaoController', function(AtendimentoService, $scope, $rootScope, fileService, $timeout, $http){
    $scope.fileList = [];
    $scope.atendimentoDetalhe = AtendimentoService.getAtendimentoDetalhe();
    $scope.app = app;
    $scope.removeFile = function(file){
        var index = $scope.fileList.indexOf(file);
        $scope.fileList.splice(index, 1);
    };
    $("#images").on('change', function(event){

        var files = event.target.files;
        var message = verificaArquivoInvalido(files);
        if(message)
        {
            ons.notification.alert({
                title: "Arquivo não permitido", 
                message: message
            });
            return;
        }

        $.each(files, function(i, file){
            $timeout(function(){
                $scope.fileList.push(file);
            });
            var reader = new FileReader();
            reader.onload = function(e){
                $timeout(function(){
                    file.data = reader.result; 
                });       
            }
            reader.readAsDataURL(file);
        });
    }).on('click',function(e){
        if($rootScope.ajaxLoading)
            e.preventDefault();
    });
    function verificaArquivoInvalido(fileList){
        var mensagem = null;
        var somaTamanhoArquivos = 0;
        $.each(fileList, function(index, file){
            somaTamanhoArquivos += file.size;
            if(file.size > 41943040 )
                mensagem =  "O arquivo " + file.name + " é muito grande. (Tamanho máximo = 40Mb)";
            
            $.each($scope.fileList, function(i, fileAdd){
                if(fileAdd.name === file.name && fileAdd.size === file.size)
                    mensagem = "O arquivo " + file.name + " já foi inserido.";
            }); 

            if(!fileService.verifyAcceptedMimeType(file.type))
                mensagem = "O tipo do arquivo " + file.name + " não é aceito."
        });
        if(mensagem)
            return mensagem;

        var somaTamanhoArquivosJaInseridos = 0;
        $.each($scope.fileList, function(i, fileAdd){
            somaTamanhoArquivosJaInseridos += fileAdd.size;
        }); 
        if((somaTamanhoArquivosJaInseridos + somaTamanhoArquivos) > 41943040)
            return "O tamanho total de arquivos inseridos deve ser de até 40Mb";        
    }


    $scope.sendForm = function(){
        var formData = new FormData();
        $.each($scope.fileList, function(index, item){
            formData.append("Arquivo"+ index, item);
        });
        formData.append('NumeroChamado', $scope.atendimentoDetalhe.IdChamadoCC);
        formData.append('ObservacaoChamado', $scope.observacao);
        formData.append('Vinculo', $rootScope.numeroVinculo);
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "Atendimento/InserirObservacaoChamadoComImagens",
            data: formData,
            success: function (data) {
                ons.notification.alert({
                    title: "Enviada",
                    message: "Comunicação enviada com sucesso."
                });
                app.navigate("views/restrito/atendimento-lista.html", true);                
            }
        });
    };
});