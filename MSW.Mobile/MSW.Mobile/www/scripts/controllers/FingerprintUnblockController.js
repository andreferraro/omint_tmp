app.controller("FingerprintUnblockController", function($scope, $rootScope, FingerprintUnblockService) {
    $scope.isEnabled = FingerprintUnblockService.isEnabled();
    $scope.pageTitle = ons.platform.isIOS() ? "Habilitar TouchID" : "Habilitar Fingerprint";
    var options = window.mainpage.nav.getCurrentPage().options;
    if(options && options.params){
        $scope.refreshMenu = parseInt(options.params.split('=')[1]);
    }
    $scope.handleChange = function() {
        var isEnabled = (!$scope.isEnabled).toString();
        FingerprintUnblockService.setFingerprintUnblock(isEnabled);
        if($scope.refreshMenu && isEnabled){
            $rootScope.$broadcast("refreshMenu");
        }
    }    
});