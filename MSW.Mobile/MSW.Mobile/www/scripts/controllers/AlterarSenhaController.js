﻿app.controller("AlterarSenhaController", function ($scope, $http, $rootScope) {
    
    
    $scope.submit = function () {
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "MinhaConta/AlterarSenha",
            params: {
                senhaAtualEncrypted: app.rsaEncrypt($scope.senhaAtual),
                novaSenhaEncrypted: app.rsaEncrypt($scope.novaSenha)
            },
            success: function (data) {
                ons.notification.alert({
                    title: "Informação",
                    message: "Senha alterada com sucesso."
                });
                app.navigate("views/restrito/home.html", true);
            }
        });
    }
});