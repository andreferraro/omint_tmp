﻿app.controller("PendenciaCheckinController", function ($scope, $http, $rootScope, PendenciaCheckinService) {
    
    $scope.data = PendenciaCheckinService.getData();

    app.ajax({
        scope: $scope,
        http: $http,
        url: app.apiUrl + "Associado/Obtertelefone",
        params: {
            codigoTitulo: $rootScope.numeroVinculo,
            seq: "00"
        },
        success: function (data) {
            $scope.Telefone = data;
        }
    });

    // tentar obter a posição geográfica do cliente para enviar à API do Atendimento Sem Papel
    navigator.geolocation.getCurrentPosition(function(result){
        app.geolocationPosition.latitude = result.coords.latitude;
        app.geolocationPosition.longitude = result.coords.longitude;
        console.log("posição atual", app.geolocationPosition)
    }, function(error){
        console.log("Geolocation watchPosition error: code: " + error.code + ". message: " + error.message + ".");
        app.geolocationPosition = { latitude: 0,longitude: 0};
        console.log("geolocation position cleared after error", new Date().toString());
    });




    $scope.submitApproval = function (){
        var approval = {
            codAtendimento: $scope.data.CodAtendimento,
            statusAprovacao: 1,
            chaveAtendimento: $scope.data.ChaveAtendimento,
            telefoneBeneficiario: $scope.Telefone, //todo: pegar o telefone atual do associado
            localizacaoAssociado: {
                            latitude: app.geolocationPosition.latitude,
                            longitude: app.geolocationPosition.longitude
                        },
            codAssociado: $scope.data.CodAssociado
            
        }
        $scope.submit(approval, "approval");
    }

    $scope.submitNegation = function (){
        
        var negation = {
            codAtendimento: $scope.data.CodAtendimento,
            statusAprovacao: 2,
            chaveAtendimento: $scope.data.ChaveAtendimento,
            telefoneBeneficiario: $scope.Telefone,  //todo: pegar o telefone atual do associado
            localizacaoAssociado: {
                            latitude: app.geolocationPosition.latitude,
                            longitude: app.geolocationPosition.longitude
                        },
            codAssociado: $scope.data.CodAssociado
        } 
        $scope.submit(negation, "negation");

    }

    $scope.submit = function (data, kindOfMessage) {
        var msgSuccessfulCheckin = "Atendimento LIBERADO. Em até 24 horas enviaremos uma pesquisa de satisfação sobre esse atendimento no aplicativo."
        var msgUnsuccessfulCheckin = "Atendimento NEGADO. Em caso de dúvida, contate nossa Central de Atendimento - 0800 726 4000."
 
        var message = "";
        if (kindOfMessage === "approval"){
            message = msgSuccessfulCheckin;
        }
        if (kindOfMessage === "negation"){
            message = msgUnsuccessfulCheckin;
        }

        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "Pendencia/ResolverPendenciaCheckin",
            data: data,
            headers: {"Content-Type": "application/json"},
            success: function (data) {
                ons.notification.alert({
                    title: "Atendimento Sem Papel",
                    message: message
                });
                app.navigate("views/restrito/home.html", true);
            }
        });
    }
});