﻿app.controller("PrimeiroAcessoCadastrarEmailController", function($scope, $http, $rootScope) {
    
    $scope.info = app.primeiroAcesso.info;
    
    var vinculoTeste = app.rsaEncrypt($rootScope.vinculo.toString());

    $scope.CadastrarEmail = function () {
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "PrimeiroAcesso/CadastrarEmail",
            params: {
                email: $scope.email,
                hash: app.primeiroAcesso.hash,
                vinculoEncrypted: vinculoTeste
            },
            success: function (data) {
                app.primeiroAcesso.info = data.Data;
                app.navigate("views/primeiroacesso-enviar-confirmacao.html");
            }
        });
    }
});