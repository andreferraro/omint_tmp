﻿app.controller("RedeCredenciadaSelecaoPlanoController", function($scope, $http, $rootScope, RedeCredenciadaService) {


    $scope.planos = $rootScope.redeCredenciada.planos.PlanosSubordinados;
    
    $scope.itemClick = function (item)
    {
        RedeCredenciadaService.setPlano(item);
        app.navigate("views/restrito/redecredenciada-home.html", true);
    };
});