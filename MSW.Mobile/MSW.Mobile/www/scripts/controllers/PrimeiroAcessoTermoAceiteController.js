﻿app.controller("PrimeiroAcessoTermoAceiteController", function($scope, $http) {

    $scope.termo = "";
    $scope.aceito = false;

    app.ajax({
        scope: $scope,
        http: $http,
        url: app.apiUrl + "PrimeiroAcesso/TermoAceiteAtual",
        params: {
            device: app.getDeviceId()
        },
        success: function(data)
        {
            app.primeiroAcesso.hash = data.Data.Hash;
            $scope.termo = data.Data.Termo;
        }
    });

    $scope.aceitar = function()
    {
        app.ajax({
            scope: $scope,
            http: $http,
            url: app.apiUrl + "PrimeiroAcesso/AceitarTermo",
            params: {
                hash: app.primeiroAcesso.hash
            },
            success: function(data)
            {
                app.navigate("views/primeiroacesso-vinculo.html");
            }
        });
    };
});