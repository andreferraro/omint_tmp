﻿app.controller("CredencialEletronicaController", function ($scope, $http, $rootScope, 
    CredencialEletronicaService, CredencialOfflineService, CredencialImagemService) {

    $scope.credencial;
    $scope.credenciais = [];
    var titulo;
    CredencialEletronicaService.ObterListaCredencial($scope, $http, CredencialOfflineService)

    $scope.credencialMedicina = function (credencial) {
        //Removido rotação
        // screen.lockOrientation('landscape');
        
        if(credencial.NumeroCns.indexOf("CARTÃO NACIONAL DE SAÚDE (CNS):") === -1){
            credencial.NumeroCns = "CARTÃO NACIONAL DE SAÚDE (CNS): " + credencial.NumeroCns;
        }
        CredencialEletronicaService.setCredencial(credencial);
        app.navigate("views/restrito/credencial-eletronica-medicina.html");
    }

    $scope.credencialOdonto = function (credencial) {
        //Removido rotação
        // screen.lockOrientation('landscape');
        
        if(credencial.NumeroCns.indexOf("CARTÃO NACIONAL DE SAÚDE (CNS):") === -1){
            credencial.NumeroCns = "CARTÃO NACIONAL DE SAÚDE (CNS): " + credencial.NumeroCns;
        }
        CredencialEletronicaService.setCredencial(credencial);
        app.navigate("views/restrito/credencial-eletronica-odonto.html");
    }
});