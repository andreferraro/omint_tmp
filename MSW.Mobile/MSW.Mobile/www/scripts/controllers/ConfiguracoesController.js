﻿app.controller("ConfiguracoesController", function($scope, $http, $rootScope) {
    $scope.tituloHabilitarDigital = ons.platform.isIOS() ? "Habilitar TouchID" : "Habilitar Fingerprint";
    $scope.vincularFacebook = function()
    {
        ons.notification.confirm({
            title: "Confirmação",
            message: "Tem certeza que deseja utilizar sua conta do facebook para acessar o aplicativo?",
            callback: function(answer)
            {
                if(answer)
                {
                    facebookConnectPlugin.logout(function(response) {
                            FacebookLogin();
                        },
                        function(response) {
                            FacebookLogin();
                        });
                }
            }
        });
    };
    var FacebookLogin = function()
    {
        facebookConnectPlugin.login([], function(response) {
                app.ajax({
                    scope: $scope,
                    http: $http,
                    url: app.apiUrl + "MinhaConta/InserirAutenticacaoFacebook",
                    params: {
                        facebookEncrypted: app.rsaEncrypt(response.authResponse.userID)
                    },
                    success: function(data)
                    {
                        $rootScope.possuiAutenticadorFacebook = true;
                        ons.notification.alert({
                            title: "Informação",
                            message: "Sua conta do facebook foi vinculada com sucesso."
                        });
                        app.navigate("views/restrito/home.html", true);
                    }
                });
            },
            function(response) {
                console.log("Facebook error", response);
            });
    };
});