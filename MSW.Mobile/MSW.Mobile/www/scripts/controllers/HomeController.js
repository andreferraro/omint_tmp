﻿
app.controller("HomeController", function ($scope, $http, $interval, $q, NoticiaService, 
    $rootScope, PendenciaCheckinService, RedeCredenciadaService, BeneficiarioService, 
    CredencialEletronicaService, CredencialOfflineService) {
    
    var DAYS_TO_REMEMBER_FINGERPRINT = 7;
    $scope.noticias = [];
    $scope.pendencia;
    var interval;

    CredencialEletronicaService.ObterListaCredencial($scope, $http, CredencialOfflineService);

    if(localStorage.getItem('lastTouchIdRememberDate') != null){
        $scope.lastTouchIdRememberDate = new Date(localStorage.getItem('lastTouchIdRememberDate'));
    }

    var fingerPrintRemember = {
        isToRemember: function(){
            var defered = $q.defer();
            app.fingerprintAuth.isAvailable(
                function(){
                    fingerPrintRemember.checkDateToRemember(defered.resolve, defered.reject);
                },
                function(){
                    defered.reject();
                }
            );
            return defered.promise;
        },
        checkDateToRemember: function(isInDate, isNotInDate){
            if($scope.lastTouchIdRememberDate){
                if(!app.fingerprintAuth.isEnabled()){
                    var now = new Date();
                    var last = new Date($scope.lastTouchIdRememberDate);
                    //((now - last) >= DAYS_TO_REMEMBER_FINGERPRINT * 86400) ? isInDate() : isNotInDate();
                    ((now - last) >= 1800000) ? isInDate() : isNotInDate();//Para Homologacao deixei 30 minutos
                }else{
                    isNotInDate();
                }
            }else{
                isInDate();
            }
        }
    };
    
    function rememberTouchIdConfig(){
        localStorage.setItem('lastTouchIdRememberDate', new Date().toJSON()) 
        ons.notification.confirm({
            message: 'Deseja configurar o login com sua digital?',
            buttonLabels: ['Não', 'Sim'],
            title: 'Configurar Digital',
            callback: function(idx) {
                switch (idx) {
                    case 1:
                    app.navigate('views/restrito/fingerprint-unblock.html', false, true, 'refreshMenu=1');
                    break;
                }
            }
        });
    }

    fingerPrintRemember.isToRemember().then(function(){
        rememberTouchIdConfig();
    });
 

    var getCookiebyName = function(name){
        var pair = document.cookie.match(new RegExp(name + '=([^;]+)'));
        return !!pair ? pair[1] : null;
    };

    if(!getCookiebyName('validaCadastro')){
        verificaPendencias();  
    }

    $scope.cancelaAtualizacaoDados = function () {
        $scope.possuiPendencias = false;
    };

    $scope.redirecionaAtualizacao = function () {
        $scope.possuiPendencias = false;
        app.navigate("views/restrito/associado-atualizar-cadastro.html");
    }
    
    $scope.obterPendencias = function () {
        app.ajax({
                scope: $scope,
                http: $http,
                //method: "Post",
                url: app.apiUrl + "Pendencia/ObterPendencias",
                params: {
                    vinculo: $rootScope.numeroVinculo
                },
                showProgress: false,
                handleErrors: false,
                success: function (data) {
                    $scope.pendencia = (data[0]);
                    if (!$scope.$$phase) $scope.$apply()
                }
        });
        if (!$scope.$$phase) $scope.$apply()
    }

    $scope.noticiaDetalhe = function (noticia) {
        NoticiaService.setNoticiaDetalhe(noticia);
        app.navigate("views/restrito/noticia-detalhe.html");
    };

    $scope.buscaNomeSubmit = function () {
        RedeCredenciadaService.setBuscaNome($scope.buscaNome);
        app.navigate("views/restrito/redecredenciada-busca-nome.html");
    };

    $scope.navigateByViewTitle = function (title) {
        for (var index = 0; index < app.menu.length; index++) {
            if(app.menu[index].Label===title)
            {
                $rootScope.currentMenuItem = app.menu[index];
                app.navigate(app.menu[index].View);  
            }
        }
    };    

    $scope.navigateToPendingIssue = function (){
        PendenciaCheckinService.setData($scope.pendencia);
        app.navigate("views/restrito/pendencia-checkin.html", false, false);
    }    

    function refreshHome(){
        $scope.obterPendencias();
        ListarNoticias();
    }
    refreshHome();
    interval = $interval(refreshHome, 60 * 1000);

    $scope.$on('$destroy', function() {
        if (angular.isDefined(interval)) {
            $interval.cancel(interval);
            interval = undefined;
        }
    });

    if (app.push) {
        $scope.push = app.push;
        $scope.push.on('notification', function(data){
            console.log("notificação recebida", data);
            $scope.obterPendencias();
        });
    }

    function verificaPendencias() {
        $.ajax({
            method: "Get",
            url: app.apiUrl + "Associado/ObterBeneficiariosPendentes",
            data: { codigoTitulo: $rootScope.numeroVinculo },
            success: function (response) {
                VerificaBeneficiariosComPendencia(response);
                BeneficiarioService.setBeneficiarios(response);
                SETcookie();
            },
            error: function (response) {
                console.error(response);
                $scope.possuiPendencias = false;
            }
        });
    };

    function VerificaBeneficiariosComPendencia(beneficiarios) {
        if (beneficiarios.length > 0) {
            $scope.possuiPendencias = true;
            ExibePainelPendencia(beneficiarios);
        }
        else {
            $scope.possuiPendencias = false;
        }
    }

    function ExibePainelPendencia(beneficiarios) {
        var nomeBeneficiarios = "";

        for (var i = 0; i < beneficiarios.length; i++) {
            nomeBeneficiarios += beneficiarios[i].Pessoa.Nome + " - ";
        }
    }

    function ListarNoticias(){
        app.ajax({
            scope: $scope,
            http: $http,
            //method: "Post",
            url: app.apiUrl + "Noticia/Listar",
            params: {
                quantidade: 2,
            },
            showProgress: false,
            handleErrors: false,
            success: function (data) {
                $scope.noticias = data.Data;
            }
        });
    }

    function SETcookie(){
        var nextWeek = new Date();
        nextWeek.setDate(nextWeek.getDate() + 7);
        var expiresDate = nextWeek.toString();
        document.cookie = "validaCadastro=value; path=/; expires=" + expiresDate;
    }   
});