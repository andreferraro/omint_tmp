app.factory("fileService", function ($q) {


    function onError(e) {
        alert('Cannot found requested file');
    }

    function gotFile(fileEntry) {

    }

    var fileService = {
        convertFileListToBase64: function (fileList) {
            var deferred = $q.defer();
            promise_array = [];
            fileList.forEach(function (element) {
                //element = { name: imageName, pathEncoded: uriDecoded, pathDecoded: imageUri, ext: extension }
                var fileDeferred = $q.defer();
                window.resolveLocalFileSystemURL(element.pathEncoded,
                    function (fileEntry) {
                        fileEntry.file(function (file) {
                            var reader = new FileReader();
                            reader.onloadend = function (e) {
                                element.imgBase64 = this.result;
                                fileDeferred.resolve();
                            };
                            reader.readAsDataURL(file);
                        });
                    },
                    function (error) {
                        console.log(error);
                        fileDeferred.reject();
                    }
                );
                promise_array.push(fileDeferred.promise);
            });
            $q.all(promise_array)
                .then(function () {
                    console.log('todos os arquivos foram arquivos adicionados.');
                    deferred.resolve();
                })
                .catch(function (error) {
                    var error = 'um ou mais arquivos não foram adicionaos \n Erro:' + error;
                    deferred.reject();
                });
            return deferred.promise;
        },

        convertFileToBase64: function (filePath) {
            //element = { name: imageName, pathEncoded: uriDecoded, pathDecoded: imageUri, ext: extension }
            var defer = $q.defer();
            window.resolveLocalFileSystemURL(filePath,
                function (fileEntry) {
                    fileEntry.file(function (file) {
                        var reader = new FileReader();
                        reader.onloadend = function (e) {
                            defer.resolve(this.result);
                        };
                        reader.readAsDataURL(file);
                    });
                },
                function (error) {
                    console.log(error);
                    defer.reject();
                }
            );
            return defer.promise;
        },
        getMimeType: function (file) {
            var retorno = '';
            switch (file.extension) {
                case 'pdf':
                    retorno = 'application/pdf';
                    break;
                case 'jpg':
                case 'jpeg':
                    retorno = 'image/jpeg';
                    break;
                case 'png':
                    retorno = 'image/png';
                    break;
                case 'bmp':
                    retorno = 'image/bmp';
                    break;
                case 'tiff':
                    retorno = 'image/tiff';
                    break;
            }
            return retorno;
        },
        openFile: function (filePath, mimeType) {
            $(function () {
                cordova.plugins.fileOpener2.open(filePath, mimeType,
                    {
                        error: function (e) {
                            ons.notification.alert({
                                title: "Erro",
                                message: "Ocorreu um erro ao abrir o arquivo. Verifique se existem aplicativos que suportem o mesmo."
                            });
                            console.log("Error status: " + e.status + " - Error message: " + e.message);
                        },
                        success: function () { }
                    });
            });
        },
        getFileData: function (finalPath, originalPath) {
            var imageData = {};
            imageData.uriEncoded = originalPath;
            imageData.uriDecoded = decodeURIComponent(finalPath);
            var arrImagePath = imageData.uriDecoded.split('/');
            imageData.imageName = arrImagePath[arrImagePath.length - 1];
            var arrExtension = imageData.imageName.split('.');
            imageData.extension = arrExtension[arrExtension.length - 1];
            return imageData;
        },
        resolvePathAndroid: function (filePath, callback) {
            //usa o plugin cordova-plugin-filepath 
            window.FilePath.resolveNativePath(filePath,
                function (finalPath) {
                    callback(finalPath, filePath);
                },
                function (error) {
                    console.log(error);
                }
            );
        },
        base64ToBlob: function (b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        },
        saveBase64AsFile: function (folderpath, filename, content, contentType) {
            var defer = $q.defer();
            var DataBlob = fileService.base64ToBlob(content, contentType);
            window.requestFileSystem(window.TEMPORARY, 0, function (fs) {

                fs.root.getFile(filename, { create: true }, function (fileEntry) {
                    fileEntry.createWriter(function (fileWriter) {
                        fileWriter.onwriteend = function () {
                            defer.resolve({ path: folderpath + filename, contentType: contentType });
                        };

                        fileWriter.onerror = function (e) {
                            defer.reject('Não foi possível salvar o arquivo ' + folderpath + ' ' + e.toString());
                        };

                        fileWriter.write(DataBlob);
                    }, function (e) {
                        defer.reject('Não foi possível salvar o arquivo ' + folderpath + ' ' + e.toString());
                    });
                });
            });

            return defer.promise;
        },
        verifyAcceptedMimeType: function(mimeType){
            var types = ['application/pdf','image/jpeg','image/png','image/bmp','image/tiff'];
            return types.indexOf(mimeType) >= 0;
        }
    };

    return fileService;

});