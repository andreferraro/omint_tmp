﻿app.factory("ReembolsoService", function() {
    var reembolsoDetalhe = {};
    var reembolsoDetalheCache = {};

    var filtro = {
        ano: new Date().getUTCFullYear().toString(),
        mes: "0",
        situacao: "0",
        servico: "1"
    };

    return {
        setReembolsoDetalhe: function(reembolso)
        {
            reembolsoDetalhe = reembolso;
        },
        setReembolsoDetalheToCache: function(detalhe)
        {
            reembolsoDetalheCache["k" + detalhe.Reembolso.IdReembolso] = detalhe;
        },
        getReembolsoDetalheFromCache: function(idReembolso)
        {
            return reembolsoDetalheCache["k" + idReembolso];
        },
        getReembolsoDetalhe: function()
        {
            return reembolsoDetalhe;
        },
        setFiltro: function(novoFiltro)
        {
            filtro = novoFiltro;
        },
        getFiltro: function()
        {
            return filtro;
        }
    };

});