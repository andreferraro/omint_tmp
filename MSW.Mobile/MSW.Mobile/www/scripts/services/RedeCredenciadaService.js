﻿app.factory("RedeCredenciadaService", function() {
    var buscaNome = "";
    var buscaTipoAtendimento = undefined;
    var buscaEspecialidade = undefined;
    var buscaRegiao = undefined;
    var unidade = undefined;
    var unidadeDetalhes = undefined;
    var buscarCredenciadosInativos = false;
    var buscaProximidadeTipo = undefined;
    var plano = undefined;
    var buscarOutrasRedes = false;

    return {
        setPlano: function(newPlano)
        {
            plano = newPlano;
        },
        getPlano: function()
        {
            return plano;
        },
        setBuscarOutrasRedes: function(newBuscarOutrasRedes)
        {
            buscarOutrasRedes = newBuscarOutrasRedes;
        },
        getBuscarOutrasRedes: function()
        {
            return buscarOutrasRedes;
        },
        setBuscarCredenciadosInativos: function(newBuscarCredenciadosInativos)
        {
            buscarCredenciadosInativos = newBuscarCredenciadosInativos;
        },
        getBuscarCredenciadosInativos: function()
        {
            return buscarCredenciadosInativos;
        },
        setBuscaProximidadeTipo: function(newBuscaProximidadeTipo)
        {
            buscaProximidadeTipo = newBuscaProximidadeTipo;
        },
        getBuscaProximidadeTipo: function()
        {
            return buscaProximidadeTipo;
        },
        setBuscaRegiao: function(newBuscaRegiao)
        {
            buscaRegiao = newBuscaRegiao;
        },
        getBuscaRegiao: function()
        {
            return buscaRegiao;
        },
        setUnidadeDetalhes: function(newUnidadeDetalhes)
        {
            unidadeDetalhes = newUnidadeDetalhes;
        },
        getUnidadeDetalhes: function()
        {
            return unidadeDetalhes;
        },
        setUnidade: function(newUnidade)
        {
            unidade = newUnidade;
        },
        getUnidade: function()
        {
            return unidade;
        },
        setBuscaNome: function(newBuscaNome)
        {
            buscaNome = newBuscaNome;
        },
        getBuscaNome: function()
        {
            return buscaNome;
        },
        setBuscaEspecialidade: function(newBuscaEspecialidade)
        {
            buscaEspecialidade = newBuscaEspecialidade;
        },
        getBuscaEspecialidade: function()
        {
            return buscaEspecialidade;
        },
        setBuscaTipoAtendimento: function(newBuscaTipoAtendimento)
        {
            buscaTipoAtendimento = newBuscaTipoAtendimento;
        },
        getBuscaTipoAtendimento: function()
        {
            return buscaTipoAtendimento;
        },

    };

});