﻿app.factory("NoticiaService", function () {
    var noticiaDetalhe = {};

    return {
        setNoticiaDetalhe: function (noticia) {
            noticiaDetalhe = noticia;
        },
        getNoticiaDetalhe: function () {
            return noticiaDetalhe;
        }
    };
});