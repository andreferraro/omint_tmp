﻿app.factory("AutorizacaoService", function() {
    var autorizacaoDetalhe = {};
    var filtro = {
        ano: new Date().getUTCFullYear().toString(),
        mes: "0"
    };

    return {
        setAutorizacaoDetalhe: function(autorizacao)
        {
            autorizacaoDetalhe = autorizacao;
        },
        getAutorizacaoDetalhe: function()
        {
            return autorizacaoDetalhe;
        },
        setFiltro: function(novoFiltro)
        {
            filtro = novoFiltro;
        },
        getFiltro: function()
        {
            return filtro;
        }
    };

});