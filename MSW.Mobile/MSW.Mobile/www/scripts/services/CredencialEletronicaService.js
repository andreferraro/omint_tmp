﻿app.factory("CredencialEletronicaService", function (CredencialImagemService) {
    var _credencial = {};

    return {
        setCredencial: function (credencial) {
            _credencial = credencial;
        },
        getCredencial: function () {
            return _credencial;
        },
        ObterListaCredencial: function ($scope, $http, CredencialOfflineService) 
        {
            $scope.credenciais = []
            $scope.numeroVinculoTitular = $scope.numeroVinculo;
            app.ajax({
                scope: $scope,
                http: $http,
                url: app.apiUrl + "Associado/ObterCredencialEletronica",
                showProgress: false,
                params: {
                    codigoTitulo: $scope.numeroVinculoTitular,
                },
                showProgress: true,
                handleErrors: false,
                success: handleCredencialPrincipal
            });

            function handleCredencialPrincipal(data) 
            {
             
                $scope.credencial = data.Data;
                //todo: Transformar em promise
                app.ajax({
                    scope: $scope,
                    http: $http,
                    showProgress: false,
                    url: app.apiUrl + "Associado/ListaDadosPlanoV2",
                    params: {
                        // vinculo: "21021456"
                        vinculo: $scope.numeroVinculo
                        //com dependentes = 21021456
                    },
                    success: handleDependentes
                });
            }

            function handleDependentes(data) 
            {
                var credenciaisBeneficiarios = data.Data.Beneficiarios.map(function(beneficiario)
                {
                  
                    return {
                        NomeBeneficiario : beneficiario.Nome,
                        NumeroCredenciaMedicina : beneficiario.NumeroCredencialMedica,
                        CodigoCredencialMedicina : $scope.credencial.CodigoCredencialMedicina,
                        CodigoPlanoMedicina : beneficiario.CodigoPlanoMedico,
                        NumeroCredenciaOdonto : beneficiario.NumeroCredencialOdonto,
                        CodigoCredencialOdonto : $scope.credencial.CodigoCredencialOdonto,
                        CodigoPlanoOdonto : beneficiario.CodigoPlanoOdontologico,
                        DataIngresso: beneficiario.DataIngresso,
                        CaminhoImagemCredenciaMedicina: $scope.credencial.CaminhoImagemCredenciaMedicina,
                        CaminhoImagemCredenciaOdonto: $scope.credencial.CaminhoImagemCredenciaOdonto,
                        DataFimCredencialMedicina: beneficiario.DataFimCredencialMedica,
                        DataFimCredencialOdonto: beneficiario.DataFimCredencialOdonto,
                        NumeroCns: beneficiario.NumeroCns
                    }
                })
                $scope.credenciais = $scope.credenciais.concat(credenciaisBeneficiarios);
                CredencialOfflineService.limparBanco().then(function()
                {
                    CredencialImagemService.urlToBase64($scope.credencial.CaminhoImagemCredenciaMedicina, 
                        function(imagemMedicina)
                        {
                            CredencialImagemService.urlToBase64($scope.credencial.CaminhoImagemCredenciaOdonto, 
                            function(imagemOdonto)
                            {
                         
                                $scope.credenciais.forEach(function(credencial)
                                {
                                    CredencialOfflineService.gravar(credencial, imagemMedicina, imagemOdonto)
                                    console.log("Credencial salva")
                                    
                                })
                            })
                        })
                })
            }
        }
    };

});