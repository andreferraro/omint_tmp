app.factory("FingerprintUnblockService", function() {
    return {
        // Check if the user has enabled the fingerprint unblock option.
        isEnabled: function () {
            return window.localStorage.getItem('fingerprintUnblock') == "true";
        },
        // Set fingerprintUnblock storage variable.
        setFingerprintUnblock: function (isEnabled) {
            window.localStorage.setItem('fingerprintUnblock', isEnabled);
        }
    };
});