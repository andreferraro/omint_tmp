app.factory("MenuArquivosService", function($q, $rootScope, $http, $rootScope) {
    var urlKit = 'http://omtseg-prod.omintseguros.com.br/KitEletronico/DownloadKit'
    return {
        obterDocumentos: ObterDocumentos
    };

    function ObterDocumentos(numeroVinculo){
        var deferred = $q.defer();
        $http.get(urlKit + "?id="+ numeroVinculo)
        // $http.get("http://omtseg-prod.omintseguros.com.br/KitEletronico/DownloadKit?id=24258527")
        .then(function(result)
        {
            var listaDocumentoAgrupado = result.data.map(function(grupo)
            {
                grupo.itemCollection = grupo.itemCollection.map(function(item)
                {
                    if(item.IdDocumento)
                        item.FullUrl = urlKit + "/DownloadGed?idDocumento=" + item.IdDocumento;
                    else
                        item.FullUrl = "http://omint01.omint.com.br/" + item.Url;
                        
                    return item;
                })
                return grupo;
            });
            deferred.resolve(listaDocumentoAgrupado);
        }, 
        function(result)
        {                
            deferred.reject(result);
        });
        return deferred.promise;
    }   
});