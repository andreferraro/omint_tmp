﻿app.factory("AtendimentoService", function($http, fileService, $q) {
    var atendimentoDetalhe = {};
    var idNovoChamado = null;
    var demandas = {};
    var filtro = {
        situacao: undefined,
        idChamado: undefined,
        idClassifica: undefined,
        idTipoDemanda: undefined
    };

    return {
        setAtendimentoDetalhe: function(atendimento)
        {
            atendimentoDetalhe = atendimento;
        },
        getAtendimentoDetalhe: function()
        {
            return atendimentoDetalhe;
        },
        setDemandas: function(novasDemandas)
        {
            demandas = novasDemandas;
        },
        getDemandas: function()
        {
            return demandas;
        },
        setFiltro: function(novoFiltro)
        {
            filtro = novoFiltro;
        },
        getFiltro: function()
        {
            return filtro;
        },
        setIdNovoChamado: function(idChamado){
            idNovoChamado = idChamado;
        },
        getIdNovoChamado: function(){
            return idNovoChamado;
        },
        adicionarArquivosAoChamado: function(imageList, idChamado, $scope){
            var liPromise = [];
            imageList.forEach(function (arquivo) {
                var defer = $q.defer();
                fileService.convertFileToBase64(arquivo.uriEncoded).then(
                    function (base64string) {
                        arquivo.base64 = base64string;                        
                        arquivo.percentComplete = 0;
                        arquivo.sending = true;
                        app.ajax({
                            scope: $scope,
                            http: $http,
                            modalLoading: false,
                            uploadEventHandlers: {
                                progress: function (evt) {
                                    console.log(evt);
                                    if (evt.lengthComputable) {
                                        var percentComplete = evt.loaded / evt.total;
                                        percentComplete = parseInt(percentComplete * 100);
                                        arquivo.percentComplete = percentComplete;
                                    }
                                }
                            },
                            url: app.apiUrl + "Atendimento/AdicionarArquivoChamado",
                            data: {
                                IdChamado: idChamado,
                                Extensao: arquivo.extension,
                                Base64File: arquivo.base64
                            },
                            headers: {
                                "Content-Type": "application/json"
                            },
                            success: function (data) {
                                arquivo.sent = true;
                                arquivo.sending = false;
                                arquivo.id_documento = data.Data;
                                arquivo.tp_arquivo = arquivo.extension;
                                defer.resolve();
                            }
                        });                        
                    },
                    function () {
                        defer.reject();
                    }
                );
                liPromise.push(defer.promise);
            });
            return liPromise;
        }
    };

});

 app.factory('focus', function($timeout, $window) {
    return function(id) {

      $timeout(function() {
        var element = $window.document.getElementById(id);
        if(element)
          element.focus();
      });
    };
  });