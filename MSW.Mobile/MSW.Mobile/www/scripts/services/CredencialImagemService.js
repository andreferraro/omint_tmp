app.factory("CredencialImagemService", function($filter)
{
    var userDataImage;
    var userCardimage;
    var ctx;
    var callback;
    var canvas;
    var imagemCredencial;
    var credencial;

    function getCredencialImagem(divCredencial, imagem, credencialCallback, credencialInfo)
    {
        // html2canvas($("#credencial-eletronica"), { onrendered: onRenderCanvas });
        imagemCredencial = imagem;
        callback = credencialCallback;
        credencial = credencialInfo;

        html2canvas(divCredencial, { onrendered: onRenderCanvas });
    }

    function onRenderCanvas(credencialCanvas)
    {
        canvas = credencialCanvas;
        userDataImage = new Image();

        userDataImage.src = canvas.toDataURL();

        userDataImage.onload = loadUserData;
    }

    function loadUserData()
    {
        ctx = canvas.getContext("2d");
        userCardimage = new Image();

        userCardimage.onload = sobreporImagens;
        userCardimage.setAttribute('crossOrigin', 'anonymous');
        userCardimage.src = imagemCredencial;
    }

    function urlToBase64(src, callback) 
    {
        if(!src) return callback("data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==")
        var img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = function() {
            var canvas = document.createElement('CANVAS');
            var ctx = canvas.getContext('2d');
            var dataURL;
            canvas.height = this.height;
            canvas.width = this.width;
            ctx.drawImage(this, 0, 0);
            dataURL = canvas.toDataURL();
            callback(dataURL);
        };
        img.src = src;
        if (img.complete || img.complete === undefined) {
            img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
            img.src = src;
        }
    }

    function sobreporImagens() 
    {
        ctx.drawImage(userCardimage, 0, 0, userCardimage.width, 
        userCardimage.height, 0, 0, canvas.width, canvas.height);
        
        if(credencial.tipo === "medicina"){
            ctx.fillStyle = "#876e3d";
            ctx.font = "bold 22px Roboto";
            ctx.fillText(credencial.NumeroCredenciaMedicina, 35, 180);
        
            ctx.font = "bold 16px Roboto";
            
            var dtValida = moment(credencial.DataFimCredencialMedicina, "DD/MM/YYYY", true).isValid();
            var dtValidaISO = moment(credencial.DataFimCredencialMedicina, moment.ISO_8601, true).isValid();

            var dtFimCredencialMedicina;

            if(dtValida || dtValidaISO){
                dtFimCredencialMedicinaFormatada = (dtValida)? moment(credencial.DataFimCredencialMedicina, "DD/MM/YYYY", true).format("DD/MM/YYYY") : moment(credencial.DataFimCredencialMedicina, moment.ISO_8601, true).format("DD/MM/YYYY");
            }
            
            ctx.fillText(dtFimCredencialMedicinaFormatada, 292, 254);
            ctx.fillText(credencial.CodigoPlanoMedicina, 448, 254);
            
        }else if(credencial.tipo === "odontologico"){
            ctx.fillStyle = "#041534";
            ctx.font = "bold 22px Roboto";
            
            ctx.fillText(credencial.NumeroCredenciaOdonto, 35, 180);

            ctx.font = "bold 16px Roboto";
            
            var dtFimCredencialOdontoFormatada;
            
            var dtValida = moment(credencial.DataFimCredencialOdonto, "DD/MM/YYYY", true).isValid();
            var dtValidaISO = moment(credencial.DataFimCredencialOdonto, moment.ISO_8601, true).isValid();

            if(dtValida || dtValidaISO){
                dtFimCredencialOdontoFormatada = (dtValida)? moment(credencial.DataFimCredencialOdonto, "DD/MM/YYYY", true).format("DD/MM/YYYY") : moment(credencial.DataFimCredencialOdonto, moment.ISO_8601, true).format("DD/MM/YYYY");
            }
            
            ctx.fillText(dtFimCredencialOdontoFormatada, 292, 254);

            ctx.fillText(credencial.CodigoPlanoOdonto, 448, 254);
        }
        
        ctx.font = "16px Roboto";
        ctx.fillText(credencial.NomeBeneficiario, 35, 218);

        ctx.font = "14px Roboto";
        ctx.fillText("INGRESSO", 35, 254);

        ctx.fillText("VALIDADE", 219, 254);

        ctx.fillText("PLANO", 400, 254);

        ctx.font = "bold 16px Roboto";

        var dtValida = moment(credencial.DataIngresso, "DD/MM/YYYY", true).isValid();
        var dtValidaISO = moment(credencial.DataIngresso, moment.ISO_8601, true).isValid();

        var dtIngressoFormatada;

        if(dtValida || dtValidaISO){
            dtIngressoFormatada = (dtValida)? moment(credencial.DataIngresso, "DD/MM/YYYY", true).format("DD/MM/YYYY") : moment(credencial.DataIngresso, moment.ISO_8601, true).format("DD/MM/YYYY");
        }
        
        ctx.fillText(dtIngressoFormatada, 110, 254);
        
        ctx.font = "10px Roboto";
        ctx.fillText(credencial.NumeroCns, 35, 295);

        var dataURL = canvas.toDataURL();

        callback(dataURL);
      
    }
    return { get: getCredencialImagem, urlToBase64: urlToBase64};
})