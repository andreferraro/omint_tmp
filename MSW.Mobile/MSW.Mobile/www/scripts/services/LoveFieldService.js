app.factory("LoveFieldService", function()
{
    var databaseVersion = 6;
    var databaseBuilder = lf.schema.create('minhaOmintDB', databaseVersion);


    databaseBuilder.createTable("credencial")
        .addColumn("id", lf.Type.INTEGER)
        .addColumn("NomeBeneficiario", lf.Type.STRING)
        .addColumn("NumeroCredenciaMedicina", lf.Type.STRING)
        .addColumn("CodigoCredencialMedicina", lf.Type.STRING)
        .addColumn("CodigoPlanoMedicina", lf.Type.STRING)
        .addColumn("NumeroCredenciaOdonto", lf.Type.STRING)
        .addColumn("CodigoCredencialOdonto", lf.Type.STRING)
        .addColumn("CodigoPlanoOdonto", lf.Type.STRING)
        .addColumn("DataIngresso", lf.Type.STRING)    
        .addColumn("CaminhoImagemCredenciaMedicina", lf.Type.STRING)
        .addColumn("CaminhoImagemCredenciaOdonto", lf.Type.STRING)
        .addColumn("DataFimCredencialMedicina", lf.Type.STRING)
        .addColumn("DataFimCredencialOdonto", lf.Type.STRING)
        .addColumn("NumeroCns", lf.Type.STRING)
        .addNullable(['NumeroCredenciaMedicina','CodigoCredencialMedicina', 
            'CodigoPlanoMedicina', 'NumeroCredenciaOdonto', 'CodigoCredencialOdonto', 
            'CodigoPlanoOdonto', 'CaminhoImagemCredenciaMedicina', 'CaminhoImagemCredenciaOdonto', 
            'DataFimCredencialMedicina', 'DataFimCredencialOdonto'])
        .addPrimaryKey(['id']);
    return databaseBuilder.connect();
})