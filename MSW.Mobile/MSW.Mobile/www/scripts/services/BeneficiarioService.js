app.factory("BeneficiarioService", function () {
    var beneficiarios = {};

    return {
        setBeneficiarios: function (beneficiariosPlano) {
            beneficiarios = beneficiariosPlano;
        },
        getBeneficiarios: function () {
            return beneficiarios;
        }
    };
});