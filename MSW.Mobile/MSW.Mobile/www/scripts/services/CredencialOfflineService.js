app.factory("CredencialOfflineService", function(LoveFieldService) 
{
    function limparBanco()
    {
        return LoveFieldService.then(function(database)
        {
            var tabelaCredencial = database.getSchema().table("credencial");
            return database.delete().from(tabelaCredencial).exec()
        })

    }

    function gravarCredencialGenerica(credencial)
    {
        return LoveFieldService.then(function(database)
        {
            var tabelaCredencial = database.getSchema().table("credencial");
            var credencialDTO = tabelaCredencial.createRow(credencial);
            return database.insertOrReplace()
                .into(tabelaCredencial)
                .values([credencialDTO])
                .exec();
        })
    }

    function gravar(credencial, imagemMedicina, imagemOdonto)
    {
        credencial.id = parseInt(credencial.NumeroCredenciaMedicina.replace(/ /g, ""));
        credencial.CaminhoImagemCredenciaMedicina = imagemMedicina;
        credencial.CaminhoImagemCredenciaOdonto = imagemOdonto;
        return gravarCredencialGenerica(credencial);
    }

    function listar()
    {
        return LoveFieldService.then(function(database)
        {
            var tabelaCredencial = database.getSchema().table("credencial");
            return database.select().from(tabelaCredencial).exec();
        })
    }

    return {
        gravar: gravar,
        listar: listar,
        limparBanco: limparBanco
    }
});