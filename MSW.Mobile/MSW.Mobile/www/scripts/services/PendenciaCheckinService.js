﻿app.factory("PendenciaCheckinService", function () {
    var data = {};

    return {
        setData: function (dataToSet) {
            data = dataToSet;
            console.log("pendencia incluída:", data);
        },
        getData: function () {
            console.log("retornando pendência:", data);
            return data;
        }
    };
});