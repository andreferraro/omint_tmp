app.directive('afterRender', function ($timeout) 
{
    return {
        restrict: 'A',
        terminal: true,
        transclude: false,
        link: function (scope, element, attrs) {
            console.log(attrs.afterRender)
            $timeout(scope.$eval(attrs.afterRender), 0);  
        }
    };
});