﻿"use strict";
var app = ons.bootstrap("minhaOmint", ["ngSanitize"]);


String.prototype.capitalize = function () {
    return this.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0)[txt.length > 2 ? "toUpperCase" : "toLowerCase"]() + txt.substr(1).toLowerCase();
    });
};

String.prototype.formatCep = function () {
    return this.replace(/(\d{5})(\d{3})/, "$1-$2");
};


app.getObjectSize = function (obj) {
    return Object.keys(obj).length;
};
app.navigate = function (page, resetStack, closeMenu, navParams) {
    resetStack = resetStack === true;
    closeMenu = closeMenu !== false;

    setTimeout(function () {
        if (window.mainpage.nav.getCurrentPage().name !== page) {
            window.mainpage.nav[resetStack ? "resetToPage" : "pushPage"](page, { animation: "navCarousel", params: navParams });
        }
        if (closeMenu) {
            window.menu.closeMenu();
        }
    }, 40);
};

app.return = function () {
    window.mainpage.nav.popPage();
};

app.enableMenu = function (enable) {
    window.menu.setSwipeable(enable !== false);
};

app.notificarCamposObrigatorios = function (campos) {
    var required = "";
    for (var k in campos) {
        if (!campos[k].valor) {
            required += (required ? ", " : "") + campos[k].campo;
        }
    }

    if (required) {
        ons.notification.alert({
            title: "Erro",
            message: "Preenchimento obrigatório: \r\n" + required
        });
        return true;
    }
    return false;
};

// Status 500
app.handleHttpError = function (data) {
    console.log("handleHttpError", data);
    ons.notification.alert({
        title: "Erro",
        message: data && data.Error
            ? data.Error
            : "Verifique sua conexão com a Internet e tente novamente."
    });
};

// Status 400
app.handleOAuthError = function (data) {
    console.log("handleOAuthError", data);
    ons.notification.alert({
        title: data.error,
        message: data.error_description
            ? data.error_description
            : "Ocorreu um erro de autenticação com o servidor." + JSON.stringify(data)
    });
};

app.fingerprintAuth = {
    // Check if the user has enabled the fingerprint unblock option.
    isEnabled: function () {
        return window.localStorage.getItem('fingerprintUnblock') == "true";
    },
    // Set fingerprintUnblock storage variable.
    setFingerprintUnblock: function (isEnabled) {
        window.localStorage.setItem('fingerprintUnblock', isEnabled);
    },
    // Check if user has fingerprint authentication configured.
    isAvailable: function (isAvailableCallback, isNotAvailableCallback) {
        if (typeof window.Fingerprint != 'undefined') {
            window.Fingerprint.isAvailable(isAvailableCallback, isNotAvailableCallback);
        } else {
            isNotAvailableCallback();
        }
    },
    // Show Fingerprint validation component.
    validateIdentity: function (successCallback, errorCallback) {
        window.Fingerprint.show({
            clientId: "Minha Omint",
            clientSecret: "password" //Only necessary for Android
        }, successCallback, errorCallback);
    }
};

// Status 401
app.handleOAuthExpiredError = function () {
    if(app.fingerprintAuth.isEnabled() && !app.fingerPrintAuthenticated){
        app.fingerprintAuth.isAvailable(app.redirectToTokenRenewal, app.redirectToLogin);
    }else{
        ons.notification.alert({ title: "Erro", message: "Acesso Expirado" });
        app.redirectToLogin();
    }
};

// Delete login data and redirect to login.
app.redirectToLogin = function (realizarAutenticacaoNovamente) {
    window.localStorage.credentials = "";
    window.localStorage.loginData = "";
    app.fingerPrintAuthenticated = realizarAutenticacaoNovamente;
    app.navigate("views/login.html", true);
};

// Redirects to token renewal controller.
app.redirectToTokenRenewal = function () {
    app.fingerPrintAuthenticated = true;
    app.navigate("views/token-renewal.html", true);
};

app.getDeviceId = function () {
    if (typeof window.device === "undefined" || !window.device.uuid) {
        return "UUID";
    }
    return window.device.uuid;
};

app.ajax = function (options) {
    if (options.showProgress !== false) {
        options.scope.$root.ajaxLoading = true;
    }
    if (options.modalLoading === true) {
        options.scope.$root.ajaxLoadingModal = true;
    }

    var headers = options.headers || {};
    if (window.localStorage.loginData) {
        headers.Authorization = "Bearer " + JSON.parse(window.localStorage.loginData).access_token;
    }
    //options.data.idAtendimento = $rootScope.idAtendimento;
    options.http({
        method: [options.method || "POST"],
        url: options.url,
        params: options.params,
        data: options.data,
        headers: headers,
        uploadEventHandlers: options.uploadEventHandlers
    })
        .then(function (data) {
            if (options.showProgress !== false)
                options.scope.$root.ajaxLoading = false;

            if (options.modalLoading === true)
                options.scope.$root.ajaxLoadingModal = false;

                options.success(data.data);
        }, function (data) {
            if (options.handleErrors !== false) {
                switch (data.status) {
                    case 400:
                        app.handleOAuthError(data.data);
                        break;
                    case 401:
                        app.handleOAuthExpiredError(options.scope);
                        break;
                    default:
                        app.handleHttpError(data.data);
                }
            }
            if (options.showProgress !== false)
                options.scope.$root.ajaxLoading = false;

            if (options.modalLoading === true)
                options.scope.$root.ajaxLoadingModal = false;

            options.failure && options.failure(data.data, data.status);
            if(options.onError)
                options.onError(data);
        });
};

app.rsaEncrypt = function (text) {
    var rsaPublicKey = "-----BEGIN PUBLIC KEY-----\r\n" +
        "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqIjVCznUZ03ONWZATrY0\r\n" +
        "xKUygjJuZGe63dx6uugNf2qFN50B4P7pE5RrP1YvR4DqW+7D+pvL6uWD14X5Od6r\r\n" +
        "Cgr02P2ds5cKfjxSr0JZr2nEu4EqozaZtD29vTpUvKZb5jfCWXK2gzr8iwAHgqu7\r\n" +
        "K6CG0XRSmWuqhab391R23/V/tPbK1+89neE+beJJYnOgXUOMqL5pDOiRYpFJwN/v\r\n" +
        "QI3PX7UQG+XWzh8vZVUz8qlkO/YBMhbiJtT0xuyi885XimL//98QSPPmIVD7MDoy\r\n" +
        "ENXlgeQzSWo4ZYrFoDr7xpawy9jyIZnPcQJA7NR0xiyMetpIBWqss91zAyBPn75E\r\n" +
        "DQIDAQAB\r\n" +
        "-----END PUBLIC KEY-----";

    var encrypt = new JSEncrypt();
    encrypt.setPublicKey(rsaPublicKey);
    return encrypt.encrypt(text);
};

app.primeiroAcesso = { hash: "" };
app.recuperarSenha = { email: "" };
app.offline = false;
app.geolocationPosition = {};

var clearGeolocationPosition = _(function () {
    app.geolocationPosition = {
        latitude: 0,
        longitude: 0
    };
    console.log("geolocation position cleared", new Date().toString());
}).debounce(120000);

app.apiUrl = "http://omtseg-homolog.omintseguros.com.br:2000/api-dev/api/";
//app.apiUrl = "http://omtseg-homolog.omintseguros.com.br:2000/API_273/api/";
//app.apiUrl = "http://omtseg-prod.omintseguros.com.br/MSW/API_273/api/";
//app.apiUrl = "http://192.168.41.126/API/api/";


app.menu = null;

var appScreenSize;

app.unlockScreenOrientation = function () {
    if (window.cordova && window.device) {
        screen.unlockOrientation();
    }
};

document.addEventListener("deviceready", function () {

    //if (window.cordova && window.device && window.device.isVirtual !== undefined) {
    //    if (cordova.platformId === "android") {
    //        StatusBar.backgroundColorByHexString("#272A26");
    //    }

    //    app.apiUrl = "http://omtseg-homolog.omintseguros.com.br:2000/API_AppSaude_270607/api/";

    //    console.log("Current API URL", app.apiUrl);
    //}

    document.addEventListener("backbutton", function (e) {
        if (window.cordova && window.device) {
            screen.unlockOrientation();
        }
    }, false);

    navigator.geolocation.watchPosition(function (position) {
        app.geolocationPosition = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        };

        clearGeolocationPosition();
    }, function (error) {
        console.log("Geolocation watchPosition error: code: " + error.code + ". message: " + error.message + ".");
    }, {
            timeout: 30000,
            enableHighAccuracy: true,
            maximumAge: 30000
        });

    window.plugins.screensize.get(successCallback, errorCallback);

    function successCallback(result) {
        appScreenSize = result;
    }

    function errorCallback(result) {
        appScreenSize = result;
    }
}, false);



app.registerDevice = function (nrVinculo, s, h, $rootScope, ReembolsoService) {
    app.nrVinculo = nrVinculo;
    var push;
    try {
        push = PushNotification.init({
            android: {
                senderID: "1097192347158"
            },
            ios: {
                alert: "true",
                badge: "true",
                sound: "true"
            },
            windows: {}
        });
        console.log("register device", push);
        app.push = push;
    } catch (error) {
        console.log("register device error", error);
        return;
    }

    push.on('registration', function (data) {

        console.log(data.registrationId);

        var d = "idOperacaoPerfilOmint=1" +
            "&nrVinculo=" + nrVinculo +
            "&deviceId=" + data.registrationId +
            "&sistemaOperacional=" + device.platform +
            "&versaoSistemaOperacional=" + device.version;

        var url = app.apiUrl + "/PushNotification/RegistrarDevice?" + d;

        app.ajax({
            scope: s,
            http: h,
            url: url,
            headers: { 'Content-Type': "application/x-www-form-urlencoded" },
            success: function (data) {
                console.log("done");
            }
        });
    });

    var clearAllNotifications = function () {
        push.clearAllNotifications(function () {
            console.log('success');
        }, function () {
            console.log('error');
        });
    };

    push.on('notification', function (info) {
        push.getApplicationIconBadgeNumber(
            function(badgeNumber) {
                badgeNumber = (badgeNumber || 0) + 1;
                push.setApplicationIconBadgeNumber(
                    function() {
                        console.log("numero atualizado no icone.")
                    }, 
                    function() {
                        console.log("erro ao atualizar o numero no icone.")
                    }, 
                    badgeNumber
                );
            }, 
            function() {
                console.log("erro ao obter o numero do icone do sistema")
            }
        );
        ons.notification.alert({
            title: "Minha Omint",
            message: info.message,
            callback: function () {

                clearAllNotifications();

                var pushData = info.additionalData.data || info.additionalData;
                if(!pushData || !pushData.Vinculo) 
                    return app.navigate("views/restrito/home.html");
                
                if (!$rootScope.atendimentoDetalhe) 
                    $rootScope.atendimentoDetalhe = {};

                $rootScope.atendimentoDetalhe.IdChamado = pushData.Referencia; 
                $rootScope.pushData = pushData;
                app.navigate("views/restrito/" + pushData.url_pagina, true);      
            }
        });

    });


    push.on('error', function (e) {
        ons.notification.alert({
            title: "Minha Omint",
            message: e
        });
    });

    clearAllNotifications();
}

app.config([
    "$compileProvider",
    function ($compileProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|geo|maps|tel|file):/);
    }
]);
app.run(function ($rootScope, NavigatorView, navCarousel) {
    $rootScope._ = _;
    NavigatorView.registerTransitionAnimator("navCarousel", new navCarousel);
});