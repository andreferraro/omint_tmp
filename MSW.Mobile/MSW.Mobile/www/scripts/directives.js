﻿app.directive("customToolbar", function() {
    return {
        restrict: "E",
        transclude: true,
        templateUrl: "views/directives/custom-toolbar.html",
        link: function(scope, elem, attr)
        {
            scope.backButton = attr.backButton !== undefined;
            scope.menuButton = attr.menuButton !== undefined;
            scope.hideHomeButton = attr.hideHomeButton !== undefined;
            scope.title = attr.title;

            scope.$parent.setMenuPermissions = function()
            {
                app.enableMenu(scope.menuButton);
            };
            app.enableMenu(scope.menuButton);
        }
    };
});

app.directive("pushView", function() {
    return {
        restrict: "A",
        replace: "false",
        compile: function(elem, attrs)
        {
            var navparams = attrs.pushViewNavparams;
            elem.on("click", function() {
                app.navigate("views/" + attrs.pushView + ".html", null, null, navparams);
            });
        }
    };
});

app.directive("bindParentTop", function() {
    return {
        restrict: "A",
        link: function(scope, elem)
        {
            $(elem).css("top", "100%");
            var interval = setInterval(function() {
                var parentTop = $(elem).parent().position().top;

                if(parentTop !== 0)
                {
                    if($(elem).position().top === parentTop)
                    {
                        clearInterval(interval);
                        return;
                    }
                    $(elem).css("top", parentTop + "px");
                }

            }, 20);
        }
    };
});

app.directive("bindSelectorBottom", function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs)
        {
            var anchor = $(elem).offsetParent().find(attrs.bindSelectorBottom);
            if(elem.length !== 1)
            {
                console.log("Invalid selector", attrs.bindSelectorBottom);
                return;
            }

            var interval = setInterval(function() {
                var anchorTop = anchor.position().top;

                var newBottom = $(elem).offsetParent().height() - anchorTop;

                if(anchorTop !== 0)
                {
                    if($(elem).css("bottom") === newBottom + "px")
                    {
                        clearInterval(interval);
                        return;
                    }

                    $(elem).css("bottom", newBottom + "px");
                }

            }, 20);
        }
    };
});

app.directive("mapLink", function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs)
        {
            if(window.device && attrs.latitude && attrs.longitude)
            {
                var query = attrs.mapLink || "Localização";
                $(elem).attr("target", "_system");

                if(window.device.platform === "iOS")
                {
                    $(elem).attr("href", "maps://?ll=" + attrs.latitude + "," + attrs.longitude + "&q=" + query);
                }
                else if(window.device.platform === "Android")
                {
                    $(elem)
                        .attr("href",
                            "geo:0,0?q=" + attrs.latitude + "," + attrs.longitude + "(" + query + ")&z=14");
                }
            }
        }
    };
});

app.directive("steps", function() {
    return {
        restrict: "E",
        template: "",
        link: function(scope, elem, attrs)
        {
            for(var i = 0; i < attrs.count; i++)
            {
                var span = $("<span />");
                if(attrs.position === (i + 1).toString())
                {
                    span.addClass("current");
                }
                span.html("•");

                span.appendTo(elem);
            }
        }
    };
});

app.directive("checkbox", function() {
    return {
        restrict: "E",
        transclude: true,
        scope: {
            ngModel: "=attrModel",
            ngDisabled: "=attrDisabled",
            ngChange: "=attrChange"
        },
        template: '<label class="switch">\
                <input ng-model="ngModel" ng-change="ngChange()" ng-disabled="ngDisabled" class="switch__input" type="checkbox">\
                <div class="switch__toggle"></div>\
            </label> <ng-transclude/>'
    };
});

app.directive("radio", function() {
    return {
        restrict: "E",
        transclude: true,
        scope: {
            ngModel: "=attrModel",
            ngValue: "=attrValue"
        },
        template: '<label class="radio-button">\
                        <input ng-model="ngModel" type="radio" ng-value="ngValue">\
                        <div class="radio-button__checkmark"></div>\
                        <ng-transclude/>\
                    </label>'
    };
});

app.directive("mapButton", function() {
    return {
        restrict: "E",
        template: '<ons-icon icon="fa-map-marker"></ons-icon>Ver Mapa'
    };
});

app.directive("titledBox", function() {
    return {
        restrict: "E",
        transclude: true,
        scope: {
            title: "@",
            narrowPx: "@",
            narrowPct: "@"
        },
        link: function(scope, element, attrs, ctrl, transclude)
        {
            transclude(scope.$parent, function(clone, scope) {
                element.find("placeholder").replaceWith(clone);
            });
        },
        template: '<div>{{title}}</div>\
    <div ng-class="{\'narrow-px\': narrowPx !== undefined, \'narrow-pct\': narrowPct !== undefined}">\
        <placeholder/>\
    </div>'
    };
});


app.directive("titledTextBlock", function() {
    return {
        restrict: "E",
        transclude: true,
        scope: {
            title: "@"
        },
        link: function(scope, element, attrs, ctrl, transclude)
        {
            transclude(scope.$parent, function(clone, scope) {
                element.find("placeholder").replaceWith(clone);
            });
        },
        template: '<div class="title">{{title}}</div>\
        <div class="text-block"><placeholder /></div>'
    };
});

app.directive("systemOpenClick", function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs)
        {
            $(elem).click(function() {
                window.open(attrs.systemOpenClick, "_system");
            });
        }
    };
});

app.directive("loadingText", function() {
    return {
        restrict: "E",
        template: "<div>Aguarde...</div>",
        compile: function(elem, attrs)
        {
            $(elem).children("div").attr("ng-show", "ajaxLoading" + (attrs.ngShow ? (" && " + attrs.ngShow) : ""));
        }
    };
});

app.directive("noItemsText", function() {
    return {
        restrict: "E",
        transclude: true,
        template: "<div>Nenhum registro encontrado.</div>",
        compile: function(elem, attrs)
        {
            $(elem).children("div").attr("ng-show", "!ajaxLoading" + (attrs.ngShow ? (" && " + attrs.ngShow) : ""));

            return function link(scope, elem, attrs, ctrl, transclude)
            {
                transclude(scope, function(clone) {
                    if(clone.length)
                    {
                        $(elem).children("div").html(clone);
                    }
                });
            };
        }
    };
});

app.directive('imageList', function () 
{
    return {
        restrict: 'E',
        scope: {
            showAddButton : '=',
            imageList: '=',
            isUploading: '='
        },
        templateUrl: 'views/directives/image-list.html',
        link: function (scope, element, attrs) {

        },
        controller: function($scope, $timeout, fileService){
            if(!$scope.imageList)
                $scope.imageList = [];

            $scope.selectDocumentOrigin = function(){
                ons.createDialog('dialog-document-origin.html',  {parentScope: $scope}).then(function(dialog) {
                    dialog.show();
                });
            };
            
            $scope.deleteImage = function(image){
                var index = $scope.imageList.indexOf(image);
                $scope.imageList.splice(index, 1);
            };

            $scope.selectFromFileSystem = function(){
                $(function(){
                    if(window.cordova && window.device.platform === "Android"){
                        fileChooser.open(addFile);       
                    }else if(window.cordova && window.device.platform === "iOS"){
                        //https://github.com/jcesarmobile/FilePicker-Phonegap-iOS-Plugin
                        FilePicker.pickFile(
                            addFile,
                            handleError,
                            ["public.data", "public.image", "public.jpeg", "public.png", "public.composite-content "]
                        );
                    }
                });
            };
            $scope.takeFromCamera = function(){
                var srcType = Camera.PictureSourceType.CAMERA;
                var options = setOptions(srcType);
                navigator.camera.getPicture(
                    addFile, 
                    handleError,
                    options
                );
            };

            $scope.viewFile = function(file){
                fileService.openFile(file.uriDecoded, fileService.getMimeType(file));
            };

            $scope.suportedExtensions = function(){
                ons.createDialog('dialog-document-extension.html',  {parentScope: $scope}).then(function(dialog) {
                    dialog.show();
                });
            };

            function setOptions(srcType) {
                var options = {
                    // Some common settings are 20, 50, and 100
                    quality: 50,
                    destinationType: Camera.DestinationType.FILE_URI,
                    // In this app, dynamically set the picture source, Camera or photo gallery
                    sourceType: srcType,
                    encodingType: Camera.EncodingType.JPEG,
                    mediaType: Camera.MediaType.PICTURE,
                    allowEdit: false,
                    correctOrientation: true  //Corrects Android orientation quirks
                }
                return options;
            }

            function addFile(fileUri){
                resolveLocalFileSystemURL(fileUri, function(entry){
                    var nativePath = entry.toURL();
                    if(window.cordova && window.device.platform === "Android"){
                        fileService.resolvePathAndroid(nativePath, addFileToList);     
                    }else if(window.cordova && window.device.platform === "iOS"){
                        addFileToList(filePath, filePath);
                    }                    
                }); 
            }
            function handleError(error){
                console.log(error);
            }
            
            function addFileToList(finalPath, originalPath){
                var imageData = fileService.getFileData(finalPath, originalPath);
                if(fileService.getMimeType(imageData.extension))
                {
                    ons.notification.alert({
                        title: "Extensão inválida",
                        message: "Extensão não suportada."
                    });
                    return;
                }
                var itemExistente = _.find($scope.imageList,function(item){
                    return item.imageName == imageData.imageName && item.extension == imageData.extension;
                });

                if(itemExistente){
                    ons.notification.alert({
                        title: "Duplicado",
                        message: "O arquivo selecionado já está na lista de arquivos."
                    });
                    return;
                }
                    
                $timeout(function(){
                    $scope.imageList.push(imageData);
                });      
            }
        }
    };
});

