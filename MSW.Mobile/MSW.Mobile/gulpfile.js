/// <binding BeforeBuild='install' />
var gulp = require("gulp"),
    bower = require("gulp-bower"),
    rimraf = require("rimraf"),
    serve = require("gulp-serve"),
    sass = require("gulp-sass"),
    browserSync = require("browser-sync").create();

function _sass()
{
    return gulp.src("./www/css/*.scss")
       .pipe(sass())
       .pipe(gulp.dest("./www/css"));
}

gulp.task("sass", function() {
    return _sass();
});

gulp.task("clean", function(cb) {
    rimraf("./bower_components", function() {
        rimraf("./www/lib", cb);
    });
});

gulp.task("install", ["sass"], function(cb) {
    return bower()
        .pipe(gulp.dest("www/lib/"));
});

gulp.task("watch", ["install"], function() {
    gulp.watch("www/**", function () {
        _sass();
        browserSync.reload();
    });
});

gulp.task("serve", ["watch"], function() {
    browserSync.init({
        server: {
            baseDir: __dirname + "/www/",
            directory: false
        },
        ghostMode: false,
        notify: false,
        debounce: 200,
        index: "index.html"
    });
});