﻿using System.Web;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;
using System.Device.Location;

namespace MSW.Web.Helpers.Extensions
{
    public static class OmintExtensions
    {
        public static string AsQueryString(this object parameters)
        {
            var reflectedPropertys = GetProperties(parameters)
                .Select(
                    x =>
                        $"{HttpUtility.UrlEncode(x.Name)}={HttpUtility.UrlEncode(x.GetValue(parameters, null).ToString())}");

            return $"?{string.Join("&", reflectedPropertys)}";
        }

        public static string FormatCep(this string cep)
        {
            if (string.IsNullOrWhiteSpace(cep))
                return string.Empty;

            return Regex.Replace(cep, @"(\d{5})(\d{3})", "$1-$2");
        }

        public static string FormatPhone(this string phone)
        {
            if (string.IsNullOrWhiteSpace(phone))
                return string.Empty;

            string[] splitedPhone = phone.Split(' ');

            if (splitedPhone.Length > 2)
                return $"({splitedPhone[0]}) {splitedPhone[1]}-{splitedPhone[2]}";
            else if (splitedPhone.Length > 1)
                return $"{splitedPhone[0]}-{splitedPhone[1]}";
            else
                return string.Empty;
        }

        public static string FormatDistance(this double distance, string unitString)
        {
            return string.Format("{0:0.00}{1}", distance, unitString);
        }

        static IEnumerable<PropertyInfo> GetProperties(object obj)
        {
            return obj.GetType()
                .GetProperties()
                .Where(s => s.GetValue(obj, null) != null);
        }
    }

    public static class CoordenadasExtensions
    {
        public static double DistanceTo(this Coordenadas baseCoordinates, Coordenadas targetCoordinates)
        {
            var initPosition = new GeoCoordinate(baseCoordinates.Latitude, baseCoordinates.Longitude);
            var destPosition = new GeoCoordinate(targetCoordinates.Latitude, targetCoordinates.Longitude);
            return initPosition.GetDistanceTo(destPosition) / 1000;

        }

        public class UnitOfLength
        {
            public static UnitOfLength Kilometers = new UnitOfLength(1.609344);
            public static UnitOfLength NauticalMiles = new UnitOfLength(0.8684);
            public static UnitOfLength Miles = new UnitOfLength(1);

            private readonly double _fromMilesFactor;

            private UnitOfLength(double fromMilesFactor)
            {
                _fromMilesFactor = fromMilesFactor;
            }

            public double ConvertFromMiles(double input)
            {
                return input * _fromMilesFactor;
            }
        }

    }

    public static class Consts
    {
        public const string LATITUDE_PADRAO_BRASIL = "-15.7217175";
        public const string LONGITUDE_PADRAO_BRASIL = "-48.0783207";

        public const string LISTA_DADOS_PRESTADORES = "Sessao_Dados_Prestadores";
        public const string MULTIPLOS_FILTROS = "Sessao_Multiplos_Filtros";
        public const string LISTA_DADOS_PRESTADORES_PROXIMIDADE = "Sessao_Dados_Prestadores_Proximidade";
        public const string CIDADE_SELECIONADA = "Sessao_Cidade_Selecionadas";
        public const string ESTADO_SELECIONADO = "Sessao_Estado_Selecionado";
        public const string LISTA_PLANOS_ATENDIDOS = "Sessao_Planos_Atendidos";
        public const string LISTA_ESPECIALIDADES = "Sessao_Lista_Especialidades";
        public const string CODIGO_REDE = "Codigo_Rede";
        public const string TIPO_FILTRO_BUSCA = "Tipo_Filtro_Busca";

        public const int DEFAULT_PAGE_SIZE = 10;
        public const int DEFAULT_PAGE_INIT = 1;

        public const string CODIGO_PLANO = "CODIGO_PLANO";
        public const string RESULTADO_PESQUISA_SEM_FILTRO = "Resultado_Pesquisa_Sem_Filtro";
        public const string RESULTADO_PESQUISA_SEM_FILTRO_LOCALIDADE = "Resultado_Pesquisa_Sem_Filtro_Localidade";

        public const string TIPO_ATENDIMENTO_ODONTO = "dentclin";
    }
}