﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace MSW.Web.Helpers.GMaps
{
    public class GMapsDistanceCalc
    {
        [JsonProperty(PropertyName = "destination_addresses")]
        public IEnumerable<string> DestinationAddress { get; protected set; }

        [JsonProperty(PropertyName = "origin_addresses")]
        public IEnumerable<string> OriginAddress { get; protected set; }

        [JsonProperty(PropertyName = "rows")]
        public IEnumerable<GMapsDistanceRow> Rows { get; protected set; }
    }
}