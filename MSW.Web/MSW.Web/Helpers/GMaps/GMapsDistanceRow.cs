﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace MSW.Web.Helpers.GMaps
{
    public class GMapsDistanceRow
    {
        [JsonProperty(PropertyName = "elements")]
        public IEnumerable<GMapsDistanceElement> Elements { get; protected set; }
    }
}