﻿namespace MSW.Web.Helpers.GMaps
{
    public class GMapsDuration : IGMapsDistanceInfo
    {
        public string Text { get; protected set; }
        public int Value { get; protected set; }
    }
}