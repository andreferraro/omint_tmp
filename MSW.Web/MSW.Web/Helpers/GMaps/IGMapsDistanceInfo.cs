﻿using Newtonsoft.Json;

namespace MSW.Web.Helpers.GMaps
{
    interface IGMapsDistanceInfo
    {
        [JsonProperty(PropertyName = "text")]
        string Text { get; }

        [JsonProperty(PropertyName = "value")]
        int Value { get; }
    }
}
