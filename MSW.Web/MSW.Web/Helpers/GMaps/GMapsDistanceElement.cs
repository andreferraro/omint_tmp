﻿using Newtonsoft.Json;

namespace MSW.Web.Helpers.GMaps
{
    public class GMapsDistanceElement
    {
        [JsonProperty(PropertyName = "distance")]
        public GMapsDistance Distance { get; protected set; }

        [JsonProperty(PropertyName = "duration")]
        public GMapsDuration Duration { get; protected set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; protected set; }

    }
}