﻿using System.Web;
using MSW.Web.Models;
using System.Collections.Generic;
using MSW.Web.Models.Response;

namespace MSW.Web.Helpers
{
    public class Contexto
    {
        const string CHAVE_DADOS_USUARIO = "DadosUsuario";
        const string CHAVE_DADOS_LOCALIZACAO = "DadosLocalizacao";
        const string CHAVE_LISTA_PRESTADORES = "ListaPrestadores";
         
        public static DadosUsuario DadosUsuario
        {
            get { return ObterDados<DadosUsuario>(CHAVE_DADOS_USUARIO); }
            set { AdicionarDados(CHAVE_DADOS_USUARIO, value); }
        }

        public static Stack<List<DadosLocalizacao>> DadosLocalizacaoFiltro
        {
            get { return ObterDados<Stack<List<DadosLocalizacao>>>(CHAVE_DADOS_LOCALIZACAO); }
            set { AdicionarDados(CHAVE_DADOS_LOCALIZACAO, value); }
        }

        public static Stack<List<Prestador>> ListaPrestadoresFiltro 
        {
            get { return ObterDados<Stack<List<Prestador>>>(CHAVE_LISTA_PRESTADORES); }
            set { AdicionarDados(CHAVE_LISTA_PRESTADORES, value); }
        }

        public static void AdicionarDados(string chave, object valor)
        {
            HttpContext.Current.Session[chave] = valor;        
        }

        public static T ObterDados<T>(string chave)
            where T : class
        {
            if (HttpContext.Current.Session[chave] != null)
                return HttpContext.Current.Session[chave] as T;

            return null;
        }

        public static void Limpar(string chave)
        {
            HttpContext.Current.Session.Remove(chave);
        }
    }
}