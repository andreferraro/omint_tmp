﻿namespace MSW.Web.Helpers
{
    public enum TipoOrdenacao
    {
        Nome = 1,
        PrestadorPreferencial
    }

    public enum OrigemVoltar
    {
        Filtro = 1,
        Detalhes
    }
}