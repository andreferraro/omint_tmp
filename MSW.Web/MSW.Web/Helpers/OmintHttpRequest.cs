﻿using System;
using System.IO;
using System.Net;
using System.Configuration;
using System.Threading.Tasks;
using MSW.Web.Helpers.Extensions;
using Microsoft.ApplicationInsights;

namespace MSW.Web.Helpers
{
    public class OmintHttpRequest
    {
        public ResultadoRequest Get(string path, object parameters)
        {
            try
            {
                HttpWebRequest webRequest = PrepararGetRequest(path, parameters);
                HttpWebResponse webResponse = webRequest.GetResponse() as HttpWebResponse;

                using (StreamReader reader = new StreamReader(webResponse.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();

                    if (webResponse.StatusCode != HttpStatusCode.OK)
                    {
                        return new ResultadoRequest(new ErroRequest(result));
                    }

                    return new ResultadoRequest(result);
                }
            }
            catch (Exception ex)
            {
                var telemetry = new TelemetryClient();
                telemetry.TrackException(ex);

                return new ResultadoRequest(new ErroRequest(ex.Message));
            }
        }

        HttpWebRequest PrepararGetRequest(string path, object parameters)
        {
            Uri uri = new Uri($"{ConfigurationManager.AppSettings["mswApi:BaseUrl"]}{path}{parameters.AsQueryString()}");

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Accept = "application/json";
            request.Method = "GET";

            return request;
        }        
    }
}