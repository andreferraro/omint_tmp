﻿namespace MSW.Web.Helpers
{
    public class ErroRequest 
    {
        public string Mensagem { get; }

        public ErroRequest(string mensagem)
        {
            Mensagem = mensagem;
        }
    }
}