﻿using Newtonsoft.Json;

namespace MSW.Web.Helpers
{
    public class ResultadoRequest
    {
        private string json;
        public ErroRequest Erro { get; }

        public ResultadoRequest(ErroRequest result)
        {
            Erro = result;
        }

        public ResultadoRequest(string json)
        {
            this.json = json;
        }

        public T ObterResultado<T>()
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}