﻿using System;
using System.Globalization;

namespace MSW.Web.Helpers
{
    public class Coordenadas
    {
        public double Latitude { get; private set; }
        public double Longitude { get; private set; }

        public Coordenadas(string latitude, string longitude)
        {
            Latitude = string.IsNullOrWhiteSpace(latitude) ? 0 : double.Parse(latitude,new CultureInfo("en-us"));
            Longitude = string.IsNullOrWhiteSpace(longitude) ? 0 : double.Parse(longitude, new CultureInfo("en-us"));
        }
    }
}