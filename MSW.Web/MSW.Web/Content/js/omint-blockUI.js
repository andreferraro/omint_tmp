﻿var Blocker = (function ($) {

    var _message,
        _css = {};

    var Blocker = function (message, css) {
        _message = message;
        _css = css;
    }

    Blocker.prototype = {

        createBlocker: function () {
            $.blockUI({
                message: $(_message),
                css: _css
            });
        },

        removeBlocker: function () {
            $.unblockUI();
        },

        changeCss: function (css) {
            _css = css;
        },

        changeMessage: function (message) {
            _message = message;
        }
    }

    return Blocker;

})(this.jQuery);