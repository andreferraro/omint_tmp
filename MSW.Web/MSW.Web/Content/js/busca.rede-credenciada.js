﻿var buscaRedeCredenciada = (function (buscaRedeCredenciada, $) {

    var blocker = new Blocker('#loading', { border: 'none', backgroundColor: 'transparent' });

    var DetalhesBusca = (function () {

        var Origem = {
            DETALHES: 'detalhes',
            LOCALIZACAO: 'localizacao'
        };

        var DetalhesBusca = function (options) {
            var self = this;

            self.$formData = $(options.formData);
            self.reloadMap = function () { }

            $.extend(true, self, options);
        }

        DetalhesBusca.prototype = {

            configurarDetalhesBusca: function () {
                var self = this;

                $(document).on('click', self.selectorDetalhes, { origem: Origem.DETALHES }, function (e) { self.obterDetalhes(e); });
                $(document).on('click', self.selectorVerLocalizacao, { origem: Origem.LOCALIZACAO }, function (e) { self.obterDetalhes(e); })
                $(document).on('click', self.seletorDetalhesLocalizacao, function (e) {
                    self.reloadMap(e);
                });
                $(document).on('click', self.selectorPlanosAtendidos, function (e) { self.obterPlanosAtendidos(e); });
            },

            obterDetalhes: function (e) {
                e.stopPropagation();

                var self = this;
                var data = $(e.currentTarget).closest('.div-table-container').siblings();

                $.ajax({
                    url: self.urlDetalhes,
                    dataType: 'text',
                    type: 'POST',
                    data: converterParaObjeto(data),
                    beforeSend: function (xhr) {
                        blocker.createBlocker();
                    },
                    complete: function () {
                        blocker.removeBlocker();
                    }
                })
                .done(function (result) {
                    $(self.divContainerDetalhes).html(result);
                    //$(self.selectorNotaEstrela).notaEstrela();

                    if (e.data.origem === Origem.LOCALIZACAO) {
                        self.reloadMap(e);
                    }
                })
                .fail(function (jqxhr, statusText, errorThrown) {
                   alert('oops, ocorreu um erro ao realizar a busca, favor tente novamente mais tarde!');
                });
            },

            obterPlanosAtendidos: function (e)
            {
                var self = this;

                $.ajax({
                    url: self.urlPlanosAtendidos,
                    dataType: 'text',
                    type: 'POST',
                    data: { Rede: $("#CodigoRede").val(), CodigoPrestador: $("#CodigoPrestador").val() },
                    beforeSend: function (xhr) {
                        blocker.createBlocker();
                    },
                    complete: function () {
                        blocker.removeBlocker();
                    }
                })
                .done(function (result) {
                    $(self.divContainerPlanosAtendidos).html(result);
                })
                .fail(function (jqxhr, statusText, errorThrown) {
                    alert('oops, ocorreu um erro ao realizar a busca, favor tente novamente mais tarde!');
                });
            }
        }

        function converterParaObjeto(domArray) {
            var objeto = {};
            for (var i = 0; i < domArray.length; i++) {
                var nome = domArray[i].name.split('_').shift();
                objeto[nome] = domArray[i].value;
            }

            return objeto;
        }

        return DetalhesBusca;

    }());

    buscaRedeCredenciada.DetalhesBusca = DetalhesBusca;

    var BuscaEspecialidade = (function () {

        var _formData = {};

        var BuscaEspecialidade = function (options) {
            var self = this;

            self.urlBuscaLocalizacao = options.urlBuscaLocalizacao;
            self.urlBuscaAvancada = options.urlBuscaAvancada;
            self.divContainer = options.divContainer;
        }

        BuscaEspecialidade.prototype = {

            onClickEspecialidade: function (e, props) {
                var self = this;
                var $element = $(e.target);

                var urlParms = $.extend(true, {
                    especialidade: $element.text(),
                    codigoEspecialidade: $element.data('codigoEspecialidade'),

                }, props);

                $(e.target).attr('href', self.urlBuscaLocalizacao.concat($.param(urlParms))).click();
            },

            buscarPrestadoresEspecialidade: function (formData) {
                var self = this;

                _formData = formData;
                executarRequest(self);
            },

            buscarPrestadores: function (formData) {
                var self = this;
                _formData = formData;

                window.parent.postMessage("esconder-header", "*");
                executarRequest(self);
            }
        }

        function executarRequest(context) {

            if (Object.keys(_formData).length <= 0) {
                throw new Error('o objeto formData não foi setado!');
            }

            $.ajax({
                url: context.urlBuscaAvancada,
                dataType: 'text',
                type: 'POST',
                data: _formData,
                beforeSend: function (xhr) {
                    blocker.createBlocker();
                },
                complete: function () {
                    blocker.removeBlocker();
                }
            })
            .done(function (result) {
                $(context.divContainer).html(result);
            })
            .fail(function (jqxhr, statusText, errorThrown) {
                alert('Ooops, ocorreu um erro na busca, favor tente mais tarde!');
            });
        }

        return BuscaEspecialidade;

    }());

    buscaRedeCredenciada.BuscaEspecialidade = BuscaEspecialidade;

    var BuscaLocalizacao = (function () {

        var BuscaLocalizacao = function (options) {
            var self = this;

            self.$form = $(options.form);
            self.urlObterCidades = options.urlObterCidades;
            self.urlObterBairros = options.urlObterBairros;

            self.endpoints = {
                buscaProximidadeLocal: options.urlBuscaProximidadeLocal,
                buscaRegiao: options.urlBuscaRegiao,
                buscaProximidadeResidencia: options.urlBuscaProximidadeResidencia
            };

            self.buscaSelectors = {};

            $.extend(true, self, options);
        }

        BuscaLocalizacao.prototype = {

            configurarBuscaLocalizacao: function () {
                var self = this;

                $(document).on('click', self.buscaSelectors['btnBuscaRegiao'], function (e) { self.buscarPorRegiao(e); });
                $(document).on('click', self.buscaSelectors['btnBuscaProximosAMinhaResidencia'], function (e) { self.buscarProximosEnderecoResidencial(e); });

                $(document).on('change', self.buscaSelectors['ddlEstado'], function () { self.obterCidades(); });
                $(document).on('change', self.buscaSelectors['ddlCidade'], function () { self.obterBairros(); });


                verificarSuporteGeoLocalizacao(self);
            },

            obterBairros: function () {
                var self = this,
                    selectorEstado = self.buscaSelectors['ddlEstado'].concat(' option:selected'),
                    selectorCidade = self.buscaSelectors['ddlCidade'].concat(' option:selected');
                

                $.ajax({
                    url: self.urlObterBairros,
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        codigoUf: $(selectorEstado).val(),
                        cidade: $(selectorCidade).val()
                    }
                })
                    .done(function (result) {
                        $(self.buscaSelectors['ddlBairro']).empty();
                        adicionarDadosComboBox(result, '#ddlBairro', 'Todos os Bairros');
                    })
                    .fail(function (jqxhr, statusText, errorThrown) {
                        console.log(jqxhr.errorThrown);
                        alert('oops, ocorreu um erro na busca de bairros, favor tente mais tarde!');
                    });
            },

            obterCidades: function () {
                var self = this,
                    selectorEstado = self.buscaSelectors['ddlEstado'].concat(' option:selected');

                $.ajax({
                    url: self.urlObterCidades,
                    dataType: 'JSON',
                    type: 'POST',
                    data: { codigoUf: $(selectorEstado).val() }
                })
                .done(function (result) {
                    $(self.buscaSelectors['ddlCidade']).empty();
                    $(self.buscaSelectors['ddlBairro']).empty();

                    $(self.buscaSelectors['ddlBairro']).append($("<option></option>")
                            .attr("value", '')
                            .text('Selecione'))

                    adicionarDadosComboBox(result, self.buscaSelectors['ddlCidade'], 'Selecione');
                })
                .fail(function (jqxhr, statusText, errorThrown) {
                    console.log(jqxhr.errorThrown);
                    alert('oops, ocorreu um erro na busca de cidades, favor tente mais tarde!');
                });
            },
           
            buscarPorRegiao: function (e) {
                var self = this,
                    selectorCidade = self.buscaSelectors['ddlCidade'].concat(' option:selected'),
                    selectorEstado = self.buscaSelectors['ddlEstado'].concat(' option:selected'),
                    selectorBairro = self.buscaSelectors['ddlBairro'].concat(' option:selected');

                var cidade = $(selectorCidade).val();
                var estado = $(selectorEstado).val();
                var bairro = $(selectorBairro).val();

                if (estado === '') {
                    alert('favor selecionar um estado!');
                    return;
                }

                if (cidade === '') {
                    alert('favor selecionar uma cidade!');
                    return;
                }

                $(self.buscaSelectors['hdnCidade']).val(cidade);
                $(self.buscaSelectors['hdnCodigoEstado']).val(estado);
                $(self.buscaSelectors['hdnBairro']).val(bairro);

                window.parent.postMessage("esconder-header", "*");

                executarBusca(e, self);
            },

            buscarProximosEnderecoResidencial: function (e) {
                var self = this;
                $(self.buscaSelectors['buscaProximidadeTipo']).val('R');
                
                executarBusca(e, self);
            },

            buscarProximosLocalizacaoAtual: function (e) {
                var self = this;

                obterPosicaoCorrente(self)
                    .then(function (p) {
                        $(p.self.buscaSelectors['latitude']).val(p.pos.coords.latitude)
                        $(p.self.buscaSelectors['longitude']).val(p.pos.coords.longitude);
                        $(p.self.buscaSelectors['buscaProximidadeTipo']).val('L');

                        window.parent.postMessage("esconder-header", "*");
                        executarBusca(e, p.self);
                    })
                    .fail(function () {
                        alert('oops, ocorreu um problema inesperado, tente novamente mais tarde!');
                    });
            }
        }

        function verificarSuporteGeoLocalizacao(self) {

            if ('geolocation' in navigator) {
                $(self.buscaSelectors['btnBuscaProximosLocalizacao']).show();
                $(document).on('click', self.buscaSelectors['btnBuscaProximosLocalizacao'], function (e) { self.buscarProximosLocalizacaoAtual(e); });
            } else {
                $(self.buscaSelectors['btnBuscaProximosLocalizacao']).hide();
            }
        }

        function obterPosicaoCorrente(self) {
            var deferred = $.Deferred();

            try { navigator.geolocation.getCurrentPosition(function (p) { deferred.resolve({ pos: p, self: self }) }); }
            catch (e) { deferred.reject(e); }

            return deferred.promise();
        }

        function executarBusca(e, self) {
            var btnId = $(e.target).attr('id');

            switch (btnId) {
                case 'btnProximosAMim':
                    self.$form.attr('action', self.endpoints['buscaProximidadeLocal']).submit();
                    break;
                case 'btnBuscaRegiao':
                    self.$form.attr('action', self.endpoints['buscaRegiao']).submit();
                    break;
                default:
                    self.$form.attr('action', self.endpoints['buscaProximidadeResidencia']).submit();
                    break;
            }
        }

        function adicionarDadosComboBox(listaDados, comboBox, textoPrimeiraPosicao) {
            if (textoPrimeiraPosicao) {
                $(comboBox).append($("<option></option>")
                    .attr("value", '')
                    .text(textoPrimeiraPosicao))
            }

            $.each(listaDados, function (key, value) {
                $(comboBox).append($("<option></option>")
                    .attr("value", value.Valor)
                    .text(value.Texto));
            });
        }

        return BuscaLocalizacao;
    }());

    buscaRedeCredenciada.BuscaLocalizacao = BuscaLocalizacao;

    var Imprimir = (function () {

        var Imprimir = function (options) {
            var self = this;
            
            $.extend(true, self, options);
        }

        Imprimir.prototype = {

            configurar: function () {
                var self = this;
                $(self.checkboxSelector).on('click', function (e) { onCheckboxSelecionado(self, e); });
                $(self.botaoImprimir).on('click', function (e) { imprimirRegistros(self, e); });
            }
        }

        function onCheckboxSelecionado(self, e) {
            e.stopPropagation();
            $(e.currentTarget).closest('tr').removeClass(self.classRegistroSelecionado);
        }

        function imprimirRegistros(self, e) {
            e.stopPropagation();
            var registrosSelecionados = $('table > tbody > tr').find('.chkImprimir:checked');

            if (registrosSelecionados.length == 0) {
                alert('Favor selecionar os registros para impressão!');
                return;
            }

            window.print();
            limparSelecionados(self.classRegistroSelecionado, registrosSelecionados);
        }

        function limparSelecionados(cssClass, selecionados) {
            $.each(selecionados, function (key, value) {
                $(value).attr('checked', false);
                $(value).closest('tr').addClass(cssClass);
            });
        }

        return Imprimir;

    })();

    buscaRedeCredenciada.Imprimir = Imprimir;

    return buscaRedeCredenciada;

}(buscaRedeCredenciada = this.buscaRedeCredenciada || {}, this.jQuery));