﻿var MapaBuscaRedeCredenciada = (function ($, window) {

    var _directionService;

    var MapaBuscaRedeCredenciada = function (options) {
        var _divMapa = typeof options.divContainer === 'string' ? document.getElementById(options.divContainer) : options.divContainer;
        var self = this;
        var markersData = options.markersData || [];

        self.options = {
            divMapa: _divMapa,

            configuration: {
                zoom: markersData.length > 0 ? 7 : 3,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            },

            markersData: []
        };

        if (options.distance)
            self.options.configuration = $.extend(true, self.options.configuration, {
                center: new google.maps.LatLng(options.distance.lat, options.distance.lng)
            }); 

        _directionService = new google.maps.DirectionsService();

        $.extend(true, self.options, options);
    }

    MapaBuscaRedeCredenciada.prototype = {

        iniciarGoogleMaps: function () {
            var self = this;

            self.mapa = criarMapa(self.options.divMapa, self.options.configuration);

            if (self.options.markersData.length > 0)
                self.mostrarMarcadores();
        },

        mostrarMarcadores: function () {
            var self = this,
                bounds = new google.maps.LatLngBounds();
            for (var i = 0; i < self.options.markersData.length; i++)
            {                
                var latlng = new google.maps.LatLng(self.options.markersData[i].lat, self.options.markersData[i].lng);
                self.criarMarcadores(latlng, self.options.markersData[i].nome, i);
                bounds.extend(latlng);
            }

            self.mapa.fitBounds(bounds);
        },

        criarMarcadores: function (latLng, nome, position) {
            
            var self = this;

            var marker = new google.maps.Marker({
                position: latLng,
                map: self.mapa,
                icon: self.options.icon
            });
            var infowindow = new google.maps.InfoWindow({content: nome, maxWidth: 200 });

            marker.addListener("click", function ()
            {
                var mapab = new MapaBuscaRedeCredenciada({
                    divContainer: 'map-canvas',
                    icon: '@Url.Content("~/Content/img/pim-mapa-omint.png")'
                });

                mapab.configurarMapeamentoRota({
                    lat: latLng.lat(),
                    lng: latLng.lng()
                });
                console.log(".div-prestador .span-texto-conteudo-prestador > a");
                var link = $(".div-prestador .span-texto-conteudo-prestador > a")[position];
                link.click();
            })

            marker.addListener('mouseover', function ()
            {
                infowindow.open(self.mapa, marker);
            });
            marker.addListener('mouseout', function () {
                infowindow.close();
            });

        },

        configurarMapeamentoRota: function (destPos) {
            var self = this;

            self.directionsRederer = new google.maps.DirectionsRenderer();
            self.mapa = criarMapa(self.options.divMapa, self.options.configuration);
            self.directionsRederer.setMap(self.mapa);

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {

                    var defaultPoint = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    self.mapa.setCenter(defaultPoint);

                    self.criarMapeamentoRota(defaultPoint, new google.maps.LatLng(destPos.lat, destPos.lng));

                });
            } else {

                document.getElementById(self.options.divMapa).innerHTML = '<span> Verifique se o navegador oferece suporte para geolocalização </span>';
            }
        },

        criarMapeamentoRota: function (startPos, endPos) {
            var self = this;

            var request = {
                origin: startPos,
                destination: endPos,
                travelMode: google.maps.TravelMode.DRIVING
            };

            _directionService.route(request, function (result, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    self.directionsRederer.setDirections(result);
                } else {
                    alert('oops, ocorreu um erro ao traçar a rota do mapa, verifique se o seu navegador possui suporte a geolocalização!');
                }
            });
        }
    }

    function criarMapa(div, config) {
        return new google.maps.Map(div, config);
    }

    return MapaBuscaRedeCredenciada;

})(this.jQuery, this);