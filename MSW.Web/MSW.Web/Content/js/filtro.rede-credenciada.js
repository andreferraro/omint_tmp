﻿var FiltroRedeCredenciada = (function ($) {

    var TipoOrdenacao = {
        nome: '1',
        prestadorPreferencial: '2'
    }

    var FiltroRedeCredenciada = function (options) {

        this.options = {

            endpoints: {},

            baseProperties: {
                dataType: 'JSON',
                type: 'POST'
            },

            divContainer: '',
            selectorNotaEstrela: '.star > span.nota-estrela',
            selectorDdlOrdenacao: '#ddlOrdenacao',
            afterRenderView: function () { }
        };

        $.extend(true, this.options, options);
    }

    FiltroRedeCredenciada.prototype = {

        configurarAcaoFiltro: function () {
            var self = this;
            $(document).on('click', 'a[data-tipo-filtro]', function (e) { self.executarFiltro(e); });
            $(document).on('click', self.options.selectorAvaliacao, function (e) { self.executarFiltro(e); });
            $(document).on('change', self.options.selectorDdlOrdenacao, function (e) { self.ordenarResultado(e); });
            $(document).on('click',
                '#btn-filtrar',
                function (e) {
                    e.preventDefault();
                    self.buscarMultiplosFiltros();
                });
        },

        executarFiltro: function (e) {
            var self = this,
                descricao = $(e.currentTarget).data('descricaoFiltro'),
                tipo = $(e.currentTarget).data('tipoFiltro');

            var parametros = self.obterParametrosRequest(tipo, descricao);

            self.executerRequest(parametros)
                .then(function (result) {
                    $(self.options.divContainer).html(result.ViewResult);
                    //$(self.options.selectorNotaEstrela).notaEstrela();

                    self.options.afterRenderView(result);
                })
                .fail(function (jqxhr, statusText, errorThrown) {
                    alert('oops, ocorreu um erro ao realizar o filtro, favor tente mais tarde!');
                });
        },
        buscarMultiplosFiltros: function (e) {
            var self = this;

            var data = { estados: [], cidades: [], especialidades: [] };

            $('ul#estados').find('li input:checked').each(function (i, el) {
                data.estados.push($(el).data('descricaoFiltro'));
            });

            $('ul#cidades').find('li input:checked').each(function (i, el) {
                data.cidades.push($(el).data('descricaoFiltro'));
            });

            $('ul#especialidades').find('li input:checked').each(function (i, el) {
                data.especialidades.push($(el).data('descricaoFiltro'));
            });


            var request = new XMLHttpRequest();
            request.open('POST', self.options.endpoints['multiplosFiltros'], true);
            request.setRequestHeader("Content-type", "application/json;charset=UTF-8");


            request.onreadystatechange = function () {
                if (request.readyState === 4 && request.status === 200) {
                    var result = JSON.parse(request.responseText);
                    $(self.options.divContainer).html(result.ViewResult);
                    self.options.afterRenderView(result);
                } else if (request.readyState === 4) {
                    console.log('%c REQUEST ERROR: ' + request.responseText, 'color: red; font-size: 20px;');
                    alert('oops, ocorreu um erro ao realizar o filtro, favor tente mais tarde!');
                }
            }
            request.send(JSON.stringify(data));
        },
        obterParametrosRequest: function (tipo, descricao) {
            var self = this;

            switch (tipo) {
                case 'servico':
                    return { data: { servico: descricao }, url: self.options.endpoints['filtrarPorServico'] };
                case 'rede':
                    return { data: { codigoRede: descricao }, url: self.options.endpoints['filtrarPorRede'] };
                case 'plano':
                    return { data: { codigoPlano: descricao }, url: self.options.endpoints['filtrarPorPlano'] };
                case 'avaliacao':
                    return { data: { scoreTotal: descricao }, url: self.options.endpoints['filtrarPorAvaliacao'] };
                default:
                    return {};
            }
        },

        executerRequest: function (data) {
            var self = this,
                deferred = $.Deferred();

            $.ajax($.extend(true, self.options.baseProperties, data))
                .then(deferred.resolve)
                .fail(deferred.reject);

            return deferred.promise();
        },

        ordenarResultado: function (e) {
            var self = this;
            var selectedValue = $(e.currentTarget).val();

            if (selectedValue === "")
                return;

            if (selectedValue === 'distancia') {
                if (navigator.geolocation) {

                    navigator.geolocation.getCurrentPosition(function (position) {

                        self.executerRequest({
                            url: self.options.endpoints['ordenarResultadoPorDistancia'],
                            data: { latitude: position.coords.latitude, longitude: position.coords.longitude }
                        })
                            .then(function (result) {
                                $(self.options.divContainer).html(result.ViewResult);
                                //$(self.options.selectorNotaEstrela).notaEstrela();

                                self.options.afterRenderView(result);
                            }).fail(function (jqxhr, statusText, errorThrown) {
                                alert('oops, ocorreu um erro na ordenação, favor tente mais tarde!');
                            });
                    });
                } else {
                    alert('seu navegador não oferece suporte a geolocalização!');
                }
            } else {

                self.executerRequest({
                    url: self.options.endpoints['ordenarResultado'],
                    data: { ordenacao: TipoOrdenacao[selectedValue] }
                })
                    .then(function (result) {
                        $(self.options.divContainer).html(result.ViewResult);
                        //$(self.options.selectorNotaEstrela).notaEstrela();

                        self.options.afterRenderView(result);
                    })
                    .fail(function (jqxhr, statusText, errorThrown) {
                        alert('oops, ocorreu um erro na ordenação, favor tente mais tarde!');
                    });
            }
        }
    };



    return FiltroRedeCredenciada;

}(this.jQuery));