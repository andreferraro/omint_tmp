﻿(function ($) {

    $.fn.notaEstrela = function () {
        return $(this).each(function () {
            $(this).html($('<span />').width(Math.max(0, (Math.min(5, parseFloat($(this).html())))) * 16));
        });
    }

}(this.jQuery));