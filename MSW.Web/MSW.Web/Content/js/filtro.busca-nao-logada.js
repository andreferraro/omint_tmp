﻿var FiltroBuscaNaoLogada = (function($){

    var ABA = {
        REDE_MEDICA: 'medica',
        REDE_ODONTO: 'odonto'
    };

    var FiltroBuscaNaoLogada = function (options) {
        var self = this;

        self.selectorAbaMedica = options.selectorAbaMedica;
        self.selectorAbaOdonto = options.selectorAbaOdonto;
        self.selectorCartao = options.selectorCartao;
        self.urlFormularioBusca = options.urlFormularioBusca;
        self.divContainer = options.divContainer;
        self.selctorDropDown = options.selctorDropDown;
        self.selectorBotaoOk = options.selectorBotaoOk;

        self.imagemAbaAtiva = options.imagemAbaAtiva;
        self.imagemBordMed = options.imagemBordMed;
        self.imagemBulletMaisDourado = options.imagemBulletMaisDourado;
        self.imagemAbaInativa = options.imagemAbaInativa;
        self.imagemBulletMaisClaro = options.imagemBulletMaisClaro;
        self.imagemBordOdonto = options.imagemBordOdonto;
        self.imagemBulletBocaDourada = options.imagemBulletBocaDourada;
        self.imagemBulletBocaClara = options.imagemBulletBocaClara;

        self.$rede = $(options.selectorRede);
        self.$plano = $(options.selectorPlano);
    }

    FiltroBuscaNaoLogada.prototype = {

        configurarFiltro: function () {
            var self = this;

            $(document).on('click', self.selectorAbaMedica, function () { self.gerenciarAbas(ABA.REDE_MEDICA); });
            $(document).on('click', self.selectorAbaOdonto, function () { self.gerenciarAbas(ABA.REDE_ODONTO); });
            $(document).on('click', self.selectorCartao, function (e) { self.onSelecionarCartao(e); });
            $(document).on('click', self.selectorBotaoOk, function (e) { self.obterFormularioBusca(e); });
        },

        obterFormularioBusca: function (e) {
            var self = this;

            var data = {
                codigoRede: self.$rede.val(),
                descricao: $(self.selctorDropDown + ' option:selected').val(),
                plano: self.$plano.val()
            };

            $.ajax({
                url: self.urlFormularioBusca,
                dataType: 'text',
                type: 'POST',
                data: data
            })
             .done(function (result) {
                 $(self.divContainer).html(result);
             })
             .fail(function (jqxhr, statusText, errorThrown) {
                alert('oops, ocorreu um erro, favor tente mais tarde!');
             });
        },

        onSelecionarCartao: function (e) {
            e.stopPropagation();

            var self = this;

            $(".rede_conteudo_rede_consulta_select select")
                .find('option')
                .remove()
                .end();

            var codigoRede = $(e.currentTarget).data('codigoRede');
            var plano = $(e.currentTarget).data('nomePlano');

            self.$rede.val(codigoRede);
            self.$plano.val(plano);

            switch (codigoRede) {
                case '0001':
                    $(".rede_conteudo_rede_consulta_select").append('<option class="rede_conteudo_rede_consulta_option" value="REDE OMINT PREMIUM DE MEDICINA">REDE OMINT PREMIUM DE MEDICINA</option>');
                    $(".rede_conteudo_rede_consulta_select").append('<option class="rede_conteudo_rede_consulta_option" value="REDE OMINT ACCESS DE MEDICINA">REDE OMINT ACCESS DE MEDICINA</option>');
                    $(".rede_conteudo_rede_consulta_select").append('<option class="rede_conteudo_rede_consulta_option" value="REDE OMINT CORPORATE MEDICINA">REDE OMINT CORPORATE MEDICINA</option>');
                    $(".rede_conteudo_rede_consulta_select").append('<option class="rede_conteudo_rede_consulta_option" value="REDE OMINT SKILL DE MEDICINA">REDE OMINT SKILL DE MEDICINA</option>');
                    break;

                case '0004':
                    $(".rede_conteudo_rede_consulta_select").append('<option class="rede_conteudo_rede_consulta_option" value="REDE OMINT ACCESS DE MEDICINA">REDE OMINT ACCESS DE MEDICINA</option>');
                    $(".rede_conteudo_rede_consulta_select").append('<option class="rede_conteudo_rede_consulta_option" value="REDE OMINT CORPORATE MEDICINA">REDE OMINT CORPORATE MEDICINA</option>');
                    $(".rede_conteudo_rede_consulta_select").append('<option class="rede_conteudo_rede_consulta_option" value="REDE OMINT SKILL DE MEDICINA">REDE OMINT SKILL DE MEDICINA</option>');
                    break;

                case '0009':
                    $(".rede_conteudo_rede_consulta_select").append('<option class="rede_conteudo_rede_consulta_option" value="REDE OMINT CORPORATE MEDICINA">REDE OMINT CORPORATE MEDICINA</option>');
                    $(".rede_conteudo_rede_consulta_select").append('<option class="rede_conteudo_rede_consulta_option" value="REDE OMINT SKILL DE MEDICINA">REDE OMINT SKILL DE MEDICINA</option>');
                    break;

                case '0003':
                    $(".rede_conteudo_rede_consulta_select").append('<option class="rede_conteudo_rede_consulta_option" value="REDE OMINT SKILL DE MEDICINA">REDE OMINT SKILL DE MEDICINA</option>');
                    break;                    
            }

            if (codigoRede !== '0002') {
                $(e.currentTarget).parent()
                    .parent()
                    .hide();

                $(".rede_conteudo_2_5").show();
            } else {
                self.obterFormularioBusca(e);
            }
        },

        gerenciarAbas: function (tipoAba) {
            var self = this;

            var aba_ativa = "";
            if (tipoAba == ABA.REDE_MEDICA) {
                top_down_med = 5;
                top_down_odo = 1;
                aba_ativa = "med";

            } else {
                top_down_med = 1;
                top_down_odo = 5;
                aba_ativa = "odo";
            }
            var time_move = 500;

            $(".rede_aba_medica_clck").stop().animate({
                width: '115px'
            }, time_move, function () {

                $(".red_med").css({ 'z-index': top_down_med });

                if (aba_ativa == "med") {
                    $('.rede_aba_medica_clck').removeClass('rede_aba_inativa_med');
                    $('.rede_aba_medica_clck').addClass('rede_aba_ativa_med');
                    $('.rede_aba_ativa_med').css({ background: 'url(' + self.imagemAbaAtiva + ') repeat-x' });
                    $('.rede_aba_divisao_medica').css({ background: 'url(' + self.imagemBordMed + ') 0px 0px no-repeat' });
                    $('.rede_aba_ativa_med p').css({ color: '#a19062' });
                    $('.rede_aba_ativa_med img').attr({ src: self.imagemBulletMaisDourado });

                } else {
                    $('.rede_aba_medica_clck').removeClass('rede_aba_ativa_med');
                    $('.rede_aba_medica_clck').addClass('rede_aba_inativa_med');
                    $('.rede_aba_inativa_med').css({ background: 'url(' + self.imagemAbaInativa + ') repeat-x' });
                    $('.rede_aba_divisao_medica').css({ background: 'url(' + self.imagemBordMed + ') -31px 0px no-repeat' });
                    $('.rede_aba_inativa_med').css({ background: 'url(' + self.imagemAbaInativa + ') repeat-x' });
                    $('.rede_aba_inativa_med p').css({ color: '#f3f3f3' });
                    $('.rede_aba_inativa_med img').attr({ src: self.imagemBulletMaisClaro });
                }

                $(this).css({
                    'z-index': top_down_med
                }).animate({
                    width: '124px'//original
                }, time_move, function () {

                });

                $('.rede_conteudo_1').show();
                $('.rede_conteudo_2').hide();
            });

            $(".rede_aba_odonto_clck").stop().animate({
                width: '720px'
            }, time_move, function () {
                $(".red_odo").css({ 'z-index': top_down_odo });

                if (aba_ativa == "odo") {
                    $('.rede_aba_odonto_clck').removeClass('rede_aba_inativa_odo');
                    $('.rede_aba_odonto_clck').addClass('rede_aba_ativa_odo');
                    $('.rede_aba_ativa_odo').css({ background: 'url(' + self.imagemAbaAtiva + ') repeat-x' });
                    $('.rede_aba_divisao_odonto').css({ background: 'url(' + self.imagemBordOdonto + ') 0px 0px no-repeat' });
                    $('.rede_aba_ativa_odo p').css({ color: '#a19062' });
                    $('.rede_aba_ativa_odo img').attr({ src: self.imagemBulletBocaDourada });

                    $('.rede_conteudo_1').hide();
                    $('.rede_conteudo_2').show();

                } else {
                    $('.rede_aba_odonto_clck').removeClass('rede_aba_ativa_odo');
                    $('.rede_aba_odonto_clck').addClass('rede_aba_inativa_odo');
                    $('.rede_aba_inativa_odo').css({ background: 'url(' + self.imagemAbaInativa + ') repeat-x' });
                    $('.rede_aba_divisao_odonto').css({ background: 'url(' + self.imagemBordOdonto + ') -28px 0px no-repeat' });
                    $('.rede_aba_inativa_odo p').css({ color: '#f3f3f3' });
                    $('.rede_aba_inativa_odo img').attr({ src: self.imagemBulletBocaClara });

                    $('.rede_conteudo_1').show();
                    $('.rede_conteudo_2').hide();
                }

                $(this).css({
                    'z-index': top_down_odo
                }).animate({
                    width: '758px'//original
                }, time_move, function () {

                });
            });
        }
    };

    return FiltroBuscaNaoLogada;

})(this.jQuery);