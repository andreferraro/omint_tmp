﻿namespace MSW.Web.Models
{
    public class Contato
    {
        public string EnderecoTelefoneDdd { get; set; }
        public string EnderecoTelefonePrefixo { get; set; }
        public string EnderecoTelefone { get; set; }
        public string EnderecoExtensao { get; set; }
    }
}