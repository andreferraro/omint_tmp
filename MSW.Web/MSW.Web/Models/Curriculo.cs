﻿using System.Collections.Generic;

namespace MSW.Web.Models
{
    public class Curriculo
    {
        public string Tipo { get; set; }
        public List<string> Registros { get; set; }

        public Curriculo()
        {
            Registros = new List<string>();
        }
    }
}