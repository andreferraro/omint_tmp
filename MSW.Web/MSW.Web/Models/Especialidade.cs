﻿namespace MSW.Web.Models
{
    public class Especialidade
    {
        public string CodigoEspecialidade { get; set; }
        public string DescricaoEspecialidade { get; set; }
    }
}