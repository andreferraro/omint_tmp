﻿using System.Collections.Generic;

namespace MSW.Web.Models.Response
{
    public class RedePrestadorResponse
    {
        public List<Prestador> Data { get; set; }
        public string Vinculo { get; set; }
        public bool Inativos { get; set; }
        public string Nome { get; set; }
        public int QuantidadeLocalizados { get; set; }
        public string DefaultLat { get; set; }
        public string DefaultLng { get; set; }
        public string MarcadoresGoogleMapsPosition { get; set; }

        public RedePrestadorResponse()
        {
            Data = new List<Prestador>();
        }
    }
}