﻿using System.Collections.Generic;

namespace MSW.Web.Models.Response
{
    public class TituloResponse
    {
        public TituloResult Data { get; set; }

        public TituloResponse()
        {
            Data = new TituloResult();
        }
    }

    public class TituloResult
    {
        public DadosTituloResult Titulo { get; set; }
        public DadosRedeCredenciada DadosRede { get; set; }

        public TituloResult()
        {
            Titulo = new DadosTituloResult();
            DadosRede = new DadosRedeCredenciada();
        }
    }

    public class DadosTituloResult
    {
        public List<Endereco> Enderecos { get; set; }

        public DadosTituloResult()
        {
            Enderecos = new List<Endereco>();
        }
    }

}