﻿using System.Collections.Generic;

namespace MSW.Web.Models.Response
{
    public class RedeEspecialidadeResponse
    {
        public List<DadosRedeEspecialidade> Data { get; set; }

        public RedeEspecialidadeResponse()
        {
            Data = new List<DadosRedeEspecialidade>();
        }

    }

    public class DadosRedeEspecialidade
    {
        public string CodigoEspecialidade { get; set; }
        public string DescricaoEspecialidade { get; set; }

    }
}