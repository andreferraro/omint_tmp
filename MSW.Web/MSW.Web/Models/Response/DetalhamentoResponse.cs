﻿using System.Collections.Generic;

namespace MSW.Web.Models.Response
{
    public class DetalhamentoResponse
    {
        public DadosDetalhamento Data { get; set; }

        public string NomePrestador { get; set; }
        public string Registro { get; set; }
        public string Rede { get; set; }
        public string Endereco { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string CEP { get; set; }
        public string DescricaoObservacao { get; set; }
        public string DataDescredenciamento { get; set; }
        public string CodigoPrestador { get; set; }
        public string CodigoRede { get; set; }
        public string Telefone { get; set; }
        public int NrAvaliacao { get; set; }
        public int QuantidadeLocalizados { get; set; }
        public int ScoreTotal { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public DetalhamentoResponse()
        {
            Data = new DadosDetalhamento();
        }
    }

    public class DadosDetalhamento
    {
        public List<string> Especialidades { get; set; }
        public List<string> Telefones { get; set; }
        public List<string> Acessos { get; set; }
        public List<Contato> Contatos { get; set; }
        public List<Curriculo> Curriculo { get; set; }
        public List<RedePrestadorQualificacaoAns> Qualificacoes { get; set; }
      
        public DadosDetalhamento()
        {
            Especialidades = new List<string>();
            Acessos = new List<string>();
            Contatos = new List<Contato>();
            Curriculo = new List<Curriculo>();
            Qualificacoes = new List<RedePrestadorQualificacaoAns>();
            Telefones = new List<string>();
        }
    }

}