﻿using System.Collections.Generic;

namespace MSW.Web.Models.Response
{
    public class BuscaRegioesResponse
    {
        public List<Prestador> Data { get; set; }

        public string Especialidade { get; set; }
        public string Atendimento { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Bairro { get; set; }

        public string Nome { get; set; }
        public int QuantidadeLocalizados { get; set; }

        public string DefaultLat { get; set; }
        public string DefaultLng { get; set; }
        public string MarcadoresGoogleMapsPosition { get; set; }

        public BuscaRegioesResponse()
        {
            Data = new List<Prestador>();
        }
    }
}