﻿using System.Collections.Generic;

namespace MSW.Web.Models.Response
{
    public class ProximidadeResponse
    {
        public List<DadosProximidade> Data { get; set; }
        public string Especialidade { get; set; }
        public string BuscaProximidadeTipo { get; set; }
        public string Atendimento { get; set; }
        public string Cep { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }


        public ProximidadeResponse()
        {
            Data = new List<DadosProximidade>();
        }

        public string ObterBuscaEspecialidadeTipo()
        {
            switch (BuscaProximidadeTipo)
            {
                case "L" :
                    return "Próximos a mim";
                case "R":
                    return $"Próximos a minha residência CEP {Cep} {Cidade}/{Estado}";
                default:
                    return string.Empty;
            }
        }
    }

    public class DadosProximidade
    {
        public double Distancia { get; set; }
        public Prestador Prestador { get; set; }
    }
}