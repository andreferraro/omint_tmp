﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;

namespace MSW.Web.Models.Response
{
    public class RegioesResponse
    {
        public DadosRegioes Data { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }

        public RegioesResponse()
        {
            Data = new DadosRegioes();
        }

        public List<SelectListItem> ListaEstados
        {
            get
            {
                List<SelectListItem> itens = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Selecione", Value = "", Selected = false }
                };

                itens.AddRange(Data.Regioes.Keys.Select(s => new SelectListItem { Text = s, Value = s, Selected = s.Equals(Data.EnderecoResidencial?.Estado ?? "SP", StringComparison.OrdinalIgnoreCase) }));

                return itens;
            }
        }

        public List<SelectListItem> ListaCidades
        {
            get
            {
                List<SelectListItem> itens = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Selecione", Value = "", Selected = false }
                };

                itens.AddRange(Data.Regioes
                                   .Where(s => s.Key.Equals(Data.EnderecoResidencial?.Estado ?? "SP", StringComparison.OrdinalIgnoreCase))
                                   .SelectMany(s => s.Value)
                                   .Select(s => new SelectListItem { Text = s.Key, Value = s.Key, Selected = s.Key.Equals(Data.EnderecoResidencial?.Cidade ?? "Sao Paulo", StringComparison.OrdinalIgnoreCase) }));
                return itens;
            }
        }

        public List<SelectListItem> ListaBairros
        {
            get
            {
                List<SelectListItem> itens = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Todos os Bairros", Value = "", Selected = true }
                };

                itens.AddRange(Data.Regioes
                                   .Where(s => s.Key.Equals(Data.EnderecoResidencial?.Estado ?? "SP", StringComparison.OrdinalIgnoreCase))
                                   .SelectMany(s => s.Value)
                                   .Where(s => s.Key.Equals(Data.EnderecoResidencial?.Cidade ?? "Sao Paulo", StringComparison.OrdinalIgnoreCase))
                                   .SelectMany(s => s.Value)
                                   .Select(s => new SelectListItem { Text = s, Value = s, Selected = false }));
                return itens;
            }
        }
    }

    public class DadosRegioes
    {
        public Dictionary<string, Dictionary<string, IEnumerable<string>>> Regioes { get; set; }
        public EnderecoResidencial EnderecoResidencial { get; set; }

        public DadosRegioes()
        {
            Regioes = new Dictionary<string, Dictionary<string, IEnumerable<string>>>();
        }
    }
}