﻿using MSW.Web.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace MSW.Web.Models.Response
{
    public class LocalizacaoResponse
    {
        public List<DadosLocalizacao> Data { get; set; }
        public string Especialidade { get; set; }
        public string BuscaProximidadeTipo { get; set; }
        public string Atendimento { get; set; }

        public int QuantidadeLocalizados { get; set; }
        public string Nome { get; set; }
        public string DefaultLat { get; set; }
        public string DefaultLng { get; set; }
        public string MarcadoresGoogleMapsPosition { get; set; }


        public LocalizacaoResponse()
        {
            Data = new List<DadosLocalizacao>();
        }

        public string ObterBuscaEspecialidadeTipo()
        {
            var endereco = Contexto.DadosUsuario?.Enderecos.FirstOrDefault(x => string.Equals("RES", x.TipoEndereco.Codigo, System.StringComparison.OrdinalIgnoreCase));
            
            switch (BuscaProximidadeTipo)
            {
                case "L":
                    return "Próximos a mim";
                case "R":
                    return $"Próximos a minha residência CEP {endereco?.Logradouro.CEP} {endereco?.Logradouro.Municipio.Nome}/{endereco?.Logradouro.Municipio.CodigoUF}";
                default:
                    return string.Empty;
            }
        }
    }

    public class DadosLocalizacao
    {
        public double Distancia { get; set; }

        public Prestador Prestador { get; set; } 
    }
}