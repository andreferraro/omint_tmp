﻿using System.Collections.Generic;

namespace MSW.Web.Models.Response
{
    public class PlanosAtendidosResponse
    {
        public List<DadosPlanosAtendidos> Data { get; set; }
        public string Nome { get; set; }
        public string Registro { get; set; }

        public PlanosAtendidosResponse()
        {
            Data = new List<DadosPlanosAtendidos>();
        }
    }

    public class DadosPlanosAtendidos
    {
        public string CodigoPlano { get; set; }
        public string DescricaoAnsSituacao { get; set; }
        public string DescricaoContratacao { get; set; }
        public string DescricaoDivulgacao { get; set; }
        public string DescricaoSegmentacao { get; set; }
        public string NumeroRegistro { get; set; }
    }
}