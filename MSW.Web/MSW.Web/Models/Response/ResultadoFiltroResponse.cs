﻿using System.Linq;
using MSW.Web.Helpers;
using System.Collections.Generic;
using MSW.Web.Helpers.Extensions;

namespace MSW.Web.Models.Response
{
    public class ResultadoFiltroResponse
    {
        public string ViewResult { get; set; }
        public string DefaultLat { get; }
        public string DefaultLng { get; }
        public IEnumerable<object> MarcadoresGoogleMaps { get; }

        public ResultadoFiltroResponse()
        {
            ContextoBuscaFiltro contextoBusca;
            System.Enum.TryParse(Contexto.ObterDados<string>(Consts.TIPO_FILTRO_BUSCA), out contextoBusca);

            if (contextoBusca == ContextoBuscaFiltro.BuscaPrestadores)
            {
                List<Prestador> prestadores = Contexto.ObterDados<List<Prestador>>(Consts.LISTA_DADOS_PRESTADORES);

                DefaultLat = prestadores.Where(x => !string.IsNullOrEmpty(x.GmLatitude))?.FirstOrDefault()?.GmLatitude ?? Consts.LATITUDE_PADRAO_BRASIL;
                DefaultLng = prestadores.Where(x => !string.IsNullOrEmpty(x.GmLongitude))?.FirstOrDefault()?.GmLongitude ?? Consts.LONGITUDE_PADRAO_BRASIL;
                MarcadoresGoogleMaps = prestadores?.GroupBy(x => new { x.GmLatitude, x.GmLongitude })?.Where(w => !string.IsNullOrEmpty(w.Key.GmLatitude) && !string.IsNullOrEmpty(w.Key.GmLongitude))?.Select(x => new { lat = x.Key.GmLatitude, lng = x.Key.GmLongitude });
            }
            else
            {
                List<DadosLocalizacao> dadosLocalizacao = Contexto.ObterDados<List<DadosLocalizacao>>(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE);

                DefaultLat = dadosLocalizacao?.Where(x => !string.IsNullOrEmpty(x.Prestador.GmLatitude))?.FirstOrDefault()?.Prestador.GmLatitude ?? Consts.LATITUDE_PADRAO_BRASIL;
                DefaultLng = dadosLocalizacao?.Where(x => !string.IsNullOrEmpty(x.Prestador.GmLongitude))?.FirstOrDefault()?.Prestador.GmLongitude ?? Consts.LONGITUDE_PADRAO_BRASIL;
                MarcadoresGoogleMaps = dadosLocalizacao?.GroupBy(x => new { x.Prestador.GmLatitude, x.Prestador.GmLongitude })?.Where(w => !string.IsNullOrEmpty(w.Key.GmLatitude) && !string.IsNullOrEmpty(w.Key.GmLongitude))?.Select(x => new { lat = x.Key.GmLatitude, lng = x.Key.GmLongitude });
            }
        }
    }
}