﻿using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using System.Security.Cryptography.X509Certificates;
using MSW.Web.Helpers;
using MSW.Web.Helpers.Extensions;

namespace MSW.Web.Models
{
    public class FiltroMenu
    {
        private List<FiltroMenuItem> _redes;
        private List<FiltroMenuItem> _planos;
        private List<FiltroMenuItem> _servicos;
        private List<FiltroMenuItem> _estados;
        private List<FiltroMenuItem> _cidades;
        private List<FiltroMenuItem> _especialidades;
        private List<FiltroMenuItem> _avaliacoes;
        private List<string> _codigosRedeOdonto;

        private List<KeyValuePair<string, string>> _listaRedes = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>("0001", "REDE OMINT PREMIUM DE MEDICINA"),
            new KeyValuePair<string, string>("0002", "REDE OMINT PREMIUM DE ODONTOLOGIA"),
            new KeyValuePair<string, string>("0004", "REDE OMINT ACCESS DE MEDICINA"),
            new KeyValuePair<string, string>("0009", "REDE OMINT CORPORATE MEDICINA"),
            new KeyValuePair<string, string>("0003", "REDE OMINT SKILL DE MEDICINA")
        };

        private List<KeyValuePair<string, string>> _listaPlanos = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>("0001", "Estilo"),
            new KeyValuePair<string, string>("0001", "Saúde Integral"),
            new KeyValuePair<string, string>("0001", "Medicina Completo"),
            new KeyValuePair<string, string>("0001", "Hospitalar"),
            new KeyValuePair<string, string>("0004", "Estilo"),
            new KeyValuePair<string, string>("0004", "Saúde Integral"),
            new KeyValuePair<string, string>("0004", "Medicina Completo"),
            new KeyValuePair<string, string>("0004", "Hospitalar"),
            new KeyValuePair<string, string>("0009", "Corporate"),
            new KeyValuePair<string, string>("0003", "Skill"),
            new KeyValuePair<string, string>("0002", "Premium"),
            new KeyValuePair<string, string>("0002", "Saúde Integral"),
            new KeyValuePair<string, string>("0002", "Skill")
        };

        public ReadOnlyCollection<FiltroMenuItem> Redes => _redes.AsReadOnly();
        public ReadOnlyCollection<FiltroMenuItem> Planos => _planos.AsReadOnly();

        public ReadOnlyCollection<FiltroMenuItem> Servicos => _servicos.AsReadOnly();
        public ReadOnlyCollection<FiltroMenuItem> Estados => _estados.AsReadOnly();
        public ReadOnlyCollection<FiltroMenuItem> Cidades => _cidades.AsReadOnly();
        public ReadOnlyCollection<FiltroMenuItem> Especialidades => _especialidades.AsReadOnly();
        public ReadOnlyCollection<FiltroMenuItem> Avaliacoes => _avaliacoes.AsReadOnly();
        public int QuantidadeLocalizados { get; set; }
        public bool BuscaLogada { get; set; }

        public FiltroMenu()
        {
            _redes = new List<FiltroMenuItem>();
            _planos = new List<FiltroMenuItem>();
            _servicos = new List<FiltroMenuItem>();
            _estados = new List<FiltroMenuItem>();
            _cidades = new List<FiltroMenuItem>();
            _especialidades = new List<FiltroMenuItem>();
            _avaliacoes = new List<FiltroMenuItem>();

            _codigosRedeOdonto = new List<string> { "0002", "0005", "0006" };
        }

        public void AdicionarEspecialidades(IEnumerable<Prestador> prestadores)
        {
            if (prestadores == null || !prestadores.Any())
                return;

            bool contemEspecialidade = prestadores.Any(x => x.Especialidades.Any());

            if (contemEspecialidade)
                _especialidades.AddRange(prestadores
                    .SelectMany(s => s.Especialidades)
                    .GroupBy(x => x)
                    .OrderBy(x => x.Key)
                    .Select(s => new FiltroMenuItem
                    {
                        DescricaoFiltro = s.Key.Trim(),
                        QuantidadeRegistrosFiltro = s.Count()
                    }));
            else
                _especialidades.AddRange(prestadores.GroupBy(x => x.DescricaoEspecialidade).OrderBy(x => x.Key).Select(s => new FiltroMenuItem { DescricaoFiltro = s.Key.Trim(), QuantidadeRegistrosFiltro = s.Count() }));
        }

        public void AdicionarEstados(IEnumerable<Prestador> prestadores)
        {
            if (prestadores == null || !prestadores.Any())
                return;

            IEnumerable<FiltroMenuItem> estadoSPRJ = prestadores.Where(x => !string.IsNullOrWhiteSpace(x.CodigoEstado) && x.CodigoEstado == "SP" || x.CodigoEstado == "RJ")
                                                          .OrderByDescending(x => x.CodigoEstado)
                                                          .GroupBy(x => x.CodigoEstado)
                                                          .Select(s => new FiltroMenuItem { DescricaoFiltro = s.Key.Trim(), QuantidadeRegistrosFiltro = s.Count() });

            IEnumerable<FiltroMenuItem> outrosEstados = prestadores.Where(x => !string.IsNullOrWhiteSpace(x.CodigoEstado) && x.CodigoEstado != "SP" && x.CodigoEstado != "RJ")
                                                          .GroupBy(x => x.CodigoEstado)
                                                          .Select(s => new FiltroMenuItem { DescricaoFiltro = s.Key.Trim(), QuantidadeRegistrosFiltro = s.Count() });


            _estados.AddRange(estadoSPRJ);
            _estados.AddRange(outrosEstados);
        }

        public void AdicionarCidades(IEnumerable<Prestador> prestadores)
        {
            if (prestadores == null || !prestadores.Any())
                return;

            IEnumerable<FiltroMenuItem> data = prestadores
                                                          .Where(x => !string.IsNullOrWhiteSpace(x.EnderecoCidade))
                                                          .GroupBy(x => x.EnderecoCidade)
                                                          .Select(s => new FiltroMenuItem { DescricaoFiltro = s.Key.Trim(), QuantidadeRegistrosFiltro = s.Count() })
                                                          .ToList();


            var saoPaulo = data.FirstOrDefault(x => x.DescricaoFiltro.Equals("sao paulo", StringComparison.OrdinalIgnoreCase));
            var rio = data.FirstOrDefault(x => x.DescricaoFiltro.Equals("rio de janeiro", StringComparison.OrdinalIgnoreCase));

            if (saoPaulo != null)
            {
                _cidades.Add(saoPaulo);
            }

            if (rio != null)
            {
                _cidades.Add(rio);
            }

            _cidades.AddRange(data.Where(x=>!x.DescricaoFiltro.Equals("sao paulo", StringComparison.OrdinalIgnoreCase) && !x.DescricaoFiltro.Equals("rio de janeiro", StringComparison.OrdinalIgnoreCase)));

        }

        public void AdicionarAvaliacoes(IEnumerable<Prestador> prestadores)
        {
            if (prestadores == null || !prestadores.Any())
                return;

            IEnumerable<FiltroMenuItem> data = prestadores.GroupBy(x => x.ScoreTotal)
                                                          .Select(s => new FiltroMenuItem { QuantidadeRegistrosFiltro = s.Count(), DescricaoFiltro = s.Key.ToString() });
            _avaliacoes.AddRange(data);
        }

        public void AdicionarPlano(IEnumerable<Prestador> prestadores)
        {
            if (!prestadores.Any())
                return;

            IEnumerable<FiltroMenuItem> filtro;
            IEnumerable<string> codigosRede = prestadores.GroupBy(s => s.CodigoRede).Select(s => s.Key);

            var planosUsuarioLogado = Contexto.DadosUsuario?
                                              .DadosRede
                                              .Planos;

            if (planosUsuarioLogado != null)
                filtro = planosUsuarioLogado.Where(x => codigosRede.Contains(x.CodigoRede.Trim()))
                                            .GroupBy(x => x.CodigoPlano.Trim())
                                            .Select(s => new FiltroMenuItem
                                            {
                                                DescricaoFiltro = s.Key,
                                                Codigo = string.Join(",", s.Select(k => k.CodigoRede)),
                                                QuantidadeRegistrosFiltro = prestadores.Where(w => string.Join(",", s.Select(k => k.CodigoRede)).Contains(w.CodigoRede)).Count()
                                            });  
            else                                            
                filtro = _listaPlanos.Where(x => codigosRede.Contains(x.Key))
                                     .GroupBy(x => x.Value)
                                     .Select(s => new FiltroMenuItem
                                     {
                                         DescricaoFiltro = s.Key,
                                         Codigo = string.Join(",", s.Select(k => k.Key)),
                                         QuantidadeRegistrosFiltro = prestadores.Where(w => string.Join(",", s.Select(k => k.Key)).Contains(w.CodigoRede)).Count()
                                     });

            _planos.AddRange(filtro);
        }

        public void AdicionarRede(AgregadorPlanoRede[] redes)
        {
            _redes.AddRange(_listaRedes.Where(w => redes.Any(a => a.CodigoRede == w.Key)).Select(s => new FiltroMenuItem
            {
                Codigo = s.Key,
                DescricaoFiltro = s.Value,
                QuantidadeRegistrosFiltro = redes.FirstOrDefault(f => f.CodigoRede == s.Key).Quantidade

            }));
        }

        public void AdicionarServico(IEnumerable<Prestador> prestadores)
        {
            if (prestadores == null || !prestadores.Any())
                return;

            var planosOdonto = prestadores.Count(x => _codigosRedeOdonto.Contains(x.CodigoRede.Trim()));
            var planosMedicos = prestadores.Count() - planosOdonto;

            if (planosOdonto > 0)
                _servicos.Add(new FiltroMenuItem { DescricaoFiltro = "Odontológico", QuantidadeRegistrosFiltro = planosOdonto });

            if (planosMedicos > 0)
                _servicos.Add(new FiltroMenuItem { DescricaoFiltro = "Médico", QuantidadeRegistrosFiltro = planosMedicos });
        }

        public string ObterRedeDoPlano(string plano)
        {
            var planos = Contexto.DadosUsuario?
                                 .DadosRede
                                 .Planos;

            if (planos != null)
                return planos.FirstOrDefault(x => x.CodigoPlano == plano)
                             .CodigoRede;

            return _planos
                .First(p => p.DescricaoFiltro == plano)
                .Codigo;
        }

    }

    public class FiltroMenuItem
    {
        public string DescricaoFiltro { get; set; }
        public int QuantidadeRegistrosFiltro { get; set; }
        public string Codigo { get; set; }
    }
}