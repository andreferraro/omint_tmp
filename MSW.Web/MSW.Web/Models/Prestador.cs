﻿using System;
using System.Collections.Generic;

namespace MSW.Web.Models
{
    public class Prestador
    {
        public string CodigoRede { get; set; }
        public string CodigoPrestador { get; set; }
        public string DescricaoPrestador { get; set; }
        public string SrEndereco { get; set; }
        public string TipoLogradouro { get; set; }
        public string EnderecoRua { get; set; }
        public string EnderecoNumero { get; set; }
        public string EnderecoComplemento { get; set; }
        public string EnderecoBairro { get; set; }
        public string EnderecoCidade { get; set; }
        public string CodigoEstado { get; set; }
        public string CodigoCep { get; set; }
        public string Zona { get; set; }
        public string DescricaoRede { get; set; }
        public List<string> Especialidades { get; set; }
        public string DescricaoEspecialidade { get; set; }
        public string EnderecoTelefone { get; set; }
        public string EnderecoTelefoneDdd { get; set; }
        public string EnderecoTelefonePrefixo { get; set; }
        public string GmLatitude { get; set; }
        public string GmLongitude { get; set; }
        public List<string> Telefones { get; set; }
        public int ScoreTotal { get; set; }
        public int NrAvaliacao { get; set; }
        public string DescricaoObservacao { get; set; }
        public string DescricaoRegistro { get; set; }
        public DateTime? DataDescredenciamento { get; set; }
        public string Distancia { get; set; }
        public int DistanciaMetros { get; set; }

        public string TelefoneFormatado
        {
            get
            {
                if (string.IsNullOrWhiteSpace(EnderecoTelefonePrefixo) || string.IsNullOrWhiteSpace(EnderecoTelefone))
                    return string.Empty;

                if (string.IsNullOrWhiteSpace(EnderecoTelefoneDdd))
                    return $"{EnderecoTelefonePrefixo}-{EnderecoTelefone}";

                return $"({EnderecoTelefoneDdd}) {EnderecoTelefonePrefixo}-{EnderecoTelefone}";   
            }
        }

        public Prestador()
        {
            Telefones = new List<string>();
            Especialidades = new List<string>();
        }
    }
}