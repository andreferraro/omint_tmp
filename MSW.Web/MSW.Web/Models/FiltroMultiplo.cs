﻿
using System.Collections.Generic;

namespace MSW.Web.Models
{
    public class FiltroMultiplo
    {
        public FiltroMultiplo()
        {
            Estados = new List<string>();
            Cidades = new List<string>();
            Especialidades = new List<string>();
        }
        public List<string> Estados { get; set; }
        public List<string> Cidades { get; set; }
        public List<string> Especialidades { get; set; }
    }
}