﻿namespace MSW.Web.Models.Request
{
    public class BuscaPlanosAtendidosRequest
    {
        public string Rede { get; set; }
        public string CodigoPrestador { get; set; }
        public string NomePrestador { get; set; }
        public string Registro { get; set; }
    }
}