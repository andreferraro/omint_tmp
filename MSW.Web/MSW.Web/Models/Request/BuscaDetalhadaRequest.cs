﻿namespace MSW.Web.Models.Request
{
    public class BuscaDetalhadaRequest
    {
        public string Rede { get; set; }
        public string CodigoPrestador { get; set; }
        public string SrEndereco { get; set; }
        public bool Inativos { get; set; }
        public string NomePrestador { get; set; }
        public string Registro { get; set; }
        public string Endereco { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string CEP { get; set; }
        public string DescricaoObservacao { get; set; }
        public string DataDescredenciamento { get; set; }
        public string Estado { get; set; }
        public int NrAvaliacao { get; set; }
        public string Telefone { get; set; }
        public int ScoreTotal { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}