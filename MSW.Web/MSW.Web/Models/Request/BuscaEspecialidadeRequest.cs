﻿using System.Collections.Specialized;

namespace MSW.Web.Models.Request
{
    public class BuscaEspecialidadeRequest
    {
        private string rede;


        public string Vinculo { get; set; }

        public string Rede
        {
            get { return rede; }
            set { rede = value ?? string.Empty; }
        }
        
        public string TipoAtendimento { get; set; }
        public string NomeEspecialidade { get; set; }
        public string CodigoEspecialidade { get; set; }
        public bool Inativos { get; set; }

        public BuscaEspecialidadeRequest()
        {
        }

        public BuscaEspecialidadeRequest(NameValueCollection query)
        {
            NomeEspecialidade = query["especialidade"];
            CodigoEspecialidade = query["codigoEspecialidade"];
            TipoAtendimento = query["tipoAtendimento"];
            Rede = query["rede"];
        }
    }
}