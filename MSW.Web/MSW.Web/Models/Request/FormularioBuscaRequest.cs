﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSW.Web.Models.Request
{
    public class FormularioBuscaRequest
    {
        public string CodigoRede { get; set; }
        public string Descricao { get; set; }
        public string Plano { get; set; }
    }
}