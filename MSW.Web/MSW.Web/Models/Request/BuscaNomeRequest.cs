﻿namespace MSW.Web.Models.Request
{
    public class BuscaNomeRequest
    {
        public string Vinculo { get; set; }
        public string Nome { get; set; }
        public bool Inativos { get; set; }
        public string CodigoRede { get; set; }
        public string DescricaoRede { get; set; }
        public string Plano { get; set; }
    }
}