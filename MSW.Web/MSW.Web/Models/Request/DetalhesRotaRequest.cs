﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSW.Web.Models.Request
{
    public class DetalhesRotaRequest
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Nome { get; set; }
        public int QuantidadeLocalizados { get; set; }
        public string Prestador { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get; set; }
    }
}