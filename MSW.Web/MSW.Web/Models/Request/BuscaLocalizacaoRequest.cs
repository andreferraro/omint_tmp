﻿namespace MSW.Web.Models.Request
{
    public class BuscaLocalizacaoRequest
    {
        public string Vinculo { get; set; }
        public string Rede { get; set; }
        public string Cep { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }
        public string CodigoEstado { get; set; }
        public string Atendimento { get; set; }
        public string Especialidade { get; set; }
        public string CodigoEspecialidade { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string TipoAtendimento { get; set; }
    }
}