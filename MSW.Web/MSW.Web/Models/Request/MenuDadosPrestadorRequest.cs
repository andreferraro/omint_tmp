﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSW.Web.Models.Request
{
    public class MenuDadosPrestadorRequest
    {
        public List<Prestador> Prestadores { get; set; }
    }
}