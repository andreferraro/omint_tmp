﻿namespace MSW.Web.Models.Request
{
    public class BuscaProximidadeLocalRequest 
    {
        private string _rede;

        public string Vinculo { get; set; }
        public string Rede
        {
            get { return _rede; }
            set { _rede = value ?? string.Empty; }
        }
        public string TipoAtendimento { get; set; }
        public string CodigoEspecialidade { get; set; }
        public string Especialidade { get; set; }
        public string Atendimento { get; set; }
        public bool Inativos { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string BuscaProximidadeTipo { get; set; }
    }
}