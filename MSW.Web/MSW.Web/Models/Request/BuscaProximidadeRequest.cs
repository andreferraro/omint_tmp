﻿namespace MSW.Web.Models.Request
{
    public class BuscaProximidadeRequest
    {
        public string Vinculo { get; set; }
        public string Rede { get; set; }
        public string TipoAtendimento { get; set; }
        public string CodigoEspecialidade { get; set; }
        public bool Inativos { get; set; }
        public string BuscaProximidadeTipo { get; set; }
        public string Especialidade { get; set; }
        public string Atendimento { get; set; }
    }
}