﻿using MSW.Web.Models.Response;
using System.Collections.Generic;

namespace MSW.Web.Models
{
    public class DadosUsuario
    {
        public string Vinculo { get; set; }
        public List<Endereco> Enderecos { get; set; }
        public DadosRedeCredenciada DadosRede { get; set; }
        public DadosRegioes Localizacao { get; set; }

        public DadosUsuario()
        {
            Enderecos = new List<Endereco>();
            DadosRede = new DadosRedeCredenciada();
        }
    }
    
    public class DadosRedeCredenciada
    {
        public List<RedeTipoAtendimentoBeneficiario> TiposAtendimentoPadrao { get; set; }
        public List<RedeCredenciadaVinculo> RedeCredenciadaVinculo { get; set; }
        public List<TipoAtendimentoBeneficiario> Planos { get; set; }

        public DadosRedeCredenciada()
        {
            TiposAtendimentoPadrao = new List<RedeTipoAtendimentoBeneficiario>();
            RedeCredenciadaVinculo = new List<RedeCredenciadaVinculo>();
            Planos = new List<TipoAtendimentoBeneficiario>();
        }
    }

    public class RedeCredenciadaVinculo
    {
        public string CodigoRede { get; set; }
        public string Descricao { get; set; }
    }

    public class RedeTipoAtendimentoBeneficiario
    {
        public string CodigoProduto { get; set; }
        public string CodigoRede { get; set; }
        public string DescricaoTipoAtendimento { get; set; }
        public int? IdPrioridade { get; set; }
        public int? IdTipoAtendimento { get; set; }
        public string TipoView { get; set; }
    }

    public class TipoAtendimentoBeneficiario
    {
        public string CodigoPlano { get; set; }
        public string CodigoProduto { get; set; }
        public string CodigoRede { get; set; }
        public string DescricaoRede { get; set; }
        public int? IdBeneficiarioPlano { get; set; }
        public int? IdPrioridade { get; set; }
    }
}