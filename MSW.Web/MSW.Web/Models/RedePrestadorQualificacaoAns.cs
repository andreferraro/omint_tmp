﻿namespace MSW.Web.Models
{
    public class RedePrestadorQualificacaoAns
    {
        public int? CodigoCertificacao { get; set; }
        public string CodigoIcone { get; set; }
        public string CodigoPrestador { get; set; }
        public string CodigoRede { get; set; }
        public string DescricaoCertificacao { get; set; }
        public string TipoCertificacao { get; set; }
        public string TipoView { get; set; }
        public string UrlIconeImagem { get; set; }
    }
}