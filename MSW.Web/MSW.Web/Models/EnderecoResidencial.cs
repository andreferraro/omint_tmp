﻿namespace MSW.Web.Models
{
    public class Endereco
    {
        public Logradouro Logradouro { get; set; }
        public TipoEndereco TipoEndereco { get; set; }
    }

    public class Logradouro
    {
        public string Bairro { get; set; }
        public string CEP { get; set; }
        public Municipio Municipio { get; set; }
    }

    public class TipoEndereco
    {
        public string Codigo { get; set; }
    }

    public class Municipio
    {
        public string CodigoUF { get; set; }
        public string Nome { get; set; }
    }

    public class EnderecoResidencial
    {
        public string Cidade { get; set; }
        public string Estado { get; set; }
    }

}