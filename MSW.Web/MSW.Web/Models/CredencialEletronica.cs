﻿using System;

namespace MSW.Web.Models.Response
{
    public class CredencialEletronica
    {
        public string AcomodacaoTerceira { get; set; }
        public int BeneficiarioPlanoMedicina { get; set; }
        public int BeneficiarioPlanoOdonto { get; set; }
        public string CaminhoImagemCredenciaMedicina { get; set; }
        public string CaminhoImagemCredenciaOdonto { get; set; }
        public string CodigoCredencialMedicina { get; set; }
        public string CodigoCredencialOdonto { get; set; }
        public string CodigoPlanoMedicina { get; set; }
        public string CodigoPlanoOdonto { get; set; }
        public DateTime DataFimCredencialMedicina { get; set; }
        public DateTime DataFimCredencialOdonto { get; set; }
        public DateTime DataIngresso { get; set; }
        public DateTime DataNascimento { get; set; }
        public string DescricaoCptMedicina { get; set; }
        public string DescricaoCptOdonto { get; set; }
        public string DescricaoCredencialMedicina { get; set; }
        public string DescricaoCredencialOdonto { get; set; }
        public string NomeBeneficiario { get; set; }
        public string NumeroCns { get; set; }
        public string NumeroCodigoBarra { get; set; }
        public string NumeroCredenciaMedicina { get; set; }
        public string NumeroCredenciaOdonto { get; set; }
        public string NumeroTerceira { get; set; }
        public string PlanoTerceira { get; set; }
        public string Sexo { get; set; }
    }
}