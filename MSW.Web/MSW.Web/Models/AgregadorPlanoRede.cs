﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSW.Web.Models
{
    public class AgregadorPlanoRede
    {
        public string CodigoRede { get; set; }

        public string Descricao { get; set; }

        public int Quantidade { get; set; }
    }
}