﻿using PagedList;
using System.Linq;
using System.Web.Mvc;
using MSW.Web.Helpers;
using MSW.Web.Helpers.Extensions;
using MSW.Web.Models.Response;
using Newtonsoft.Json;
using System.Collections.Generic;
using MSW.Web.Models;

namespace MSW.Web.Controllers
{
    public class PaginacaoController : Controller
    {
        public ActionResult PaginarPrestadores(int? page)
        {
            int currentPageIndex = page ?? Consts.DEFAULT_PAGE_INIT;
            ResultadoBuscaResponse response = new ResultadoBuscaResponse();
            List<Prestador> prestadores = Contexto.ObterDados<List<Prestador>>(Consts.LISTA_DADOS_PRESTADORES);

            response.DefaultLat = prestadores?.Where(x => !string.IsNullOrEmpty(x.GmLatitude))?.FirstOrDefault()?.GmLatitude ?? Consts.LATITUDE_PADRAO_BRASIL;
            response.DefaultLng = prestadores?.Where(x => !string.IsNullOrEmpty(x.GmLongitude))?.FirstOrDefault()?.GmLongitude ?? Consts.LONGITUDE_PADRAO_BRASIL;
            response.MarcadoresGoogleMapsPosition = JsonConvert.SerializeObject(prestadores?.GroupBy(x =>  new {
                                                                                                                   x.GmLatitude,
                                                                                                                   x.GmLongitude,
                                                                                                                   x.DescricaoPrestador
                                                                                                               })
                                                                                                               ?.Where(w => !string.IsNullOrEmpty(w.Key.GmLatitude) && !string.IsNullOrEmpty(w.Key.GmLongitude))
                                                                                                               ?.Select(x => new { lat = x.Key.GmLatitude, lng = x.Key.GmLongitude, nome = x.Key.DescricaoPrestador }));

            ViewBag.PagedList = prestadores?.ToPagedList(currentPageIndex, Consts.DEFAULT_PAGE_SIZE);
            return View("~/Views/Resultados/ResultadoBusca.cshtml", response);
        }

        public ActionResult PaginarPrestadoresProximidade(int? page)
        {
            int currentPageIndex = page ?? Consts.DEFAULT_PAGE_INIT;
            LocalizacaoResponse response = new LocalizacaoResponse();
            List<DadosLocalizacao> dadosLocalizacao = Contexto.ObterDados<List<DadosLocalizacao>>(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE);

            response.DefaultLat = dadosLocalizacao?.Where(x => !string.IsNullOrEmpty(x.Prestador.GmLatitude))?.FirstOrDefault()?.Prestador?.GmLatitude ?? Consts.LATITUDE_PADRAO_BRASIL;
            response.DefaultLng = dadosLocalizacao?.Where(x => !string.IsNullOrEmpty(x.Prestador.GmLongitude))?.FirstOrDefault()?.Prestador?.GmLongitude ?? Consts.LONGITUDE_PADRAO_BRASIL;
            response.MarcadoresGoogleMapsPosition = JsonConvert.SerializeObject(dadosLocalizacao?.GroupBy(x => new
                                                                                                    {
                                                                                                        x.Prestador.GmLatitude,
                                                                                                        x.Prestador.GmLongitude
                                                                                                    })
                                                                                                    ?.Where(w => !string.IsNullOrEmpty(w.Key.GmLatitude) && !string.IsNullOrEmpty(w.Key.GmLongitude))
                                                                                                    ?.Select(x => new { lat = x.Key.GmLatitude, lng = x.Key.GmLongitude }));

            ViewBag.PagedList = dadosLocalizacao?.ToPagedList(currentPageIndex, Consts.DEFAULT_PAGE_SIZE);
            return View("~/Views/Resultados/ResultadoBuscaProximidade.cshtml", response);
        }
    }
}