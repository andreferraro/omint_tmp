﻿using System;
using System.Linq;
using MSW.Web.Models;
using System.Web.Mvc;
using MSW.Web.Helpers;
using MSW.Web.Models.Request;
using MSW.Web.Models.Response;
using MSW.Web.Helpers.Extensions;
using Newtonsoft.Json;
using PagedList;
using System.Collections.Generic;
using System.Configuration;

namespace MSW.Web.Controllers
{
    public class BuscaPrestadoresController : PaginacaoController
    {
        private readonly OmintHttpRequest request = new OmintHttpRequest();

        public ActionResult Index()
        {
            string vinculo = Request.QueryString["Vinculo"] ?? Contexto.DadosUsuario?.Vinculo;
            ResultadoBuscaResponse buscaModel = new ResultadoBuscaResponse();

            if (!string.IsNullOrEmpty(vinculo) && Contexto.DadosUsuario == null)
            {
                ResultadoRequest response = request.Get("/RedeCredenciada/ObterDadosUsuario", new { vinculo = vinculo });

                if (response.Erro != null)
                {
                    ViewBag.Erro = response.Erro.Mensagem;
                    return View(buscaModel);
                }

                TituloResponse result = response.ObterResultado<TituloResponse>();

                Contexto.DadosUsuario = new DadosUsuario
                {
                    Vinculo = vinculo,
                    Enderecos = result.Data.Titulo.Enderecos,
                    DadosRede = result.Data.DadosRede
                };
            }

            buscaModel.Vinculo = vinculo;

            return View("ObterFormularioBusca", buscaModel);
        }

        [HttpPost]
        public ActionResult ObterFormularioBusca(FormularioBuscaRequest request)
        {
            ResultadoBuscaResponse response = new ResultadoBuscaResponse
            {
                CodigoRede = request.CodigoRede,
                DescricaoRede = request.Descricao,
                Plano = request.Plano
            };

            return PartialView("~/Views/Shared/_FormularioBuscaNome.cshtml", response);
        }

        [HttpPost]
        public ActionResult ResultadoBusca(BuscaNomeRequest request)
        {
            request.Vinculo = string.IsNullOrEmpty(request.Vinculo) ? Contexto.DadosUsuario?.Vinculo : request.Vinculo; 

            ResultadoRequest response = this.request.Get("/RedeCredenciada/ObterPorNome", request);
            ResultadoBuscaResponse redePrestadorResponse = new ResultadoBuscaResponse();

            if (response.Erro != null)
            {
                ViewBag.Erro = response.Erro.Mensagem;
                return View("~/Views/Resultados/ResultadoBusca.cshtml", redePrestadorResponse);
            }

            redePrestadorResponse = response.ObterResultado<ResultadoBuscaResponse>();
            List<Prestador> prestadores = redePrestadorResponse?.Data.OrderByDescending(x => x.ScoreTotal).ToList();

            if (prestadores != null)
            {
                string codigoRede = Contexto.ObterDados<string>(Consts.CODIGO_REDE);

                if (!string.IsNullOrEmpty(request.CodigoRede))
                {
                    if (request.CodigoRede != codigoRede)
                        Contexto.AdicionarDados(Consts.CODIGO_REDE, request.CodigoRede);
                }

                Contexto.AdicionarDados(Consts.RESULTADO_PESQUISA_SEM_FILTRO, prestadores);
                Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES, prestadores);
                Contexto.AdicionarDados(Consts.TIPO_FILTRO_BUSCA, ContextoBuscaFiltro.BuscaPrestadores.ToString());
            }

            redePrestadorResponse.Nome = request.Nome;
            redePrestadorResponse.QuantidadeLocalizados = redePrestadorResponse.Data.Count;

            redePrestadorResponse.DefaultLat = redePrestadorResponse.Data?.Where(x => !string.IsNullOrEmpty(x.GmLatitude))?.FirstOrDefault()?.GmLatitude ?? Consts.LATITUDE_PADRAO_BRASIL;
            redePrestadorResponse.DefaultLng = redePrestadorResponse.Data?.Where(x => !string.IsNullOrEmpty(x.GmLongitude))?.FirstOrDefault()?.GmLongitude ?? Consts.LONGITUDE_PADRAO_BRASIL;
            redePrestadorResponse.MarcadoresGoogleMapsPosition = JsonConvert.SerializeObject(redePrestadorResponse.Data
                                                                                                                  ?.GroupBy(x => 
                                                                                                                  new
                                                                                                                  {
                                                                                                                     x.GmLatitude,
                                                                                                                     x.GmLongitude,
                                                                                                                     x.DescricaoPrestador
                                                                                                                  })
                                                                                                                  ?.Where(w => !string.IsNullOrEmpty(w.Key.GmLatitude) && !string.IsNullOrEmpty(w.Key.GmLongitude))
                                                                                                                  ?.Select(x => new { nome = x.Key.DescricaoPrestador, lat = x.Key.GmLatitude, lng = x.Key.GmLongitude }));
            ViewBag.PagedList = prestadores.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE);
            return View("~/Views/Resultados/ResultadoBusca.cshtml", redePrestadorResponse);
        }

        [HttpPost]
        public ActionResult ResultadoBuscaAvancada(BuscaEspecialidadeRequest request)
        {
            ResultadoRequest response = this.request.Get("/RedeCredenciada/ObterPorEspecialidade", request);

            if (response.Erro != null)
            {
                ViewBag.Erro = response.Erro.Mensagem;
                return PartialView("~/Views/Resultados/_ResultadoBuscaAvancada.cshtml", new RedeEspecialidadeResponse());
            }

            RedeEspecialidadeResponse resultadoOperacao = response.ObterResultado<RedeEspecialidadeResponse>();
            return PartialView("~/Views/Resultados/_ResultadoBuscaAvancada.cshtml", resultadoOperacao);
        }

        [HttpPost]
        public ActionResult Detalhes(BuscaDetalhadaRequest request)
        {
            object reqParams = new
            {
                Rede = request.Rede,
                CodigoPrestador = request.CodigoPrestador,
                SrEndereco = request.SrEndereco,
                Inativos = request.Inativos
            };

            ResultadoRequest response = this.request.Get("/RedeCredenciada/ObterDetalhamento", reqParams);

            if (response.Erro != null)
            {
                ViewBag.Erro = response.Erro.Mensagem;
                return PartialView("~/Views/Resultados/_DetalhesBusca.cshtml", new DetalhamentoResponse());
            }

            DetalhamentoResponse resultadoOperacao = response.ObterResultado<DetalhamentoResponse>();

            ContextoBuscaFiltro contextoBusca;
            Enum.TryParse(Contexto.ObterDados<string>(Consts.TIPO_FILTRO_BUSCA), out contextoBusca);

            List<Prestador> prestadores = contextoBusca == ContextoBuscaFiltro.BuscaPrestadores ?
            Contexto.ObterDados<List<Prestador>>(Consts.LISTA_DADOS_PRESTADORES) :
            Contexto.ObterDados<List<DadosLocalizacao>>(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE)?.Select(s => s.Prestador).ToList();

            resultadoOperacao.Data.Especialidades = prestadores.Where(p => p.CodigoPrestador == request.CodigoPrestador).FirstOrDefault()?.Especialidades ?? new List<string>();
            resultadoOperacao.Data.Telefones = prestadores.Where(p => p.CodigoPrestador == request.CodigoPrestador).FirstOrDefault()?.Telefones ?? new List<string>();

            RedeCredenciadaVinculo redeCredenciadaVinculo = Contexto.DadosUsuario?.DadosRede.RedeCredenciadaVinculo.FirstOrDefault(x => x.CodigoRede.Equals(request.Rede, StringComparison.OrdinalIgnoreCase));

            resultadoOperacao.NomePrestador = request.NomePrestador;
            resultadoOperacao.Rede = redeCredenciadaVinculo?.Descricao?? request.Rede;
            resultadoOperacao.Registro = request.Registro;
            resultadoOperacao.Bairro = request.Bairro;
            resultadoOperacao.CEP = request.CEP.FormatCep();
            resultadoOperacao.Cidade = $"{request.Cidade}/{request.Estado}";
            resultadoOperacao.Complemento = request.Complemento;
            resultadoOperacao.Endereco = request.Endereco;
            resultadoOperacao.DescricaoObservacao = request.DescricaoObservacao;
            resultadoOperacao.DataDescredenciamento = request.DataDescredenciamento;
            resultadoOperacao.CodigoPrestador = request.CodigoPrestador;
            resultadoOperacao.CodigoRede = request.Rede;
            resultadoOperacao.NrAvaliacao = request.NrAvaliacao;
            resultadoOperacao.Telefone = request.Telefone;
            resultadoOperacao.ScoreTotal = request.ScoreTotal;
            resultadoOperacao.Latitude = request.Latitude;
            resultadoOperacao.Longitude = request.Longitude;

            return PartialView("~/Views/Resultados/_DetalhesBusca.cshtml", resultadoOperacao);
        }

        [HttpPost]
        public ActionResult PlanosAtendidos(BuscaPlanosAtendidosRequest request)
        {
            object reqParams = new
            {
                Rede = request.Rede ?? Contexto.ObterDados<string>(Consts.CODIGO_REDE),
                CodigoPrestador = request.CodigoPrestador
            };

            ResultadoRequest response = this.request.Get("/RedeCredenciada/ObterPlanosAtendidos", reqParams);
            PlanosAtendidosResponse resultadoOperacao = new PlanosAtendidosResponse();

            if (response.Erro != null)
            {
                ViewBag.Erro = response.Erro.Mensagem;
                return PartialView("~/Views/Resultados/_ResultadoPlanosAtendidos.cshtml", resultadoOperacao);
            }

            resultadoOperacao = response.ObterResultado<PlanosAtendidosResponse>();
            return PartialView("~/Views/Resultados/_ResultadoPlanosAtendidos.cshtml", resultadoOperacao);
        }

        public ActionResult BuscaAvancada()
        {
            BuscaEspecialidadeRequest buscaEspecialidade = new BuscaEspecialidadeRequest();
            return View(buscaEspecialidade);
        }

        public ActionResult BuscaEspecialidade()
        {
            BuscaEspecialidadeRequest buscaEspecialidade = GetBuscaEspecialidade();
            ResultadoRequest response = this.request.Get("/RedeCredenciada/ObterPorEspecialidade", buscaEspecialidade);
            RedeEspecialidadeResponse redeEspecialidade = response.Erro == null ?
                response.ObterResultado<RedeEspecialidadeResponse>() : 
                new RedeEspecialidadeResponse();

            ViewData["resultadoOperacao"] = redeEspecialidade;
            return View(buscaEspecialidade);
        }

        public ActionResult BuscaEspecialidadeNome()
        {
            BuscaEspecialidadeRequest buscaEspecialidade = GetBuscaEspecialidade();

            return View(buscaEspecialidade);
        }

        private BuscaEspecialidadeRequest GetBuscaEspecialidade()
        {
            string tipoAtendimento = Request.QueryString["TipoAtendimento"];

            BuscaEspecialidadeRequest buscaEspecialidade = new BuscaEspecialidadeRequest
            {
                Vinculo = Contexto.DadosUsuario?.Vinculo,
                Rede = ObterCodigoRede(tipoAtendimento),
                TipoAtendimento = tipoAtendimento
            };

            string codigoRede = Contexto.ObterDados<string>(Consts.CODIGO_REDE);

            if (!string.IsNullOrEmpty(buscaEspecialidade.Rede))
            {
                if (buscaEspecialidade.Rede != codigoRede)
                    Contexto.AdicionarDados(Consts.CODIGO_REDE, buscaEspecialidade.Rede);
            }

            return buscaEspecialidade;
        }

        private string ObterCodigoRede(string tipoAtendimento)
        {
            if (Contexto.DadosUsuario == null)
                return string.Empty;

            return Consts.TIPO_ATENDIMENTO_ODONTO.Equals(tipoAtendimento, StringComparison.OrdinalIgnoreCase) ? "0002" : "0001";
        }

        [HttpPost]
        public JsonResult ValidarUsuario(string usuario, string senha)
        {
            var omintusuario = ConfigurationManager.AppSettings["buscaCredenciada:UsurioPadrao"];
            var omintsenha = ConfigurationManager.AppSettings["buscaCredenciada:SenhaPadrao"];
            return Json(new { Autorizado = omintusuario.Equals(usuario) && omintsenha.Equals(senha) });
        }
    }
}