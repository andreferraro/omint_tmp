﻿using System;
using System.Linq;
using MSW.Web.Models;
using System.Web.Mvc;
using MSW.Web.Helpers;
using MSW.Web.Models.Request;
using MSW.Web.Models.Response;
using System.Collections.Generic;
using Newtonsoft.Json;
using MSW.Web.Helpers.Extensions;
using PagedList;
using System.Web;

namespace MSW.Web.Controllers
{
    public class LocalizacaoController : PaginacaoController
    {
        private readonly OmintHttpRequest request = new OmintHttpRequest();

        public ActionResult BuscaLocalizacao()
        {
            RedeTipoAtendimentoBeneficiario atendimento = Contexto.DadosUsuario?.DadosRede.TiposAtendimentoPadrao.FirstOrDefault(x => x.TipoView.Equals(Request.QueryString["TipoAtendimento"], StringComparison.OrdinalIgnoreCase));
            Endereco endereco = Contexto.DadosUsuario?.Enderecos.FirstOrDefault(x => string.Equals("RES", x.TipoEndereco.Codigo, System.StringComparison.OrdinalIgnoreCase));

            BuscaLocalizacaoRequest buscaLocalizacao = new BuscaLocalizacaoRequest
            {
                Atendimento = atendimento?.DescricaoTipoAtendimento,
                Especialidade = Request.QueryString["Especialidade"],
                Cep = endereco?.Logradouro.CEP,
                Cidade = endereco?.Logradouro.Municipio.Nome,
                CodigoEstado = endereco?.Logradouro.Municipio.CodigoUF,
                Vinculo = Contexto.DadosUsuario?.Vinculo,
                Rede = Request.QueryString["Rede"] ?? "",
                TipoAtendimento = Request.QueryString["TipoAtendimento"],
                CodigoEspecialidade = Request.QueryString["CodigoEspecialidade"]
            };

            return View(buscaLocalizacao);
        }

        [ChildActionOnly]
        public ActionResult ObterRegioes(BuscaEspecialidadeRequest request)
        {
            ResultadoRequest response = this.request.Get("/RedeCredenciada/ObterRegioes", request);

            if (response.Erro != null)
            {
                ViewBag.Erro = response.Erro.Mensagem;
                return PartialView("_BuscaRegiao", new RegioesResponse());
            }

            RegioesResponse resultadoOperacao = response.ObterResultado<RegioesResponse>();

            if (Contexto.DadosUsuario != null && Contexto.DadosUsuario.Localizacao == null)
            {
                Contexto.DadosUsuario.Localizacao = resultadoOperacao.Data;
            }

            return PartialView("_BuscaRegiao", resultadoOperacao);
        }

        [HttpPost, ActionName("BuscaRegiao")]
        public ActionResult ObterPorRegiao(BuscaRegiaoRequest request)
        {
            object reqParams = new
            {
                Vinculo = request.Vinculo,
                Rede = request.Rede ?? "",
                TipoAtendimento = request.TipoAtendimento,
                CodigoEspecialidade = request.CodigoEspecialidade,
                CodigoEstado = request.CodigoEstado,
                Cidade = request.Cidade,
                Bairro = request.Bairro ?? string.Empty,
                Inativos = request.Inativos
            };

            ResultadoRequest response = this.request.Get("/RedeCredenciada/ObterPorRegiao", reqParams);

            if (response.Erro != null)
            {
                ViewBag.Erro = response.Erro.Mensagem;
                return View("~/Views/Resultados/ResultadoBusca.cshtml", new ResultadoBuscaResponse());
            }

            ResultadoBuscaResponse resultadoOperacao = response.ObterResultado<ResultadoBuscaResponse>();
            List<Prestador> prestadores = resultadoOperacao.Data.OrderByDescending(x => x.ScoreTotal).ToList();

            if (prestadores != null)
            {
                Contexto.AdicionarDados(Consts.RESULTADO_PESQUISA_SEM_FILTRO, prestadores);
                Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES, prestadores);
                Contexto.AdicionarDados(Consts.TIPO_FILTRO_BUSCA, ContextoBuscaFiltro.BuscaPrestadores.ToString());
            }

            resultadoOperacao.QuantidadeLocalizados = resultadoOperacao.Data.Count;

            resultadoOperacao.DefaultLat = prestadores?.Where(x => !string.IsNullOrEmpty(x.GmLatitude))?.FirstOrDefault()?.GmLatitude ?? Consts.LATITUDE_PADRAO_BRASIL;
            resultadoOperacao.DefaultLng = prestadores?.Where(x => !string.IsNullOrEmpty(x.GmLongitude))?.FirstOrDefault()?.GmLongitude ?? Consts.LONGITUDE_PADRAO_BRASIL;
            resultadoOperacao.MarcadoresGoogleMapsPosition = JsonConvert.SerializeObject(prestadores?.GroupBy(x =>
                                                                                                                  new
                                                                                                                  {
                                                                                                                      x.GmLatitude,
                                                                                                                      x.GmLongitude,
                                                                                                                      x.DescricaoPrestador
                                                                                                                  })
                                                                                                                  ?.Where(w => !string.IsNullOrEmpty(w.Key.GmLatitude) && !string.IsNullOrEmpty(w.Key.GmLongitude))
                                                                                                                  ?.Select(x => new { lat = x.Key.GmLatitude, lng = x.Key.GmLongitude, nome = x.Key.DescricaoPrestador }));


            ViewBag.PagedList = prestadores.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE);
            return View("~/Views/Resultados/ResultadoBusca.cshtml", resultadoOperacao);
        }

        [HttpPost, ActionName("BuscaProximidadeResidencia")]
        public ActionResult ObterPorProximidade(BuscaProximidadeRequest request)
        {
            object requestParams = new
            {
                Vinculo = request.Vinculo,
                Rede = request.Rede ?? "",
                TipoAtendimento = request.TipoAtendimento,
                CodigoEspecialidade = request.CodigoEspecialidade,
                Inativos = request.Inativos,
                BuscaProximidadeTipo = request.BuscaProximidadeTipo
            };

            ResultadoRequest response = this.request.Get("/RedeCredenciada/ObterPorProximidade", requestParams);

            if (response.Erro != null)
            {
                ViewBag.Erro = response.Erro.Mensagem;
                return View("~/Views/Resultados/ResultadoBuscaProximidade.cshtml", new LocalizacaoResponse());
            }

            LocalizacaoResponse resultadoOperacao = response.ObterResultado<LocalizacaoResponse>();
            List<Prestador> prestadores = resultadoOperacao.Data.Select(s => s.Prestador).OrderByDescending(s => s.ScoreTotal).ToList();

            if (resultadoOperacao != null)
            {
                Contexto.AdicionarDados(Consts.RESULTADO_PESQUISA_SEM_FILTRO_LOCALIDADE, resultadoOperacao.Data);
                Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE, resultadoOperacao.Data);
                Contexto.AdicionarDados(Consts.TIPO_FILTRO_BUSCA, ContextoBuscaFiltro.BuscaPrestadoresLocalizacao.ToString());
            }

            resultadoOperacao.Atendimento = request.Atendimento;
            resultadoOperacao.Especialidade = request.Especialidade;
            resultadoOperacao.BuscaProximidadeTipo = request.BuscaProximidadeTipo;

            resultadoOperacao.QuantidadeLocalizados = prestadores.Count;

            resultadoOperacao.DefaultLat = prestadores?.Where(x => !string.IsNullOrEmpty(x.GmLatitude))?.FirstOrDefault()?.GmLatitude ?? Consts.LATITUDE_PADRAO_BRASIL;
            resultadoOperacao.DefaultLng = prestadores?.Where(x => !string.IsNullOrEmpty(x.GmLongitude))?.FirstOrDefault()?.GmLongitude ?? Consts.LONGITUDE_PADRAO_BRASIL;
            resultadoOperacao.MarcadoresGoogleMapsPosition = JsonConvert.SerializeObject(prestadores
                                                                                                                  ?.GroupBy(x =>
                                                                                                                  new
                                                                                                                  {
                                                                                                                      x.GmLatitude,
                                                                                                                      x.GmLongitude
                                                                                                                  })
                                                                                                                  ?.Where(w => !string.IsNullOrEmpty(w.Key.GmLatitude) && !string.IsNullOrEmpty(w.Key.GmLongitude))
                                                                                                                  ?.Select(x => new { lat = x.Key.GmLatitude, lng = x.Key.GmLongitude }));

            ViewBag.PagedList = resultadoOperacao.Data.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE);

            return View("~/Views/Resultados/ResultadoBuscaProximidade.cshtml", resultadoOperacao);
        }

        [HttpPost, ActionName("BuscaProximidadeLocal")]
        public ActionResult ObterPorProximidadeAtual(BuscaProximidadeLocalRequest request)
        {
            ResultadoRequest response = this.request.Get("/RedeCredenciada/ObterPorProximidadeAtual", request);

            if (response.Erro != null)
            {
                ViewBag.Erro = response.Erro.Mensagem;
                return View("~/Views/Resultados/ResultadoBuscaProximidade.cshtml", new LocalizacaoResponse());
            }

            LocalizacaoResponse resultadoOperacao = response.ObterResultado<LocalizacaoResponse>();
            List<Prestador> prestadores = resultadoOperacao.Data.Select(s => s.Prestador).OrderByDescending(x => x.ScoreTotal).ToList();

            if (resultadoOperacao != null)
            {
                Contexto.AdicionarDados(Consts.RESULTADO_PESQUISA_SEM_FILTRO_LOCALIDADE, resultadoOperacao.Data);
                Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE, resultadoOperacao.Data);
                Contexto.AdicionarDados(Consts.TIPO_FILTRO_BUSCA, ContextoBuscaFiltro.BuscaPrestadoresLocalizacao.ToString());
            }

            resultadoOperacao.QuantidadeLocalizados = prestadores.Count;

            resultadoOperacao.DefaultLat = prestadores?.Where(x => !string.IsNullOrEmpty(x.GmLatitude))?.FirstOrDefault()?.GmLatitude ?? Consts.LATITUDE_PADRAO_BRASIL;
            resultadoOperacao.DefaultLng = prestadores?.Where(x => !string.IsNullOrEmpty(x.GmLongitude))?.FirstOrDefault()?.GmLongitude ?? Consts.LONGITUDE_PADRAO_BRASIL;
            resultadoOperacao.MarcadoresGoogleMapsPosition = JsonConvert.SerializeObject(prestadores
                                                                                                                  ?.GroupBy(x =>
                                                                                                                  new
                                                                                                                  {
                                                                                                                      x.GmLatitude,
                                                                                                                      x.GmLongitude,
                                                                                                                      x.DescricaoPrestador
                                                                                                                  })
                                                                                                                  ?.Where(w => !string.IsNullOrEmpty(w.Key.GmLatitude) && !string.IsNullOrEmpty(w.Key.GmLongitude))
                                                                                                                  ?.Select(x => new { lat = x.Key.GmLatitude, lng = x.Key.GmLongitude, nome = x.Key.DescricaoPrestador }));

            ViewBag.PagedList = resultadoOperacao.Data.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE);

            return View("~/Views/Resultados/ResultadoBuscaProximidade.cshtml", resultadoOperacao);
        }

        [HttpPost]
        public ActionResult DetalhesRota(DetalhesRotaRequest request)
        {
            DetalhesRotaResponse response = new DetalhesRotaResponse
            {
                Latitude = request.Latitude,
                Longitude = request.Longitude,
                Nome = request.Nome,
                QuantidadeLocalizados = request.QuantidadeLocalizados,
                Prestador = request.Prestador,
                Endereco = request.Endereco,
                Telefone = request.Telefone
            };

            return View("~/Views/Resultados/VerDetalhesRota.cshtml", response);
        }

        [HttpPost]
        public JsonResult ObterCidades(string codigoUf)
        {
            if (Contexto.DadosUsuario != null)
            {
                var cidades = Contexto.DadosUsuario?
                 .Localizacao
                 .Regioes
                 .Where(x => x.Key.Equals(codigoUf, StringComparison.OrdinalIgnoreCase))
                 .SelectMany(s => s.Value);
                return Json(cidades.Select(s => new { Texto = s.Key, Valor = s.Key }));
            }
            var regiao = ObterDadosRegiaoPorQuery(Request);
            var listaCidade = regiao.Regioes.First(x => x.Key == codigoUf)
                .Value.Select(x => new { Texto = x.Key, Valor = x.Key });
            return Json(listaCidade);
        }

        public DadosRegioes ObterDadosRegiaoPorQuery(HttpRequestBase request)
        {
            var query = HttpUtility.ParseQueryString(request.UrlReferrer.Query);
            var buscaRequest = new BuscaEspecialidadeRequest(query);
            var response = this.request.Get("/RedeCredenciada/ObterRegioes", buscaRequest);
            return response.ObterResultado<RegioesResponse>().Data;
        }

        [HttpPost]
        public JsonResult ObterBairros(string codigoUf, string cidade)
        {
            var dadosContexto =
                Contexto.DadosUsuario?
                        .Localizacao
                        .Regioes;

            if (dadosContexto == null) dadosContexto = ObterDadosRegiaoPorQuery(Request).Regioes;

            IEnumerable<KeyValuePair<string, IEnumerable<string>>> cidades = dadosContexto.Where(x => x.Key.Equals(codigoUf, StringComparison.OrdinalIgnoreCase))
                                       .SelectMany(s => s.Value);

            IEnumerable<string> bairros = cidades.Where(x => x.Key.Equals(cidade, StringComparison.OrdinalIgnoreCase))
                                                 .SelectMany(s => s.Value);

            return Json(bairros.Select(s => new { Texto = s, Valor = s }));
        }
    }
}