﻿using System.IO;
using System.Linq;
using System.Web.Mvc;
using MSW.Web.Models;
using MSW.Web.Helpers;
using MSW.Web.Models.Response;
using System.Collections.Generic;
using PagedList;
using MSW.Web.Helpers.Extensions;
using System;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;
using MSW.Web.Helpers.GMaps;

namespace MSW.Web.Controllers
{
    public class FiltroMenuController : Controller
    {
        [ChildActionOnly]
        public ActionResult MenuFiltro()
        {
            FiltroMenu filtroMenu = ObterFiltroMenu();
            return PartialView("~/Views/Shared/_FiltroMenuLateral.cshtml", filtroMenu);
        }

        private static FiltroMenu ObterFiltroMenu()
        {
            FiltroMenu filtroMenu = new FiltroMenu();

            ContextoBuscaFiltro contextoBusca;
            System.Enum.TryParse(Contexto.ObterDados<string>(Consts.TIPO_FILTRO_BUSCA), out contextoBusca);

            List<Prestador> prestadores = contextoBusca == ContextoBuscaFiltro.BuscaPrestadores ?
                    Contexto.ObterDados<List<Prestador>>(Consts.LISTA_DADOS_PRESTADORES) :
                    Contexto.ObterDados<List<DadosLocalizacao>>(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE)?.Select(s => s.Prestador).ToList();

            filtroMenu.AdicionarEspecialidades(prestadores);
            filtroMenu.AdicionarAvaliacoes(prestadores);
            filtroMenu.AdicionarCidades(prestadores);
            filtroMenu.AdicionarEstados(prestadores);
            filtroMenu.AdicionarServico(prestadores);

            var planosLogado = Contexto.DadosUsuario?.DadosRede
                                          .Planos
                                          .Select(x => x.CodigoRede)
                                          .ToArray();

            filtroMenu.AdicionarPlano(planosLogado != null ? prestadores.Where(x => planosLogado.Contains(x.CodigoRede)) : prestadores);

            filtroMenu.AdicionarRede(planosLogado != null ? prestadores.Where(x => planosLogado.Contains(x.CodigoRede)).GroupBy(s => new { s.CodigoRede, s.DescricaoRede }).Select(s => new AgregadorPlanoRede { CodigoRede = s.Key.CodigoRede, Descricao = s.Key.DescricaoRede, Quantidade = s.Count() }).ToArray()
                : prestadores.GroupBy(s => s.CodigoRede).Select(s => new AgregadorPlanoRede { CodigoRede = s.Key, Quantidade = s.Count() }).ToArray());

            filtroMenu.QuantidadeLocalizados = prestadores?.Count() ?? 0;
            return filtroMenu;
        }

        [HttpPost]
        public ActionResult Filtrar(FiltroMultiplo filtro)
        {
            if (filtro == null)
            {
                filtro = new FiltroMultiplo();
            }

            System.Web.HttpContext.Current.Items[Consts.MULTIPLOS_FILTROS] = filtro;

            ContextoBuscaFiltro contextoBusca;
            Enum.TryParse(Contexto.ObterDados<string>(Consts.TIPO_FILTRO_BUSCA), out contextoBusca);

            if (contextoBusca == ContextoBuscaFiltro.BuscaPrestadores)
            {
                List<Prestador> prestadoresFiltrados = Contexto.ObterDados<List<Prestador>>(Consts.LISTA_DADOS_PRESTADORES)
                                                           ?.Where(w => (!filtro.Estados.Any() || filtro.Estados.Contains(w.CodigoEstado.Trim()))
                                                           && (!filtro.Cidades.Any() || filtro.Cidades.Contains(w.EnderecoCidade))
                                                           && (!filtro.Especialidades.Any() || filtro.Especialidades.Any(w.Especialidades.Contains)))
                                                           .ToList() ?? new List<Prestador>();

                Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES, prestadoresFiltrados);

                IPagedList<Prestador> prestadores = prestadoresFiltrados.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE);

                return new JsonResult
                {
                    Data = new ResultadoFiltroResponse { ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBusca.cshtml", prestadores) },
                    MaxJsonLength = int.MaxValue
                };
            }

            List<DadosLocalizacao> dadosLocalizacao = Contexto.ObterDados<List<DadosLocalizacao>>(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE)
                ?.Where(w => filtro.Cidades.Contains(w.Prestador.EnderecoCidade.Trim()))
                .ToList();

            Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE, dadosLocalizacao);

            IPagedList<DadosLocalizacao> localizacao = dadosLocalizacao.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE);

            return new JsonResult
            {
                Data = new ResultadoFiltroResponse { ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBuscaProximidade.cshtml", localizacao) },
                MaxJsonLength = int.MaxValue
            };
        }

        [HttpPost]
        public ActionResult FiltrarPorServico(string servico)
        {
            ContextoBuscaFiltro contextoBusca;
            Enum.TryParse(Contexto.ObterDados<string>(Consts.TIPO_FILTRO_BUSCA), out contextoBusca);

            string siglaServico = "Médico".Equals(servico, System.StringComparison.OrdinalIgnoreCase) ? "M" : "O";

            IEnumerable<string> codigosRede = ObterCodigosRede(siglaServico);

            if (contextoBusca == ContextoBuscaFiltro.BuscaPrestadores)
            {
                List<Prestador> prestadoresFiltrados = Contexto.ObterDados<List<Prestador>>(Consts.LISTA_DADOS_PRESTADORES)
                                                              ?.Where(w => codigosRede.Contains(w.CodigoRede.Trim()))
                                                               .ToList() ?? new List<Prestador>();

                Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES, prestadoresFiltrados);

                IPagedList<Prestador> prestadores = prestadoresFiltrados.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE);
                return new JsonResult
                {
                    Data = new ResultadoFiltroResponse { ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBusca.cshtml", prestadores) },
                    MaxJsonLength = int.MaxValue
                };
            }

            List<DadosLocalizacao> dadosLocalizacao = Contexto.ObterDados<List<DadosLocalizacao>>(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE)
                                                              ?.Where(w => codigosRede.Contains(w.Prestador.CodigoRede.Trim()))
                                                               .ToList() ?? new List<DadosLocalizacao>();

            Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE, dadosLocalizacao);

            IPagedList<DadosLocalizacao> localizacao = dadosLocalizacao.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE);

            return new JsonResult
            {
                Data = new ResultadoFiltroResponse { ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBuscaProximidade.cshtml", localizacao) },
                MaxJsonLength = int.MaxValue
            };
        }

        [HttpPost]
        public ActionResult FiltrarPorRede(string codigoRede)
        {
            ContextoBuscaFiltro contextoBusca;
            Enum.TryParse(Contexto.ObterDados<string>(Consts.TIPO_FILTRO_BUSCA), out contextoBusca);

            Contexto.AdicionarDados(Consts.CODIGO_REDE, codigoRede);
            if (contextoBusca == ContextoBuscaFiltro.BuscaPrestadores)
            {
                List<Prestador> prestadoresFiltrados = Contexto.ObterDados<List<Prestador>>(Consts.LISTA_DADOS_PRESTADORES)
                                        ?.Where(w => codigoRede == w.CodigoRede)
                                        .ToList() ?? new List<Prestador>();

                Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES, prestadoresFiltrados);

                IPagedList<Prestador> prestadores = prestadoresFiltrados.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE);
                return new JsonResult
                {
                    Data = new ResultadoFiltroResponse { ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBusca.cshtml", prestadores) },
                    MaxJsonLength = int.MaxValue
                };
            }

            List<DadosLocalizacao> dadosLocalizacao = Contexto.ObterDados<List<DadosLocalizacao>>(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE)
                                                               ?.Where(w => codigoRede == w.Prestador.CodigoRede)
                                                               .ToList();

            Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE, dadosLocalizacao);

            IPagedList<DadosLocalizacao> localizacao = dadosLocalizacao.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE);

            return new JsonResult
            {
                Data = new ResultadoFiltroResponse { ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBuscaProximidade.cshtml", localizacao) },
                MaxJsonLength = int.MaxValue
            };
        }

        [HttpPost]
        public ActionResult FiltrarPorPlano(string codigoPlano)
        {
            ContextoBuscaFiltro contextoBusca;
            Enum.TryParse(Contexto.ObterDados<string>(Consts.TIPO_FILTRO_BUSCA), out contextoBusca);

            var redePlano = ObterFiltroMenu().ObterRedeDoPlano(codigoPlano);

            if (contextoBusca == ContextoBuscaFiltro.BuscaPrestadores)
            {
                List<Prestador> prestadoresFiltrados = Contexto.ObterDados<List<Prestador>>(Consts.LISTA_DADOS_PRESTADORES)
                                        ?.Where(w => redePlano.Contains(w.CodigoRede))
                                        .ToList() ?? new List<Prestador>();

                Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES, prestadoresFiltrados);

                IPagedList<Prestador> prestadores = prestadoresFiltrados.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE);
                return new JsonResult
                {
                    Data = new ResultadoFiltroResponse { ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBusca.cshtml", prestadores) },
                    MaxJsonLength = int.MaxValue
                };
            }

            List<DadosLocalizacao> dadosLocalizacao = Contexto.ObterDados<List<DadosLocalizacao>>(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE)
                                                               ?.Where(w => redePlano.Equals(w.Prestador.CodigoRede))
                                                               .ToList();

            Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE, dadosLocalizacao);

            IPagedList<DadosLocalizacao> localizacao = dadosLocalizacao.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE);

            return new JsonResult
            {
                Data = new ResultadoFiltroResponse { ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBuscaProximidade.cshtml", localizacao) },
                MaxJsonLength = int.MaxValue
            };

        }

        [HttpPost]
        public ActionResult FiltrarPorAvaliacao(int scoreTotal)
        {
            ContextoBuscaFiltro contextoBusca;
            Enum.TryParse(Contexto.ObterDados<string>(Consts.TIPO_FILTRO_BUSCA), out contextoBusca);

            if (contextoBusca == ContextoBuscaFiltro.BuscaPrestadores)
            {
                var listaPrestadores = Contexto.ObterDados<List<Prestador>>(Consts.LISTA_DADOS_PRESTADORES);

                List<Prestador> prestadoresFiltrados = listaPrestadores
                                        ?.Where(w => scoreTotal.Equals(w.ScoreTotal))
                                        .ToList() ?? new List<Prestador>();

                Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES, prestadoresFiltrados);

                IPagedList<Prestador> prestadores = prestadoresFiltrados.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE);

                return new JsonResult
                {
                    Data = new ResultadoFiltroResponse { ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBusca.cshtml", prestadores) },
                    MaxJsonLength = int.MaxValue
                };
            }

            List<DadosLocalizacao> dadosLocalizacao = Contexto.ObterDados<List<DadosLocalizacao>>(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE)
                                                               ?.Where(w => scoreTotal.Equals(w.Prestador.ScoreTotal))
                                                               .ToList();

            Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE, dadosLocalizacao);

            IPagedList<DadosLocalizacao> localizacao = dadosLocalizacao.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE);

            return new JsonResult
            {
                Data = new ResultadoFiltroResponse { ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBuscaProximidade.cshtml", localizacao) },
                MaxJsonLength = int.MaxValue
            };
        }

        [HttpPost]
        public ActionResult OrdenarResultadoPrestadores(TipoOrdenacao ordenacao)
        {
            ContextoBuscaFiltro contextoBusca;
            ResultadoFiltroResponse response = new ResultadoFiltroResponse();

            Enum.TryParse(Contexto.ObterDados<string>(Consts.TIPO_FILTRO_BUSCA), out contextoBusca);

            if (contextoBusca == ContextoBuscaFiltro.BuscaPrestadores)
            {
                List<Prestador> sessao = Contexto.ObterDados<List<Prestador>>(Consts.LISTA_DADOS_PRESTADORES);

                List<Prestador> prestadores = ordenacao == TipoOrdenacao.Nome ?
                        sessao.OrderBy(x => x.DescricaoPrestador).ToList() :
                        sessao.OrderByDescending(x => x.ScoreTotal).ToList();

                Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES, prestadores);
                response.ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBusca.cshtml", prestadores.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE));
            }
            else
            {
                List<DadosLocalizacao> sessao = Contexto.ObterDados<List<DadosLocalizacao>>(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE);

                List<DadosLocalizacao> prestadoresProximidade = ordenacao == TipoOrdenacao.Nome ?
                                                    sessao.OrderBy(x => x.Prestador.DescricaoPrestador).ToList() :
                                                    sessao.OrderByDescending(x => x.Prestador.ScoreTotal).ToList();

                Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE, prestadoresProximidade);
                response.ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBuscaProximidade.cshtml", prestadoresProximidade.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE));
            }

            return new JsonResult
            {
                Data = response,
                MaxJsonLength = int.MaxValue
            };
        }

        [HttpPost]
        public Task<JsonResult> OrdenarPrestadoresPorDistancia(string latitude, string longitude)
        {

            ContextoBuscaFiltro contextoBusca;
            ResultadoFiltroResponse response = new ResultadoFiltroResponse();

            Enum.TryParse(Contexto.ObterDados<string>(Consts.TIPO_FILTRO_BUSCA), out contextoBusca);

            if (contextoBusca == ContextoBuscaFiltro.BuscaPrestadores)
            {

                List<Prestador> prestadores = Contexto.ObterDados<List<Prestador>>(Consts.LISTA_DADOS_PRESTADORES)
                                                             .Where(x => !string.IsNullOrEmpty(x.GmLatitude.Trim()) && !string.IsNullOrEmpty(x.GmLongitude.Trim()))
                                                             .Select(MapearPrestador)
                                                             .ToList();

                var httpClient = new HttpClient
                {
                    BaseAddress = new Uri(ConfigurationManager.AppSettings["googleMaps:ApiAddress"])
                };

                var query = string.Join("|", prestadores.Select(coord => $"{coord.GmLatitude},{coord.GmLongitude}"));
                string requestUri = $"distancematrix/json?origins={latitude},{longitude}&destinations={query}&apiKey={ConfigurationManager.AppSettings["googleMaps:ApiKey"]}";

                return httpClient.GetAsync(requestUri).ContinueWith(t =>
                {
                    if (t.IsCompleted && !t.IsCanceled)
                    {

                        var responseContent = t.Result.Content.ReadAsStringAsync();
                        responseContent.Wait();

                        if (responseContent.IsCompleted && !responseContent.IsCanceled)
                        {
                            try
                            {
                                var gmapsInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<GMapsDistanceCalc>(responseContent.Result);
                                var distances = gmapsInfo.Rows?.SelectMany(x=>x.Elements).ToArray() ?? new GMapsDistanceElement[0];

                                for (int i = 0; i < prestadores.Count; i++)
                                {
                                    if (distances[i].Status.Equals("ok", StringComparison.OrdinalIgnoreCase))
                                    {
                                        prestadores[i].Distancia = distances[i].Distance.Text;
                                        prestadores[i].DistanciaMetros = distances[i].Distance.Value;
                                    }
                                }

                                prestadores = prestadores.OrderBy(x=>x.DistanciaMetros).ToList();
                            }
                            catch (Exception ex)
                            {
                                System.Diagnostics.Trace.TraceError("não foi possivel obter a distância dos prestadores.", ex);
                            }
                        }
                    }

                    Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES, prestadores);
                    response.ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBusca.cshtml", prestadores.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE));

                    return new JsonResult
                    {
                        Data = response,
                        MaxJsonLength = int.MaxValue
                    };

                }, TaskScheduler.FromCurrentSynchronizationContext());
            }


            List<DadosLocalizacao> prestadoresLocalizacao = Contexto.ObterDados<List<DadosLocalizacao>>(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE)
                                                                        .OrderBy(x => x.Distancia)
                                                                        .ToList();
            Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE, prestadoresLocalizacao);
            response.ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBuscaProximidade.cshtml", prestadoresLocalizacao.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE));


            return Task.Factory.StartNew(() => new JsonResult
            {
                Data = response,
                MaxJsonLength = int.MaxValue
            });
        }

        [HttpPost]
        public ActionResult Voltar(OrigemVoltar origemVoltar)
        {
            ContextoBuscaFiltro contextoBusca;
            Enum.TryParse(Contexto.ObterDados<string>(Consts.TIPO_FILTRO_BUSCA), out contextoBusca);

            ResultadoFiltroResponse response = new ResultadoFiltroResponse();

            if (contextoBusca == ContextoBuscaFiltro.BuscaPrestadores)
            {
                List<Prestador> listaPrestadores = origemVoltar == OrigemVoltar.Filtro ?
                            Contexto.ObterDados<List<Prestador>>(Consts.RESULTADO_PESQUISA_SEM_FILTRO).ToList() :
                            Contexto.ObterDados<List<Prestador>>(Consts.LISTA_DADOS_PRESTADORES).ToList();

                if (listaPrestadores.Any() && origemVoltar == OrigemVoltar.Filtro)
                    Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES, listaPrestadores);

                response.ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBusca.cshtml", listaPrestadores.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE));
            }
            else
            {
                List<DadosLocalizacao> listaPrestadoresLocalizacao = origemVoltar == OrigemVoltar.Filtro ?
                    Contexto.ObterDados<List<DadosLocalizacao>>(Consts.RESULTADO_PESQUISA_SEM_FILTRO_LOCALIDADE).ToList() :
                    Contexto.ObterDados<List<DadosLocalizacao>>(Consts.LISTA_DADOS_PRESTADORES_PROXIMIDADE).ToList();

                if (listaPrestadoresLocalizacao.Any() && origemVoltar == OrigemVoltar.Filtro)
                    Contexto.AdicionarDados(Consts.LISTA_DADOS_PRESTADORES, listaPrestadoresLocalizacao);

                response.ViewResult = RenderRazorViewToString("~/Views/Shared/_ListaResultadoBuscaProximidade.cshtml", listaPrestadoresLocalizacao.ToPagedList(Consts.DEFAULT_PAGE_INIT, Consts.DEFAULT_PAGE_SIZE));
            }

            return new JsonResult
            {
                Data = response,
                MaxJsonLength = int.MaxValue
            };
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var dados = (System.Web.HttpContext.Current.Items[Consts.MULTIPLOS_FILTROS] as FiltroMultiplo) ?? new FiltroMultiplo();
            ViewData["EstadosSelecionados"] = dados.Estados;
            ViewData["CidadesSelecionadas"] = dados.Cidades;
            ViewData["EspecSelecionadas"] = dados.Especialidades;
        }

        IEnumerable<string> ObterCodigosRede(string sigla)
        {
            string[] codigosRede = Contexto.DadosUsuario?.DadosRede
                                  ?.Planos
                                  ?.Where(x => x.CodigoProduto.Equals(sigla))
                                  .Select(s => s.CodigoRede).ToArray();

            if (codigosRede == null || codigosRede.Length == 0)
            {
                if (sigla.Equals("M"))
                    return new string[] { "0001", "0003", "0004", "0007", "0009", };

                return new string[] { "0002", "0005", "0006" };
            }

            return codigosRede;
        }

        string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return sw.GetStringBuilder().ToString();
            }
        }

        Prestador MapearPrestador(Prestador x)
        {
            return new Prestador
            {
                CodigoCep = x.CodigoCep,
                CodigoEstado = x.CodigoEstado,
                CodigoPrestador = x.CodigoPrestador,
                CodigoRede = x.CodigoRede,
                DataDescredenciamento = x.DataDescredenciamento,
                DescricaoEspecialidade = x.DescricaoEspecialidade,
                DescricaoObservacao = x.DescricaoObservacao,
                DescricaoPrestador = x.DescricaoPrestador,
                DescricaoRede = x.DescricaoRede,
                DescricaoRegistro = x.DescricaoRegistro,
                EnderecoBairro = x.EnderecoBairro,
                EnderecoCidade = x.EnderecoCidade,
                EnderecoRua = x.EnderecoRua,
                EnderecoNumero = x.EnderecoNumero,
                EnderecoComplemento = x.EnderecoComplemento,
                EnderecoTelefone = x.EnderecoTelefone,
                Especialidades = x.Especialidades,
                GmLatitude = x.GmLatitude,
                GmLongitude = x.GmLongitude,
                NrAvaliacao = x.NrAvaliacao,
                ScoreTotal = x.ScoreTotal,
                SrEndereco = x.SrEndereco,
                Telefones = x.Telefones,
                TipoLogradouro = x.TipoLogradouro,
                Zona = x.Zona
            };
        }
    }
}