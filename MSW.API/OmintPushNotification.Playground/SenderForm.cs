﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Threading.Tasks;

namespace OmintPushNotification.Playground
{
    public partial class SenderForm : Form
    {
        private const string REGISTRY_USER_ROOT = "HKEY_CURRENT_USER";
        private const string REGISTRY_OMINT_KEY = "OmintPushNotificationConfig";
        private const string REGISTRY_KEYNAME = REGISTRY_USER_ROOT + "\\" + REGISTRY_OMINT_KEY;

        public SenderForm()
        {
            InitializeComponent();

            this.LoadData();

            this.sendButton.Click += async (sender, e) =>
            {
                await this.ExecuteAsync();
                this.SaveData();
            };
        }

        private async Task ExecuteAsync()
        {
            try
            {
                var canExecute = CanExecute();
                if (!canExecute)
                    return;

                var so = new List<string>();
                if (androidCheckBox.Checked) so.Add("android");
                if (iOSCheckbox.Checked) so.Add("ios");
                if (windowsCheckbox.Checked) so.Add("windows");


                Dictionary<string, string> dicCustomFields = new Dictionary<string, string>();

                foreach (var item in lbCustomFields.Items)
                {
                    string[] strArray = item.ToString().Split('|');
                    if(strArray.Length == 2)
                        dicCustomFields.Add(strArray[0].Trim(), strArray[1].Trim());
                }


                foreach (var token in lbTokenList.Items)
                {
                    foreach (var item in so)
                        await Push.SendAsync(item,
                            token.ToString(),
                            this.titleTextBox.Text,
                            this.messageTextBox.Text,
                            dicCustomFields
                        );
                }
                MessageBox.Show("Message sent!");
                //new Dictionary<string, string>
                //{
                //    ["Vinculo"] = "2361057900",
                //    ["id_CategoriaPushNotification"] = "3",
                //    ["Referencia"] = "3611048",
                //    ["AppNavigate"] = "configuracoes.html"
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error...");
            }
        }

        private bool CanExecute()
        {
            if (!androidCheckBox.Checked &&
                !iOSCheckbox.Checked &&
                !windowsCheckbox.Checked
            )
                return false;

            Func<TextBox, bool> empty = (t) => string.IsNullOrWhiteSpace(t.Text);
            Func<ListBox, bool> hasItens = (l) => l.Items.Count > 0;
            if (empty(titleTextBox) || empty(messageTextBox) || !hasItens(lbTokenList))
            {
                MessageBox.Show("Please insert the title, message and the token list.");
                return false;
            }

            return true;
        }

        private void SaveData()
        {
            Registry.SetValue(REGISTRY_KEYNAME, "android", androidCheckBox.Checked);
            Registry.SetValue(REGISTRY_KEYNAME, "ios", iOSCheckbox.Checked);
            Registry.SetValue(REGISTRY_KEYNAME, "windows", windowsCheckbox.Checked);

            Registry.SetValue(REGISTRY_KEYNAME, "title", titleTextBox.Text);
            Registry.SetValue(REGISTRY_KEYNAME, "message", messageTextBox.Text);

        }
        private void LoadData()
        {
            Action<string, CheckBox> getCheckbox = (so, cb) =>
            {
                var value = Registry.GetValue(REGISTRY_KEYNAME, so, false);
                if (value != null)
                    cb.Checked = bool.TrueString.Equals(value);
            };

            getCheckbox("android", androidCheckBox);
            getCheckbox("ios", iOSCheckbox);
            getCheckbox("windows", windowsCheckbox);

            Action<string, TextBox> getTextbox = (v, tb) =>
            {
                var value = Registry.GetValue(REGISTRY_KEYNAME, v, string.Empty);
                if (value != null)
                    tb.Text = value.ToString();
            };


            getTextbox("title", titleTextBox);
            getTextbox("message", messageTextBox);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cboFieldName.Text) || string.IsNullOrEmpty(txtValue.Text))
                return;

            var campoJaAdicionado = false;
            foreach (var item in lbCustomFields.Items)
            {
                if (item.ToString().Contains(cboFieldName.Text))
                    campoJaAdicionado = true;                    
            }
            if (campoJaAdicionado)
            {
                MessageBox.Show("Field already add.");
                return;
            }

            var text = string.Format("{0} | {1}", cboFieldName.Text, txtValue.Text);
            lbCustomFields.Items.Add(text);
            txtValue.Clear();
        }

        private void libCustomFields_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode.Equals(Keys.Delete))
                lbCustomFields.Items.Remove(lbCustomFields.SelectedItem);
        }

        private void btnAddToken_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtToken.Text))
                return;

            foreach (var item in lbTokenList.Items)
            {
                if (item.ToString().Contains(txtToken.Text))
                {
                    MessageBox.Show("Token already add.");
                    return;
                }
            }
            lbTokenList.Items.Add(txtToken.Text);
            txtToken.Clear();
        }

        private void lbTokenList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Delete))
                lbTokenList.Items.Remove(lbTokenList.SelectedItem);                
        }
    }
}