﻿namespace OmintPushNotification.Playground
{
    partial class SenderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.windowsCheckbox = new System.Windows.Forms.CheckBox();
            this.iOSCheckbox = new System.Windows.Forms.CheckBox();
            this.androidCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.messageTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtToken = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.sendButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cboFieldName = new System.Windows.Forms.ComboBox();
            this.lbCustomFields = new System.Windows.Forms.ListBox();
            this.btnAddField = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lbTokenList = new System.Windows.Forms.ListBox();
            this.btnAddToken = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.windowsCheckbox);
            this.groupBox1.Controls.Add(this.iOSCheckbox);
            this.groupBox1.Controls.Add(this.androidCheckBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(112, 124);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Systems";
            // 
            // windowsCheckbox
            // 
            this.windowsCheckbox.AutoSize = true;
            this.windowsCheckbox.Location = new System.Drawing.Point(11, 84);
            this.windowsCheckbox.Name = "windowsCheckbox";
            this.windowsCheckbox.Size = new System.Drawing.Size(70, 17);
            this.windowsCheckbox.TabIndex = 2;
            this.windowsCheckbox.Text = "Windows";
            this.windowsCheckbox.UseVisualStyleBackColor = true;
            // 
            // iOSCheckbox
            // 
            this.iOSCheckbox.AutoSize = true;
            this.iOSCheckbox.Location = new System.Drawing.Point(11, 54);
            this.iOSCheckbox.Name = "iOSCheckbox";
            this.iOSCheckbox.Size = new System.Drawing.Size(43, 17);
            this.iOSCheckbox.TabIndex = 1;
            this.iOSCheckbox.Text = "iOS";
            this.iOSCheckbox.UseVisualStyleBackColor = true;
            // 
            // androidCheckBox
            // 
            this.androidCheckBox.AutoSize = true;
            this.androidCheckBox.Location = new System.Drawing.Point(11, 23);
            this.androidCheckBox.Name = "androidCheckBox";
            this.androidCheckBox.Size = new System.Drawing.Size(62, 17);
            this.androidCheckBox.TabIndex = 0;
            this.androidCheckBox.Text = "Android";
            this.androidCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.messageTextBox);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.titleTextBox);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(131, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(379, 123);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Message";
            // 
            // messageTextBox
            // 
            this.messageTextBox.Location = new System.Drawing.Point(64, 45);
            this.messageTextBox.MaxLength = 500;
            this.messageTextBox.Multiline = true;
            this.messageTextBox.Name = "messageTextBox";
            this.messageTextBox.Size = new System.Drawing.Size(309, 63);
            this.messageTextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Message";
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(64, 19);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(309, 20);
            this.titleTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Title";
            // 
            // txtToken
            // 
            this.txtToken.Location = new System.Drawing.Point(12, 42);
            this.txtToken.Name = "txtToken";
            this.txtToken.Size = new System.Drawing.Size(429, 20);
            this.txtToken.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Token:";
            // 
            // sendButton
            // 
            this.sendButton.Location = new System.Drawing.Point(436, 571);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(75, 23);
            this.sendButton.TabIndex = 2;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cboFieldName);
            this.groupBox3.Controls.Add(this.lbCustomFields);
            this.groupBox3.Controls.Add(this.btnAddField);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.txtValue);
            this.groupBox3.Location = new System.Drawing.Point(12, 350);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(498, 215);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Custom information";
            // 
            // cboFieldName
            // 
            this.cboFieldName.FormattingEnabled = true;
            this.cboFieldName.Items.AddRange(new object[] {
            "Vinculo",
            "id_CategoriaPushNotification",
            "Referencia",
            "url_pagina"});
            this.cboFieldName.Location = new System.Drawing.Point(11, 38);
            this.cboFieldName.Name = "cboFieldName";
            this.cboFieldName.Size = new System.Drawing.Size(166, 21);
            this.cboFieldName.TabIndex = 9;
            // 
            // lbCustomFields
            // 
            this.lbCustomFields.FormattingEnabled = true;
            this.lbCustomFields.Location = new System.Drawing.Point(11, 65);
            this.lbCustomFields.Name = "lbCustomFields";
            this.lbCustomFields.Size = new System.Drawing.Size(481, 134);
            this.lbCustomFields.TabIndex = 8;
            this.lbCustomFields.KeyDown += new System.Windows.Forms.KeyEventHandler(this.libCustomFields_KeyDown);
            // 
            // btnAddField
            // 
            this.btnAddField.Location = new System.Drawing.Point(447, 38);
            this.btnAddField.Name = "btnAddField";
            this.btnAddField.Size = new System.Drawing.Size(45, 20);
            this.btnAddField.TabIndex = 4;
            this.btnAddField.Text = "Add";
            this.btnAddField.UseVisualStyleBackColor = true;
            this.btnAddField.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(180, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Value:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Field Name:";
            // 
            // txtValue
            // 
            this.txtValue.Location = new System.Drawing.Point(183, 38);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(258, 20);
            this.txtValue.TabIndex = 1;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lbTokenList);
            this.groupBox4.Controls.Add(this.btnAddToken);
            this.groupBox4.Controls.Add(this.txtToken);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Location = new System.Drawing.Point(12, 142);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(498, 202);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Token List";
            // 
            // lbTokenList
            // 
            this.lbTokenList.FormattingEnabled = true;
            this.lbTokenList.Location = new System.Drawing.Point(11, 68);
            this.lbTokenList.Name = "lbTokenList";
            this.lbTokenList.Size = new System.Drawing.Size(481, 121);
            this.lbTokenList.TabIndex = 10;
            this.lbTokenList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lbTokenList_KeyDown);
            // 
            // btnAddToken
            // 
            this.btnAddToken.Location = new System.Drawing.Point(447, 42);
            this.btnAddToken.Name = "btnAddToken";
            this.btnAddToken.Size = new System.Drawing.Size(45, 20);
            this.btnAddToken.TabIndex = 10;
            this.btnAddToken.Text = "Add";
            this.btnAddToken.UseVisualStyleBackColor = true;
            this.btnAddToken.Click += new System.EventHandler(this.btnAddToken_Click);
            // 
            // SenderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 606);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.sendButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SenderForm";
            this.ShowIcon = false;
            this.Text = "Sender";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox windowsCheckbox;
        private System.Windows.Forms.CheckBox iOSCheckbox;
        private System.Windows.Forms.CheckBox androidCheckBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox messageTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.TextBox txtToken;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox lbCustomFields;
        private System.Windows.Forms.Button btnAddField;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.ComboBox cboFieldName;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnAddToken;
        private System.Windows.Forms.ListBox lbTokenList;
    }
}

