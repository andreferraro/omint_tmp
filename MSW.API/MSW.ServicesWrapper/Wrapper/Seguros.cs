﻿using MSW.ServicesWrapper.Obs.ServicosSeguro;
using System;
using System.Linq;

namespace MSW.ServicesWrapper.Wrapper
{
    public static class Seguros
    {
        public static string ObterNumeroBilhetePessoalAtivo(string cpfSegurado)
        {
            using (SeguroViagemClient _seguroClient = new SeguroViagemClient())
            {
                var bilhete = _seguroClient.ObterBilhetesPorCpf(cpfSegurado)
                                    .Where(x => x.DataFimViagem >= DateTime.Now 
                                        && x.DataCancelamento == default(DateTime) 
                                        && x.CpfSegurado == cpfSegurado)
                                    .FirstOrDefault();

                return bilhete != null ? bilhete.NumeroBilhete : null;
            };
        }

    }
}
