﻿using System;
using System.Linq;
using System.Configuration;
using MSW.ServicesWrapper.OBS.ServicosAssociado;
using MSW.ServicesWrapper.OBS.ServicosUsuariosWeb;

namespace MSW.ServicesWrapper.Wrapper
{
    public static class UsuariosWeb
    {
        public static OBS.ServicosUsuariosWeb.Usuario ObterPorDocumento(string tipo, string numero)
        {
            var servico = new AdministracaoClient();
            var usuario = servico.ObterPorDocumento(tipo, numero);
            return usuario;
        }

        public static OBS.ServicosUsuariosWeb.Usuario ObterPorIdUsuario(int idUsuario)
        {
            var servico = new AdministracaoClient();
            var usuario = servico.ObterPorIdUsuario(idUsuario);
            return usuario;
        }

        public static void AdicionarNovoUsuarioOmint(string login, string senha, DateTime dataNascimento, string nome, string tipoDocumento, string documento)
        {
            var servico = new AdministracaoClient();
            var aut = new AutenticadorOmint
            {
                Login = login,
                Senha = senha
            };
            var usr = new OBS.ServicosUsuariosWeb.Usuario
            {
                TipoDocumento = tipoDocumento,
                NumeroDocumento = documento,
                DataNascimento = dataNascimento,
                NomeUsuario = nome
            };
            servico.CriarNovoUsuario(aut, usr);
        }

        public static void AdicionarNovoUsuarioOmint(DateTime dataNascimento, string nome, string tipoDocumento, string documento)
        {
            var servico = new AdministracaoClient();
            var usr = new OBS.ServicosUsuariosWeb.Usuario
            {
                TipoDocumento = tipoDocumento,
                NumeroDocumento = documento,
                DataNascimento = dataNascimento,
                NomeUsuario = nome
            };
            servico.CriarNovoUsuario(null, usr);
        }

        public static void AdicionarNovoUsuarioFacebook(string idFacebook, DateTime dataNascimento, string nome, string tipoDocumento, string documento)
        {
            var servico = new AdministracaoClient();
            var aut = new AutenticadorFacebook
            {
                IdFacebook = idFacebook
            };
            var usr = new OBS.ServicosUsuariosWeb.Usuario
            {
                TipoDocumento = tipoDocumento,
                NumeroDocumento = documento,
                DataNascimento = dataNascimento,
                NomeUsuario = nome
            };
            servico.CriarNovoUsuario(aut, usr);
        }

        public static OBS.ServicosUsuariosWeb.Usuario ObterUsuarioOmint(string login, string senha)
        {
            var servico = new AdministracaoClient();
            var usuarioOmint = servico.ObterUsuariosAutenticador(new AutenticadorOmint())
                                      .Cast<AutenticadorOmint>()
                                      .FirstOrDefault(x => x.Login == login && x.Senha == senha);
            if (usuarioOmint == null)
                return null;

            return servico.ObterPorIdUsuario(usuarioOmint.IdUsuario);
        }

        public static OBS.ServicosUsuariosWeb.Usuario ObterUsuarioOmint(string cpf, DateTime dataNascimento)
        {
            var servico = new AdministracaoClient();

            var usuario = servico.ObterPorDocumento("CPF", cpf);

            if (usuario != null &&
                usuario.DataNascimento.Year == dataNascimento.Year &&
                usuario.DataNascimento.Month == dataNascimento.Month &&
                usuario.DataNascimento.Day == dataNascimento.Day)
                return usuario;

            return null;
        }

        public static OBS.ServicosUsuariosWeb.Usuario ObterUsuarioDotNetNuke(string login, string senha)
        {
            var servico = new OEX.DesktopModules.WebServiceSoapClient();

            int portalId = Convert.ToInt32(ConfigurationManager.AppSettings["PortalIdNuke"]);

            var usuarioNuke = servico.GetUser(GetAuthenticator(), portalId, login);

            AssociadoClient proxy = new AssociadoClient();

            Beneficiario beneficiarioOBS = proxy.ObterDadosUsuarioDotNetNuke(usuarioNuke.UserID.ToString());

            if (beneficiarioOBS == null || usuarioNuke == null)
            {
                return null;
            }

            //se a senha retornada é igual a senha informada no login
            if (usuarioNuke.Membership.Password.Equals(senha))
            {
                return new OBS.ServicosUsuariosWeb.Usuario
                {
                    TipoDocumento = "CPF",
                    NumeroDocumento = beneficiarioOBS.NumeroCPF,
                    DataNascimento = beneficiarioOBS.DataNascimento,
                    NomeUsuario = beneficiarioOBS.DescricaoBeneficiario
                };
            }

            return null;
        }

        private static ServicesWrapper.OEX.DesktopModules.IWebAuthendicationHeader GetAuthenticator()
        {
            var webAuthendicationHeader = new ServicesWrapper.OEX.DesktopModules.IWebAuthendicationHeader
            {
                PortalID = 1,
                Username = "host",
                Password = "newbery",
                Encrypted = "false"
            };

            return webAuthendicationHeader;
        }

        public static OBS.ServicosUsuariosWeb.Usuario ObterUsuarioFacebook(string idFacebook)
        {
            var servico = new AdministracaoClient();
            var usuarioFacebook = servico.ObterUsuariosAutenticador(new AutenticadorFacebook())
                                         .Cast<AutenticadorFacebook>()
                                         .FirstOrDefault(x => x.IdFacebook == idFacebook);
            if (usuarioFacebook == null)
                throw new Exception("Facebook não cadastrado");

            return servico.ObterPorIdUsuario(usuarioFacebook.IdUsuario);
        }

        public static BeneficiarioKit IdentificarVinculo(string vinculo)
        {
            var servico = new AssociadoClient();
            return servico.ObterBeneficiarioCredencial(vinculo);
        }
    }
}
