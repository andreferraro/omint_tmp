﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace Omint.AtendimentoSemPapel.EnvioPesquisa
{
    class Program
    {
        static void Main(string[] args)
        {
            var sender = new Sender();
            sender.Send();

            Console.ReadLine();
        }
    }

    public class Sender
    {
        private const string HTML = "<html>" +
            "<body>" +
            "Acesse sua pesquisa de satisfação clicando <a href='{0}'>aqui</a>" +
            "</body>" +
            "</html>";

        private readonly string _smtpServer;
        private readonly int _smtpPort;
        private readonly string _smtpUsername;
        private readonly string _smtpPassword;
        private readonly string _smtpFrom;
        private readonly string _apiUrl;

        public Sender()
        {
            this._smtpServer = ConfigurationManager.AppSettings.Get("SMTP_SERVER");
            this._smtpPort = int.Parse(ConfigurationManager.AppSettings.Get("SMTP_PORT"));
            this._smtpUsername = ConfigurationManager.AppSettings.Get("SMTP_USERNAME");
            this._smtpPassword = ConfigurationManager.AppSettings.Get("SMTP_PASSWORD");
            this._smtpFrom = ConfigurationManager.AppSettings.Get("SMTP_FROM");

            this._apiUrl = ConfigurationManager.AppSettings.Get("API_URL");
        }

        public void Send()
        {
            var repositorio = new Repositorio.AtendimentoRepositorio();
            var atendimentos = repositorio.ObterUltimos();

            foreach (var atendimento in atendimentos)
            {
                try
                {
                    var smtp = new SmtpClient(this._smtpServer, this._smtpPort)
                    {
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(this._smtpUsername, this._smtpPassword)
                    };
                    var api = string.Format(this._apiUrl, atendimento.Hash);
                    var template = string.Format(HTML, api);

                    var mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress(this._smtpFrom);
                    //mailMessage.To.Add("johny.lima@esx.com.br");
                    mailMessage.To.Add(atendimento.Email);
                    mailMessage.Subject = "Pesquisa de Satisfação";
                    mailMessage.Body = template;
                    mailMessage.IsBodyHtml = true;
                    smtp.Send(mailMessage);
                    Console.WriteLine("Email enviado para " + atendimento.Email);
                    repositorio.AtualizaStatusEnvioEmail(atendimento.CodAtendimento);
                    Console.WriteLine("Atendimento chamado para " + atendimento.CodAtendimento);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}
