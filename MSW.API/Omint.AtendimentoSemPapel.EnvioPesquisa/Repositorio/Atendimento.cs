﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omint.AtendimentoSemPapel.EnvioPesquisa.Repositorio
{
    public class AtendimentoRepositorio
    {
        private readonly string _connectionString;
        public AtendimentoRepositorio()
        {
            this._connectionString = ConfigurationManager.ConnectionStrings["OmintExterno"].ConnectionString;
        }

        public IEnumerable<Atendimento> ObterUltimos(int horas = 8)
        {
            var sql =
             "SELECT d.CodAtendimento AS CodAtendimento, " +
             "        d.CodPrestador   AS CodPrestador,  " +
             "        d.CodAssociado   AS CodAssociado, " +
             "        b.[login]        AS Email, " +
             "        d.DtAvaliacao    AS DtAvaliacao, " +
             "        d.EnvioAvaliacao AS EnvioAvaliacao " +
             "FROM [AdministracaoDeUsuarios].[AdmUserWeb].[tb_Usuario] a " +
             "JOIN [AdministracaoDeUsuarios].[AdmUserWeb].[tb_AutenticadorOmint] b " +
             "ON a.id_usuario = b.id_usuario " +
             "JOIN [AdministracaoDeUsuarios].[AdmUserWeb].[tb_OperacaoPerfilVinculoOmint] c " +
             "ON a.nr_documento = c.nr_documento " +
             "JOIN [externo].[SemPapel].[Atendimento] d " +
             "ON SUBSTRING(c.nr_IdentificacaoVinculo,0,11) = SUBSTRING(d.CodAssociado,0,11) " +
             "WHERE DtAtendimento >= dateadd(hour, -24, GETDATE()) " +
             " and (d.EnvioAvaliacao is null or d.EnvioAvaliacao = 0)";
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var atendimentos = connection.Query<Atendimento>(sql);
                return atendimentos;
            }
        }

        public void AtualizaStatusEnvioEmail(string codAtendimento)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var command = new SqlCommand(" update [externo].[SemPapel].[Atendimento] " +
                    "set EnvioAvaliacao = 1 " +
                    "where CodAtendimento = @CodAtendimento",
                    connection);
                command.Parameters.AddWithValue("@CodAtendimento", codAtendimento);
                command.ExecuteNonQuery();
            }
        }

    }

    public class Atendimento
    {
        public string CodAtendimento { get; set; }
        public string CodPrestador { get; set; }
        public string CodAssociado { get; set; }
        public string Email { get; set; }
        public string Hash => AtendimentoHash.Criar(CodAtendimento, CodPrestador);
    }

    public class AtendimentoHash
    {
        public static string Criar(string codAtendimento, string codPrestador)
        {
            var text = System.Text.Encoding.UTF8.GetBytes($"{codAtendimento}.{codPrestador}");
            return System.Convert.ToBase64String(text);
        }

        public static void Converter(string hash, out string codAtendimento, out string codPrestador)
        {
            var text = System.Convert.FromBase64String(hash);
            var value = System.Text.Encoding.UTF8.GetString(text);
            var parts = value.Split('.');
            codAtendimento = parts[0];
            codPrestador = parts[1];
        }
    }
}
