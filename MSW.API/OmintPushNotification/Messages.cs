﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace OmintPushNotification
{
    public static class Messages
    {
        public static async Task<List<OmintMessage>> GetAsync()
        {
            try
            {
                var url = Config.Instance.Endpoint + "PushNotification/Listar/?status=A";
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(url);
                    var json = await response.Content.ReadAsStringAsync();
                    var internalMessages = JsonConvert.DeserializeObject<InternalData>(json);
                    var messages = internalMessages.Data
                                   .Where(m => m.Status.Equals("A"));

                    return messages.ToList();
                }
            }
            catch
            {
                return null;
            }
        }

        public static void ChangeStatus(OmintMessage message, Status status)
        {
            var value = GetStatusValue(status);
            var url = Config.Instance.Endpoint + $"PushNotification/Atualizar?idPushNotification={message.Id}&jsonMessage=&status={value}";
            try
            {
                using (var client = new HttpClient())
                {
                    var response = client.PostAsync(url, null).Result;
                }
            }
            catch { }
        }

        private static string GetStatusValue(Status status)
        {
            switch (status)
            {
                case Status.ErroInterno:
                case Status.ErroServidor:
                    return "I";
                case Status.Enviado:
                default:
                    return "E";
            }
        }
    }

    internal class InternalData
    {
        public List<OmintMessage> Data { get; set; }
    }

    public class OmintMessage
    {
        public int Id { get; set; }
        public string DeviceId { get; set; }
        public string SistemaOperacional { get; set; }
        public string Status { get; set; }
        public string JsonMessage { get; set; }

        public string id_chamado { get; set; }
        public string Vinculo { get; set; }
        public string id_CategoriaPushNotification { get; set; }
        public string Referencia { get; set; }
        public string url_pagina { get; set; }
    }

    public enum Status
    {
        Enviado,
        ErroInterno,
        ErroServidor
    }
}
