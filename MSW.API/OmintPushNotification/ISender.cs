﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace OmintPushNotification
{
    public interface ISender
    {
        Task<Status> SendAsync(string token, 
                               string title, 
                               string body, 
                               IReadOnlyDictionary<string, string> data = null);
    }
}