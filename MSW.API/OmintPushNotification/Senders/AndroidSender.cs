﻿using System.Threading.Tasks;
using FCM.Net;
using System.Collections.Generic;

namespace OmintPushNotification.Senders
{
    public class AndroidSender : ISender
    {
        public async Task<Status> SendAsync(string token, string title, string body, IReadOnlyDictionary<string, string> data = null)
        {
            using (var sender = new Sender(Config.Instance.ServerKey))
            {
                var message = new Message
                {
                    RegistrationIds = new List<string> { token },
                    Notification = new Notification
                    {
                        Title = title,
                        Body = body
                    },
                    Data= Utils.DictinaryToJsonObject(data)
                };
                var result = await sender.SendAsync(message);

                if (result.MessageResponse.Success == 1)
                    return Status.Enviado;

                return Status.ErroServidor;
            }
        }
    }
}