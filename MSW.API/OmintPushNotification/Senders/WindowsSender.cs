﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OmintPushNotification.Senders
{
    public class WindowsSender : ISender
    {
        public async Task<Status> SendAsync(string token, string title, string body, IReadOnlyDictionary<string, string> data = null)
        {
            var message = this.ConvertToWindowsTileNotification(title, body);

            HttpWebRequest sender = (HttpWebRequest)WebRequest.Create(token);
            sender.Method = "POST";

            sender.ContentLength = message.Length;
            sender.ContentType = "text/xml";
            sender.Headers.Add("X-WindowsPhone-Target", "token");
            sender.Headers.Add("X-NotificationClass", "1");

            using (Stream requestStream = sender.GetRequestStream())
                await requestStream.WriteAsync(message, 0, message.Length);

            HttpWebResponse response = (HttpWebResponse)sender.GetResponse();
            var notificationStatus = response.Headers["X-NotificationStatus"];

            if (response.StatusCode == HttpStatusCode.OK)
                return Status.Enviado;

            return Status.ErroServidor;
        }

        private byte[] ConvertToWindowsTileNotification(string title, string body)
        {
            string message = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                                "<wp:Notification xmlns:wp=\"WPNotification\">" +
                                    "<wp:Tile>" +
                                    "<wp:BackgroundImage>" + "red.jpg" + "</wp:BackgroundImage>" +
                                    "<wp:Count>" + 1 + "</wp:Count>" +
                                    "<wp:Title>" + title + "</wp:Title>" +
                                    "<wp:BackTitle>" + title + "</wp:BackTitle>" +
                                    "<wp:BackContent>" + body + "</wp:BackContent>" +
                                "</wp:Tile> " +
                                "</wp:Notification>";

            byte[] notificationMessage = Encoding.Default.GetBytes(message);
            return notificationMessage;
        }
    }
}