﻿using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static PushSharp.Apple.ApnsConfiguration;

namespace OmintPushNotification.Senders
{
    public class iOSSender : ISender
    {
        public async Task<Status> SendAsync(string token, string title, string body, IReadOnlyDictionary<string, string> data = null)
        {
            ApnsServerEnvironment environment = ApnsServerEnvironment.Production;

            if (Config.Instance.Environment != null && Config.Instance.Environment.Equals("Sandbox", StringComparison.InvariantCultureIgnoreCase))
                environment = ApnsServerEnvironment.Sandbox;

            var config = new ApnsConfiguration(environment, Config.Instance.CertificatePath, Config.Instance.CertificatePassword);

            var apnsBroker = new ApnsServiceBroker(config);
            apnsBroker.OnNotificationFailed += (n, aex) =>
            {
                aex.Handle(ex =>
                {
                    if (ex is ApnsNotificationException)
                    {
                        var notificationException = (ApnsNotificationException)ex;
                        var statusCode = notificationException.ErrorStatusCode;
                        Console.WriteLine($"Apple Notification Failed: ID={notificationException.Notification.Identifier}, Code={statusCode}");
                    }
                    else
                    {
                        Console.WriteLine($"Apple Notification Failed for some unknown reason : {ex.InnerException}");
                    }
                    return true;
                });
            };

            apnsBroker.OnNotificationSucceeded += (n) => Console.WriteLine("Apple Notification Sent!");

            apnsBroker.Start();

            var message = this.ConvertToApnsNotification(token, body, data);
            apnsBroker.QueueNotification(message);

            apnsBroker.Stop();

            await Task.Delay(0);
            return Status.Enviado;
        }

        private ApnsNotification ConvertToApnsNotification(string token, 
                                                           string body, 
                                                           IReadOnlyDictionary<string, string> data)
        {
            var notification = new ApnsNotification
            {
                DeviceToken = token,
                Payload = JObject.FromObject(new { aps = new { alert = body, badge = 1, data=Utils.DictinaryToJsonObject(data) } })
            };

            return notification;
        }
    }
}