﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace OmintPushNotification
{
    class Program
    {
        static int Main(string[] args)
        {
            var program = new Program();
            var numberOfFailures = program.SendAsync().Result;
            return numberOfFailures;
        }

        private async Task<int> SendAsync()
        {
            var numberOfFailures = 0;
            var messages = await Messages.GetAsync();
            foreach (var message in messages)
            {
                var dynamic = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(message.JsonMessage);
                var status = await Push.SendAsync(message.SistemaOperacional, 
                                                  message.DeviceId, 
                                                  dynamic.data.title.Value, 
                                                  dynamic.data.body.Value,
                                                  new Dictionary<string, string>
                                                  {
                                                      ["Vinculo"] = message.Vinculo,
                                                      ["id_CategoriaPushNotification"] = message.id_CategoriaPushNotification,
                                                      ["Referencia"] = message.Referencia,
                                                      ["url_pagina"] = message.url_pagina
                                                  });

                Messages.ChangeStatus(message, status);

                if (status != Status.Enviado)
                    numberOfFailures++;
            }

            return numberOfFailures;
        }
    }
}