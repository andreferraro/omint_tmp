﻿using System.Configuration;

namespace OmintPushNotification
{
    public class Config
    {
        private static object _lock = new object();
        private static Config _instance;
        public static Config Instance
        {
            get
            {
                lock (_lock)
                {
                    if (_instance == null)
                        _instance = new Config();

                    return _instance;
                }
            }
        }

        public string Endpoint { get; private set; }
        public string ServerKey { get; private set; }

        public string CertificatePath { get; set; }
        public string Environment { get; set; }
        public string CertificatePassword { get; set; }

        private Config()
        {
            this.Endpoint = ConfigurationManager.AppSettings.Get("Endpoint");
            this.ServerKey = ConfigurationManager.AppSettings.Get("ServerKey");

            this.Environment = ConfigurationManager.AppSettings.Get("Environment");
            this.CertificatePath = ConfigurationManager.AppSettings.Get("CertificatePath");
            this.CertificatePassword = ConfigurationManager.AppSettings.Get("CertificatePassword");
        }
    }
}