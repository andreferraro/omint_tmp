﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OmintPushNotification
{
    public class Push
    {
        public static async Task<Status> SendAsync(string so, 
                                                   string token, 
                                                   string title, 
                                                   string body, 
                                                   IReadOnlyDictionary<string, string> data = null)
        {
            try
            {
                ISender sender = GetSender(so);
                return await sender.SendAsync(token, title, body, data);
            }
            catch (Exception ex)
            {
                return Status.ErroInterno;
            }
        }

        private static ISender GetSender(string os)
        {
            Func<string, bool> equals = (o) => os.Equals(o, StringComparison.InvariantCultureIgnoreCase);

            if (equals("android"))
                return new Senders.AndroidSender();

            if (equals("ios"))
                return new Senders.iOSSender();

            if (equals("windows"))
                return new Senders.WindowsSender();

            return null;
        }
    }
}
