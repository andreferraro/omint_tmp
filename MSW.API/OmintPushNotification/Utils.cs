﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;

namespace OmintPushNotification
{
    internal static class Utils
    {
        public static object DictinaryToJsonObject(IReadOnlyDictionary<string, string> data)
        {
            if (data == null) return null;

            var sb = new StringBuilder();

            sb.Append("{");
            foreach (var item in data)
                sb.Append($"'{item.Key}':'{item.Value}',");
            sb.Append("'json':'true'");
            sb.Append("}");

            var @object = JsonConvert.DeserializeObject(sb.ToString());
            return @object;
        }
    }
}