﻿using MSW.API.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace MSW.API.Controllers
{
    public class NoticiaController : CustomController
    {



        [HttpPost]
        public HttpResponseMessage Listar(int quantidade)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     string sharepointUrl = System.Configuration.ConfigurationManager.AppSettings["SharepointNoticiasSaudeUrl"];

                     var results = ObterNoticiasSharepoint(sharepointUrl, quantidade)
                     .Select(x =>
                     {
                         var dataModificacao = DateTime.Parse(x.Modified.ToString());
                         string _body = x.PublishingPageContent;
                         return new
                         {
                             title = x.Title.ToString(),
                             body = _body,
                             modified = dataModificacao,
                             diaModificacao = dataModificacao.Day,
                             mesModificacao = dataModificacao.ToString("MMM", CultureInfo.InvariantCulture),
                             anoModificacao = dataModificacao.Year,
                             horaModificacao = dataModificacao.Hour,
                             minutoModificacao = dataModificacao.Minute
                         };
                     })
                     .OrderByDescending(x => x.modified)
                     .Take(quantidade);

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(results));
                 },
                 "Ocorreu um erro ao buscar as noticias");
        }

        [HttpGet]
        public HttpResponseMessage ListarSeguroViagem(int quantidade)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     string sharepointUrl = System.Configuration.ConfigurationManager.AppSettings["SharepointNoticiasSeguroViagemUrl"];

                     var results = ObterNoticiasSharepoint(sharepointUrl, quantidade)
                     .Select(x =>
                     {
                         var dataModificacao = DateTime.Parse(x.Modified.ToString());
                         string _body = x.PublishingPageContent;
                         return new
                         {
                             title = x.Title.ToString(),
                             body = _body,
                             firstImage = Utils.Utils.GetImageUrl(_body),
                             modified = dataModificacao
                         };
                     })
                     .OrderByDescending(x => x.modified)
                     .Take(quantidade);

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(results));
                 },
                 "Ocorreu um erro ao buscar as noticias");
        }

        [NonAction]
        private IEnumerable<dynamic> ObterNoticiasSharepoint(string url, int quantidade)
        {
            string sharepointServer = System.Configuration.ConfigurationManager.AppSettings["SharepointServer"];
            string sharepointUrl = string.Concat(sharepointServer, url, quantidade.ToString());
            string sharepointUserName = System.Configuration.ConfigurationManager.AppSettings["SharepointUser"];
            string sharepointPassword = System.Configuration.ConfigurationManager.AppSettings["SharepointPassword"];
            string sharepointDefaultPage = System.Configuration.ConfigurationManager.AppSettings["SharepointNoticiasDefaultPage"];

            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(sharepointUrl);
            req.Method = "GET";
            req.Accept = "application/json;odata=verbose";
            req.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes("username:password"));
            req.Credentials = new NetworkCredential(sharepointUserName, sharepointPassword);

            HttpWebResponse resp = req.GetResponse() as HttpWebResponse;
            var encoding = ASCIIEncoding.ASCII;

            IEnumerable<dynamic> noticiasSharepoint;

            using (var reader = new StreamReader(resp.GetResponseStream(), encoding))
            {
                dynamic jsonObject = JsonConvert.DeserializeObject(reader.ReadToEnd());
                noticiasSharepoint = jsonObject.d.results;
            }

            noticiasSharepoint.ToList()
                .ForEach(x => {
                    string body = x.PublishingPageContent;
                    body = body.Replace("<img src=\"/", string.Format("<img src=\"{0}", sharepointServer));
                    x.PublishingPageContent = body;
                });

            return noticiasSharepoint
                .Where(x => !x.Title.ToString().Contains(sharepointDefaultPage));
        }
    }
}
