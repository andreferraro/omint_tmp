﻿using MSW.API.Utils;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using MSW.ServicesWrapper.OBS.PushNotification;

namespace MSW.API.Controllers
{
    public class PushNotificationController : CustomController
    {
        private readonly PushNotificationClient _pushNotificationClient = new PushNotificationClient();

        [HttpGet]
        public HttpResponseMessage Listar(string vinculo = null, string status = "A", int qtdItens = 20)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var result = _pushNotificationClient.ObterPushNotifications(vinculo, status, qtdItens)
                                  .Select(x => new {
                                      x.Id,
                                      x.DeviceId,
                                      x.SistemaOperacional,
                                      x.JsonMessage,
                                      x.Status,                                      
                                      x.Vinculo,
                                      x.id_CategoriaPushNotification,
                                      x.Referencia,
                                      x.url_pagina
                                  });

                     return Request.DataMessage(result);
                 },
                 "Ocorreu um erro ao listar os push notifications.");
        }

        [HttpPost]
        public HttpResponseMessage Atualizar(int idPushNotification, string jsonMessage, string status)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     _pushNotificationClient.AtualizarPushNotification(idPushNotification, jsonMessage, status);

                     return Request.EmptyDataMessage();
                 },
                 "Ocorreu um erro ao atualizar os dados do push notification.");
        }

        [HttpPost]
        public HttpResponseMessage RegistrarDevice(int idOperacaoPerfilOmint, string nrVinculo, string deviceId, string sistemaOperacional, string versaoSistemaOperacional)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     _pushNotificationClient.GravarUserDevice(idOperacaoPerfilOmint, nrVinculo, deviceId, sistemaOperacional, versaoSistemaOperacional);

                     return Request.EmptyDataMessage();
                 },
                 "Ocorreu um erro ao registrar o dispositivo do usuário.");
        }
    }
}