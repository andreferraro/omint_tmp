﻿using MSW.ServicesWrapper.OBS.AtendimentoSemPapel;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MSW.API.Controllers
{
    [RoutePrefix("api/checkin-atendimento-sem-papel")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CheckinAtendimentoSemPapelController : BaseApiController
    {
        private readonly AtendimentoSemPapelClient _service = new AtendimentoSemPapelClient();

        public IHttpActionResult CheckinValidation([FromBody] CheckinValidation request)
        {
            CheckinValidation validation = null;
            try
            {
                validation = _service.CheckinValidation(request);
                if (validation != null)
                    return Ok(validation);
                else
                    return NotFound();
            }
            catch
            {
                return Content(HttpStatusCode.BadRequest, validation);
            }
        }

        public IHttpActionResult CheckinConfirmation([FromBody]CheckinConfirmation request)
        {
            CheckinConfirmation confirmation = null;
            try
            {
                confirmation = _service.CheckinConfirmation(request);
                if (confirmation != null)
                    return Ok(confirmation);
                else
                    return NotFound();
            }
            catch
            {
                return Content(HttpStatusCode.BadRequest, confirmation);
            }
        }
    }
}