﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security;
using MSW.API.Utils;
using MSW.ServicesWrapper.OBS.ServicosApp;
using MSW.ServicesWrapper.OBS.ServicosAssociado;
using MSW.ServicesWrapper.OBS.ServicosAtendimentoApp;
using MSW.ServicesWrapper.OBS.ServicosUsuariosWeb;
using MSW.ServicesWrapper.Wrapper;
using System;

namespace MSW.API.Controllers
{
    public class MenuController : CustomController
    {
        private readonly AdministracaoClient _administracaoClient = new AdministracaoClient();
        private readonly AssociadoClient _associadoClient = new AssociadoClient();
        private readonly AtendimentoAppClient _atendimentoAppClient = new AtendimentoAppClient();
        private readonly ServicosAppClient _servicosAppClient = new ServicosAppClient();

        private IAuthenticationManager Authentication => HttpContext.Current.GetOwinContext().Authentication;

        [Route("~/api/chamados")]
        [HttpPost, Authorize]
        public HttpResponseMessage CriarChamadoLoginUsuario(Models.UsuarioVinculo vinculo)
        {
            return HandleErrors(Request, () =>
            {
                var idAtendimento = _atendimentoAppClient.GravaChamadoAtendimento("I", null, 13, vinculo.Vinculo, 4722, "Login pela App.", "0");
                return Request.CreateResponse(HttpStatusCode.Created, idAtendimento);

            }, "Ocorreu um erro ao gravar o chamado de login");
        }

        [Route("~/api/vinculos")]
        [HttpGet, Authorize]
        public HttpResponseMessage ObterVinculos()
        {
            return HandleErrors(Request, () =>
            {
                var tipoDocumento = Authentication.User.FindFirst(x => x.Type == "TipoDocumento").Value;
                var numeroDocumento = Authentication.User.FindFirst(x => x.Type == "NumeroDocumento").Value;
                
                var vinculos =  _administracaoClient.ObterPerfilVinculo(null, numeroDocumento, tipoDocumento);
                var retorno = new
                {
                    CPF = vinculos.FirstOrDefault(x => x.IdNivel == 1)?.NumeroDocumento ?? "",
                    Vinculos = vinculos
                };

                return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(retorno));

            }, "Ocorreu um erro ao obter os vinculos disponíveis");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage AlterarNotificacaoStatus(string tipoServico, string idServico, string novoStatus)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var tipoDocumento = Authentication.User.Claims.First(x => x.Type == "TipoDocumento").Value;
                     var numeroDocumento = Authentication.User.Claims.First(x => x.Type == "NumeroDocumento").Value;

                     var vinculos = _administracaoClient.ObterPerfilVinculo(null, numeroDocumento, tipoDocumento);

                     var vinculo = vinculos.FirstOrDefault
                         (x => x.IdStatus == 1 && x.IdOperacaoOmint == 1 && x.IdPerfilOmint == 1);
                     if(vinculo != null)
                     {
                         var credencial = vinculo.NumeroIdentificacaoVinculo;

                         switch (tipoServico)
                         {
                             case "A": //Atendimentos
                                 _servicosAppClient.GravaServicoStatusHistorico
                                 (credencial.Substring(0, 8), null, tipoServico, idServico, novoStatus);
                                 break;
                             case "D": //Documentos
                                 _servicosAppClient.ListaServicoStatusHistorico(credencial.Substring(0, 8), null, "D")
                                 .ToList()
                                 .ForEach((item) =>
                                 {
                                     _servicosAppClient.GravaServicoStatusHistorico(credencial.Substring(0, 8), null, tipoServico, item.IdServico, novoStatus);
                                 });                                 
                                 break;
                             default:
                                 break;
                         }
                     }

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<bool>(true));
                 },
                 "Ocorreu um erro ao atualizar a notificação de status do item atual.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage ObterNotificacoes()
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var tipoDocumento = Authentication.User.Claims.First(x => x.Type == "TipoDocumento").Value;
                     var numeroDocumento = Authentication.User.Claims.First(x => x.Type == "NumeroDocumento").Value;

                     var vinculos = _administracaoClient.ObterPerfilVinculo(null, numeroDocumento, tipoDocumento);

                     var notificacoes = new Dictionary<string, IEnumerable<string>>();

                     var vinculo = vinculos.FirstOrDefault
                         (x => x.IdStatus == 1 && x.IdOperacaoOmint == 1 && x.IdPerfilOmint == 1);
                     if(vinculo != null)
                     {
                         var credencial = vinculo.NumeroIdentificacaoVinculo;

                         var atendimentos = _atendimentoAppClient.ListaChamados
                             (null, credencial.Substring(0, 8), null, 1, 13, null, 1, null);

                         var historicoAtendimento = _servicosAppClient.ListaServicoStatusHistorico(credencial.Substring(0, 8), null, "A");

                         var historicoDocumentos = _servicosAppClient.ListaServicoStatusHistorico(credencial.Substring(0, 8), null, "D");

                         foreach(var v in
                             atendimentos.Where
                                 (x =>
                                  x.AtStatus != "E" && historicoAtendimento.All(x2 => x2.IdServico != x.IdChamado)))
                         {
                             _servicosAppClient.GravaServicoStatusHistorico
                                 (credencial.Substring(0, 8), null, "A", v.IdChamado.ToString(), v.AtStatus);
                         }

                         var statusAlterados = historicoAtendimento.Join
                             (atendimentos,
                              x => x.IdServico,
                              x => x.IdChamado,
                              (historico, atendimento) => new
                              {
                                  Historico = historico,
                                  Atendimento = atendimento
                              }).Where(x => x.Atendimento.AtStatus != x.Historico.DsStatus).Select(x => x.Atendimento.IdChamado);

                         notificacoes.Add("Atendimento", statusAlterados);

                         notificacoes.Add("Meus Documentos", historicoDocumentos.Where(x => x.DsStatus != "E").Select(x => x.IdServico.ToString()));                        
                     }

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(notificacoes));
                 },
                 "Ocorreu um erro ao listar as notificações de status.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage ObterNotificacoesSeguros(string numeroBilhete)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var notificacoes = new Dictionary<string, IEnumerable<string>>();

                     if (!String.IsNullOrEmpty(numeroBilhete))
                     {
                         var atendimentos = _atendimentoAppClient.ListaChamados
                             (null, numeroBilhete, null, 1, 13, null, 10, null);

                         var historicoAtendimento = _servicosAppClient.ListaServicoStatusHistorico
                             (numeroBilhete, null, "A");

                         foreach (var v in
                             atendimentos.Where
                                 (x =>
                                  x.AtStatus != "E" && historicoAtendimento.All(x2 => x2.IdServico != x.IdChamado)))
                         {
                             _servicosAppClient.GravaServicoStatusHistorico
                                 (numeroBilhete, null, "A", v.IdChamado.ToString(), v.AtStatus);
                         }

                         var statusAlterados = historicoAtendimento.Join
                             (atendimentos,
                              x => x.IdServico,
                              x => x.IdChamado,
                              (historico, atendimento) => new
                              {
                                  Historico = historico,
                                  Atendimento = atendimento
                              }).Where(x => x.Atendimento.AtStatus != x.Historico.DsStatus).Select(x => x.Atendimento.IdChamado);

                         notificacoes.Add("Atendimento", statusAlterados);
                     }

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(notificacoes));
                 },
                 "Ocorreu um erro ao listar as notificações de status.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage AlterarNotificacaoStatusSeguros(string numeroBilhete, string tipoServico, string idServico, string novoStatus)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     if (!String.IsNullOrEmpty(numeroBilhete) && tipoServico == "A")
                             _servicosAppClient.GravaServicoStatusHistorico(numeroBilhete, null, tipoServico, idServico, novoStatus);

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<bool>(true));
                 },
                 "Ocorreu um erro ao atualizar a notificação de status do item atual.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage ObterServicosDisponiveis(Models.UsuarioVinculo vinculoSelecionado)
         {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var tipoDocumento = Authentication.User.Claims.First(x => x.Type == "TipoDocumento").Value;
                     var numeroDocumento = Authentication.User.Claims.First(x => x.Type == "NumeroDocumento").Value;

                     var vinculos =
                         _administracaoClient.ObterPerfilVinculo(null, numeroDocumento, tipoDocumento)
                             .OrderBy(x => x.IdOperacaoOmint)
                             .ThenBy(x => x.IdPerfilOmint);

                     var vinculoDefault = vinculos.Where(x => x.NumeroIdentificacaoVinculo == vinculoSelecionado.Vinculo).FirstOrDefault(x => x.TipoDocumento.Trim() == "CPF") ?? vinculos.First();
                     //vinculos.FirstOrDefault(x => x.TipoDocumento.Trim() == "CPF") ?? vinculos.First();
                     //vinculos.Where(x => x.NumeroIdentificacaoVinculo.Trim() == vinculoSelecinado);

                     var menu = new[]
                     {
                         new
                         {
                             Titulo = "Minha Omint",
                             Itens = new[]
                             {
                                 new MenuItem
                                 {
                                     Label = "Página Inicial",
                                     Icon = "fa-home",
                                     View = "views/restrito/home.html"
                                 }
                             }
                         },
                         new
                         {
                             Titulo = "Associados",
                             Itens = new[]
                             {
                                 new MenuItem
                                 {
                                     Label = "Meu Plano",
                                     Icon = "fa-user",
                                     View = "views/restrito/associado-plano-lista.html",
                                     Operacao = 1,
                                     Perfil = 1
                                 },
                                 new MenuItem
                                 {
                                     Label = "Rede Credenciada",
                                     Icon = "fa-thumb-tack",
                                     View = "views/restrito/redecredenciada-home.html",
                                     Operacao = 1,
                                     Perfil = 1
                                 },
                                 new MenuItem
                                 {
                                     Label = "Atendimento",
                                     Icon = "fa-comments",
                                     View = "views/restrito/atendimento-lista.html",
                                     Operacao = 1,
                                     Perfil = 1
                                 },
                                 new MenuItem
                                 {
                                     Label = "Credencial Digital",
                                     Icon = "fa-credit-card",
                                     View = "views/restrito/credencial-eletronica.html",
                                     Operacao = 1,
                                     Perfil = 1
                                 },
                                 new MenuItem
                                 {
                                     Label = "Faturas",
                                     Icon = "fa-file-text-o",
                                     View = "views/restrito/associado-lista-faturas.html",
                                     Operacao = 1,
                                     Perfil = 1
                                 },
                                 new MenuItem
                                 {
                                     Label = "Reembolsos",
                                     Icon = "fa-dollar",
                                     View = "views/restrito/reembolso-lista.html",
                                     Operacao = 1,
                                     Perfil = 1
                                 },
                                 
                                 new MenuItem
                                 {
                                     Label = "Autorizações",
                                     Icon = "fa-gavel",
                                     View = "views/restrito/autorizacao-lista.html",
                                     Operacao = 1,
                                     Perfil = 1
                                 },
                                 new MenuItem
                                 {
                                     Label = "Meus Documentos",
                                     Icon = "fa-archive",
                                     View = "views/restrito/meus-arquivos.html",
                                     Operacao = 1,
                                     Perfil = 1
                                 }
                             }
                         }
                     }.Select
                         (x => new
                         {
                             x.Titulo,
                             Itens = x.Itens.Select
                                   (x2 =>
                                   {
                                       x2.Vinculo = vinculos.FirstOrDefault
                                           (x3 =>
                                            x3.IdStatus == 1 && x2.Operacao == x3.IdOperacaoOmint
                                            && x2.Perfil == x3.IdPerfilOmint);
                                       return x2;
                                   }).Where(x2 => x2.Operacao == 0 && x2.Perfil == 0 || x2.Vinculo != null)
                         }).Where(x => x.Itens.Any());

                     var usuario = UsuariosWeb.ObterPorDocumento(tipoDocumento, numeroDocumento);

                     var redePlanosRaw = _associadoClient.ListarRedeAcessoBeneficiario(vinculoDefault.NumeroIdentificacaoVinculo);

                     var redePlanos = new
                     {
                         TodosPlanos = redePlanosRaw,
                         PlanosPadrao = redePlanosRaw.Where(x => x.IdPrioridade == 1),
                         TiposAtendimentoPadrao =
                             _associadoClient.ListarRedeTipoAtendimentoBeneficiario
                                 (vinculoDefault.NumeroIdentificacaoVinculo, "").OrderBy(x => x.IdPrioridade),
                         PlanosSubordinados = redePlanosRaw.Where(x => x.IdPrioridade != 1).Select
                             (x => new
                             {
                                 Plano = x,
                                 TiposAtendimento =
                                       _associadoClient.ListarRedeTipoAtendimentoBeneficiario
                                       (vinculoDefault.NumeroIdentificacaoVinculo, x.CodigoRede).OrderBy(x2 => x2.IdPrioridade)
                             })
                     };

                     return Request.CreateResponse
                         (HttpStatusCode.OK,
                          new Result<object>
                              (new
                              {
                                  Menu = menu,
                                  PossuiAutenticadorOmint =
                                   _administracaoClient.ObterUsuariosAutenticador(new AutenticadorOmint())
                                   .Cast<AutenticadorOmint>()
                                   .Any(x => x.IdUsuario == usuario.IdUsuario),
                                  PossuiAutenticadorFacebook =
                                   _administracaoClient.ObterUsuariosAutenticador(new AutenticadorFacebook())
                                   .Cast<AutenticadorFacebook>()
                                   .Any(x => x.IdUsuario == usuario.IdUsuario),
                                  RedePlanos = redePlanos,
                                  RedeTiposBuscaProximidade =
                                   _associadoClient.ListarRedeBuscaProximidade(vinculoDefault.NumeroIdentificacaoVinculo),
                                  EnderecoResidencial =
                                   _associadoClient.ObterDadosTitulo
                                   (vinculoDefault.NumeroIdentificacaoVinculo, true, true, true, true, true)
                                   .Enderecos.First(x => x.TipoEndereco.Codigo == "RES")
                              }));
                 },
                 "Ocorreu um erro ao listar os serviços disponíveis.");
        }

        private class MenuItem
        {
            public string Icon = "";
            public string Label = "";
            public int Operacao;
            public int Perfil;
            public string View = "";
            public PerfilVinculo Vinculo;
        }
        
    }
}
