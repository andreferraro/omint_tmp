﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MSW.API.Utils;
using MSW.ServicesWrapper.OBS.ServicosApp;
using MSW.ServicesWrapper.OBS.ServicosAssociado;
using MSW.ServicesWrapper.OBS.ServicosAtendimentoApp;
using MSW.ServicesWrapper.OBS.ServicosUsuariosWeb;
using MSW.ServicesWrapper.OBS.Util;
using MSW.ServicesWrapper.Wrapper;
using NLog;
using System.Runtime.Remoting.Contexts;

namespace MSW.API.Controllers
{
    public class PrimeiroAcessoController : CustomController
    {
        private static readonly Logger Logger = LoggingManager.GetCurrentClassLogger();

        private static readonly Dictionary<string, ClientePrimeiroAcesso> Processos =
            new Dictionary<string, ClientePrimeiroAcesso>();

        private readonly AdministracaoClient _administracaoClient = new AdministracaoClient();
        private readonly AssociadoClient _associadoClient = new AssociadoClient();
        private readonly AtendimentoAppClient _atendimentoAppClient = new AtendimentoAppClient();
        private readonly ServicosAppClient _servicosAppClient = new ServicosAppClient();
        private readonly UtilClient _utilClient = new UtilClient();

        private static KeyValuePair<string, ClientePrimeiroAcesso> GetClienteFromHash(string hash)
        {
            Processos.Where(x => x.Value.Date.AddHours(1) < DateTime.Now).ToList().ForEach(x => Processos.Remove(x.Key));

            var cliente = Processos.FirstOrDefault(x => x.Value.Hash == hash);
            if (cliente.Key == null)
            {
                throw new ApplicationException
                    ("Ocorreu um erro de validação no processo. Por favor inicie o processo novamente.");
            }

            return cliente;
        }

        [HttpGet]
        public HttpResponseMessage GetException()
        {
            throw new Exception("Erro");
        }

        [HttpPost]
        public HttpResponseMessage TermoAceiteAtual(string device)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     if (string.IsNullOrEmpty(device))
                     {
                         throw new ApplicationException("Dispositivo inválido.");
                     }

                     if (Processos.ContainsKey(device))
                     {
                         Processos.Remove(device);
                     }

                     Processos.Add
                         (device,
                          new ClientePrimeiroAcesso
                          {
                              Date = DateTime.Now,
                              Hash = Md5.FromString(device, 2)
                          });

                     var termo = "Termo de responsabilidade de utilização - Minha Omint" + Environment.NewLine
                                 + Environment.NewLine;

                     termo += _servicosAppClient.GerarTermoAceitePrimeiroAcesso();
                     termo = "<p>" + termo.Trim().Replace(Environment.NewLine, "</p><p>") + "</p>";

                     return Request.CreateResponse
                         (HttpStatusCode.OK,
                          new Result<object>
                              (new
                              {
                                  Termo = termo,
                                  Processos[device].Hash
                              }));
                 },
                 "Ocorreu um erro ao obter o termo de aceite.");
        }

        [HttpPost]
        public HttpResponseMessage AceitarTermo(string hash)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     GetClienteFromHash(hash);

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<bool>(true));
                 },
                 "Ocorreu um erro ao aceitar o termo de responsabilidade.");
        }

        [HttpPost]
        public HttpResponseMessage ValidarVinculo(string hash, string vinculoEncrypted)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var cliente = GetClienteFromHash(hash);

                     if (string.IsNullOrEmpty(vinculoEncrypted))
                     {
                         throw new ApplicationException("Vínculo Inválido!");
                     }

                     var vinculo = Utils.Utils.RsaDecrypt(vinculoEncrypted);

                     var usr = UsuariosWeb.IdentificarVinculo(vinculo);

                     if (usr == null)
                     {
                         throw new ApplicationException("O vínculo informado não foi encontrado.");
                     }

                     var documentos = new Dictionary<string, string>
                     {
                         { "CPF", usr.Cpf },
                         { "RG", usr.NrIdentidade },
                         { "RNE", usr.Rne }
                     };

                     var perfilVinculo =
                         documentos.SelectMany(x => _administracaoClient.ObterPerfilVinculo(null, x.Value, x.Key))
                             .OrderBy(x => x.IdOperacaoOmint)
                             .ThenBy(x => x.IdPerfilOmint)
                             .ToList();

                     foreach (var documento in documentos)
                     {
                         var usuarioComVinculo = UsuariosWeb.ObterPorDocumento(documento.Key.Trim(), documento.Value?.Trim());
                         if (usuarioComVinculo != null)
                         {
                             throw new ApplicationException("Você já possui um cadastro no “Minha Omint”. Caso não se recorde, acesse a “Recuperação de Senha” em nosso site e na sequencia efetue o “Login” no App.");
                         }
                         else
                         {
                             if (usr.IdUsuaprofile != 0)
                             {
                                 throw new ApplicationException("Você já possui um cadastro no “Minha Omint”. Caso não se recorde, acesse a “Recuperação de Senha” em nosso site e na sequencia efetue o “Login” no App.");
                             }
                         }
                     }

                     cliente.Value.PerfilVinculo = perfilVinculo.FirstOrDefault(x => x.TipoDocumento.Trim() == "CPF")
                                                   ?? perfilVinculo[0];

                     cliente.Value.DadosPlano = _associadoClient.ObterDadosBeneficiariosTitulo(usr.CdTitulo, false);

                     cliente.Value.BeneficiarioKit = usr;

                     cliente.Value.Emails =
                         _servicosAppClient.ListaContatoPrimeiroAcesso
                             (cliente.Value.PerfilVinculo.IdOperacaoOmint,
                              cliente.Value.PerfilVinculo.IdPerfilOmint,
                              "E",
                              cliente.Value.PerfilVinculo.NumeroIdentificacaoVinculo).Select(x => x.ToLower()).ToList();

                     if (perfilVinculo.Count != 0)
                     {
                         cliente.Value.Emails =
                             cliente.Value.Emails.Except
                                 (_administracaoClient.ObterUsuariosAutenticador(new AutenticadorOmint())
                                      .Cast<AutenticadorOmint>()
                                      .Select(x => x.Login.ToLower())).ToList();

                         if (cliente.Value.Emails.Count == 0)
                         {
                             throw new ApplicationException
                                 ("Os emails cadastrados para este contrato já estão sendo usados como o login de outro beneficiário. Contate-nos para adicionar um novo email.");
                         }
                     }
                     return Request.CreateResponse(HttpStatusCode.OK, new Result<ClientePrimeiroAcesso>(cliente.Value));
                 },
                 "Ocorreu um erro ao validar o vínculo informado.");
        }

        [HttpPost]
        public HttpResponseMessage ValidarVinculoPorCPF(string hash, string vinculoEncrypted)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var cliente = GetClienteFromHash(hash);

                     if (string.IsNullOrEmpty(vinculoEncrypted))
                     {
                         throw new ApplicationException("Vínculo Inválido!");
                     }

                     var vinculo = Utils.Utils.RsaDecrypt(vinculoEncrypted);

                     var perfilVinculo =  _administracaoClient.ObterPerfilVinculo(null, vinculo, "CPF").ToList();
                             
                     if (perfilVinculo == null)
                     {
                         throw new ApplicationException
                             ("O vínculo informado não possui um documento válido cadastrado. Contate-nos para realizar uma atualização cadastral.");
                     }

                     var credencial = perfilVinculo.FirstOrDefault()?.NumeroIdentificacaoVinculo ?? string.Empty;

                     var usr = UsuariosWeb.IdentificarVinculo(credencial);

                     if (usr == null)
                     {
                         throw new ApplicationException("O vínculo informado não foi encontrado.");
                     }

                     var usuarioComVinculo = UsuariosWeb.ObterPorDocumento("CPF", vinculo?.Trim());
                         if (usuarioComVinculo != null)
                         {
                             throw new ApplicationException("Você já possui um cadastro no “Minha Omint”. Caso não se recorde, acesse a “Recuperação de Senha” em nosso site e na sequencia efetue o “Login” no App.");
                         }
                         else
                         {
                             if (usr.IdUsuaprofile != 0)
                             {
                                 throw new ApplicationException("Você já possui um cadastro no “Minha Omint”. Caso não se recorde, acesse a “Recuperação de Senha” em nosso site e na sequencia efetue o “Login” no App.");
                             }
                         }

                     cliente.Value.PerfilVinculo = perfilVinculo.FirstOrDefault(x => x.TipoDocumento.Trim() == "CPF")
                                                   ?? perfilVinculo[0];

                     cliente.Value.DadosPlano = _associadoClient.ObterDadosBeneficiariosTitulo(usr.CdTitulo, false);

                     cliente.Value.BeneficiarioKit = usr;

                     cliente.Value.Emails =
                         _servicosAppClient.ListaContatoPrimeiroAcesso
                             (cliente.Value.PerfilVinculo.IdOperacaoOmint,
                              cliente.Value.PerfilVinculo.IdPerfilOmint,
                              "E",
                              cliente.Value.PerfilVinculo.NumeroIdentificacaoVinculo).Select(x => x.ToLower()).ToList();

                     if (cliente.Value.Emails.Count != 0)
                     {
                         cliente.Value.Emails =
                            cliente.Value.Emails.Except
                                (_administracaoClient.ObterUsuariosAutenticador(new AutenticadorOmint())
                                     .Cast<AutenticadorOmint>()
                                     .Select(x => x.Login.ToLower())).ToList();

                         if (cliente.Value.Emails.Count == 0)
                         {
                             throw new ApplicationException
                                 ("Os emails cadastrados para este contrato já estão sendo usados como o login de outro beneficiário. Contate-nos para adicionar um novo email.");
                         }
                     }

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<ClientePrimeiroAcesso>(cliente.Value));
                 },
                 "Ocorreu um erro ao validar o vínculo informado.");
        }

        [HttpPost]
        public HttpResponseMessage CadastrarUsuarioOmint(string hash, string senhaEncrypted)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var cliente = GetClienteFromHash(hash);

                     var senha = Md5.FromString(Utils.Utils.RsaDecrypt(senhaEncrypted), 2);
                     var decrypted = Utils.Utils.RsaDecrypt(senhaEncrypted);

                     UsuariosWeb.AdicionarNovoUsuarioOmint
                         (cliente.Value.Login.ToLower(),
                          senha,
                          DateTime.Parse(cliente.Value.BeneficiarioKit.DtNascimento, CultureInfo.CreateSpecificCulture("pt-BR")),
                          cliente.Value.BeneficiarioKit.Nome.Trim(),
                          cliente.Value.PerfilVinculo.TipoDocumento.Trim(),
                          cliente.Value.PerfilVinculo.NumeroDocumento.Trim());

                     CriaUsuarioNuke(cliente, decrypted);

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<bool>(true));
                 },
                 "Ocorreu um erro ao cadastrar usuário.");
        }

        [HttpPost]
        public HttpResponseMessage CadastrarEmail(string email, string hash, string vinculoEncrypted)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                 var cliente = GetClienteFromHash(hash);

                 if (string.IsNullOrEmpty(vinculoEncrypted))
                 {
                     throw new ApplicationException("Vínculo Inválido!");
                 }

                 var vinculo = Utils.Utils.RsaDecrypt(vinculoEncrypted);
                 List<PerfilVinculo> perfilVinculo;

                 if (vinculo.Length > 11)
                 {
                         var usr = UsuariosWeb.IdentificarVinculo(vinculo);

                         var documentos = new Dictionary<string, string>
                          {
                             { "CPF", usr.Cpf },
                             { "RG", usr.NrIdentidade },
                             { "RNE", usr.Rne }
                          };

                         perfilVinculo =
                             documentos.SelectMany(x => _administracaoClient.ObterPerfilVinculo(null, x.Value, x.Key))
                                 .OrderBy(x => x.IdOperacaoOmint)
                                 .ThenBy(x => x.IdPerfilOmint)
                                 .ToList();
                 }
                 else
                 {
                         perfilVinculo = _administracaoClient.ObterPerfilVinculo(null, vinculo, "CPF").ToList();

                         if (perfilVinculo == null)
                         {
                             throw new ApplicationException
                                ("O vínculo informado não possui um documento válido cadastrado. Contate-nos para realizar uma atualização cadastral.");
                         }
                 }

                 var credencial = perfilVinculo.FirstOrDefault()?.NumeroIdentificacaoVinculo ?? string.Empty;

                     //Verifica lista de emails
                     cliente.Value.Emails = _servicosAppClient.ListaContatoPrimeiroAcesso
                    (cliente.Value.PerfilVinculo.IdOperacaoOmint,
                    cliente.Value.PerfilVinculo.IdPerfilOmint,
                    "E",
                    cliente.Value.PerfilVinculo.NumeroIdentificacaoVinculo).Select(x => x.ToLower()).ToList();

                 bool emailJaExiste = cliente.Value.Emails.Any(x => x == email);

                 if (emailJaExiste)
                 {
                    throw new ApplicationException("Esse e-mail já está cadastrado. Por favor informe um novo endereço de e-mail");
                 }

                  //Cadastrar email
                  _servicosAppClient.InserirEmail(email, credencial);

                  //Adicionar email a lista de emails
                  cliente.Value.Emails.Add(email);

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<ClientePrimeiroAcesso>(cliente.Value));
                 },
              "Ocorreu um erro ao cadastrar usuário.");
        }
        private void CriaUsuarioNuke(KeyValuePair<string, ClientePrimeiroAcesso> cliente, string senha)
        {
            int portalId = Convert.ToInt32(ConfigurationManager.AppSettings["PortalIdNuke"]);

            var minhaOmintService = new ServicesWrapper.OBS.ServicosMinhaOmint.UsuarioPortalMinhaOmintClient();
            var wsAtendimento = new ServicesWrapper.WSAtendimento.AtendimentoSoapClient();
            var omintDnn = new ServicesWrapper.OEX.DesktopModules.WebServiceSoapClient();

            // Cria usuário no DNN
            var insereUserDnn = omintDnn.CreateUser(
                Utils.Utils.GetAuthenticator(),
                portalId,
                cliente.Value.Login.ToLower(),
                cliente.Value.BeneficiarioKit.Nome.Split(' ')[0],
                cliente.Value.BeneficiarioKit.Nome.Split(' ')[1],
                cliente.Value.BeneficiarioKit.Nome,
                cliente.Value.Login.ToLower(),
                senha);

            //Concede permissões ao novo usuário no portal Minha Omint
            var contratoDnn = minhaOmintService.CriaUsuarioMinhaOmint(
                cliente.Value.Login.ToLower(),
                cliente.Value.PerfilVinculo.NumeroIdentificacaoVinculo,
                portalId,
                1);

            var usuarioNuke = omintDnn.GetUser(Utils.Utils.GetAuthenticator(), portalId, cliente.Value.Login.ToLower());

            wsAtendimento.ValidaLoginWeb("U",
                1,
                cliente.Value.PerfilVinculo.NumeroIdentificacaoVinculo.Substring(0, 8),
                cliente.Value.PerfilVinculo.NumeroIdentificacaoVinculo.Substring(8, 2),
                "NULL",
                usuarioNuke.UserID,
                "0");

            CriaChamadoTermoAceite(cliente);
        }

        private void CriaChamadoTermoAceite(KeyValuePair<string, ClientePrimeiroAcesso> cliente)
        {
            _atendimentoAppClient.GravaChamadoAtendimento
                            ("I",
                             null,
                             13,
                             cliente.Value.PerfilVinculo.NumeroIdentificacaoVinculo.Substring(0, 8),
                             3149,
                             _servicosAppClient.GerarTermoAceitePrimeiroAcesso(),
                             "0");

            Processos.Remove(cliente.Key);
        }

        [HttpPost]
        public HttpResponseMessage CadastrarUsuarioFacebook(string hash, string facebookEncrypted)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var cliente = GetClienteFromHash(hash);
                     var idFacebook = Md5.FromString(Utils.Utils.RsaDecrypt(facebookEncrypted), 2);
                     UsuariosWeb.AdicionarNovoUsuarioFacebook
                         (idFacebook,
                          DateTime.Parse(cliente.Value.BeneficiarioKit.DtNascimento, CultureInfo.CreateSpecificCulture("pt-BR")),
                          cliente.Value.BeneficiarioKit.Nome.Trim(),
                          cliente.Value.PerfilVinculo.TipoDocumento.Trim(),
                          cliente.Value.PerfilVinculo.NumeroDocumento.Trim());

                     //CriaUsuarioNuke(cliente, senha);

                     CriaChamadoTermoAceite(cliente);
                     return Request.CreateResponse(HttpStatusCode.OK, new Result<bool>(true));
                 },
                 "Ocorreu um erro ao realizar o cadastro. Esta conta do Facebook pode estar sendo utilizada por outro usuário.");
        }

        [HttpPost]
        public HttpResponseMessage EnviarConfirmacao(string hash, string email)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var cliente = GetClienteFromHash(hash);

                     cliente.Value.Login = email.ToLower();

                     var codigo =
                         _servicosAppClient.GerarChavePrimeiroAcesso
                             (cliente.Value.PerfilVinculo.IdOperacaoOmint,
                              cliente.Value.PerfilVinculo.IdPerfilOmint,
                              cliente.Value.PerfilVinculo.NumeroIdentificacaoVinculo).First();

                     var codigoInfo =
                         _servicosAppClient.ListaChavePrimeiroAcesso
                             (cliente.Value.PerfilVinculo.IdOperacaoOmint,
                              cliente.Value.PerfilVinculo.IdPerfilOmint,
                              cliente.Value.PerfilVinculo.NumeroIdentificacaoVinculo)
                             .First(x => DateTime.Now >= x.DataInicio && DateTime.Now <= x.DataFim);

                     MailChannel canal = null;
                     do
                     {
                         var tmpCanal = _utilClient.ObterListaCanalsEmail("APP").FirstOrDefault();
                         if (tmpCanal == null)
                         {
                             _utilClient.IncluirCanalEmail
                                 (new MailChannel
                                 {
                                     Name = "APP"
                                 });
                         }
                         else
                         {
                             canal = _utilClient.ObterCanalEmail(tmpCanal.Id);
                         }
                     } while (canal == null);

                     var remetente = _utilClient.ObterListaRemetentesEmail(null, null, "minhaomint@omint.com.br").First();

                     var substitutionList = new Dictionary<string, string>
                     {
                         { "%%Nome%%", cliente.Value.BeneficiarioKit.Nome },
                         { "%%Codigo%%", codigo },
                         { "%%Validade%%", codigoInfo.DataFim.ToString("dd/MM/yyyy") }
                     };

                     var sucesso = _utilClient.EnviarEmail
                         (canal.Id,
                          remetente.Id,
                          int.Parse(ConfigurationManager.AppSettings["IdTipoEmailPrimeiroAcesso"]),
                          int.Parse(ConfigurationManager.AppSettings["IdTemplateEmailPrimeiroAcesso"]),
                          ConfigurationManager.AppSettings["EmailPrimeiroAcessoDestinatarioFixo"] ?? cliente.Value.Login,
                          cliente.Value.BeneficiarioKit.Nome,
                          string.Empty,
                          "Omint - Código de Primeiro Acesso",
                          substitutionList,
                          "Omint - Código de Primeiro Acesso");

                     if (!sucesso)
                     {
                         throw new ApplicationException
                             ("Ocorreu um erro ao tentar enviar o código de confirmação para o email selecionado. Por favor tente novamente.");
                     }

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<bool>(true));
                 },
                 "Ocorreu um erro ao enviar o email de confirmação.");
        }

        [HttpPost]
        public HttpResponseMessage InformarConfirmacao(string hash, string codigo)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var cliente = GetClienteFromHash(hash);

                     var codigoGerado =
                         _servicosAppClient.GerarChavePrimeiroAcesso
                             (cliente.Value.PerfilVinculo.IdOperacaoOmint,
                              cliente.Value.PerfilVinculo.IdPerfilOmint,
                              cliente.Value.PerfilVinculo.NumeroIdentificacaoVinculo).First();

                     if (codigo != codigoGerado)
                     {
                         throw new ApplicationException("O código informado é inválido.");
                     }

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<bool>(true));
                 },
                 "Ocorreu um erro ao informar o código de confirmação.");
        }
    }
}
