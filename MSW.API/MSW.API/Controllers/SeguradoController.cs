﻿using Microsoft.Owin.Security;
using MSW.API.Utils;
using MSW.ServicesWrapper.Obs.ServicosSeguro;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Linq;
using System;
using MSW.ServicesWrapper.OBS.ServicosApp;
using MSW.ServicesWrapper.Wrapper;

namespace MSW.API.Controllers
{
    public class SeguradoController : CustomController
    {
        private readonly SeguroViagemClient _seguroViagemClient = new SeguroViagemClient();
        private readonly ServicosAppClient _servicosAppCliente = new ServicosAppClient();

        private IAuthenticationManager Authentication => HttpContext.Current.GetOwinContext().Authentication;

        [HttpPost, Authorize]
        public HttpResponseMessage ListaBilhetesAtivosSeguroViagem(string cpf)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     if (string.IsNullOrEmpty(cpf))
                         return Request.CreateResponse(HttpStatusCode.BadRequest);

                 var result = _seguroViagemClient.ObterBilhetesPorCpf(cpf)
                                    .Where(x => x.DataFimViagem >= DateTime.Now && x.DataCancelamento == default(DateTime))
                                    .Select
                                     (x => new
                                     {
                                         x.NumeroBilhete,
                                         x.NomeSegurado,
                                         x.NomeProduto,
                                         DataInicioViagem = x.DataInicioViagem.ToString("dd/MM/yyyy"),
                                         DataFimViagem = x.DataFimViagem.ToString("dd/MM/yyyy"),
                                         GrupoFamiliar = x.CpfContratante == cpf && x.CpfSegurado != cpf ? true : false,
                                         x.UrlBilhetePdf
                                     })
                                     .OrderBy(x => x.GrupoFamiliar)
                                     .ThenBy(x => x.NumeroBilhete);

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
                 },
                 "Ocorreu um erro ao listar os bilhetes.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage ObterNumeroBilhetePessoalAtivo(string cpf)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     if (string.IsNullOrEmpty(cpf))
                         return Request.CreateResponse(HttpStatusCode.BadRequest);

                     var result = Seguros.ObterNumeroBilhetePessoalAtivo(cpf);

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
                 },
                 "Ocorreu um erro ao listar os bilhetes.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage GuardarPosicaoAtualUsuario(string numeroBilhete, string latitude, string longitude)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     if (!String.IsNullOrEmpty(numeroBilhete))
                     {
                         _servicosAppCliente.GravarLocalizacao(latitude, longitude, DateTime.Now, numeroBilhete, null, DateTime.Now);
                     }

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(true));
                 },
                 "Ocorreu um erro ao salvar a atualizacao atual.");
        }

    }
}