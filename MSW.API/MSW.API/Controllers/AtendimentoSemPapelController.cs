﻿using System.Web.Http;
using System.Web.Http.Cors;
using System.Net.Http;
using MSW.ServicesWrapper.OBS.AtendimentoSemPapel;
using System.Threading.Tasks;
using OmintPushNotification;

namespace MSW.API.Controllers
{
    [RoutePrefix("api/atendimento-sem-papel")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AtendimentoSemPapelController : ApiController
    {
        private readonly AtendimentoSemPapelClient _service = new AtendimentoSemPapelClient();

        //public AtendimentoSemPapelController()
        //{  

        //}

        [HttpPost]
        [Route("gerar-chave-atendimento")]
        public async Task<IHttpActionResult> GenerateAttendanceKey([FromBody] GeradorChaveAtendimento request)
        {
            var msg = _service.GeraChaveDeAtendimento(request);
            if (msg == null)
                return BadRequest();

            foreach (var d in msg.Devices)
                await Push.SendAsync(d.ds_SistemaOperacional,
                                     d.nr_DeviceId,
                                     msg.Message.MessageTitle,
                                     msg.Message.MessageBody);

            return Ok();
        }

        [HttpGet]
        [Route("{hash}")]
        public IHttpActionResult GetAttendance(string hash)
        {
            var atendimento = _service.ObtemAtendimento(hash); 
            return Ok(atendimento);
        }

        [HttpPost]
        [Route("{hash}")]
        public IHttpActionResult GetAttendance(string hash, [FromBody] Avaliacao avaliacao)
        {
            var result = _service.EfetuaAvaliacaoAtendimento(hash, avaliacao);
            if (!result)
                return BadRequest();

            return Ok();
        }

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }
    }

    public class BaseApiController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.OK };
        }
    }
}