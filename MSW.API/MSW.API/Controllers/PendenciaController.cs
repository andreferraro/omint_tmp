﻿using Microsoft.Owin.Security;
using MSW.API.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace MSW.API.Controllers
{
    public class PendenciaController : CustomController
    {

        private IAuthenticationManager Authentication => HttpContext.Current.GetOwinContext().Authentication;
        [HttpPost, Authorize]
        public HttpResponseMessage ObterPendencias(string vinculo)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var servicosAppClient = new MSW.ServicesWrapper.OBS.ServicosApp.ServicosAppClient();

                     var pendências = servicosAppClient.ListarPendencias(
                         new ServicesWrapper.OBS.ServicosApp.ListarPendenciasRequest() { CodigoTitulo = vinculo });

                     var result = pendências.Pendencias;
                     return Request.CreateResponse(HttpStatusCode.OK, result);
                 });
        }

        [HttpPost, Authorize]
        public HttpResponseMessage ResolverPendenciaCheckin(CheckinConfirmationRequest request)
        {

            return HandleErrors
               (Request,
                () =>
                {
                    try
                    {
                        var baseUrl = System.Configuration.ConfigurationManager.AppSettings["AtendimentoSemPapelApiurl"];
                        var url = baseUrl + "/checkin/confirmation";
                        var req = (HttpWebRequest)WebRequest.Create(url);
                        req.Method = "POST";
                        req.Accept = "application/json";
                        req.ContentType = "application/json";
                        using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                        {
                            string json = request.ToJson();

                            streamWriter.Write(json);
                            streamWriter.Flush();
                        }

                        var resp = req.GetResponse();
                        using (var reader = new StreamReader(resp.GetResponseStream()))
                        {
                            var response = reader.ReadToEnd();
                            return Request.CreateResponse(HttpStatusCode.OK, response);
                        }
                    }
                    catch (Exception ex)
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());

                    }

                });
        }
    }


    public class CheckinConfirmationRequest
    {
        public string codAtendimento { get; set; }
        public int statusAprovacao { get; set; }
        public string chaveAtendimento { get; set; }
        public string telefoneBeneficiario { get; set; }
        public Localizacao localizacaoAssociado { get; set; }
        public string codAssociado { get; set; }
    }

    public class CheckinConfirmationResponse
    {
        public string Message { get; set; }

    }

    /// <summary>
    /// Localização Geográfica
    /// </summary>
    public class Localizacao
    {
        /// <summary>
        /// Coordenadas - Latitude.
        /// </summary>
        public decimal Latitude { get; set; }

        /// <summary>
        /// Coordenadas - Longitude.
        /// </summary>
        public decimal Longitude { get; set; }
    }
}
