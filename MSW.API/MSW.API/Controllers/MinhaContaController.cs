﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security;
using MSW.API.Utils;
using MSW.ServicesWrapper.OBS.ServicosApp;
using MSW.ServicesWrapper.OBS.ServicosUsuariosWeb;
using MSW.ServicesWrapper.Wrapper;
using System.Configuration;

namespace MSW.API.Controllers
{
    public class MinhaContaController : CustomController
    {
        private readonly AdministracaoClient _administracaoClient = new AdministracaoClient();
        private readonly ServicosAppClient _servicosAppClient = new ServicosAppClient();

        private IAuthenticationManager Authentication => HttpContext.Current.GetOwinContext().Authentication;

        [HttpPost, Authorize]
        public HttpResponseMessage AlterarSenha(string senhaAtualEncrypted, string novaSenhaEncrypted)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var senhaAtual = Utils.Utils.RsaDecrypt(senhaAtualEncrypted);
                     var novaSenha = Utils.Utils.RsaDecrypt(novaSenhaEncrypted);

                     var tipoDocumento = Authentication.User.Claims.First(x => x.Type == "TipoDocumento").Value;
                     var numeroDocumento = Authentication.User.Claims.First(x => x.Type == "NumeroDocumento").Value;

                     var usuario = UsuariosWeb.ObterPorDocumento(tipoDocumento, numeroDocumento);

                     var usuarioAutenticador =
                         _administracaoClient.ObterUsuariosAutenticador(new AutenticadorOmint())
                             .Cast<AutenticadorOmint>()
                             .First(x => x.IdUsuario == usuario.IdUsuario);
                     if (usuarioAutenticador.Senha != Md5.FromString(senhaAtual, 2))
                     {
                         throw new ApplicationException("A senha atual não foi informada corretamente.");
                     }

                     usuarioAutenticador.Senha = Md5.FromString(novaSenha, 2);

                     _administracaoClient.AlterarAutenticadorOmint(usuarioAutenticador);

                     // Altera usuário no DNN
                     var omintDnn = new ServicesWrapper.OEX.DesktopModules.WebServiceSoapClient();

                     int portalId = Convert.ToInt32(ConfigurationManager.AppSettings["PortalIdNuke"]);

                     string sucesso = omintDnn.AlterUser(Utils.Utils.GetAuthenticator(),
                     portalId,
                     usuarioAutenticador.Login,
                     usuario.NomeUsuario.Split()[0],
                     usuario.NomeUsuario.Split()[1],
                     usuario.NomeUsuario,
                     usuarioAutenticador.Login,
                     novaSenha);

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<bool>(true));
                 },
                 "Ocorreu um erro ao alterar a senha.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage InserirAutenticacaoFacebook(string facebookEncrypted)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var tipoDocumento = Authentication.User.Claims.First(x => x.Type == "TipoDocumento").Value;
                     var numeroDocumento = Authentication.User.Claims.First(x => x.Type == "NumeroDocumento").Value;

                     var idFacebook = Md5.FromString(Utils.Utils.RsaDecrypt(facebookEncrypted), 2);

                     var usuario = UsuariosWeb.ObterPorDocumento(tipoDocumento, numeroDocumento);

                     _administracaoClient.InserirAutenticadorFacebook
                         (new AutenticadorFacebook
                         {
                             IdUsuario = usuario.IdUsuario,
                             IdFacebook = idFacebook
                         });

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<bool>(true));
                 },
                 "Ocorreu um erro ao vincular com Facebook.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage ListarEmailsDisponiveis()
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var tipoDocumento = Authentication.User.Claims.First(x => x.Type == "TipoDocumento").Value;
                     var numeroDocumento = Authentication.User.Claims.First(x => x.Type == "NumeroDocumento").Value;

                     var vinculos =
                         _administracaoClient.ObterPerfilVinculo(null, numeroDocumento, tipoDocumento)
                             .OrderBy(x => x.IdOperacaoOmint)
                             .ThenBy(x => x.IdPerfilOmint);

                     var vinculoDefault = vinculos.FirstOrDefault(x => x.TipoDocumento.Trim() == "CPF") ?? vinculos.First();

                     var emails =
                         _servicosAppClient.ListaContatoPrimeiroAcesso
                             (vinculoDefault.IdOperacaoOmint,
                              vinculoDefault.IdPerfilOmint,
                              "E",
                              vinculoDefault.NumeroIdentificacaoVinculo).Select(x => x.ToLower()).ToList();

                     if (emails.Count == 0)
                     {
                         throw new ApplicationException
                             ("Seu contrato não possui um email válido cadastrado. Contate-nos para adicionar um novo email.");
                     }

                     emails =
                         emails.Except
                             (_administracaoClient.ObterUsuariosAutenticador(new AutenticadorOmint())
                                  .Cast<AutenticadorOmint>()
                                  .Select(x => x.Login.ToLower())).ToList();

                     if (emails.Count == 0)
                     {
                         throw new ApplicationException
                             ("Os emails cadastrados para este contrato já estão sendo usados como o login de outro beneficiário. Contate-nos para adicionar um novo email.");
                     }

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(emails));
                 },
                 "Ocorreu um erro ao listar emails disponiveis.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage InserirAutenticacaoOmint(string login, string senhaEncrypted)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var tipoDocumento = Authentication.User.Claims.First(x => x.Type == "TipoDocumento").Value;
                     var numeroDocumento = Authentication.User.Claims.First(x => x.Type == "NumeroDocumento").Value;

                     var senha = Md5.FromString(Utils.Utils.RsaDecrypt(senhaEncrypted), 2);

                     var usuario = UsuariosWeb.ObterPorDocumento(tipoDocumento, numeroDocumento);

                     _administracaoClient.InserirAutenticadorOmint
                         (new AutenticadorOmint
                         {
                             IdUsuario = usuario.IdUsuario,
                             Login = login,
                             Senha = senha
                         });

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<bool>(true));
                 },
                 "Ocorreu um erro ao vincular com Facebook. Esta conta pode estar sendo utilizada por outro usuário.");
        }
    }
}
