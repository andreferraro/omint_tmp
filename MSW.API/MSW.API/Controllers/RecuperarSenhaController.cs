﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using MSW.API.Utils;
using MSW.ServicesWrapper.OBS.ServicosUsuariosWeb;
using MSW.ServicesWrapper.OBS.Util;
using NLog;
using MSW.ServicesWrapper.Wrapper;

namespace MSW.API.Controllers
{
    public class RecuperarSenhaController : CustomController
    {
        private readonly AdministracaoClient _administracaoClient = new AdministracaoClient();
        private readonly UtilClient _utilClient = new UtilClient();

        public string GerarCodigoConfirmacao(string chave)
        {
            return (from Match v in new Regex("\\d").Matches(Md5.FromString(DateTime.Now.ToString("ddMM") + chave))
                    select v.Value).Take(6).Aggregate("", (s, s1) => s + s1);
        }

        [HttpPost]
        public HttpResponseMessage EnviarCodigo(string email)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     email = email.Trim();

                     var usuarioAutenticador =
                         _administracaoClient.ObterUsuariosAutenticador(new AutenticadorOmint())
                             .Cast<AutenticadorOmint>()
                             .FirstOrDefault(x => string.Equals(x.Login, email, StringComparison.CurrentCultureIgnoreCase));

                     if(usuarioAutenticador == null)
                     {
                         throw new ApplicationException("Não foi localizado cadastro no App para este e-mail. Caso já tenha um cadastro no Minha Omint Web, utilize a recuperação de senha em nosso site. Não possuindo um cadastro no Minha Omint, utilize a opção “Primeiro Acesso”.");
                     }

                     var codigo = GerarCodigoConfirmacao(email);

                     MailChannel canal = null;
                     do
                     {
                         var tmpCanal = _utilClient.ObterListaCanalsEmail("APP").FirstOrDefault();
                         if(tmpCanal == null)
                         {
                             _utilClient.IncluirCanalEmail
                                 (new MailChannel
                                 {
                                     Name = "APP"
                                 });
                         }
                         else
                         {
                             canal = _utilClient.ObterCanalEmail(tmpCanal.Id);
                         }
                     } while(canal == null);

                     var remetente = _utilClient.ObterListaRemetentesEmail(null, null, "minhaomint@omint.com.br").First();

                     var substitutionList = new Dictionary<string, string>
                     {
                         { "%%Codigo%%", codigo }
                     };

                     var sucesso = _utilClient.EnviarEmail
                         (canal.Id,
                          remetente.Id,
                          int.Parse(ConfigurationManager.AppSettings["IdTipoEmailRecuperarSenha"]),
                          int.Parse(ConfigurationManager.AppSettings["IdTemplateEmailRecuperarSenha"]),
                          ConfigurationManager.AppSettings["EmailPrimeiroAcessoDestinatarioFixo"] ?? email,
                          null,
                          string.Empty,
                          "Omint - Código de Recuperação de Senha",
                          substitutionList,
                          "Omint - Código de Recuperação de Senha");

                     if(!sucesso)
                     {
                         throw new ApplicationException
                             ("Ocorreu um erro ao tentar enviar o código de confirmação para o email informado. Por favor tente novamente.");
                     }

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<bool>(true));
                 },
                 "Ocorreu um erro ao enviar o código de confirmação.");
        }
        private static readonly Logger Logger = LoggingManager.GetCurrentClassLogger();
        [HttpPost]
        public HttpResponseMessage InformarCodigo(string codigo, string email, string senhaEncrypted)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     email = email.Trim();

                     var codigoGerado = GerarCodigoConfirmacao(email);

                     if(codigo.Trim() != codigoGerado)
                     {
                         throw new ApplicationException("O código informado está expirado ou é invalido.");
                     }

                     var usuarioAutenticador =
                         _administracaoClient.ObterUsuariosAutenticador(new AutenticadorOmint())
                             .Cast<AutenticadorOmint>()
                             .First(x => string.Equals(x.Login, email.Trim(), StringComparison.CurrentCultureIgnoreCase));

                     usuarioAutenticador.Senha = Md5.FromString(Utils.Utils.RsaDecrypt(senhaEncrypted), 2);

                     _administracaoClient.AlterarAutenticadorOmint(usuarioAutenticador);

                     var novaSenha = Utils.Utils.RsaDecrypt(senhaEncrypted);
                     var usuario = UsuariosWeb.ObterPorIdUsuario(usuarioAutenticador.IdUsuario);
                     var omintDnn = new ServicesWrapper.OEX.DesktopModules.WebServiceSoapClient();
                     int portalId = Convert.ToInt32(ConfigurationManager.AppSettings["PortalIdNuke"]);
                     string sucesso = omintDnn.AlterUser(Utils.Utils.GetAuthenticator(),
                     portalId,
                     usuarioAutenticador.Login,
                     usuario.NomeUsuario.Split()[0],
                     usuario.NomeUsuario.Split()[1],
                     usuario.NomeUsuario,
                     usuarioAutenticador.Login,
                     novaSenha);

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<bool>(true));
                 },
                 "Ocorreu um erro ao enviar o código de confirmação.");
        }
    }
}
