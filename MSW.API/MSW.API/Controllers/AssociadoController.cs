﻿using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security;
using MSW.API.Utils;
using MSW.ServicesWrapper.OBS.ServicosAssociado;
using MSW.ServicesWrapper.OBS.ServicosApp;
using System;
using MSW.ServicesWrapper.OBS.ServicosUsuariosWeb;
using MSW.ServicesWrapper.OBS.ServicosAtendimentoApp;
using System.Data;
using System.Collections.Generic;
using System.Configuration;

namespace MSW.API.Controllers
{
    public class AssociadoController : CustomController
    {
        private readonly AssociadoClient _associadoClient = new AssociadoClient();
        private readonly ServicosAppClient _servicosAppCliente = new ServicosAppClient();
        private readonly AdministracaoClient _administracaoClient = new AdministracaoClient();
        private readonly AtendimentoAppClient _atendimentoAppClient = new AtendimentoAppClient();
        private readonly ServicesWrapper.WSAtendimento.AtendimentoSoapClient wsAtendimento = new ServicesWrapper.WSAtendimento.AtendimentoSoapClient();

        private IAuthenticationManager Authentication => HttpContext.Current.GetOwinContext().Authentication;

        [HttpPost, Authorize]
        public HttpResponseMessage ListaDadosPlano(string vinculo)
        {      
            return HandleErrors
                (Request,
                 () =>
                 {
                     int idClassifica;
                     int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaDadosPlano"], out idClassifica);
                     _atendimentoAppClient.GravaChamadoAtendimento
                         ("I", null, 13, vinculo.Substring(0, 8), idClassifica, "Listagem de dados do plano pela App.", "0");

                     var titulo = _associadoClient.ObterDadosBeneficiariosTitulo(vinculo.Substring(0, 8), false);
                     var titular = titulo.First(x => x.Titular);

                     var dados = new
                     {
                         Beneficiario = titular.DescricaoBeneficiario,
                         Vinculo = titular.Codigo + titular.Sequencia,
                         DataNascimento = titular.DataNascimento.ToString("dd/MM/yyyy"),
                         DataIngresso = titular.DataIngresso.ToString("dd/MM/yyyy"),
                         NumeroCns = titular.NumeroCns,
                         titular.GrauParentesco,
                         Planos = new[]
                         {
                             new
                             {
                                 CodigoPlano = titular.CodigoPlanoMedico,
                                 TipoPlano = "Medicina",
                                 RegistroAns = titular.NumeroRegistroMedicina,
                                 ClassificacaoPlano = titular.DescricaoContratacaoMedicina,
                                 SituacaoAns = titular.DescricaoSituacaoMedicina,
                                 Credencial = titular.NumeroCredencialMedica,
                                 DataFimCredencial = titular.DataFimCredencialMedica?.ToString("dd/MM/yyyy")
                             },
                             new
                             {
                                 CodigoPlano = titular.CodigoPlanoOdontologico,
                                 TipoPlano = "Odontologia",
                                 RegistroAns = titular.NumeroRegistroOdonto,
                                 ClassificacaoPlano = titular.DescricaoContratacaoOdonto,
                                 SituacaoAns = titular.DescricaoSituacaoOdonto,
                                 Credencial = titular.NumeroCredencialOdonto,
                                 DataFimCredencial = titular.DataFimCredencialOdonto?.ToString("dd/MM/yyyy")
                             }
                         },
                         Beneficiarios = titulo.
                            Where(x => !x.Titular).
                            Select(y => new {
                                Vinculo = y.Codigo + y.Sequencia,
                                Nome = y.DescricaoBeneficiario,
                                Sexo = (y.Sexo == "M") ? "Masculino" : "Feminino",
                                DataNascimento = y.DataNascimento.ToString("dd/MM/yyyy"),
                                y.NumeroCPF,
                                DataIngresso = y.DataIngresso.ToString("dd/MM/yyyy"),
                                y.GrauParentesco,
                                y.NumeroCns,
                                y.CodigoPlanoMedico,
                                y.NumeroCredencialMedica,
                                y.NumeroCredencialOdonto,
                                y.CodigoPlanoOdontologico,
                                DataFimCredencialMedica = (y.DataFimCredencialMedica==null)? null: ((DateTime) y.DataFimCredencialMedica).ToString("dd/MM/yyyy"),
                                DataFimCredencialOdonto = (y.DataFimCredencialOdonto==null)? null: ((DateTime) y.DataFimCredencialOdonto).ToString("dd/MM/yyyy")
                            })
                     };

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(dados));
                 },
                 "Ocorreu um erro ao listar os dados do plano.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage ListaDadosPlanoV2(string vinculo)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     int idClassifica;
                     int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaDadosPlano"], out idClassifica);
                     _atendimentoAppClient.GravaChamadoAtendimento
                         ("I", null, 13, vinculo.Substring(0, 8), idClassifica, "Listagem de dados do plano pela App.", "0");

                     var titulo = _associadoClient.ObterDadosBeneficiariosTitulo(vinculo.Substring(0, 8), false);
                     var titular = titulo.First(x => x.Titular);

                     var dados = new
                     {
                         Beneficiario = titular.DescricaoBeneficiario,
                         Vinculo = titular.Codigo + titular.Sequencia,
                         DataNascimento = titular.DataNascimento.ToString("dd/MM/yyyy"),
                         DataIngresso = titular.DataIngresso.ToString("dd/MM/yyyy"),
                         NumeroCns = titular.NumeroCns,
                         titular.GrauParentesco,
                         Planos = new[]
                         {
                             new
                             {
                                 CodigoPlano = titular.CodigoPlanoMedico,
                                 TipoPlano = "Medicina",
                                 RegistroAns = titular.NumeroRegistroMedicina,
                                 ClassificacaoPlano = titular.DescricaoContratacaoMedicina,
                                 SituacaoAns = titular.DescricaoSituacaoMedicina,
                                 Credencial = titular.NumeroCredencialMedica,
                                 DataFimCredencial = titular.DataFimCredencialMedica?.ToString("dd/MM/yyyy")
                             },
                             new
                             {
                                 CodigoPlano = titular.CodigoPlanoOdontologico,
                                 TipoPlano = "Odontologia",
                                 RegistroAns = titular.NumeroRegistroOdonto,
                                 ClassificacaoPlano = titular.DescricaoContratacaoOdonto,
                                 SituacaoAns = titular.DescricaoSituacaoOdonto,
                                 Credencial = titular.NumeroCredencialOdonto,
                                 DataFimCredencial = titular.DataFimCredencialOdonto?.ToString("dd/MM/yyyy")
                             }
                         },
                         Beneficiarios = titulo.
                            Select(y => new {
                                Vinculo = y.Codigo + y.Sequencia,
                                Nome = y.DescricaoBeneficiario,
                                Sexo = (y.Sexo == "M") ? "Masculino" : "Feminino",
                                DataNascimento = y.DataNascimento.ToString("dd/MM/yyyy"),
                                y.NumeroCPF,
                                DataIngresso = y.DataIngresso.ToString("dd/MM/yyyy"),
                                y.GrauParentesco,
                                y.NumeroCns,
                                y.CodigoPlanoMedico,
                                y.NumeroCredencialMedica,
                                y.NumeroCredencialOdonto,
                                y.CodigoPlanoOdontologico,
                                DataFimCredencialMedica = (y.DataFimCredencialMedica == null) ? null : ((DateTime)y.DataFimCredencialMedica).ToString("dd/MM/yyyy"),
                                DataFimCredencialOdonto = (y.DataFimCredencialOdonto == null) ? null : ((DateTime)y.DataFimCredencialOdonto).ToString("dd/MM/yyyy")
                            })
                     };

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(dados));
                 },
                 "Ocorreu um erro ao listar os dados do plano.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage ListaFaturas(string vinculo)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     int idClassifica;
                     int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaFatura"], out idClassifica);
                     _atendimentoAppClient.GravaChamadoAtendimento
                         ("I", null, 13, vinculo.Substring(0, 8), idClassifica, "Listagem de faturas pela App.", "0");

                     var result = _associadoClient.ListarFaturas(vinculo.Substring(0, 8)).Select
                         (x => new
                         {
                             x.TipoFatura,
                             x.StatusPagamento,
                             DataVencimento = x.DataVencimento?.ToString("dd/MM/yyyy"),
                             x.NrTitulo,
                             ValorFatura = x.ValorFatura?.ToString("c", CultureInfo.CreateSpecificCulture("pt-BR")),
                             DataLiquidacao = x.DataLiquidacao?.ToString("dd/MM/yyyy"),
                             MesReferencia = x.DrReferencia?.ToString("MM/yyyy")
                         });

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
                 },
                 "Ocorreu um erro ao listar as faturas.");
        }

        [HttpGet]
        public HttpResponseMessage ObterFatura(int nrTitulo)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var documentos = _associadoClient.ObterFaturaDocumento(nrTitulo);
                     if (documentos.Length == 0)
                     {
                         return new HttpResponseMessage(HttpStatusCode.NotFound)
                         {
                             Content =
                                 new StringContent
                                     ("Não foi encontrado um documento para a fatura selecionada. Por favor entre em contato.",
                                      Encoding.UTF8,
                                      "text/html")
                         };
                     }

                     var response = new HttpResponseMessage(HttpStatusCode.OK)
                     {
                         Content = new StreamContent(new MemoryStream(documentos[0].ImDocumentoImagem))
                     };
                     response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                     {
                         FileName = documentos[0].DescricaoNomeArquivo
                     };

                     return response;
                 },
                 "Ocorreu um erro ao obter a fatura.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage ObterCredencialEletronica(string codigoTitulo)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     int idClassifica;
                     int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaCredencialEletronica"], out idClassifica);
                     _atendimentoAppClient.GravaChamadoAtendimento
                         ("I", null, 13, codigoTitulo.Substring(0, 8), idClassifica, "Listagem de credencial eletronica pela App.", "0");

                     var credencialWCF = _servicosAppCliente.ObterCredencialEletronica(codigoTitulo);
                     if (credencialWCF == null)
                     {
                         return new HttpResponseMessage(HttpStatusCode.NotFound)
                         {
                             Content =
                                 new StringContent
                                     ("Não foi encontrado uma credencia eletrônica. Por favor entre em contato.",
                                      Encoding.UTF8,
                                      "text/html")
                         };
                     }

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(credencialWCF));
                 },
                 "Ocorreu um erro ao obter a credencial eletrônica.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage GuardarPosicaoAtualUsuario(string latitude, string longitude)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var tipoDocumento = Authentication.User.Claims.First(x => x.Type == "TipoDocumento").Value;
                     var numeroDocumento = Authentication.User.Claims.First(x => x.Type == "NumeroDocumento").Value;

                     var vinculos = _administracaoClient.ObterPerfilVinculo(null, numeroDocumento, tipoDocumento);

                     var vinculo = vinculos.FirstOrDefault
                         (x => x.IdStatus == 1 && x.IdOperacaoOmint == 1 && x.IdPerfilOmint == 1);

                     string credencial = string.Empty;

                     if (vinculo != null)
                     {
                         credencial = vinculo.NumeroIdentificacaoVinculo;
                     }

                     _servicosAppCliente.GravarLocalizacao(latitude, longitude, DateTime.Now,
                         credencial.Substring(0, 8), credencial.Substring(8, 2), DateTime.Now);

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(true));
                 },
                 "Ocorreu um erro ao salvar a atualizacao atual.");
        }

        public HttpResponseMessage ObterTelefone(string codigoTitulo, string seq)
        {
            return HandleErrors
            (Request,
             () =>
             {
                 var telefone = _associadoClient.ObterTelefones(0, codigoTitulo, seq);
                 if (telefone.Length == 0)
                 {
                     return new HttpResponseMessage(HttpStatusCode.NotFound)
                     {
                         Content =
                             new StringContent
                                 ("Não foi encontrado um telefone. Por favor entre em contato.",
                                  Encoding.UTF8,
                                  "text/html")
                     };
                 }

                 var response = new HttpResponseMessage(HttpStatusCode.OK)
                 {

                     Content =
                             new StringContent
                                  (telefone.FirstOrDefault().DDD.ToString() + telefone.FirstOrDefault().NumeroFormatado, 
                                  Encoding.UTF8,
                                  "text/html")
                 };

                 return response;
             },
             "Ocorreu um erro ao obter a fatura.");

        }

        [HttpGet]
        public HttpResponseMessage ObterCampanhas(int tipoUsuario)
        {
            return HandleErrors(Request,
                () =>
                {
                    List<object> campanhasAtivasObjeto = new List<object>();
                    var campanhasAtivas = wsAtendimento.GetCampanhas(tipoUsuario, "S");

                    foreach (DataTable table in campanhasAtivas.Tables)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            campanhasAtivasObjeto.Add(new
                            {
                                IdCampanha = row.Field<string>("id_Campanha"),
                                IdTipoCliente = row.Field<string>("id_tpCliente"),
                                DescricaoCampanha = row.Field<string>("ds_Campanha"),
                                DataInicio = row.Field<string>("dt_Inicio"),
                                DataFim = row.Field<string>("dt_Fim"),
                                DescricaoResumo = row.Field<string>("ds_Resumo"),
                                CampanhaAtiva = row.Field<string>("at_Ativo") == "S" ? true : false,
                                Path = row.Field<string>("ds_Path"),
                            });
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, campanhasAtivasObjeto);
                }, "Ocorreu um erro ao tentar obter campanhas.");
        }

        [HttpGet]
        public HttpResponseMessage ObterBeneficiariosPendentes(string codigoTitulo)
        {
            return HandleErrors(Request,
                () =>
                {
                    var beneficiariosAtivos = _associadoClient.ObterPlanosBeneficiarios(codigoTitulo, true, false);
                    var beneficiariosSemCPF = new List<Beneficiario>();

                    foreach (var b in beneficiariosAtivos)
                    {
                        if (b.Documentos.Where(d => d.TipoDocumento.Nome == "CPF").Count() == 0 &&
                            b.Pessoa.DataNascimento.AddYears(16) <= DateTime.Today)
                        {
                            beneficiariosSemCPF.Add(b);
                        } 
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, beneficiariosSemCPF);
                }, "Ocorreu um erro ao validar as informações dos beneficiários");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage AtualizarCpfBeneficiario(string codigoTitulo, string id_pessoa, string cpf)
        {
            return HandleErrors(Request,
                () =>
                {
                    //TIPO DOCUMENTO 3 = CPF
                    var tipoDocumento = 3;
                    var existeCpf = _associadoClient.ObterCPF(0, tipoDocumento, 0, cpf) != null;

                    if (existeCpf)
                        return Request.CreateResponse(HttpStatusCode.BadRequest, $"O Cpf {cpf} já está cadastrado!");

                    _associadoClient.AtualizarDocumento(codigoTitulo, id_pessoa, cpf, tipoDocumento);

                    return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(true));
                }, "Ocorreu um erro ao tentar atualizar o cpf");
        }

        [HttpGet]
        public HttpResponseMessage ObterConfirmacaoAceiteTermo(string codigoTitulo, string sqBeneficiario)
        {
            return HandleErrors(Request,
                () =>
                {
                    int idTermo;
                    var result = int.TryParse(ConfigurationManager.AppSettings["idTermoAceiteSeguroViagem"], out idTermo);
                    var aceite = _associadoClient.ObterConfirmacaoAceiteTermo(codigoTitulo, sqBeneficiario, idTermo);

                    return Request.CreateResponse(HttpStatusCode.OK, aceite);
                }, "Ocorreu um erro ao validar as informações dos beneficiários");
        }

        [HttpGet]
        public HttpResponseMessage ObterConvocacoesPendentes(string codigoTitulo)
        {
            return HandleErrors(Request,
                () =>
                {
                    var chamados = _atendimentoAppClient.ListaChamados(null, codigoTitulo, null, null, null, 8, null, null);

                    return Request.CreateResponse(HttpStatusCode.OK, chamados);
                }, "Ocorreu um erro ao validar as informações dos beneficiários");
        }
    }
}
