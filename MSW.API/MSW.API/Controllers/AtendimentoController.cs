﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using MSW.API.Utils;
using MSW.ServicesWrapper.OBS.ServicosAtendimentoApp;
using System;
using System.Configuration;
using MSW.API.Models;
using MSW.ServicesWrapper.WSChamado;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;

namespace MSW.API.Controllers
{
    public class AtendimentoController : CustomController
    {
        private readonly AtendimentoAppClient _atendimentoAppClient = new AtendimentoAppClient();
        private readonly ChamadoSoapClient _chamadoSoapClient = new ChamadoSoapClient();

        private IAuthenticationManager Authentication => HttpContext.Current.GetOwinContext().Authentication;

        [HttpPost, Authorize]
        public HttpResponseMessage NovoAtendimento(string vinculo, int idClassifica, string observacao)
        {
            var userId = Authentication.User.Identity.GetUserId();

            return HandleErrors
                (Request,
                 () =>
                 {
                    string idChamado = _atendimentoAppClient.GravaChamadoAtendimento
                         ("I", null, 13, vinculo, idClassifica, observacao, "0");

                     //TODO: proc precisa retornar IdChamado
                     return Request.CreateResponse(HttpStatusCode.OK, new Result<string>(idChamado));
                 },
                 "Ocorreu um erro ao criar novo atendimento.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage NovoAtendimentoComImagens()
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var userId = Authentication.User.Identity.GetUserId();

                     var vinculo = HttpContext.Current.Request.Form["Vinculo"];
                     var idClassifica = int.Parse(HttpContext.Current.Request.Form["IdClassifica"]);
                     var observacao = HttpContext.Current.Request.Form["Observacao"];

                     if (string.IsNullOrEmpty(vinculo))
                         throw new Exception("Número do vinculo não informado.");

                     if (string.IsNullOrEmpty(observacao))
                         throw new Exception("Observacao não informada.");

                     string idChamado = _atendimentoAppClient.GravaChamadoAtendimento
                     ("I", null, 13, vinculo, idClassifica, observacao, "0");

                     Dictionary<int, string> documentos = new Dictionary<int, string>();
                     foreach (string fileName in HttpContext.Current.Request.Files)
                     {
                         var file = HttpContext.Current.Request.Files[fileName];
                         var arrFileName = file.FileName.Split('.');
                         var extension = arrFileName[arrFileName.Length - 1];
                         int fileSizeInBytes = file.ContentLength;
                         MemoryStream target = new MemoryStream();
                         file.InputStream.CopyTo(target);
                         byte[] data = target.ToArray();
                         var base64String = Convert.ToBase64String(data);
                         int idDocumento = _chamadoSoapClient.GravaDocumentoChamadoBase64(Convert.ToDecimal(idChamado), base64String, extension);
                         documentos.Add(idDocumento, file.FileName);
                     }
                     var chamado = _atendimentoAppClient.ListaChamados(idChamado, vinculo, null, null, null, null, null, null);
                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(new
                     {
                         chamado[0].IdChamado,
                         documentos
                     }));
                 },
                 "Ocorreu um erro ao gravar o atendimento.");

        }

        public HttpResponseMessage AnexarArquivoAoChamado(string idChamado)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     return Request.CreateResponse(HttpStatusCode.OK, new Result<string>(idChamado));
                 },
                 "Ocorreu um erro ao anexar um arquivo ao chamado.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage Lista(string vinculo, string situacao, string idChamado, int idClassifica, int idTipoDemanda, int idTipoCliente = 1, string cpf = null)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var atendimentoAppClient = new AtendimentoAppClient();

                     var statusList = new Dictionary<string, string>
                     {
                         { "A", "Aberto" },
                         { "M", "Em andamento" },
                         { "E", "Encerrado" }
                     };

                     var chamados =
                         atendimentoAppClient.ListaChamados
                             (idChamado.Contains("-1") ? null : idChamado,
                              vinculo == string.Empty ? null : vinculo,
                              idClassifica == -1 ? (int?)null : idClassifica,
                              1,
                              13,
                              idTipoDemanda == -1 ? (int?)null : idTipoDemanda,
                              idTipoCliente,
                              cpf)
                             .Where(x => string.IsNullOrEmpty(situacao) || x.AtStatus == situacao)
                             .OrderByDescending(x => x.DataHoraEncerramento ?? x.DataHoraAtendeInicio);

                     var atendimentos = new
                     {
                         Atendimentos = chamados.Select
                             (x => new
                             {
                                 x.IdChamado,
                                 x.IdChamadoCC,
                                 CodigoStatus = x.AtStatus,
                                 DataInicio = x.DataHoraAtendeInicio?.ToString("dd/MM/yyyy"),
                                 DataHoraInicio = x.DataHoraAtendeInicio?.ToString("dd/MM/yyyy HH:mm"),
                                 DataHoraEncerramento = x.DataHoraEncerramento?.ToString("dd/MM/yyyy HH:mm"),
                                 DsTipoDemanda = x.DsTipoDemanda?.Trim(),
                                 DsAssunto = x.DsAssunto?.Trim(),
                                 Status = statusList[x.AtStatus],
                                 NumeroBilhete = x.NumeroBilhete?.Trim(),
                                 x.IdTipoDemanda
                             }),
                     };

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(atendimentos));
                 },
                 "Ocorreu um erro ao listar os atendimentos.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage Detalhe(string vinculo, string situacao, string idChamado, int idClassifica, int idTipoDemanda, int idTipoCliente = 1, string cpf = null)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var atendimentoAppClient = new AtendimentoAppClient();

                     var statusList = new Dictionary<string, string>
                     {
                         { "A", "Aberto" },
                         { "M", "Em andamento" },
                         { "E", "Encerrado" }
                     };

                     int CodUsuarioAssociado = Convert.ToInt32(ConfigurationManager.AppSettings["CodUsuarioAssociado"]);

                     var chamados =
                         atendimentoAppClient.ListaChamados
                             (idChamado.Contains("-1") ? null : idChamado,
                              vinculo == string.Empty ? null : vinculo,
                              idClassifica == -1 ? (int?)null : idClassifica,
                              1,
                              13,
                              idTipoDemanda == -1 ? (int?)null : idTipoDemanda,
                              idTipoCliente,
                              cpf);

                     var atendimentos = new
                     {
                         Atendimentos = chamados.Select
                             (x => new
                             {
                                 x.IdChamado,
                                 x.IdChamadoCC,
                                 NumeroBilhete = x.NumeroBilhete?.Trim(),
                                 CodigoStatus = x.AtStatus,
                                 DataHoraInicio = x.DataHoraAtendeInicio?.ToString("dd/MM/yyyy HH:mm"),
                                 DataHoraEncerramento = x.DataHoraEncerramento?.ToString("dd/MM/yyyy HH:mm"),
                                 DsTipoDemanda = x.DsTipoDemanda?.Trim(),
                                 DsAssunto = x.DsAssunto?.Trim(),
                                 Status = statusList[x.AtStatus],
                                 Imagens = _chamadoSoapClient.CarregaImagensChamado(x.IdChamadoCC),
                                 Observacoes =
                                       atendimentoAppClient.ListaObservacoesChamado(x.IdChamado)
                                       .OrderByDescending(x2 => x2.DataInclusao)
                                       .Select
                                       (x2 => new
                                       {
                                           x2.DescricaoObservacao,
                                           DataInclusao = x2.DataInclusao?.ToString("dd/MM/yyyy HH:mm"),
                                           TipoMensagem = x2.CodUsuario == CodUsuarioAssociado ? "A" : "O",
                                           x2.DescricaoUsuario
                                       })
                             })
                     };

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(atendimentos));
                 },
                 "Ocorreu um erro ao detalhar o atendimento.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage Filtro(int idTipoCliente = 1)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var atendimentoAppClient = new AtendimentoAppClient();

                     var atendimentos = new
                     {
                         Demandas = atendimentoAppClient.ObterDemandas(null, 1, 13, null, idTipoCliente)
                     };

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(atendimentos));
                 },
                 "Ocorreu um erro ao detalhar o atendimento.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage InserirObservacaoChamado(string numeroChamado, string observacaoChamado)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     string chamadoResult = _atendimentoAppClient.GravaObservacao
                         (null, numeroChamado, "000", observacaoChamado);

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<string>(chamadoResult));
                 },
                 "Ocorreu um erro ao criar novo atendimento.");
        }

        /// <summary>
        /// Insere as observações a um chamado existente inserindo também as imagens enviadas
        /// </summary>
        /// <remarks>
        /// Método sem parâmetros, recebe os campos {NumeroChamado}, {ObservacaoChamado} 
        /// e uma lista de imagens (geralmente enviados por um input type="file") de um formulário
        /// </remarks>
        /// <returns></returns>
        [HttpPost, Authorize]
        public HttpResponseMessage InserirObservacaoChamadoComImagens()
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var userId = Authentication.User.Identity.GetUserId();

                     var numeroChamado = HttpContext.Current.Request.Form["NumeroChamado"];
                     var observacaoChamado = HttpContext.Current.Request.Form["ObservacaoChamado"];
                     var vinculo = HttpContext.Current.Request.Form["Vinculo"];

                     if (string.IsNullOrEmpty(numeroChamado))
                         throw new Exception("Número do chamado não informado.");

                     if (string.IsNullOrEmpty(observacaoChamado))
                         throw new Exception("Observacao não informada.");

                     if (string.IsNullOrEmpty(vinculo))
                         throw new Exception("Vinculo não informado.");

                     string chamadoResult = _atendimentoAppClient.GravaObservacao
                         (null, numeroChamado, "000", observacaoChamado);

                     Dictionary<int, string> documentos = new Dictionary<int, string>();
                     foreach (string fileName in HttpContext.Current.Request.Files)
                     {
                         var file = HttpContext.Current.Request.Files[fileName];
                         var arrFileName = file.FileName.Split('.');
                         var extension = arrFileName[arrFileName.Length - 1];
                         int fileSizeInBytes = file.ContentLength;
                         MemoryStream target = new MemoryStream();
                         file.InputStream.CopyTo(target);
                         byte[] data = target.ToArray();
                         var base64String = Convert.ToBase64String(data);
                         int idDocumento = _chamadoSoapClient.GravaDocumentoChamadoBase64(Convert.ToDecimal(numeroChamado), base64String, extension);
                         documentos.Add(idDocumento, file.FileName);
                     }

                     var chamado = _atendimentoAppClient.ListaChamados(numeroChamado, vinculo, null, null, null, null, null, null);
                     if(chamado[0].IdTipoDemanda == (int)TipoDemanda.Convocacao)
                     {
                         _chamadoSoapClient.EncerraChamado(1, int.Parse(numeroChamado), "000");
                     }

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(new { chamadoResult }));
                 },
                 "Ocorreu um erro ao gravar o atendimento.");
        }

        [HttpPost, Authorize]
        public HttpResponseMessage AdicionarArquivoChamado(ImagemChamado imagemChamado)
        {
            return HandleErrors(
                Request,
                () => 
                {
                    Regex rgx = new Regex(@"^data:(image|application)\/(png|jpg|jpeg|pdf|tiff|bmp);base64,");
                    string str64 = rgx.Replace(imagemChamado.Base64File, "");
                    int idDocumento = _chamadoSoapClient.GravaDocumentoChamadoBase64(imagemChamado.IdChamado, str64, imagemChamado.Extensao);
                    return Request.CreateResponse(HttpStatusCode.OK, new Result<int>(idDocumento));
                },
                 "Ocorreu um erro ao inserir as imagens no chamado.");
            
        }

        [HttpPost, Authorize]
        public HttpResponseMessage ObterDocumentoChamado(decimal idDocumento)
        {
            return HandleErrors(
                Request,
                () =>
                {
                    var data = _chamadoSoapClient.ImagemDocumento(idDocumento);
                    return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(data));
                },
                 "Ocorreu um erro ao a imagem solicitada.");
        }
    }
}
