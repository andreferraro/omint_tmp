﻿using Microsoft.Owin.Security;
using MSW.API.Utils;
using MSW.ServicesWrapper.OBS.Autorizacao;
using MSW.ServicesWrapper.OBS.ServicosAtendimentoApp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;

namespace MSW.API.Controllers
{
    public class AutorizacaoController : CustomController
    {
        private readonly AutorizacaoClient _autorizacaoClient = new AutorizacaoClient();
        private readonly AtendimentoAppClient _atendimentoAppClient = new AtendimentoAppClient();

        private IAuthenticationManager Authentication => HttpContext.Current.GetOwinContext().Authentication;

        [HttpPost, Authorize]
        public HttpResponseMessage Lista(string vinculo, int ano, int mes)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     int idClassifica;
                     int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaAutorizacao"], out idClassifica);
                     _atendimentoAppClient.GravaChamadoAtendimento
                         ("I", null, 13, vinculo.Substring(0, 8), idClassifica, "Listagem de autorizacoes pela App.", "0");

                     var autorizacoes =
                         Enumerable.Range(mes == 0 ? 1 : mes, mes == 0 ? 12 : 1)
                             .SelectMany
                             (x =>
                              _autorizacaoClient.ListarAutorizacoes
                                  (null,
                                   vinculo.Substring(0, 8),
                                   vinculo.Substring(8, 2),
                                   null,
                                   new DateTime(ano, x, 01),
                                   0,
                                   null))
                             .OrderByDescending(x => x.DataEntrada)
                             .Select
                             (x => new
                             {
                                 x.IdAutorizacao,
                                 x.NomeTitular,
                                 x.SequenciaBeneficiario,
                                 x.CodigoTitulo,
                                 x.NomeBeneficiario,
                                 x.CodigoTipoSituacao,
                                 x.TipoSituacao,
                                 x.ConstaPendencia,
                                 x.Pendencia,
                                 DataEntrada = x.DataEntrada.ToString("dd/MM/yyyy"),
                                 DataLiberacao = x.DataLiberacao.Year == 1 ? "" : x.DataLiberacao.ToString("dd/MM/yyyy"),
                                 x.TipoServico,
                                 Detalhes =
                                       _autorizacaoClient.ListarAutorizacoesDetalhe(x.IdAutorizacao)
                                       .OrderByDescending(z => z.DataSolicitacao)
                                       .Select
                                       (z => new
                                       {
                                           z.IdAutorizacaoDetalhe,
                                           z.Ordem,
                                           z.TipoAutorizacao,
                                           z.NumeroCredencial,
                                           z.Sexo,
                                           z.Idade,
                                           DataSolicitacao = z.DataSolicitacao.ToString("dd/MM/yyyy"),
                                           DataPrevista = z.DataPrevista.ToString("dd/MM/yyyy"),
                                           z.CodigoPlano,
                                           z.NomeBeneficiario,
                                           z.CodigoHonorario,
                                           z.NomeHonorario,
                                           z.QuantidadeSolicitada,
                                           z.QuantidadeAutorizada,
                                           z.DocumentoPrestadorSolicitante,
                                           z.CodigoPrestadorSolicitante,
                                           z.NomePrestadorSolicitante,
                                           z.DocumentoPrestadorExecutante,
                                           z.CodigoPrestadorExecutante,
                                           z.NomePrestadorExecutante,
                                           z.DocumentoPrestadorLocal,
                                           z.CodigoPrestadorLocal,
                                           z.NomePrestadorLocal,
                                           z.Diarias,
                                           Observacao = string.IsNullOrEmpty(z.Observacao) ? "Não consta" : z.Observacao,
                                           z.Status
                                       })
                             });

                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(autorizacoes));
                 });
        }

        [HttpGet]
        public HttpResponseMessage ObterAutorizacao(int idAutorizacao)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var documentos = _autorizacaoClient.ObterAutorizacaoArquivo(idAutorizacao);
                     if (documentos.Length == 0)
                     {
                         return new HttpResponseMessage(HttpStatusCode.NotFound)
                         {
                             Content =
                                 new StringContent
                                     ("Não foi encontrado um documento para a fatura selecionada. Por favor entre em contato.",
                                      Encoding.UTF8,
                                      "text/html")
                         };
                     }

                     var response = new HttpResponseMessage(HttpStatusCode.OK)
                     {
                         Content = new StreamContent(new MemoryStream(documentos))
                     };
                     response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                     {
                         FileName = string.Format("Autorizacao_{0}", idAutorizacao.ToString())
                 };

                     return response;
                 },
                 "Ocorreu um erro ao obter a fatura.");
        }
    }
}
