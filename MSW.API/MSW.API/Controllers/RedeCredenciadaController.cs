﻿using Microsoft.Owin.Security;
using MSW.API.Models;
using MSW.API.Utils;
using MSW.ServicesWrapper.OBS.ServicosAssociado;
using MSW.ServicesWrapper.OBS.ServicosAtendimentoApp;
using MSW.ServicesWrapper.OBS.ServicosPrestador;
using MSW.ServicesWrapper.WSBuscaPrestador;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace MSW.API.Controllers
{
	public class RedeCredenciadaController : CustomController
	{
		private readonly AssociadoClient _associadoClient = new AssociadoClient();
		private readonly PrestadorClient _prestadorClient = new PrestadorClient();
		private readonly AtendimentoAppClient _atendimentoAppClient = new AtendimentoAppClient();

		private IAuthenticationManager Authentication => HttpContext.Current.GetOwinContext().Authentication;

		#region busca sem autenticacao

		[HttpGet]
		public HttpResponseMessage ObterDadosUsuario(string vinculo)
		{
			return HandleErrors(Request, () =>
			{
				RedeAcessoBeneficiario[] redePlanosRaw = _associadoClient.ListarRedeAcessoBeneficiario(vinculo);

				var redePlanos = new
				{
					TiposAtendimentoPadrao = _associadoClient.ListarRedeTipoAtendimentoBeneficiario(vinculo, "")
															 .OrderBy(x => x.IdPrioridade),
					Planos = redePlanosRaw,

					RedeCredenciadaVinculo = redePlanosRaw
								 .GroupBy(x => x.CodigoProduto)
								 .Select(x => new
								 {
									 CodigoRede = x.OrderBy(x2 => x2.IdPrioridade)?.FirstOrDefault().CodigoRede,
									 Descricao = x.OrderBy(x2 => x2.IdPrioridade)?.FirstOrDefault().DescricaoRede,

								 })
								 .ToArray(),
				};

				var resultado = new
				{
					Titulo = _associadoClient.ObterDadosTitulo(vinculo, false, true, false, false, false),
					DadosRede = redePlanos,

				};

				return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(resultado));
			},
			"Ocorreu um erro ao realizar a busca.");
		}

		[HttpGet]
		public HttpResponseMessage ObterPorNome(string nome, bool inativos, string vinculo , string codigoRede = null, string tipoPrecisao = null)
		{
			return HandleErrors
				(Request,
				 () =>
				 {
					 List<PrestadorModel> lstprestador = new List<PrestadorModel>();
					 codigoRede = !string.IsNullOrEmpty(codigoRede) ? codigoRede : (string.IsNullOrEmpty(vinculo) ? "" : vinculo);
					 using (BuscaSoapClient soapClient = new BuscaSoapClient())
					 {
						 DataSet ds = soapClient.RedeListaPrestadorFull(codigoRede, (inativos ? "I" : "A"), "S", tipoPrecisao, nome);
						 IEnumerable<DataRow> rows = ds.Tables[0].AsEnumerable();

						 for (var i = 0; i < ds.Tables[0].Rows.Count; i++)
						 {
							 var row = ds.Tables[0].Rows[i];

							 if (lstprestador.Any(x => x.CodigoPrestador.Equals(row["cd_prestador"].ToString().Trim()) && x.SrEndereco.Equals(row["sr_endereco"].ToString()) && x.CodigoRede.Equals(row["cd_rede"].ToString().Trim())))
								 continue;

							 var prestador = new PrestadorModel(row)
							 {
								 Telefones = ObterTelefonesPorPrestador(rows, row["cd_prestador"].ToString()),
								 Especialidades = ObterEspecialidadesPorPrestador(rows, row["cd_prestador"].ToString())
							 };
							 lstprestador.Add(prestador);
						 }
					 }
					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(lstprestador));
				 },
				 "Ocorreu um erro ao realizar a busca.");
		}

		[HttpGet]
		public HttpResponseMessage ObterListaDefault(string nome, bool inativos, string vinculo, string codigoRede = null, string tipoPrecisao = null)
		{
			return HandleErrors
				(Request,
				 () =>
				 {
					 List<PrestadorModel> lstprestadorD = new List<PrestadorModel>();
					 codigoRede = !string.IsNullOrEmpty(codigoRede) ? codigoRede : (string.IsNullOrEmpty(vinculo) ? "" : vinculo);
					 using (BuscaSoapClient soapClient = new BuscaSoapClient())
					 {
						 DataSet ds = soapClient.RedeListaPrestadorFull(codigoRede, (inativos ? "I" : "A"), "S", tipoPrecisao, nome);
						 IEnumerable<DataRow> rows = ds.Tables[0].AsEnumerable();

						 for (var i = 0; i < ds.Tables[0].Rows.Count; i++)
						 {
							 var row = ds.Tables[0].Rows[i];
							 if (lstprestadorD.Any(x => x.CodigoPrestador.Equals(row["cd_prestador"].ToString().Trim()) && x.SrEndereco.Equals(row["sr_endereco"].ToString())))
								 continue;

							 var prestador = new PrestadorModel(row)
							 {
								 Telefones = ObterTelefonesPorPrestador(rows, row["cd_prestador"].ToString()),
								 Especialidades = ObterEspecialidadesPorPrestador(rows, row["cd_prestador"].ToString())
							 };
							 lstprestadorD.Add(prestador);
						 }
					 }
					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(lstprestadorD));
				 },
				 "Ocorreu um erro ao realizar a busca (Cód.2).");
		}

		private static List<string> ObterTelefonesPorPrestador(IEnumerable<DataRow> rowCollection, string codPrestador)
		{
			var queryResult = (from row in rowCollection
							   where row["cd_prestador"].ToString() == codPrestador
							   group row by row["nr_telefone"] into g
							   select g.Key.ToString().Trim()).ToList();

			return queryResult;
		}

		private static List<string> ObterEspecialidadesPorPrestador(IEnumerable<DataRow> rowCollection, string codPrestador)
		{
			var queryResult = (from row in rowCollection
							   where row["cd_prestador"].ToString() == codPrestador
							   group row by row["ds_especialidade"] into g
							   select g.Key.ToString().Trim()).ToList();

			return queryResult;
		}


		[HttpGet]
		public HttpResponseMessage ObterPorEspecialidade
			(string rede, string tipoAtendimento, bool inativos, string nomeEspecialidade = "")
		{
			return HandleErrors
				(Request,
				 () =>
				 {
					 IEnumerable<RedeEspecialidade> especialidades = _prestadorClient.ListarRedeEspecialidade
						 (rede, tipoAtendimento, inativos ? "I" : "A", "S", "", "");

					 if (!string.IsNullOrEmpty(nomeEspecialidade))
					 {
						 especialidades = especialidades.Where
							 (x =>
							  x.DescricaoEspecialidade.IndexOf
								  (nomeEspecialidade.Trim(), StringComparison.CurrentCultureIgnoreCase) != -1);
					 }

					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(especialidades));
				 },
				 "Ocorreu um erro ao realizar a busca.");
		}

		[HttpGet]
		public HttpResponseMessage ObterRegioes
		   (string rede, string tipoAtendimento, string codigoEspecialidade, bool inativos, string vinculo = "")
		{
			return HandleErrors
				(Request,
				 () =>
				 {
					 if (!string.IsNullOrEmpty(vinculo))
					 {
						 int idClassifica;
						 int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaRedeCredenciada"], out idClassifica);
						 _atendimentoAppClient.GravaChamadoAtendimento
							 ("I", null, 13, vinculo.Substring(0, 8), idClassifica, "Listagem de rede credenciada por especialidade pela App.", "0");

					 }

					 object result;

					 var regioes =
						 _prestadorClient.ListarRedePrestadorRegiao
							 (rede, tipoAtendimento, codigoEspecialidade, inativos ? "I" : "A", "S", "", "")
							 .GroupBy(x => x.CodigoEstado)
							 .ToDictionary
							 (x => x.Key.Trim(),
							  x =>
							  x.GroupBy(x2 => x2.EnderecoCidade)
								  .ToDictionary
								  (x2 => x2.Key.Trim(),
								   x2 =>
								   x2.Where(x3 => !string.IsNullOrEmpty(x3.EnderecoBairro))
									   .Select(x3 => x3.EnderecoBairro.Trim())));

					 if (!string.IsNullOrEmpty(vinculo))
					 {
						 var enderecoResidencial =
							 _associadoClient.ObterDadosTitulo(vinculo, true, true, true, true, true)
								 .Enderecos.First(x => x.TipoEndereco.Codigo == "RES");

						 var enderecoResidencialEstado =
							 regioes.FirstOrDefault(x => x.Key.Trim() == enderecoResidencial.Logradouro.Municipio.CodigoUF.Trim())
								 .Key ?? "";

						 // Existem casos onde o nome da cidade de residência do credenciado é retornado sem acento,
						 // tornando a comparação de strings no frontend inviável
						 var enderecoResidencialCidade = "";
						 if (!string.IsNullOrEmpty(enderecoResidencialEstado))
						 {
							 enderecoResidencialCidade =
								 regioes[enderecoResidencialEstado].FirstOrDefault
									 (x =>
									  string.Compare
										  (x.Key.Trim(),
										   enderecoResidencial.Logradouro.Municipio.Nome.Trim(),
										   CultureInfo.CurrentCulture,
										   CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreCase) == 0).Key ?? "";
						 }

						 result = new
						 {
							 Regioes = regioes,
							 EnderecoResidencial = new
							 {
								 Estado = enderecoResidencialEstado,
								 Cidade = enderecoResidencialCidade
							 }
						 };

						 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
					 }

					 result = new
					 {
						 Regioes = regioes,
					 };

					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
				 },
				 "Ocorreu um erro ao realizar a busca.");
		}

		[HttpGet]
		public HttpResponseMessage ObterPorRegiao
			(string rede, string tipoAtendimento, string codigoEspecialidade, string codigoEstado, string cidade,
			 string bairro, bool inativos, string vinculo)
		{
			rede = !string.IsNullOrEmpty(rede) ? rede : (string.IsNullOrEmpty(vinculo) ? "" : vinculo);
			if (!string.IsNullOrEmpty(vinculo))
			{
				int idClassifica;
				int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaRedeCredenciada"], out idClassifica);
				_atendimentoAppClient.GravaChamadoAtendimento
					("I", null, 13, vinculo.Substring(0, 8), idClassifica, "Listagem de rede credenciada por regiao pela App.", "0");

			}

			return HandleErrors
				(Request,
				 () =>
				 {
					 var result = _prestadorClient.ListarRedePrestador
						 (rede,
						  tipoAtendimento,
						  codigoEspecialidade,
						  "",
						  "",
						  inativos ? "I" : "A",
						  "S",
						  codigoEstado,
						  cidade,
						  bairro);

					 var listaRetorno = new List<RedePrestador>();

					 for (var i = 0; i < result.Count(); i++)
					 {
						 var prestador = result.ElementAt(i);

						 if (listaRetorno.Any(x => x.CodigoPrestador.Equals(prestador.CodigoPrestador) &&
													x.SrEndereco.Equals(prestador.SrEndereco) && x.CodigoRede.Equals(prestador.CodigoRede)))
							 continue;

						 listaRetorno.Add(prestador);
					 }
					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(listaRetorno));
				 },
				 "Ocorreu um erro ao realizar a busca.");
		}

		[HttpGet]
		public HttpResponseMessage ObterPorRegiaoDefault
			(string rede, string tipoAtendimento, string codigoEspecialidade, string codigoEstado, string cidade,
			 string bairro, bool inativos, string vinculo)
		{
			rede = !string.IsNullOrEmpty(rede) ? rede : (string.IsNullOrEmpty(vinculo) ? "" : vinculo);
			if (!string.IsNullOrEmpty(vinculo))
			{
				int idClassifica;
				int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaRedeCredenciada"], out idClassifica);
				_atendimentoAppClient.GravaChamadoAtendimento
					("I", null, 13, vinculo.Substring(0, 8), idClassifica, "Listagem de rede credenciada por regiao pela App.", "0");

			}

			return HandleErrors
				(Request,
				 () =>
				 {
					 var result = _prestadorClient.ListarRedePrestador
						 (rede,
						  tipoAtendimento,
						  codigoEspecialidade,
						  "",
						  "",
						  inativos ? "I" : "A",
						  "S",
						  codigoEstado,
						  cidade,
						  bairro);

					 var listaRetorno = new List<RedePrestador>();

					 for (var i = 0; i < result.Count(); i++)
					 {
						 var prestador = result.ElementAt(i);

						 if (listaRetorno.Any(x => x.CodigoPrestador.Equals(prestador.CodigoPrestador) &&
												   x.SrEndereco.Equals(prestador.SrEndereco)))
							 continue;

						 listaRetorno.Add(prestador);
					 }
					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(listaRetorno));
				 },
				 "Ocorreu um erro ao realizar a busca.");
		}

		[HttpGet]
		public HttpResponseMessage ObterPorProximidade
			(string vinculo, string rede, string tipoAtendimento, string codigoEspecialidade, string buscaProximidadeTipo,
			 bool inativos)
		{
			return HandleErrors
				(Request,
				 () =>
				 {
					 int idClassifica;
					 int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaRedeCredenciada"], out idClassifica);
					 _atendimentoAppClient.GravaChamadoAtendimento
						 ("I", null, 13, vinculo.Substring(0, 8), idClassifica, "Listagem de rede credenciada por proximidade pela App.", "0");

					 var tipo = _associadoClient.ListarRedeBuscaProximidade(vinculo)
						 .First(x => x.TipoProximidade == buscaProximidadeTipo);

					 var googleApiKey = "AIzaSyCu402K5dQcG8j9b0QGUOJziVqwvLeig-8";

					 var url =
						 $"https://maps.googleapis.com/maps/api/geocode/json?key={googleApiKey}&components=postal_code:{tipo.Cep}&address={tipo.Cidade}%20-%20{tipo.CodigoEstado}";

					 float latitude;
					 float longitude;
					 try
					 {
						 var json = new WebClient().DownloadString(url);
						 dynamic obj = JsonConvert.DeserializeObject(json);

						 latitude = obj.results[0].geometry.location.lat;
						 longitude = obj.results[0].geometry.location.lng;
					 }
					 catch (Exception)
					 {
						 throw new ApplicationException
							 ("Ocorreu um erro ao consultar a posição do endereço cadastrado em nossa base de dados. Por favor tente novamente ou contate-nos para uma atualização cadastral.");
					 }

					 return BuscaProximidade(rede, tipoAtendimento, codigoEspecialidade, latitude, longitude, inativos);
				 },
				 "Ocorreu um erro ao realizar a busca por proximidade.");
		}

		[HttpGet]
		public HttpResponseMessage ObterPorProximidadeAtual
			(string rede, string tipoAtendimento, string codigoEspecialidade, string latitude, string longitude,
			 bool inativos, string vinculo = "")
		{
			if (!string.IsNullOrEmpty(vinculo))
			{
				int idClassifica;
				int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaRedeCredenciada"], out idClassifica);
				_atendimentoAppClient.GravaChamadoAtendimento
					("I", null, 13, vinculo.Substring(0, 8), idClassifica, "Listagem de rede credenciada por proximidade atual pela App.", "0");
			}

			return HandleErrors
				(Request,
				 () =>
				 BuscaProximidade
					 (rede,
					  tipoAtendimento,
					  codigoEspecialidade,
					  float.Parse(latitude, CultureInfo.InvariantCulture),
					  float.Parse(longitude, CultureInfo.InvariantCulture),
					  inativos),
				 "Ocorreu um erro ao realizar a busca por proximidade.");
		}

		[HttpGet]
		public HttpResponseMessage ObterDetalhamento
			(string rede, string codigoPrestador, string srEndereco, bool inativos)
		{

			return HandleErrors
				(Request,
				 () =>
				 {
					 var result = new
					 {
						 /* Especialidades =  
							  _prestadorClient.ListarRedePrestador
								  (rede, "", "", codigoPrestador, srEndereco, inativos ? "I" : "A", "S", "", "", "")
								  .Select
								  (x => new
								  {
									  x.CodigoEspecialidade,
									  DescricaoEspecialidade = x.DescricaoEspecialidade.Trim()
								  }),*/
						 Rede =
							_prestadorClient.ListarRedePrestador
							("", "", "", codigoPrestador, srEndereco, inativos ? "I" : "A", "S", "", "", "")
							.Select
							 (x => x.DescricaoRede.Trim()).ToList().Distinct(),
						 Especialidades =
							 _prestadorClient.ListarRedePrestador
								 (rede, "", "", codigoPrestador, srEndereco, inativos ? "I" : "A", "S", "", "", "")
								 .Select
								 (x => x.DescricaoEspecialidade.Trim()),
						 Acessos =
							 _prestadorClient.ListarRedePrestadorAcessos
								 (rede, "", codigoPrestador, srEndereco, inativos ? "I" : "A", "S")
								 .Select(x => x.DescricaoAcesso),
						 Contatos =
							  _prestadorClient.ListarRedePrestadorContatos
								  (rede, "", codigoPrestador, srEndereco, inativos ? "I" : "A", "S").Select
								  (x => new
								  {
									  EnderecoTelefoneDdd = x.EnderecoTelefoneDdd.Trim(),
									  EnderecoTelefonePrefixo = x.EnderecoTelefonePrefixo.Trim(),
									  EnderecoTelefone = x.EnderecoTelefone.Trim(),
									  EnderecoExtensao = x.EnderecoExtensao?.Trim() ?? ""
								  }),
						 Curriculo =
							 _prestadorClient.ListarRedePrestadorCurriculo(codigoPrestador)
								 .Where(x => !string.IsNullOrEmpty(x.DescricaoRegistro))
								 .GroupBy(x => x.TipoRegistro)
								 .Select
								 (x => new
								 {
									 Tipo = x.Key.Trim(),
									 Registros = x.Select(x2 => x2.DescricaoRegistro)
								 }),

						 Qualificacoes =
							 _prestadorClient.ListarRedePrestadorQualificacaoAns
								 (rede, "", codigoPrestador, inativos ? "I" : "A", "S")
					 };
					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
				 },
				 "Ocorreu um erro ao realizar a busca.");
		}

		[HttpGet]
		public HttpResponseMessage ObterPlanosAtendidos(string rede, string codigoPrestador)
		{
			return HandleErrors
				(Request,
				 () =>
				 {
					 var result = _prestadorClient.ListarRedePrestadorPlanosAtendidos(rede, codigoPrestador);

					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
				 },
				 "Ocorreu um erro ao realizar a busca.");
		}

		[HttpGet]
		public HttpResponseMessage ObterSubstitutos(string vinculo, string rede, string codigoPrestador, string srEndereco)
		{
			return HandleErrors
				(Request,
				 () =>
				 {
					 var result = _prestadorClient.ListarRedePrestadorSubstitutos(rede, codigoPrestador, srEndereco);

					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
				 },
				 "Ocorreu um erro ao realizar a busca.");
		}


		[HttpGet]
		public HttpResponseMessage AutoComplete(string nome)
		{
			return HandleErrors
				(Request,
				 () =>
				 {
					 var result = _prestadorClient.ListarRedePrestadorAutoComplete(nome)
									.Select(x => x.Palavra.Trim()).ToList().Distinct();
					 
					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
				 },
				 "Ocorreu um erro ao realizar a busca por palavra.");
		}



		#endregion

		#region busca autenticada
		[HttpPost, Authorize]
		public HttpResponseMessage BuscaNome(string codigoRede, string vinculo, string nome, bool inativos)
		{
			return HandleErrors
				(Request,
				 () =>
				 {
					 int idClassifica;
					 int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaRedeCredenciada"], out idClassifica);
					 _atendimentoAppClient.GravaChamadoAtendimento
						 ("I", null, 13, vinculo.Substring(0, 8), idClassifica, "Listagem de rede credenciada por nome pela App.", "0");

					 var enderecoResidencial =
						 _associadoClient.ObterDadosTitulo(vinculo, true, true, true, true, true)
							 .Enderecos.First(x => x.TipoEndereco.Codigo == "RES");

					 var codigosRede = new[] { codigoRede };

					 if (string.IsNullOrEmpty(codigoRede))
					 {
						 codigosRede =
							 _associadoClient.ListarRedeAcessoBeneficiario(vinculo)
								 .GroupBy(x => x.CodigoProduto)
								 .Select(x => x.OrderBy(x2 => x2.IdPrioridade).First().CodigoRede)
								 .ToArray();
					 }

					 var result =
						 codigosRede.SelectMany
							 (x => _prestadorClient.ListarRedePrestadorNome(x, inativos ? "I" : "A", "S", nome))
							 .OrderBy(x => x.CodigoEstado.Trim() != enderecoResidencial.Logradouro.Municipio.CodigoUF.Trim())
							 .ThenBy
							 (x =>
							  string.Compare
								  (x.EnderecoCidade.Trim(),
								   enderecoResidencial.Logradouro.Municipio.Nome.Trim(),
								   CultureInfo.CurrentCulture,
								   CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreCase) != 0)
							 .GroupBy
							 (x => x.EnderecoCidade.Trim() + "/" + x.CodigoEstado.Trim(),
							  (cidade, prestadores) => new
							  {
								  Cidade = cidade,
								  Prestadores = prestadores
							  });

					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
				 },
				 "Ocorreu um erro ao realizar a busca.");
		}

		[HttpPost, Authorize]
		public HttpResponseMessage BuscaEspecialidade
			(string vinculo, string rede, string tipoAtendimento, string nomeEspecialidade, bool inativos)
		{
			return HandleErrors
				(Request,
				 () =>
				 {
					 int idClassifica;
					 int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaRedeCredenciada"], out idClassifica);
					 _atendimentoAppClient.GravaChamadoAtendimento
						 ("I", null, 13, vinculo.Substring(0, 8), idClassifica, "Listagem de rede credenciada por especialidade pela App.", "0");

					 IEnumerable<RedeEspecialidade> especialidades = _prestadorClient.ListarRedeEspecialidade
						 (rede, tipoAtendimento, inativos ? "I" : "A", "S", "", "");

					 if (!string.IsNullOrEmpty(nomeEspecialidade))
					 {
						 especialidades = especialidades.Where
							 (x =>
							  x.DescricaoEspecialidade.IndexOf
								  (nomeEspecialidade.Trim(), StringComparison.CurrentCultureIgnoreCase) != -1);
					 }

					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(especialidades));
				 },
				 "Ocorreu um erro ao realizar a busca.");
		}

		private HttpResponseMessage BuscaProximidade
			(string rede, string tipoAtendimento, string codigoEspecialidade, float latitude, float longitude, bool inativos)
		{
			var atualCoords = new Coordinates(latitude, longitude);

			int raioBusca;

			int.TryParse(System.Configuration.ConfigurationManager.AppSettings["RaioBuscaProximidade"], out raioBusca);

			var result = _prestadorClient.ListarRedePrestador
				(rede, tipoAtendimento, codigoEspecialidade, "", "", inativos ? "I" : "A", "S", "", "", "")
				.Where(x => !string.IsNullOrEmpty(x.GmLatitude.Trim()) && !string.IsNullOrEmpty(x.GmLongitude.Trim()))
				.Select
				(x => new
				{
					Prestador = x,
					Distancia =
						  atualCoords.DistanceTo
						  (new Coordinates
							   (double.Parse(x.GmLatitude.Trim(), CultureInfo.InvariantCulture),
								double.Parse(x.GmLongitude.Trim(), CultureInfo.InvariantCulture)))
				}).Where(x => x.Distancia <= raioBusca).OrderBy(x => x.Distancia);

			return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
		}

		[HttpPost, Authorize]
		public HttpResponseMessage BuscaProximidade
			(string vinculo, string rede, string tipoAtendimento, string codigoEspecialidade, string buscaProximidadeTipo,
			 bool inativos)
		{
			return HandleErrors
				(Request,
				 () =>
				 {
					 int idClassifica;
					 int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaRedeCredenciada"], out idClassifica);
					 _atendimentoAppClient.GravaChamadoAtendimento
						 ("I", null, 13, vinculo.Substring(0, 8), idClassifica, "Listagem de rede credenciada por proximidade pela App.", "0");

					 var tipo = _associadoClient.ListarRedeBuscaProximidade(vinculo)
						 .First(x => x.TipoProximidade == buscaProximidadeTipo);

					 var googleApiKey = "AIzaSyCu402K5dQcG8j9b0QGUOJziVqwvLeig-8";

					 var url =
						 $"https://maps.googleapis.com/maps/api/geocode/json?key={googleApiKey}&components=postal_code:{tipo.Cep}&address={tipo.Cidade}%20-%20{tipo.CodigoEstado}";

					 float latitude;
					 float longitude;
					 try
					 {
						 var json = new WebClient().DownloadString(url);
						 dynamic obj = JsonConvert.DeserializeObject(json);

						 latitude = obj.results[0].geometry.location.lat;
						 longitude = obj.results[0].geometry.location.lng;
					 }
					 catch (Exception)
					 {
						 throw new ApplicationException
							 ("Ocorreu um erro ao consultar a posição do endereço cadastrado em nossa base de dados. Por favor tente novamente ou contate-nos para uma atualização cadastral.");
					 }

					 return BuscaProximidade(rede, tipoAtendimento, codigoEspecialidade, latitude, longitude, inativos);
				 },
				 "Ocorreu um erro ao realizar a busca por proximidade.");
		}

		[HttpPost, Authorize]
		public HttpResponseMessage BuscaProximidadeAtual
			(string vinculo, string rede, string tipoAtendimento, string codigoEspecialidade, string latitude, string longitude,
			 bool inativos)
		{
			int idClassifica;
			int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaRedeCredenciada"], out idClassifica);
			_atendimentoAppClient.GravaChamadoAtendimento
				("I", null, 13, vinculo.Substring(0, 8), idClassifica, "Listagem de rede credenciada por proximidade atual pela App.", "0");

			return HandleErrors
				 (Request,
				  () =>
				  BuscaProximidade
					  (rede,
					   tipoAtendimento,
					   codigoEspecialidade,
					   float.Parse(latitude, CultureInfo.InvariantCulture),
					   float.Parse(longitude, CultureInfo.InvariantCulture),
					   inativos),
				  "Ocorreu um erro ao realizar a busca por proximidade.");
		}

		[HttpPost, Authorize]
		public HttpResponseMessage BuscaRegiao
			(string vinculo, string rede, string tipoAtendimento, string codigoEspecialidade, string codigoEstado, string cidade,
			 string bairro, bool inativos)
		{
			int idClassifica;
			int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaRedeCredenciada"], out idClassifica);
			_atendimentoAppClient.GravaChamadoAtendimento
				("I", null, 13, vinculo.Substring(0, 8), idClassifica, "Listagem de rede credenciada por regiao pela App.", "0");

			return HandleErrors
				(Request,
				 () =>
				 {
					 var result = _prestadorClient.ListarRedePrestador
						 (rede,
						  tipoAtendimento,
						  codigoEspecialidade,
						  "",
						  "",
						  inativos ? "I" : "A",
						  "S",
						  codigoEstado,
						  cidade,
						  bairro);

					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
				 },
				 "Ocorreu um erro ao realizar a busca.");
		}

		[HttpPost, Authorize]
		public HttpResponseMessage Regioes
			(string vinculo, string rede, string tipoAtendimento, string codigoEspecialidade, bool inativos)
		{
			return HandleErrors
				(Request,
				 () =>
				 {
					 var regioes =
						 _prestadorClient.ListarRedePrestadorRegiao
							 (rede, tipoAtendimento, codigoEspecialidade, inativos ? "I" : "A", "S", "", "")
							 .GroupBy(x => x.CodigoEstado)
							 .ToDictionary
							 (x => x.Key.Trim(),
							  x =>
							  x.GroupBy(x2 => x2.EnderecoCidade)
								  .ToDictionary
								  (x2 => x2.Key.Trim(),
								   x2 =>
								   x2.Where(x3 => !string.IsNullOrEmpty(x3.EnderecoBairro))
									   .Select(x3 => x3.EnderecoBairro.Trim())));

					 var enderecoResidencial =
						 _associadoClient.ObterDadosTitulo(vinculo, true, true, true, true, true)
							 .Enderecos.First(x => x.TipoEndereco.Codigo == "RES");

					 var enderecoResidencialEstado =
						 regioes.FirstOrDefault(x => x.Key.Trim() == enderecoResidencial.Logradouro.Municipio.CodigoUF.Trim())
							 .Key ?? "";

					 // Existem casos onde o nome da cidade de residência do credenciado é retornado sem acento,
					 // tornando a comparação de strings no frontend inviável
					 var enderecoResidencialCidade = "";
					 if (!string.IsNullOrEmpty(enderecoResidencialEstado))
					 {
						 enderecoResidencialCidade =
							 regioes[enderecoResidencialEstado].FirstOrDefault
								 (x =>
								  string.Compare
									  (x.Key.Trim(),
									   enderecoResidencial.Logradouro.Municipio.Nome.Trim(),
									   CultureInfo.CurrentCulture,
									   CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreCase) == 0).Key ?? "";
					 }

					 var result = new
					 {
						 Regioes = regioes,
						 EnderecoResidencial = new
						 {
							 Estado = enderecoResidencialEstado,
							 Cidade = enderecoResidencialCidade
						 }
					 };

					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
				 },
				 "Ocorreu um erro ao realizar a busca.");
		}

		[HttpPost, Authorize]
		public HttpResponseMessage Detalhamento
			(string vinculo, string rede, string codigoPrestador, string srEndereco, bool inativos)
		{
			return HandleErrors
				(Request,
				 () =>
				 {
					 var result = new
					 {
						 Especialidades =
							 _prestadorClient.ListarRedePrestador
								 (rede, "", "", codigoPrestador, srEndereco, inativos ? "I" : "A", "S", "", "", "")
								 .Select
								 (x => new
								 {
									 x.CodigoEspecialidade,
									 DescricaoEspecialidade = x.DescricaoEspecialidade.Trim()
								 }),
						 Acessos =
							 _prestadorClient.ListarRedePrestadorAcessos
								 (rede, "", codigoPrestador, srEndereco, inativos ? "I" : "A", "S")
								 .Select(x => x.DescricaoAcesso),
						 Contatos =
							 _prestadorClient.ListarRedePrestadorContatos
								 (rede, "", codigoPrestador, srEndereco, inativos ? "I" : "A", "S").Select
								 (x => new
								 {
									 EnderecoTelefoneDdd = x.EnderecoTelefoneDdd.Trim(),
									 EnderecoTelefonePrefixo = x.EnderecoTelefonePrefixo.Trim(),
									 EnderecoTelefone = x.EnderecoTelefone.Trim(),
									 EnderecoExtensao = x.EnderecoExtensao?.Trim() ?? ""
								 }),
						 Curriculo =
							 _prestadorClient.ListarRedePrestadorCurriculo(codigoPrestador)
								 .Where(x => !string.IsNullOrEmpty(x.DescricaoRegistro))
								 .GroupBy(x => x.TipoRegistro)
								 .Select
								 (x => new
								 {
									 Tipo = x.Key.Trim(),
									 Registros = x.Select(x2 => x2.DescricaoRegistro)
								 }),
						 Qualificacoes =
							 _prestadorClient.ListarRedePrestadorQualificacaoAns
								 (rede, "", codigoPrestador, inativos ? "I" : "A", "S")
					 };

					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
				 },
				 "Ocorreu um erro ao realizar a busca.");
		}

		[HttpPost, Authorize]
		public HttpResponseMessage PlanosAtendidos(string vinculo, string rede, string codigoPrestador)
		{
			rede = !string.IsNullOrEmpty(rede) ? rede : (string.IsNullOrEmpty(vinculo) ? "" : vinculo);
			return HandleErrors
				(Request,
				 () =>
				 {
					 var result = _prestadorClient.ListarRedePrestadorPlanosAtendidos(rede, codigoPrestador);

					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
				 },
				 "Ocorreu um erro ao realizar a busca.");
		}

		[HttpPost, Authorize]
		public HttpResponseMessage Substitutos(string vinculo, string rede, string codigoPrestador, string srEndereco)
		{
			rede = !string.IsNullOrEmpty(rede) ? rede : (string.IsNullOrEmpty(vinculo) ? "" : vinculo);
			return HandleErrors
				(Request,
				 () =>
				 {
					 var result = _prestadorClient.ListarRedePrestadorSubstitutos(rede, codigoPrestador, srEndereco);

					 return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(result));
				 },
				 "Ocorreu um erro ao realizar a busca.");
		}

		#endregion
	}
}
