﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security;
using MSW.API.Utils;
using MSW.ServicesWrapper.OBS.Reembolso;
using MSW.ServicesWrapper.OBS.ServicosAtendimentoApp;
using System.Collections.Generic;
using System.Collections;

namespace MSW.API.Controllers
{
    public class ReembolsoController : CustomController
    {
        private IAuthenticationManager Authentication => HttpContext.Current.GetOwinContext().Authentication;

        [HttpPost, Authorize]
        public HttpResponseMessage Lista(string vinculo, int servico, int situacao, int ano, int mes)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var service = new Services.Reembolso();
                     var reembolsos = service.Lista(vinculo, servico, situacao, ano, mes);
                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(reembolsos));
                 });
        }


        [HttpPost, Authorize]
        public HttpResponseMessage ListaSimples(string vinculo, int servico, int situacao, int ano, int mes)
        {
            return HandleErrors
                (Request,
                 () =>
                 {
                     var service = new Services.Reembolso();
                     var reembolsos = service.ListaSimples(vinculo, servico, situacao, ano, mes);
                     return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(reembolsos));
                 });
        }

        private readonly ReembolsoClient _reembolsoClient = new ReembolsoClient();
        private IEnumerable<dynamic> reembolso;

        [HttpPost, Authorize]
        public HttpResponseMessage Detalhe(int idReembolso, string vinculo = null)
        {
            
            if (!string.IsNullOrWhiteSpace(vinculo))
            {
                var service = new Services.Reembolso();
                reembolso = service.ListaSimples(vinculo) as IEnumerable<dynamic>;
            }

            var detalhe = _reembolsoClient.ListarReembolsoDetalhe(idReembolso)
                .OrderByDescending(z => z.DataUso)
                .Select
                (z => new
                {
                    DataUso = z.DataUso.ToString("dd/MM/yyyy"),
                    z.NomeBeneficiario,
                    ValorApresentado =
                    z.ValorApresentado.ToString("c", CultureInfo.CreateSpecificCulture("pt-BR")),
                    ValorAprovado =
                    z.ValorAprovado.ToString("c", CultureInfo.CreateSpecificCulture("pt-BR")),
                    z.DescricaoServico,
                    z.NomePrestador,
                    z.Ds_Ocorrencia_Motivo
                });

            var pagamento = _reembolsoClient.ListarReembolsoContaPagamento(idReembolso).FirstOrDefault();

            return Request.CreateResponse(HttpStatusCode.OK, new Result<object>(new
            {
                Detalhes = detalhe,
                Pagamento = pagamento,
                Reembolso = reembolso != null ? reembolso.FirstOrDefault(c => ((dynamic)c.IdReembolso) == idReembolso) : null
            }));
        }

        public HttpResponseMessage Bot(string vinculo)
        {
            return HandleErrors
            (Request,
             () =>
             {
                 var service = new Services.Reembolso();
                 var reembolsos = service.ListaSimples(vinculo);
                 var botResult = new Models.BotResult(new Dictionary<string, string>
                 {
                     [nameof(vinculo)] = vinculo,
                     ["intent"] = "reembolso"
                 }, new Dictionary<string, string>
                 {
                     ["dataLiberacao"] = "",
                     ["valor"] = "",
                     ["referenteA"] = ""
                 });

                 return Request.CreateResponse(HttpStatusCode.OK, botResult);
             });
        }
    }
}