﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security.OAuth;
using MSW.API.Utils;
using MSW.ServicesWrapper.OBS.ServicosAtendimentoApp;
using MSW.ServicesWrapper.OBS.ServicosUsuariosWeb;
using MSW.ServicesWrapper.Wrapper;
using NLog;
using System.Globalization;

namespace MSW.API.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private static readonly Logger Logger = LoggingManager.GetCurrentClassLogger();

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext c)
        {
            c.Validated();
            return Task.FromResult<object>(null);
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext c)
        {
            try
            {
                var password = Utils.Utils.RsaDecrypt(c.Password);
                var passwordMD5 = Md5.FromString(password, 2);
                var usuario = c.UserName == "Face"
                                  ? UsuariosWeb.ObterUsuarioFacebook(passwordMD5)
                                  : UsuariosWeb.ObterUsuarioOmint(c.UserName, passwordMD5);

                if (usuario == null || usuario.IdStatus != 1)
                {
                    //Se o usuário não foi validado na nova estrutura de banco de dados
                    //validar na estrutura antiga
                    var usuarioNuke = UsuariosWeb.ObterUsuarioDotNetNuke(c.UserName, password);

                    if (usuarioNuke == null)
                    {
                        throw new Exception("Usuário inexistente ou inativo.");
                    }

                    var senha = Md5.FromString(Utils.Utils.RsaDecrypt(c.Password), 2);

                    UsuariosWeb.AdicionarNovoUsuarioOmint
                     (c.UserName.ToLower(),
                      senha,
                      usuarioNuke.DataNascimento,
                      usuarioNuke.NomeUsuario.Trim(),
                      usuarioNuke.TipoDocumento.Trim(),
                      usuarioNuke.NumeroDocumento.Trim());

                    usuario = UsuariosWeb.ObterUsuarioOmint(c.UserName, Md5.FromString(password, 2));
                }

                var claimsIdentity = new ClaimsIdentity
                    (new[]
                    {
                        new Claim("TipoDocumento", usuario.TipoDocumento.Trim()),
                        new Claim("NumeroDocumento", usuario.NumeroDocumento)
                    },
                     OAuthDefaults.AuthenticationType);

                c.Validated(claimsIdentity);

                //var administracaoClient = new AdministracaoClient();
                //var atendimentoAppClient = new AtendimentoAppClient();

                //var vinculos = administracaoClient.ObterPerfilVinculo(null, usuario.NumeroDocumento, usuario.TipoDocumento.Trim());

                //var vinculo = vinculos.FirstOrDefault(x => x.IdStatus == 1 && x.IdOperacaoOmint == 1 && x.IdPerfilOmint == 1)
                //              ?? vinculos.First(x => x.IdStatus == 1);

                //atendimentoAppClient.GravaChamadoAtendimento
                //    ("I", null, 13, vinculo.NumeroIdentificacaoVinculo.Substring(0, 8), 4722, "Login pela App.", "0");

                return Task.FromResult<object>(null);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                c.SetError
                    ("Erro",
                     ex.Message.Contains("SQL Server")
                         ? "Não foi possivel se conectar com o servidor. Por favor tente novamente."
                         : "Dados de acesso incorretos. Caso não tenha uma conta registrada, realize o primeiro acesso.");

                return Task.FromResult<object>(null);
            }
        }
    }
}
