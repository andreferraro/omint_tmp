﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security.OAuth;
using MSW.API.Utils;
using MSW.ServicesWrapper.OBS.ServicosAtendimentoApp;
using MSW.ServicesWrapper.OBS.ServicosUsuariosWeb;
using MSW.ServicesWrapper.Wrapper;
using NLog;
using System.Globalization;
using MSW.ServicesWrapper.Obs.ServicosSeguro;

namespace MSW.API.Providers
{
    public class ApplicationSegurosOAuthProvider : OAuthAuthorizationServerProvider
    {
        private static readonly Logger Logger = LoggingManager.GetCurrentClassLogger();

        private const int ID_OPERACAO = 2;
        private const int ID_PERFIL = 6;

        private string NomeUsuario;
        private string NumeroBilhete;

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext c)
        {
            string cpf = c.Parameters.Where(f => f.Key == "cpf").Select(f => f.Value).SingleOrDefault()[0];
            string dataNascimento = c.Parameters.Where(f => f.Key == "dataNascimento").Select(f => f.Value).SingleOrDefault()[0];

            c.OwinContext.Set("Cpf", cpf);
            c.OwinContext.Set("DataNascimento", dataNascimento);

            c.Validated();
            return Task.FromResult<object>(null);
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext c)
        {
            try
            {
                var password = Utils.Utils.RsaDecrypt(c.Password);
                var cpf = c.OwinContext.Get<string>("Cpf"); ;
                var dataNascimento = DateTime.ParseExact(c.OwinContext.Get<string>("DataNascimento"), "dd/MM/yyyy", new CultureInfo("pt-BR"));

                if (c.UserName != "OmintSV" && password != "segurosVIAGEM")
                {
                    throw new Exception("Acesso negado.");
                }

                var numeroVinculo = ObterNumeroVinculo(cpf);
                var usuario = UsuariosWeb.ObterUsuarioOmint(cpf, dataNascimento);

                if (usuario == null || usuario.IdStatus != 1)
                {
                    // Se o usuário não foi encontrado, realiza uma tentativa de primeiro acesso.
                    usuario = RealizarPrimeiroAcesso(cpf, dataNascimento);
                    if (usuario == null || usuario.IdStatus != 1)
                        throw new Exception("Usuário inexistente ou inativo.");
                }

                NomeUsuario = usuario.NomeUsuario;
                NumeroBilhete = Seguros.ObterNumeroBilhetePessoalAtivo(cpf);

                ValidateContextTicket(ref c, cpf);

                GravarChamadoLogin(numeroVinculo);

                return Task.FromResult<object>(null);
            }
            catch(Exception ex)
            {
                Logger.Error(ex.ToString());
                c.SetError
                    ("Erro",
                     ex.Message.Contains("SQL Server")
                         ? "Não foi possivel se conectar com o servidor. Por favor tente novamente."
                         : "Dados de acesso incorretos. Não foram emitidos bilhetes para a pessoa de CPF e data de nascimento informados.");
                return Task.FromResult<object>(null);
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            context.AdditionalResponseParameters.Add("NomeUsuario", NomeUsuario);
            context.AdditionalResponseParameters.Add("NrBilhetePessoal", NumeroBilhete);

            return Task.FromResult<object>(null);
        }

        private string ObterNumeroVinculo(string cpf)
        {
            // Lança exception em caso de erro.
            var administracaoClient = new AdministracaoClient();

            var vinculo = administracaoClient.ObterPerfilVinculo(null, cpf, "CPF")
                                             .First(x => x.IdStatus == 1 
                                                 && x.IdOperacaoOmint == ID_OPERACAO 
                                                 && x.IdPerfilOmint == ID_PERFIL);

            return vinculo.NumeroIdentificacaoVinculo.Replace("/", "");
        }

        private void ValidateContextTicket(ref OAuthGrantResourceOwnerCredentialsContext c, string cpf)
        {
            var claimsIdentity = new ClaimsIdentity(
                new[]
                    {
                        new Claim("TipoDocumento", "CPF"),
                        new Claim("NumeroDocumento", cpf)
                    },
                     OAuthDefaults.AuthenticationType);

            c.Validated(claimsIdentity);
        }

        private void GravarChamadoLogin(string numeroVinculo)
        {
            //TODO: trocar id_classifica chamado para o login
            var atendimentoAppClient = new AtendimentoAppClient();
            atendimentoAppClient.GravaChamadoAtendimento("I", null, 13, numeroVinculo, 4722, "Login pela App.", "0");
        }

        private Usuario RealizarPrimeiroAcesso(string cpf, DateTime dataNascimento)
        {
            SeguroViagemClient _seguroViagemClient = new SeguroViagemClient();
            var bilhetes = _seguroViagemClient.ObterBilhetesPorCpf(cpf).Where(x => x.DataNascimentoSegurado == dataNascimento);

            if (bilhetes.Count() == 0)
                throw new Exception();

            UsuariosWeb.AdicionarNovoUsuarioOmint(dataNascimento, bilhetes.First().NomeSegurado, "CPF", cpf);

            return UsuariosWeb.ObterUsuarioOmint(cpf, dataNascimento);
        }
    }
}
