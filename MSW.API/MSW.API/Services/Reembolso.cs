﻿using MSW.ServicesWrapper.OBS.Reembolso;
using MSW.ServicesWrapper.OBS.ServicosAtendimentoApp;
using System;
using System.Globalization;
using System.Linq;
using WCF = MSW.ServicesWrapper.OBS.Reembolso;

namespace MSW.API.Services
{
    public class Reembolso
    {
        private readonly AtendimentoAppClient _atendimentoAppClient = new AtendimentoAppClient();
        private readonly ReembolsoClient _reembolsoClient = new ReembolsoClient();

        public object Lista(string vinculo, int servico = 1, int situacao = 0, int? ano = null, int mes = 0)
        {
            this.GravaChamadoAtendimento(vinculo, "Listagem de reembolso pela App.");

            var reembolsos = _reembolsoClient.ListarReembolsos
                         (null,
                          vinculo.Substring(0, 8),
                          null,
                          situacao,
                          new DateTime(ano.GetValueOrDefault(DateTime.Now.Year), 01, 01),
                          1,
                          servico,
                          null,
                          true)
                    .OrderByDescending(x => x.DataEntrada)
                    .Select
                    (x => new Models.Reembolso
                    {
                        IdReembolso = x.IdReembolso,
                        NomeTitular = x.NomeTitular,
                        IdFormaPagamento = x.IdFormaPagamento,
                        DescricaoPendencia = x.DescricaoPendencia,
                        DescricaoFormaPagamento = this.ObterDescricaoFormaPagamento(x.IdFormaPagamento),
                        NomeBeneficiario = x.NomeBeneficiario,
                        DataEntrada = x.DataEntrada.ToString("dd/MM/yyyy"),
                        DataLiberacao = x.DataLiberacao.Year == 1 ? "" : x.DataLiberacao.ToString("dd/MM/yyyy"),
                        DescricaoSituacao = x.DescricaoSituacao,
                        ValorApresentado = x.ValorApresentado.ToString("c", CultureInfo.CreateSpecificCulture("pt-BR")),
                        ValorAprovado = x.ValorAprovado.ToString("c", CultureInfo.CreateSpecificCulture("pt-BR")),
                        IdProtocolo = x.IdProtocolo,
                        Pagamento = x.DataLiberacao.Year != 1
                                  ? _reembolsoClient.ListarReembolsoContaPagamento(x.IdReembolso).FirstOrDefault()
                                  : null,
                        Detalhes =
                              _reembolsoClient.ListarReembolsoDetalhe(x.IdReembolso)
                              .OrderByDescending(z => z.DataUso)
                              .Select
                              (z => new Models.Reembolso.Detalhe
                              {
                                  DataUso = z.DataUso.ToString("dd/MM/yyyy"),
                                  NomeBeneficiario = z.NomeBeneficiario,
                                  ValorApresentado = z.ValorApresentado.ToString("c", CultureInfo.CreateSpecificCulture("pt-BR")),
                                  ValorAprovado = z.ValorAprovado.ToString("c", CultureInfo.CreateSpecificCulture("pt-BR")),
                                  DescricaoServico = z.DescricaoServico,
                                  NomePrestador = z.NomePrestador,
                                   Ds_Ocorrencia_Motivo = z.Ds_Ocorrencia_Motivo
                              })
                    });

            return reembolsos;
        }

        public object ListaSimples(string vinculo, int servico = 1, int situacao = 0, int? ano = null, int mes = 0)
        {
             this.GravaChamadoAtendimento(vinculo, "Listagem simples de reembolso pela App.");

            var inicio = (mes == 0 ? 1 : mes);
            var fim = (mes == 0 ? 12 : 1);

            var items = this.ListarReembolsos(vinculo, situacao, new DateTime(ano.GetValueOrDefault(DateTime.Now.Year), 01, 01), servico);
            var reembolsos = items
                    .OrderByDescending(x => x.DataEntrada)
                    .Select
                    (x => new Models.Reembolso
                    {
                        IdReembolso = x.IdReembolso,
                        NomeTitular = x.NomeTitular,
                        IdFormaPagamento = x.IdFormaPagamento,
                        DescricaoPendencia = x.DescricaoPendencia,
                        DescricaoFormaPagamento = this.ObterDescricaoFormaPagamento(x.IdFormaPagamento),
                        NomeBeneficiario = x.NomeBeneficiario,
                        DataEntrada = x.DataEntrada.ToString("dd/MM/yyyy"),
                        DataLiberacao = x.DataLiberacao.Year == 1 ? "" : x.DataLiberacao.ToString("dd/MM/yyyy"),
                        DescricaoSituacao = x.DescricaoSituacao,
                        ValorApresentado = x.ValorApresentado.ToString("c", CultureInfo.CreateSpecificCulture("pt-BR")),
                        ValorAprovado = x.ValorAprovado.ToString("c", CultureInfo.CreateSpecificCulture("pt-BR")),
                        IdProtocolo = x.IdProtocolo
                         
                    });

            return reembolsos;
        }

        private WCF.Reembolso[] ListarReembolsos(string vinculo, int situacao, DateTime data, int servico)
        {
            return _reembolsoClient.ListarReembolsos(null,
                                                     vinculo.Substring(0, 8),
                                                     null,
                                                     situacao,
                                                     data,
                                                     1,
                                                     servico,
                                                     null,
                                                     true);
        }

        private string ObterDescricaoFormaPagamento(int idFormaPagamento)
        {
            return (idFormaPagamento == 1 || idFormaPagamento == 2) ? "Crédito em conta" :
                   (idFormaPagamento == 3 || idFormaPagamento == 4 ? "Cheque" : "Desconhecido");
        }

        private void GravaChamadoAtendimento(string vinculo, string observacao)
        {
            int idClassifica;
            int.TryParse(System.Configuration.ConfigurationManager.AppSettings["IdClassificaConsultaReembolso"], out idClassifica);
            _atendimentoAppClient.GravaChamadoAtendimento
                ("I", null, 13, vinculo.Substring(0, 8), idClassifica, observacao, "0");
        }
    }
}