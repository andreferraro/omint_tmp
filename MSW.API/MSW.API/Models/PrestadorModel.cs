﻿using MSW.ServicesWrapper.OBS.ServicosPrestador;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace MSW.API.Models
{
    public class PrestadorModel : IEquatable<PrestadorModel>
    {
        public string CodigoRede { get; set; }
        public string CodigoPrestador { get; set; }
        public string DescricaoPrestador { get; set; }
        public string DescricaoRede { get; set; }
        public int ScoreTotal { get; set; }
        public string NrAvaliacao { get; set; }
        public string DescricaoObservacao { get; set; }
        public string DescricaoRegistro { get; set; }
        public DateTime? DataDescredenciamento { get; set; }
        public List<string> Telefones { get; set; }
        public List<string> Especialidades { get; set; }
        public string DescricaoEspecialidade { get; set; }
        public string SrEndereco { get; set; }
        public string TipoLogradouro { get; set; }
        public string EnderecoRua { get; set; }
        public string EnderecoNumero { get; set; }
        public string EnderecoComplemento { get; set; }
        public string EnderecoBairro { get; set; }
        public string EnderecoCidade { get; set; }
        public string CodigoEstado { get; set; }
        public string CodigoCep { get; set; }
        public string GmLatitude { get; set; }
        public string GmLongitude { get; set; }
        public string EnderecoTelefone { get; set; }
        public string EnderecoTelefoneDdd { get; set; }
        public string EnderecoTelefonePrefixo { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals(obj as PrestadorModel);
        }

        public bool Equals(PrestadorModel other)
        {
            if (other == null) return false;
            return string.Equals(CodigoPrestador, other.CodigoPrestador) &&
                   string.Equals(SrEndereco, other.SrEndereco);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 15;
                hash = (hash * 5) + CodigoPrestador?.GetHashCode() ?? 0;
                hash = (hash * 5) + SrEndereco?.GetHashCode() ?? 0;

                return hash;
            }
        }

        public PrestadorModel(RedePrestador prestador)
        {
            CodigoRede = prestador.CodigoRede;
            CodigoPrestador = prestador.CodigoPrestador;
            DescricaoPrestador = prestador.DescricaoPrestador;
            DescricaoObservacao = prestador.DescricaoObservacao;
            DescricaoRegistro = prestador.DescricaoRegistro;
            DataDescredenciamento = prestador.DataDescredenciamento;
            SrEndereco = prestador.SrEndereco;
            TipoLogradouro = prestador.TipoLogradouro;
            EnderecoRua = prestador.EnderecoRua;
            EnderecoNumero = prestador.EnderecoNumero;
            EnderecoComplemento = prestador.EnderecoComplemento;
            EnderecoBairro = prestador.EnderecoBairro;
            EnderecoCidade = prestador.EnderecoCidade;
            CodigoEstado = prestador.CodigoEstado;
            CodigoCep = prestador.CodigoCep;
            GmLatitude = prestador.GmLatitude;
            GmLongitude = prestador.GmLongitude;
            DescricaoEspecialidade = prestador.DescricaoEspecialidade;
            EnderecoTelefone = prestador.EnderecoTelefone;
            EnderecoTelefoneDdd = prestador.EnderecoTelefoneDdd;
            EnderecoTelefonePrefixo = prestador.EnderecoTelefonePrefixo;
        }

        public PrestadorModel(DataRow row)
        {
            CodigoRede = row["cd_rede"]?.ToString().Trim();
            CodigoPrestador = row["cd_prestador"]?.ToString().Trim();
            DescricaoPrestador = row["ds_prestador"]?.ToString().Trim();
            DescricaoRede = row["ds_rede"]?.ToString().Trim();
            ScoreTotal = Convert.ToInt32(row["ScoreTotal"]);
            NrAvaliacao = row["nr_avaliacao"]?.ToString().Trim();
            DescricaoObservacao = row["ds_observacao"]?.ToString().Trim();
            DescricaoRegistro = row["ds_registro"]?.ToString().Trim();
            DataDescredenciamento = row.Field<DateTime?>("dt_descredenciamento");
            SrEndereco = row["sr_endereco"].ToString();
            TipoLogradouro = row["tp_endereco"]?.ToString().Trim();
            EnderecoRua = row["ds_endereco"]?.ToString().Trim();
            EnderecoNumero = row["ds_numero"]?.ToString().Trim();
            EnderecoComplemento = row["ds_complemento"]?.ToString().Trim();
            EnderecoBairro = row["ds_bairro"]?.ToString().Trim();
            EnderecoCidade = row["ds_cidade"]?.ToString().Trim();
            CodigoEstado = row["ds_uf"]?.ToString().Trim();
            CodigoCep = row["cep"]?.ToString().Trim();
            GmLatitude = row["gm_lat"]?.ToString().Trim();
            GmLongitude = row["gm_long"]?.ToString().Trim();
        }
    }
}