﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSW.API.Models
{
    public class ImagemChamado
    {
        public int IdChamado { get; set; }
        public string  Extensao { get; set; }
        public string Base64File { get; set; }
    }
}