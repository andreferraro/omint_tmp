﻿using MSW.ServicesWrapper.OBS.Reembolso;
using System.Collections.Generic;

namespace MSW.API.Models
{
    public class Reembolso
    {
        public int IdReembolso { get; set; }
        public string NomeTitular { get; set; }
        public int IdFormaPagamento { get; set; }
        public string DescricaoPendencia { get; set; }
        public string DescricaoFormaPagamento { get; set; }
        public string NomeBeneficiario { get; set; }
        public string DataEntrada { get; set; }
        public string DataLiberacao { get; set; }
        public string DescricaoSituacao { get; set; }
        public string ValorApresentado { get; set; }
        public string ValorAprovado { get; set; }
        public int IdProtocolo { get; set; }
        public ReembolsoContaPagamento Pagamento { get; set; }
        public IEnumerable<Detalhe> Detalhes { get; set; }

        public class Detalhe
        {
            public string DataUso { get; set; }
            public string NomeBeneficiario { get; set; }
            public string ValorApresentado { get; set; }
            public string ValorAprovado { get; set; }
            public string DescricaoServico { get; set; }
            public string NomePrestador { get; set; }
            public string Ds_Ocorrencia_Motivo { get; set; }
        }
    }
}