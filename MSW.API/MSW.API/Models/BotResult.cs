﻿using System.Collections.Generic;

namespace MSW.API.Models
{
    public class BotResult
    {
        public Dictionary<string, string> Input { get; set; } = new Dictionary<string, string>();
        public Dictionary<string, string> Output { get; set; } = new Dictionary<string, string>();

        public BotResult()
        {

        }

        public BotResult(Dictionary<string, string> input, Dictionary<string, string> output)
        {
            this.Input = input;
            this.Output = output;
        }
    }
}