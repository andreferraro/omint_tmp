﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using MSW.ServicesWrapper.OBS.ServicosAssociado;
using MSW.ServicesWrapper.OBS.ServicosUsuariosWeb;

namespace MSW.API.Utils
{
    public class ClientePrimeiroAcesso
    {
        [ScriptIgnore]
        public BeneficiarioKit BeneficiarioKit { get; set; }

        [ScriptIgnore]
        public Beneficiario[] DadosPlano { get; set; }

        [ScriptIgnore]
        public PerfilVinculo PerfilVinculo { get; set; }

        [ScriptIgnore]
        public DateTime Date { get; set; }

        [ScriptIgnore]
        public string Login { get; set; }

        [ScriptIgnore]
        public IList<string> Emails { get; set; }

        public string Hash { get; set; }
    }
}
