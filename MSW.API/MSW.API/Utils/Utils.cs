﻿using System.Text.RegularExpressions;

namespace MSW.API.Utils
{
    public static class Utils
    {
        public static string RsaDecrypt(string text)
        {
            var rsaPrivateKey = @"-----BEGIN RSA PRIVATE KEY-----
        MIIEpAIBAAKCAQEAqIjVCznUZ03ONWZATrY0xKUygjJuZGe63dx6uugNf2qFN50B
        4P7pE5RrP1YvR4DqW+7D+pvL6uWD14X5Od6rCgr02P2ds5cKfjxSr0JZr2nEu4Eq
        ozaZtD29vTpUvKZb5jfCWXK2gzr8iwAHgqu7K6CG0XRSmWuqhab391R23/V/tPbK
        1+89neE+beJJYnOgXUOMqL5pDOiRYpFJwN/vQI3PX7UQG+XWzh8vZVUz8qlkO/YB
        MhbiJtT0xuyi885XimL//98QSPPmIVD7MDoyENXlgeQzSWo4ZYrFoDr7xpawy9jy
        IZnPcQJA7NR0xiyMetpIBWqss91zAyBPn75EDQIDAQABAoIBABKVvECXqPeXWeUD
        mJXVe+4vdWlsHEZQNggfNx2DV3G0kmo2ky13jsZM+KKsY1xUOAPZSZ1KLuuBUc+j
        g39BMcVSvftrxEc4obyX9FpFS9SPKlWvAb+r1rlMka7bTtra0YmKnpD0mHHSQGUn
        pKxhOauu8Q8J783hVJWWewO4Ob+qihH9DoxwHw6RgWv/A4+pe29OYIIzfcGNZyHg
        3VeS0E/0DdVkZchyH3FM7UBYDWc8DvIMzh80am3jjYH7flNMr9KIwstgnyWoFwzm
        LWnaqrYn9Y07mG+/r46ZQYiiLqrnkYMmthZUjLW1e7R2DUKtiXsOXvxGxiwZNmyC
        zcFanaECgYEAz/MHm3xihRNtJdEWsA2y2m7Xw0+MSuoy8grnyZlW+g1LZiV4YIx1
        YD9orhWubTHndspt0Gzf9K6LlrbcRlS3VfO67XjPFTu/eolgKWRSrsn8m0qcahkr
        McgKaFzUsyUKGzw56DL+FXnorN9/k44zY3w2VuanYbKWlJ/ac0Z5YtkCgYEAz3pF
        B9gAW8ebzivsT08eiJ9CB5ngrrG0kie8Oz3hlunHm6dUsjj5zQt1af5PZcFg+BoV
        BnCbJsXl/h57HW9Eza+3FHiPToh3qbmww6BU4MyLuXjOpuMXV3CXj76cHZbg6TQ5
        A055nOjRVjS68UZTn8gPKjvsnYtgWsRJpDvIwlUCgYBTeBe7n6fkBrIvFchnjngW
        GZvdt9Z2U59zbFExqK0y3ZdAPxYcmwSaR5l7FxTWm8tu8T43BRy4GD/LlUbBrEuS
        4qxLmcx1l2/6fZ3tv2k447bTlpVGOSgPaV5UGb7+0YVG/Vu5Co7WjAKLMYMB9XhH
        HJqrMxZIruima6vBza33sQKBgQCfc8mfuJq17pdIgSsw3I6L01GMw+8Qc1RiSxV3
        thwsvmiAsL3/5pfcxOSs3ByrCE58/paQB3TF/KvAfC6ENaJ8LYIXwnvokCrfDu3R
        uuPgjHMJzNWNHtQuiEKC36vddbpOn6r9Zo9CTw+rIAVYLoNQNhGplLpJbfdUaqxq
        AuRMrQKBgQCERgD4DS9ztqdC+OSdAulTvfOxVxjWWCVcWtNuwZxqCHG7xiNCl/SR
        E1CBaenLEbxBID1/MoAlYggeINapkNnc1kH8vZnQs4iWGKHTHVFHaCf8cNEck7mC
        4p3PXr1QmEesXr8I0OgU26RX3PWy/GKkTk8CRar8TWKIQNouJ+314g==
        -----END RSA PRIVATE KEY-----";
            return new Rsa(rsaPrivateKey).Decrypt(text);
        }

        public static ServicesWrapper.OEX.DesktopModules.IWebAuthendicationHeader GetAuthenticator()
        {
            var webAuthendicationHeader = new ServicesWrapper.OEX.DesktopModules.IWebAuthendicationHeader
            {
                PortalID = 1,
                Username = "host",
                Password = "newbery",
                Encrypted = "false"
            };

            return webAuthendicationHeader;
        }

        public static string GetImageUrl(string html)
        {
            try
            {
                return Regex.Match(html, "<img.+?src=[\"'](.+?)[\"'].+?>", RegexOptions.IgnoreCase).Groups[1].Value.Replace("&#58;", ":");
            }
            catch
            {
                return "";
            }
        }
    }
}
