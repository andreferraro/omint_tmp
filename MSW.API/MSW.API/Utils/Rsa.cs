﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Prng;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;

namespace MSW.API.Utils
{
    public class Rsa
    {
        private readonly string _privateRsaXml = "";
        private readonly string _publicRsaXml = "";

        public Rsa(PemKeys keys)
        {
            if(!string.IsNullOrEmpty(keys.Private))
            {
                _privateRsaXml = PrivatePemDataToRsaXml(keys.Private);
            }
            if(!string.IsNullOrEmpty(keys.Public))
            {
                _publicRsaXml = PublicPemDataToRsaXml(keys.Public);
            }
        }

        public Rsa(string privatePem, string publicPem = null) : this(new PemKeys
        {
            Private = privatePem,
            Public = publicPem
        }) {}

        public static PemKeys GenerateKeys(int size)
        {
            var randomGenerator = new CryptoApiRandomGenerator();
            var secureRandom = new SecureRandom(randomGenerator);
            var keyGenerationParameters = new KeyGenerationParameters(secureRandom, size);

            var keyPairGenerator = new RsaKeyPairGenerator();
            keyPairGenerator.Init(keyGenerationParameters);
            var keys = keyPairGenerator.GenerateKeyPair();

            var result = new PemKeys();

            var textWriter = new StringWriter();
            var pemWriter = new PemWriter(textWriter);
            pemWriter.WriteObject(keys.Private);
            pemWriter.Writer.Flush();
            result.Private = textWriter.ToString();

            textWriter = new StringWriter();
            pemWriter = new PemWriter(textWriter);
            pemWriter.WriteObject(keys.Public);
            pemWriter.Writer.Flush();
            result.Public = textWriter.ToString();

            return result;
        }

        public string Encrypt(string decryptedText)
        {
            if(string.IsNullOrEmpty(_publicRsaXml))
            {
                throw new Exception("You can't encrypt without a public key.");
            }

            using(var rsa = new RSACryptoServiceProvider())
            {
                try
                {
                    rsa.FromXmlString(_publicRsaXml);
                    var encryptedData = rsa.Encrypt(Encoding.UTF8.GetBytes(decryptedText), false);
                    var base64Encrypted = Convert.ToBase64String(encryptedData);
                    return base64Encrypted;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }

        private static string PrivatePemDataToRsaXml(string pemData)
        {
            var stringStream = new MemoryStream(Encoding.UTF8.GetBytes(pemData));
            var streamReader = new StreamReader(stringStream);
            var pemReader = new PemReader(streamReader);

            var keyPair = (AsymmetricCipherKeyPair)pemReader.ReadObject();
            var privateKey = keyPair.Private;
            var rsa = DotNetUtilities.ToRSA((RsaPrivateCrtKeyParameters)privateKey);

            return rsa.ToXmlString(true);
        }

        private static string PublicPemDataToRsaXml(string pemData)
        {
            var stringStream = new MemoryStream(Encoding.UTF8.GetBytes(pemData));
            var streamReader = new StreamReader(stringStream);
            var pemReader = new PemReader(streamReader);

            var publicKey = (AsymmetricKeyParameter)pemReader.ReadObject();
            var rsa = DotNetUtilities.ToRSA((RsaKeyParameters)publicKey);

            return rsa.ToXmlString(false);
        }

        public string Decrypt(string encryptedText)
        {
            if(string.IsNullOrEmpty(_privateRsaXml))
            {
                throw new Exception("You can't decrypt without a private key.");
            }

            using(var rsaProvider = new RSACryptoServiceProvider())
            {
                try
                {
                    rsaProvider.FromXmlString(_privateRsaXml);

                    var resultBytes = Convert.FromBase64String(encryptedText);
                    var decryptedBytes = rsaProvider.Decrypt(resultBytes, false);
                    var decryptedData = Encoding.UTF8.GetString(decryptedBytes);
                    return decryptedData;
                }
                finally
                {
                    rsaProvider.PersistKeyInCsp = false;
                }
            }
        }

        public struct PemKeys
        {
            public string Private;
            public string Public;
        }
    }
}
