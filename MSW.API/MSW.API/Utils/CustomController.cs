﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.ApplicationInsights;
using NLog;

namespace MSW.API.Utils
{
    public abstract class CustomController : ApiController
    {
        private static readonly Logger Logger = LoggingManager.GetCurrentClassLogger();

        protected HttpResponseMessage HandleErrors
            (HttpRequestMessage request, Func<HttpResponseMessage> code, string errorMessage = null)
        {
            var requestUri = base.ActionContext.Request.RequestUri.OriginalString;
            var requestStartTime = DateTime.Now;
            HttpResponseMessage response = null;

            try
            {
                response = code();
                return response;
            }
            catch (Exception e)
            {
                var ai = new TelemetryClient();
                ai.TrackException(e);

                Logger.Error(e);
                return request.CreateResponse
                    (HttpStatusCode.InternalServerError,
                     new
                     {
                         Error = errorMessage != null && !(e is ApplicationException) ? errorMessage : e.Message
                     });
            }
            finally {

                System.Threading.Tasks.Task.Run(() => LogRequest(requestUri, requestStartTime, response));
            }
        }

        private void LogRequest(string requestUri, DateTime requestStartTime, HttpResponseMessage response)
        {
            try
            {
                var logObject = new
                {
                    StartTime = requestStartTime,
                    FinishTime = DateTime.Now,
                    ReturnStatus = response.StatusCode,
                    ReturnContent = ((ObjectContent)response.Content).Value.ToJson()
                };
                Logger.Trace(message: $"Requisição Processada'{requestUri}': {Newtonsoft.Json.JsonConvert.SerializeObject(logObject)}");
            }
            catch (Exception)
            {
            }
        }
    }
}
