﻿namespace MSW.API.Utils
{
    public class Result<T>
    {
        public Result() {}

        public Result(T data)
        {
            Data = data;
        }

        public T Data { get; set; }
    }

    public class Result
    {
        public static object Null { get; } = new Result<object>(null);
    }
}
