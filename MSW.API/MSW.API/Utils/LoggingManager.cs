﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace MSW.API.Utils
{
    public static class LoggingManager
    {
        private static readonly string LogLayout = @"[${date}] [${level}] [${logger}] ${message}";

        private static readonly Logger Logger = GetCurrentClassLogger();

        private static bool _configured;

        public static bool IsTestRunning()
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .Any(assembly => assembly.FullName.ToLowerInvariant().StartsWith("nunit.framework"));
        }

        /// <summary>
        ///     Provides a global logging instance for quick testing purpuses. Use the
        ///     <see cref="GetCurrentClassLogger" /> for production code.
        /// </summary>
        public static Logger GetGlobalLogger() => Logger;

        /// <summary>
        ///     Configures windows event logging (Error level).
        /// </summary>
        [ExcludeFromCodeCoverage]
        private static void ConfigureEventTarget(LoggingConfiguration config)
        {
            if(Environment.UserInteractive)
            {
                return;
            }

            var eventTarget = new EventLogTarget
            {
                Layout = LogLayout
            };

            config.AddTarget("eventlog", eventTarget);

            var eventTargetRule = new LoggingRule("*", LogLevel.Error, eventTarget);
            config.LoggingRules.Add(eventTargetRule);
        }

        /// <summary>
        ///     Configures console logging (Trace level).
        /// </summary>
        private static void ConfigureConsoleTarget(LoggingConfiguration config)
        {
            var consoleTarget = new ColoredConsoleTarget
            {
                Layout = LogLayout
            };

            config.AddTarget("console", consoleTarget);

            var consoleTargetRule = new LoggingRule("*", LogLevel.Trace, consoleTarget);
            config.LoggingRules.Add(consoleTargetRule);
        }

        public static Assembly GetTestAssembly()
        {
            var framesToSkip = 1;
            Type declaringType = null;

            while(true)
            {
                var frame = new StackFrame(framesToSkip, false);

                framesToSkip++;

                var currentDeclaringType = frame.GetMethod().DeclaringType;

                if(currentDeclaringType != null)
                {
                    var moduleName = currentDeclaringType.Module.Name.ToLower();

                    if(new[] { "nunit.core.dll", "nunit.framework.dll", "system.web.dll", "system.web.mvc.dll" }.Contains
                        (moduleName))
                    {
                        break;
                    }

                    // Web Applications are tricky
                    if(moduleName != "mscorlib.dll" && currentDeclaringType.FullName != "ASP.global_asax")
                    {
                        declaringType = currentDeclaringType;
                    }
                }
            }

            return declaringType?.Assembly;
        }

        public static Assembly GetEntryAssembly()
        {
            return Assembly.GetEntryAssembly() != null ? Assembly.GetEntryAssembly() : GetTestAssembly();
        }

        /// <summary>
        ///     Configures file logging. The log file is located at the [DomainBaseDirectory]\Log (Trace level).
        /// </summary>
        [ExcludeFromCodeCoverage]
        private static void ConfigureFileTarget(LoggingConfiguration config)
        {
            //TODO: code missing
            if(IsTestRunning() /*|| !Settings.Settings.Get<Settings.Settings>().FileLog*/)
            {
                return;
            }

            var assembly = GetEntryAssembly();

            var fileTarget = new FileTarget
            {
                FileName =
                    Path.Combine
                        (AppDomain.CurrentDomain.BaseDirectory,
                         "Log",
                         $"{DateTime.Now.ToString("yyyy-MM-dd")} - {assembly.GetName().Name}.log"),
                Layout = LogLayout
            };

            config.AddTarget("file", fileTarget);

            var fileTargetRule = new LoggingRule("*", LogLevel.Trace, fileTarget);
            config.LoggingRules.Add(fileTargetRule);

            Trace.WriteLine($"Registering Log file: {fileTarget.FileName}");
        }

        public static Type GetCallingType(int framesToSkip = 1)
        {
            Type declaringType;
            do
            {
                declaringType = new StackFrame(framesToSkip, false).GetMethod().DeclaringType;
                framesToSkip++;
            } while(declaringType != null && declaringType.Module.Name.ToLower() == "mscorlib.dll");

            return declaringType;
        }

        /// <summary>
        ///     Gets the calling type name based on the current call stack.
        /// </summary>
        public static string GetCallingTypeName(int framesToSkip = 1)
        {
            return GetCallingType(framesToSkip + 1)?.FullName != null ? GetCallingType(framesToSkip + 1)?.FullName : string.Empty;
        }

        /// <summary>
        ///     Returns a logger instance for the current calling class.
        /// </summary>
        public static Logger GetCurrentClassLogger()
        {
            var logConfigured = false;
            if(!_configured || IsTestRunning())
            {
                _configured = true;

                var config = new LoggingConfiguration();

                ConfigureConsoleTarget(config);
                ConfigureEventTarget(config);
                ConfigureFileTarget(config);

                LogManager.Configuration = config;

                logConfigured = true;
            }

            var callingClass = GetCallingTypeName(2);
            var logger = LogManager.GetLogger(callingClass);
            if(logConfigured)
            {
                //TODO: remover suigetsu
                //logger.Trace("Log configured. First class: {0}", callingClass);
            }
            return logger;
        }
    }
}
