﻿namespace MSW.API.Utils
{
    public static class StringExtensions
    {
        public static int ToIntDefault(this string str, int def)
        {
            int result;
            if(!int.TryParse(str, out result))
            {
                result = def;
            }
            return result;
        }
    }
}
