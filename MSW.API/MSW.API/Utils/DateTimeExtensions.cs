﻿using System;

namespace MSW.API.Utils
{
    public static class DateTimeExtensions
    {
        private static TimeSpan GetEpochTimeSpan(this DateTime date)
        {
            return date.Subtract(new DateTime(1970, 1, 1));
        }

        public static long GetTimestamp(this DateTime date, bool seconds = false)
        {
            var timeSpan = date.ToUniversalTime().GetEpochTimeSpan();
            return (long)(seconds ? timeSpan.TotalSeconds : timeSpan.TotalMilliseconds);
        }

        public static DateTime TrimMilliseconds(this DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second, 0);
        }
    }
}
