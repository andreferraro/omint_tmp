﻿using MSW.API.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.IO;
using System.Reflection;

namespace System.Net.Http
{
    public class IgnoreStreamsResolver : DefaultContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty property = base.CreateProperty(member, memberSerialization);
            if (typeof(Stream).IsAssignableFrom(property.PropertyType))
            {
                property.Ignored = true;
            }
            return property;
        }
    }

    public static class HttpRequestMessageExtentions
    {
        public static HttpResponseMessage EmptyDataMessage(this HttpRequestMessage self)
            => self.CreateResponse(HttpStatusCode.OK, Result.Null);

        public static HttpResponseMessage DataMessage(this HttpRequestMessage self, object data)
            => self.CreateResponse(HttpStatusCode.OK, new Result<object>(data));

        public static string ToJson(this object input)
        {

            var output = JsonConvert.SerializeObject(input,
                Newtonsoft.Json.Formatting.None,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    ContractResolver = new IgnoreStreamsResolver()
                });
            return output;
        }
    }
}