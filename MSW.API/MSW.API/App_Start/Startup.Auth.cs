﻿using System;
using System.Configuration;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using MSW.API.Providers;
using MSW.API.Utils;
using Owin;

namespace MSW.API
{
    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);

            // Para utilizar o Header "Authorization" nas requisições
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            // Ativar o método para gerar o OAuth Token
            app.UseOAuthAuthorizationServer
                (new OAuthAuthorizationServerOptions
                {
                    TokenEndpointPath = new PathString("/api/Login"),
                    Provider = new ApplicationOAuthProvider(),
                    AccessTokenExpireTimeSpan =
                        TimeSpan.FromSeconds(ConfigurationManager.AppSettings["TokenExpirationSeconds"].ToIntDefault(5840000)),
                   
                    AllowInsecureHttp = true
                });

           

            // Ativar o método para gerar o OAuth Token
            app.UseOAuthAuthorizationServer
                (new OAuthAuthorizationServerOptions
                {
                    TokenEndpointPath = new PathString("/api/LoginSegurosViagem"),
                    Provider = new ApplicationSegurosOAuthProvider(),
                    AccessTokenExpireTimeSpan =
                        TimeSpan.FromDays(ConfigurationManager.AppSettings["TokenExpirationDays"].ToIntDefault(1)),
                    AllowInsecureHttp = true
                });
        }
    }
}
