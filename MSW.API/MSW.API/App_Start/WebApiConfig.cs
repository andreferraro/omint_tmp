﻿using Elmah.Contrib.WebApi;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace MSW.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Services.Add(typeof(IExceptionLogger), new ElmahExceptionLogger());

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute
                ("DefaultApi",
                 "api/{controller}/{action}/{id}",
                 new
                 {
                     id = RouteParameter.Optional
                 });
        }
    }
}
