﻿using Microsoft.Owin;
using MSW.API;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace MSW.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
